CREATE TABLE IF NOT EXISTS category (
    id                 BIGSERIAL PRIMARY KEY,
    name               VARCHAR(100) NOT NULL,
    parent_category_id BIGINT,
    image              VARCHAR(255),
    CONSTRAINT FK_category_category FOREIGN KEY (parent_category_id) REFERENCES category (id)
);

CREATE TABLE IF NOT EXISTS category_translation (
    id             BIGINT       NOT NULL,
    locale         VARCHAR(10)  NOT NULL,
    localized_name VARCHAR(100) DEFAULT '',
    CONSTRAINT PK_category_translation PRIMARY KEY (id, locale),
    CONSTRAINT FK_translation_category FOREIGN KEY (id) REFERENCES category (id) ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS producer (
    id   BIGSERIAL PRIMARY KEY,
    name VARCHAR(100) NOT NULL
);

CREATE TABLE IF NOT EXISTS product (
    id          BIGSERIAL       PRIMARY KEY,
    name        VARCHAR(100)    NOT NULL,
    category_id BIGINT          NOT NULL,
    producer_id BIGINT          NOT NULL,
    price       BIGINT,
    image       VARCHAR(255),
    create_time BIGINT          NOT NULL,
    CONSTRAINT FK_product_category FOREIGN KEY (category_id) REFERENCES category (id),
    CONSTRAINT FK_product_producer FOREIGN KEY (producer_id) REFERENCES producer (id)
);

CREATE TABLE IF NOT EXISTS product_parameter_type (
    id          BIGSERIAL       PRIMARY KEY,
    category_id BIGINT          NOT NULL,
    name        VARCHAR(100)    NOT NULL,
    UNIQUE (category_id, name),
    CONSTRAINT FK_product_parameter_category FOREIGN KEY (category_id) REFERENCES category (id) ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS product_parameter_type_translation (
    id                  BIGINT       NOT NULL,
    locale              VARCHAR(10)  NOT NULL,
    localized_name      VARCHAR(100) DEFAULT '',
    CONSTRAINT PK_parameter_type_translation PRIMARY KEY (id, locale),
    CONSTRAINT FK_translation_parameter_type FOREIGN KEY (id) REFERENCES product_parameter_type (id) ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS product_parameter (
    id                  BIGSERIAL       PRIMARY KEY,
    parameter_type_id   BIGINT          NOT NULL,
    sense               VARCHAR(255)    NOT NULL,
    UNIQUE (parameter_type_id, sense),
    CONSTRAINT FK_parameter_parameter_type  FOREIGN KEY (parameter_type_id) REFERENCES product_parameter_type (id) ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS product_product_parameter (
    id                  BIGSERIAL       PRIMARY KEY,
    product_id          BIGINT          NOT NULL,
    parameter_id        BIGINT          NOT NULL,
    UNIQUE (product_id, parameter_id),
    CONSTRAINT FK_parameter_product         FOREIGN KEY (product_id)   REFERENCES product (id) ON DELETE CASCADE,
    CONSTRAINT FK_parameter_product_parameter_type  FOREIGN KEY (parameter_id) REFERENCES product_parameter (id) ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS product_translation (
    id             BIGINT       NOT NULL,
    locale         VARCHAR(10)  NOT NULL,
    localized_name VARCHAR(100) DEFAULT '',
    localized_desc VARCHAR(100) DEFAULT '',
    CONSTRAINT PK_product_translation PRIMARY KEY (id, locale),
    CONSTRAINT FK_translation_product FOREIGN KEY (id) REFERENCES product (id) ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS roles (
    id   BIGSERIAL PRIMARY KEY,
    name VARCHAR(100) UNIQUE
);

CREATE TABLE IF NOT EXISTS user_status (
    id   BIGSERIAL PRIMARY KEY,
    name VARCHAR(100) UNIQUE
);

CREATE TABLE IF NOT EXISTS users (
    id         BIGSERIAL PRIMARY KEY,
    login      VARCHAR(255) NOT NULL,
    first_name VARCHAR(255) NOT NULL,
    last_name  VARCHAR(255),
    email      VARCHAR(255),
    password   VARCHAR(255) NOT NULL,
    locale     VARCHAR(10)  NOT NULL,
    status_id  BIGINT       NOT NULL,
    role_id    BIGINT       NOT NULL,
    UNIQUE (login),
    CONSTRAINT FK_users_status FOREIGN KEY (status_id) REFERENCES user_status (id) ON DELETE CASCADE,
    CONSTRAINT FK_users_roles FOREIGN KEY (role_id) REFERENCES roles (id) ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS order_status (
    id   BIGSERIAL PRIMARY KEY,
    name VARCHAR(100) UNIQUE
);

CREATE TABLE IF NOT EXISTS shopping_cart (
    id                      BIGSERIAL PRIMARY KEY,
    user_id                 BIGINT      NOT NULL,
    product_id              BIGINT      NOT NULL,
    quantity                INT         NOT NULL    DEFAULT 1,
    status_id               BIGINT,
    registration_date_time  BIGINT,
    CONSTRAINT FK_shopping_cart_user FOREIGN KEY (user_id) REFERENCES users (id) ON DELETE CASCADE,
    CONSTRAINT FK_shopping_cart_product FOREIGN KEY (product_id) REFERENCES product (id) ON DELETE CASCADE,
    CONSTRAINT FK_shopping_cart_status FOREIGN KEY (status_id) REFERENCES order_status (id) ON DELETE CASCADE
);
