package com.zhuravel.market_servlet.config;

/**
 * @author Evgenii Zhuravel created on 31.05.2022
 * @version 1.0
 */
public class Constants {

    public static final class Mapping {

        public static final String MAPPING_LOGIN = "/login";

        public static final String MAPPING_LOGOUT = "/logout";

        public static final String MAPPING_CATEGORIES = "/admin/categories";

        public static final String MAPPING_CATEGORY_DELETE = "/admin/categories/delete";

        public static final String MAPPING_CATEGORY_EDIT = "/admin/categories/edit";

        public static final String MAPPING_CATEGORY_ADD = "/admin/categories/add";

        public static final String MAPPING_PARAMETER_TYPE_DELETE = "/admin/parameter-type/delete";

        public static final String MAPPING_PARAMETER_TYPE_EDIT = "/admin/parameter-type/edit";

        public static final String MAPPING_PARAMETER_TYPE_ADD = "/admin/parameter-type/add";

        public static final String MAPPING_PARAMETER_DELETE = "/admin/parameter/delete";

        public static final String MAPPING_PARAMETER_EDIT = "/admin/parameter/edit";

        public static final String MAPPING_PARAMETER_ADD = "/admin/parameter/add";

        public static final String MAPPING_USERS = "/admin/users";

        public static final String MAPPING_USERS_EDIT = "/admin/users/edit";

        public static final String MAPPING_ORDERS = "/admin/orders";

        public static final String MAPPING_ORDERS_PAID = "/admin/orders/paid";

        public static final String MAPPING_ORDERS_CANCELED = "/admin/orders/canceled";

        public static final String MAPPING_SHOPPING_CART = "/shopping-cart";

        public static final String MAPPING_SHOPPING_CART_ADD = "/shopping-cart/add";

        public static final String MAPPING_CATALOG = "/catalog";

        public static final String MAPPING_CATALOG_EDIT = "/catalog/edit";

        public static final String MAPPING_CATALOG_DELETE = "/catalog/delete";

        public static final String MAPPING_REGISTRATION = "/registration";

        public static final String MAPPING_PROFILE = "/profile-edit";

        public static final String MAPPING_PRODUCT_PAGE = "/product-page";
    }

    public static final class Attributes {

        public static final String ATTRIBUTE_PRODUCT_LIST = "productList";

        public static final String ATTRIBUTE_PRODUCT = "product";

        public static final String ATTRIBUTE_PRODUCT_ID = "productId";

        public static final String ATTRIBUTE_PRODUCERS = "producers";

        public static final String ATTRIBUTE_MESSAGE = "message";

        public static final String ATTRIBUTE_CATEGORY = "category";

        public static final String ATTRIBUTE_CATEGORY_FORM = "categoryForm";

        public static final String ATTRIBUTE_PARAMETER_TYPE = "parameterType";

        public static final String ATTRIBUTE_PARAMETER_TYPES = "parameterTypes";

        public static final String ATTRIBUTE_PARAMETER = "parameter";

        public static final String ATTRIBUTE_PARAMETERS = "parameters";

        public static final String ATTRIBUTE_USER = "user";

        public static final String ATTRIBUTE_ROLES = "roles";

        public static final String ATTRIBUTE_STATUSES = "statuses";

        public static final String ATTRIBUTE_CATEGORIES = "categories";

        public static final String ATTRIBUTE_PARENT_CATEGORY = "parentCategory";

        public static final String ATTRIBUTE_PARENT_CATEGORIES = "upperCategories";

        public static final String ATTRIBUTE_CATEGORY_ID = "categoryId";

        public static final String ATTRIBUTE_SELECTED_PRODUCTS = "selectedProducts";

        public static final String ATTRIBUTE_ORDERS = "orders";

        public static final String FILTER_PARAMETERS = "filterCriteria";
    }

    public static final class SessionAttributes {

        public static final String ATTRIBUTE_USER = "user";

        public static final String ATTRIBUTE_ERRORS = "errors";
    }

    public static final class Parameters {

        public static final String PARAMETER_LANGUAGE = "language";

        public static final String PARAMETER_ID = "id";
    }

    public static final class Pages {

        public static final String PAGE_CATEGORY_EDIT = "/WEB-INF/templates/admin/admin-category-edit.jsp";

        public static final String PAGE_SUCCESS = "/WEB-INF/templates/message_success.jsp";

        public static final String PAGE_ERROR = "/WEB-INF/templates/message_error.jsp";
    }
}
