package com.zhuravel.market_servlet.config;

import javax.servlet.http.HttpServletRequest;
import java.util.Locale;

import static com.zhuravel.market_servlet.config.Constants.Parameters.PARAMETER_LANGUAGE;

/**
 * @author Evgenii Zhuravel created on 18.06.2022
 * @version 1.0
 */
public enum LocaleHolder {

    INSTANCE();

    String locale = "en";

    public Locale getLocale() {
        return new Locale(locale);
    }

    public String getStringLocale() {
        return locale;
    }

    public String receiveStringLocale(HttpServletRequest request) {
        String locale = request.getParameter(PARAMETER_LANGUAGE);

        if (locale != null) {
            this.locale = locale;
        }

        return this.locale;
    }

    public void setLocale(String locale) {
        this.locale = locale;
    }
}