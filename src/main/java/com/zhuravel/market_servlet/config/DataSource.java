package com.zhuravel.market_servlet.config;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Properties;

/**
 * @author Evgenii Zhuravel created on 08.06.2022
 * @version 1.0
 */
public class DataSource {
    private static final HikariConfig config = new HikariConfig();

    private static HikariDataSource ds;

    static {
        try {
            Properties props = PropertyReceiver.getProperties();

            String url = props.getProperty("jdbc.url");
            String user = props.getProperty("jdbc.username");
            String password = props.getProperty("jdbc.password");
            String driverClassName = props.getProperty("jdbc.driver-class-name");

            config.setJdbcUrl(url);
            config.setUsername(user);
            config.setPassword(password);
            config.setDriverClassName(driverClassName);
            config.addDataSourceProperty("cachePrepStmts", "true");
            config.addDataSourceProperty("prepStmtCacheSize", "250");
            config.addDataSourceProperty("prepStmtCacheSqlLimit", "2048");

            ds = new HikariDataSource(config);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private DataSource() {

    }

    public static Connection getConnection() throws SQLException {
        return ds.getConnection();
    }

    public static void reload() {

    }
}
