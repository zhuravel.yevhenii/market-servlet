package com.zhuravel.market_servlet.config;

import java.util.MissingResourceException;
import java.util.ResourceBundle;

/**
 * @author Evgenii Zhuravel created on 03.06.2022
 * @version 1.0
 */
public class MessageResourceReceiver {

    public static String getString(String key) throws MissingResourceException {
        return ResourceBundle.getBundle("messages", LocaleHolder.INSTANCE.getLocale())
                    .getString(key);
    }
}
