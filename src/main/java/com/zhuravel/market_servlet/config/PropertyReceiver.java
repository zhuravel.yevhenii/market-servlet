package com.zhuravel.market_servlet.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Objects;
import java.util.Properties;

/**
 * @author Evgenii Zhuravel created on 08.06.2022
 * @version 1.0
 */
public class PropertyReceiver {
    private static final Logger logger = LoggerFactory.getLogger(PropertyReceiver.class);

    public static String get(String name) throws IOException {
        return getProperties().getProperty(name);
    }

    public static String get(String name, String defaultValue) {
        try {
            return getProperties().getProperty(name);
        } catch (IOException e) {
            logger.debug(e.getMessage());

            return defaultValue;
        }
    }

    public static Integer get(String name, Integer defaultValue) {
        try {
            return Integer.parseInt(getProperties().getProperty(name));
        } catch (IOException e) {
            logger.debug(e.getMessage());

            return defaultValue;
        }
    }

    public static Properties getProperties() throws IOException {
        String rootPath = Objects.requireNonNull(Thread.currentThread().getContextClassLoader().getResource("")).getPath();
        String appConfigPath = rootPath + "application.properties";

        Properties appProps = new Properties();
        appProps.load(new FileInputStream(appConfigPath));

        return appProps;
    }
}
