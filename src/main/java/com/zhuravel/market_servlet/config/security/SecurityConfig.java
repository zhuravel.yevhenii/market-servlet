package com.zhuravel.market_servlet.config.security;

import com.zhuravel.market_servlet.model.RoleType;
import org.reflections.Reflections;

import javax.servlet.annotation.WebServlet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @author Evgenii Zhuravel created on 01.07.2022
 * @version 1.0
 */
public class SecurityConfig {

    private static final Map<RoleType, List<String>> mapConfig = new HashMap<>();

    static {
        init();
    }

    private static void init() {
        Reflections scanner = new Reflections("com.zhuravel.market_servlet.controller");

        Set<Class<?>> classes = scanner.getTypesAnnotatedWith(Authenticated.class);

        for(Class<?> c: classes) {
            WebServlet webServlet = c.getAnnotation(WebServlet.class);
            Authenticated authenticated = c.getAnnotation(Authenticated.class);

            if (webServlet != null && authenticated != null) {
                addUrl(authenticated.role(), webServlet.urlPatterns());
            }
        }
    }

    public static Set<RoleType> getAllAppRoles() {
        return mapConfig.keySet();
    }

    public static List<String> getUrlPatternsForRole(RoleType role) {
        return mapConfig.get(role);
    }

    private static void addUrl(RoleType[] roles, String[] urls) {
        for (RoleType role: roles) {
            if (!mapConfig.containsKey(role)) {
                mapConfig.put(role, new ArrayList<>());
            }

            for (String url : urls) {
                mapConfig.get(role).add(url);
            }
        }
    }
}
