package com.zhuravel.market_servlet.config.security;

import com.zhuravel.market_servlet.model.RoleType;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Set;

/**
 * @author Evgenii Zhuravel created on 01.07.2022
 * @version 1.0
 */
public class SecurityUtils {

    public static boolean isSecurityPage(HttpServletRequest req) {
        String urlPattern = UrlPatternUtils.getUrlPattern(req);

        Set<RoleType> roles = SecurityConfig.getAllAppRoles();

        for (RoleType role: roles) {
            List<String> urlPatterns = SecurityConfig.getUrlPatternsForRole(role);

            if (urlPattern != null && urlPatterns.contains(urlPattern)) {
                return true;
            }
        }

        return false;
    }

    public static boolean hasPermission(HttpServletRequest req) {
        String urlPattern = UrlPatternUtils.getUrlPattern(req);

        Set<RoleType> allRoles = SecurityConfig.getAllAppRoles();

        for (RoleType role: allRoles) {
            if (!req.isUserInRole(role.name())) {
                continue;
            }

            List<String> urlPatterns = SecurityConfig.getUrlPatternsForRole(role);

            if (urlPattern != null && urlPatterns.contains(urlPattern)) {
                return true;
            }
        }

        return false;
    }
}
