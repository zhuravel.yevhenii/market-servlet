package com.zhuravel.market_servlet.config.security;

import javax.servlet.ServletContext;
import javax.servlet.ServletRegistration;
import javax.servlet.http.HttpServletRequest;
import java.util.Collection;
import java.util.Map;

/**
 * @author Evgenii Zhuravel created on 01.07.2022
 * @version 1.0
 */
public class UrlPatternUtils {

    public static String getUrlPattern(HttpServletRequest req) {
        ServletContext servletContext = req.getServletContext();

        String servletPath = req.getServletPath();
        String pathInfo = req.getPathInfo();

        String urlPattern;

        if (pathInfo != null) {
            urlPattern = servletPath + "/*";
            return urlPattern;
        }

        urlPattern = servletPath;

        if(hasUrlPattern(servletContext, urlPattern)) {
            return urlPattern;
        }

        int i = servletPath.lastIndexOf('.');
        if (i != 1) {
            String ext = servletPath.substring(i + 1);
            urlPattern = "*." + ext;

            if (hasUrlPattern(servletContext, urlPattern)) {
                return urlPattern;
            }
        }

        return "/";
    }

    private static boolean hasUrlPattern(ServletContext servletContext, String urlPattern) {
        Map<String, ? extends ServletRegistration> map = servletContext.getServletRegistrations();

        for(String servletName: map.keySet()) {
            ServletRegistration sr = map.get(servletName);

            Collection<String> mappings = sr.getMappings();
            if(mappings.contains(urlPattern)) {
                return true;
            }
        }
        return false;
    }
}
