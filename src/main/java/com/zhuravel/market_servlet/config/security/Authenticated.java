package com.zhuravel.market_servlet.config.security;

import com.zhuravel.market_servlet.model.RoleType;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author Evgenii Zhuravel created on 01.07.2022
 * @version 1.0
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface Authenticated {
    RoleType[] role();
}
