package com.zhuravel.market_servlet.config.security;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;
import java.security.Principal;

/**
 * @author Evgenii Zhuravel created on 03.07.2022
 * @version 1.0
 */
public class UserRoleRequestWrapper extends HttpServletRequestWrapper {

    private final String userName;
    private final String roleName;
    private final HttpServletRequest realRequest;

    public UserRoleRequestWrapper(String userName, String roleName, HttpServletRequest request) {
        super(request);
        this.userName = userName;
        this.roleName = roleName;
        this.realRequest = request;
    }

    @Override
    public boolean isUserInRole(String role) {
        if (roleName == null) {
            return this.realRequest.isUserInRole(role);
        }

        return roleName.equals(role);
    }

    @Override
    public Principal getUserPrincipal() {
        if (this.userName == null) {
            return realRequest.getUserPrincipal();
        }

        return () -> userName;
    }
}
