package com.zhuravel.market_servlet.config.security;

import com.zhuravel.market_servlet.service.dto.UserDto;

import javax.servlet.http.HttpSession;

import java.util.HashMap;
import java.util.Map;

import static com.zhuravel.market_servlet.config.Constants.SessionAttributes.ATTRIBUTE_USER;

/**
 * @author Evgenii Zhuravel created on 03.07.2022
 * @version 1.0
 */
public class LoginUtil {

    private static int REDIRECT_ID = 0;

    private static final Map<Integer, String> id_uri_map = new HashMap<>();
    private static final Map<String, Integer> uri_id_map = new HashMap<>();

    public static void storeLoginedUser(HttpSession session, UserDto loginedUser) {
        session.setAttribute(ATTRIBUTE_USER, loginedUser);
    }

    public static UserDto getLoginedUser(HttpSession session) {
        return (UserDto) session.getAttribute(ATTRIBUTE_USER);
    }

    public static int storeRedirectUrlAfterLogin(String requestUri) {
        Integer id = uri_id_map.get(requestUri);

        if (id == null) {
            id = REDIRECT_ID++;

            uri_id_map.put(requestUri, id);
            id_uri_map.put(id, requestUri);
            return id;
        }

        return id;
    }

    public static String getRedirectUrlAfterLogin(int redirectId) {
        return id_uri_map.get(redirectId);
    }
}
