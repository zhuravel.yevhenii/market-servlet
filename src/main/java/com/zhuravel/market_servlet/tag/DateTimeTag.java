package com.zhuravel.market_servlet.tag;

import com.zhuravel.market_servlet.utils.DateTimeConverter;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.SimpleTagSupport;
import java.io.IOException;
import java.io.StringWriter;

/**
 * @author Evgenii Zhuravel created on 07.07.2022
 * @version 1.0
 */
public class DateTimeTag extends SimpleTagSupport {

    private final StringWriter sw = new StringWriter();

    @Override
    public void doTag() throws JspException, IOException {
        getJspBody().invoke(sw);

        String body = sw.toString();
        Long milliseconds = Long.parseLong(body);

        getJspContext().getOut().println(DateTimeConverter.getFormattedDateTime(milliseconds));
    }
}
