package com.zhuravel.market_servlet.service.impl;

import com.zhuravel.market_servlet.config.LocaleHolder;
import com.zhuravel.market_servlet.config.PropertyReceiver;
import com.zhuravel.market_servlet.dao.LocalizedCategoryDao;
import com.zhuravel.market_servlet.dao.exception.DaoException;
import com.zhuravel.market_servlet.dao.impl.LocalizedCategoryDaoImpl;
import com.zhuravel.market_servlet.model.entity.LocalizedCategory;
import com.zhuravel.market_servlet.model.pageable.Page;
import com.zhuravel.market_servlet.model.pageable.Pageable;
import com.zhuravel.market_servlet.service.LocalizedCategoryService;
import com.zhuravel.market_servlet.service.dto.LocalizedCategoryDto;
import com.zhuravel.market_servlet.service.dto.converter.LocalizedCategoryToDtoConverter;
import com.zhuravel.market_servlet.service.exception.EntityNotFoundException;
import com.zhuravel.market_servlet.service.exception.PropertyNotFoundException;
import com.zhuravel.market_servlet.service.exception.ServiceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.List;

import static java.lang.String.format;

/**
 * @author Evgenii Zhuravel created on 19.05.2022
 * @version 1.0
 */

public class LocalizedCategoryServiceImpl implements LocalizedCategoryService {

    private static final Logger logger = LoggerFactory.getLogger(LocalizedCategoryServiceImpl.class);

    private static final String defaultImagePath = "default.category.image.path";

    private final LocalizedCategoryDao localizedCategoryDao;

    public LocalizedCategoryServiceImpl() {
        this.localizedCategoryDao = new LocalizedCategoryDaoImpl();
    }

    @Override
    public LocalizedCategoryDto getById(Long id) throws ServiceException {
        try {
            logger.debug("get category with id: " + id);

            localizedCategoryDao.setLocale(LocaleHolder.INSTANCE.getStringLocale());

            com.zhuravel.market_servlet.model.entity.LocalizedCategory category = localizedCategoryDao.findById(id).orElseThrow(() ->
                    new EntityNotFoundException(format("LocalizedCategoryDto with %s not found", id), com.zhuravel.market_servlet.model.entity.LocalizedCategory.class));

            return LocalizedCategoryToDtoConverter.convert(category);
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public Page<LocalizedCategoryDto> getSubCategories(Long id, Pageable pageable) throws ServiceException {
        try {
            logger.debug("get sub categories with parent id: " + id);

            localizedCategoryDao.setLocale(LocaleHolder.INSTANCE.getStringLocale());
            Page<LocalizedCategory> localizedCategories = localizedCategoryDao.findAllByParentCategoryId(id, pageable);

            setDefaultIcon(localizedCategories.getContent());

            return LocalizedCategoryToDtoConverter.convert(localizedCategories);
        } catch (DaoException | PropertyNotFoundException e) {
            throw new ServiceException(e);
        }
    }

    private void setDefaultIcon(List<LocalizedCategory> categories) throws PropertyNotFoundException {
        logger.debug("set default icon for categories");

        for (LocalizedCategory category : categories) {
            String path = category.getImagePath();
            if (path == null || path.isEmpty()) {
                try {
                    String image = PropertyReceiver.get(defaultImagePath);
                    category.setImagePath(image);
                } catch (IOException e) {
                    throw new PropertyNotFoundException(format("Property '%s' not found", defaultImagePath));
                }
            }
        }
    }
}
