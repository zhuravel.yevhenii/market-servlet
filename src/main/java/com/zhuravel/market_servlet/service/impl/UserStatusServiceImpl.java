package com.zhuravel.market_servlet.service.impl;

import com.zhuravel.market_servlet.dao.UserStatusDao;
import com.zhuravel.market_servlet.dao.exception.DaoException;
import com.zhuravel.market_servlet.dao.impl.UserStatusDaoImpl;
import com.zhuravel.market_servlet.model.entity.UserStatus;
import com.zhuravel.market_servlet.model.pageable.Page;
import com.zhuravel.market_servlet.model.pageable.Pageable;
import com.zhuravel.market_servlet.service.UserStatusService;
import com.zhuravel.market_servlet.service.exception.EntityNotFoundException;
import com.zhuravel.market_servlet.service.exception.ServiceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static java.lang.String.format;

/**
 * @author Evgenii Zhuravel created on 19.05.2022
 * @version 1.0
 */
public class UserStatusServiceImpl implements UserStatusService {

    private static final Logger logger = LoggerFactory.getLogger(UserStatusServiceImpl.class);

    private final UserStatusDao UserStatusDao;

    public UserStatusServiceImpl() {
        this.UserStatusDao = new UserStatusDaoImpl();
    }

    public UserStatusServiceImpl(UserStatusDao UserStatusDao) {
        this.UserStatusDao = UserStatusDao;
    }

    @Override
    public UserStatus save(UserStatus UserStatus) throws ServiceException {
        logger.debug("Save UserStatus");
        try {
            return UserStatusDao.save(UserStatus);
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public void update(UserStatus UserStatus) throws ServiceException {
        logger.debug("update UserStatus");
        try {
            UserStatusDao.update(UserStatus);
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public void delete(Long id) throws ServiceException {
        logger.debug("delete UserStatus");
        try {
            UserStatusDao.deleteById(id);
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public UserStatus getById(Long id) throws ServiceException {
        logger.debug("getById UserStatus");
        try {
            return UserStatusDao.findById(id).orElseThrow(() ->
                    new EntityNotFoundException(format("UserStatus with id %s not found", id), UserStatus.class));
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public Page<UserStatus> getAll(Pageable page) throws ServiceException {
        try {
            return UserStatusDao.findAll(page);
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
    }
}
