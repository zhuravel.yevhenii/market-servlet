package com.zhuravel.market_servlet.service.impl;

import com.zhuravel.market_servlet.dao.ProducerDao;
import com.zhuravel.market_servlet.dao.exception.DaoException;
import com.zhuravel.market_servlet.dao.impl.ProducerDaoImpl;
import com.zhuravel.market_servlet.model.entity.Producer;
import com.zhuravel.market_servlet.model.pageable.Page;
import com.zhuravel.market_servlet.model.pageable.Pageable;
import com.zhuravel.market_servlet.service.ProducerService;
import com.zhuravel.market_servlet.service.exception.ResourceNotFoundException;
import com.zhuravel.market_servlet.service.exception.ServiceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

import static java.lang.String.format;

/**
 * @author Evgenii Zhuravel created on 19.05.2022
 * @version 1.0
 */
public class ProducerServiceImpl implements ProducerService {

    private static final Logger logger = LoggerFactory.getLogger(ProducerServiceImpl.class);

    private final ProducerDao producerDao;

    public ProducerServiceImpl() {
        this.producerDao = new ProducerDaoImpl();
    }

    @Override
    public Producer save(Producer producer) throws ServiceException {
        logger.debug("save producer");
        try {
            return producerDao.save(producer);
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public void update(Producer producer) throws ServiceException {
        logger.debug("update producer");
        try {
            producerDao.update(producer);
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public void delete(Long id) throws ServiceException {
        logger.debug("delete producer");
        try {
            producerDao.deleteById(id);
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public Producer getById(Long id) throws ServiceException {
        logger.debug("getById producer");
        try {
            return producerDao.findById(id).orElseThrow(() ->
                    new ResourceNotFoundException(format("Producer with id %s not found", id), Producer.class));
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public Page<Producer> getAll(Pageable pageable) throws ServiceException {
        logger.debug("getAll producer");
        try {
            return producerDao.findAll(pageable);
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public List<Producer> getDistinctByCategoryId(Long categoryId) throws ServiceException {
        logger.debug("find All producers By LocalizedCategoryDto Id");
        try {
            return producerDao.findDistinctByCategoryId(categoryId);
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
    }
}
