package com.zhuravel.market_servlet.service.impl;

import com.zhuravel.market_servlet.dao.ParameterTypeDao;
import com.zhuravel.market_servlet.dao.exception.DaoException;
import com.zhuravel.market_servlet.dao.impl.ParameterTypeDaoImpl;
import com.zhuravel.market_servlet.model.entity.ParameterType;
import com.zhuravel.market_servlet.model.pageable.Page;
import com.zhuravel.market_servlet.model.pageable.Pageable;
import com.zhuravel.market_servlet.service.ParameterTypeService;
import com.zhuravel.market_servlet.service.dto.ParameterTypeDto;
import com.zhuravel.market_servlet.service.dto.converter.ParameterTypeToDtoConverter;
import com.zhuravel.market_servlet.service.exception.ResourceNotFoundException;
import com.zhuravel.market_servlet.service.exception.ServiceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

import static java.lang.String.format;

/**
 * @author Evgenii Zhuravel created on 19.05.2022
 * @version 1.0
 */
public class ParameterTypeServiceImpl implements ParameterTypeService {

    private static final Logger logger = LoggerFactory.getLogger(ParameterTypeServiceImpl.class);

    private final ParameterTypeDao parameterTypeDao;

    public ParameterTypeServiceImpl() {
        this.parameterTypeDao = new ParameterTypeDaoImpl();
    }

    @Override
    public ParameterTypeDto save(ParameterTypeDto parameterType) throws ServiceException {
        try {
            ParameterType saved = parameterTypeDao.save(ParameterTypeToDtoConverter.convert(parameterType));

            parameterType.setId(saved.getId());

            return parameterType;
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public void update(ParameterTypeDto parameterType) throws ServiceException {
        try {
            ParameterType entity = ParameterTypeToDtoConverter.convert(parameterType);
            parameterTypeDao.update(entity);
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public void delete(Long id) throws ServiceException {
        try {
            parameterTypeDao.deleteById(id);
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public ParameterTypeDto getById(Long id) throws ServiceException {
        try {

            ParameterType parameterType = parameterTypeDao.findById(id).orElseThrow(() ->
                    new ResourceNotFoundException(format("LocalizedParameterType with %s not found", id), ParameterType.class));

            return ParameterTypeToDtoConverter.convert(parameterType);
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public Page<ParameterTypeDto> getAll(Pageable pageable) throws ServiceException {
        try {
            return ParameterTypeToDtoConverter.convert(parameterTypeDao.findAll(pageable));
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public List<ParameterTypeDto> getAllByCategoryId(Long id) throws ServiceException {
        try {
            List<ParameterType> parameterTypes = parameterTypeDao.findAllByCategoryId(id);

            return ParameterTypeToDtoConverter.convert(parameterTypes);
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
    }
}
