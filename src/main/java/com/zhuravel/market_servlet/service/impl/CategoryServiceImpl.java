package com.zhuravel.market_servlet.service.impl;

import com.zhuravel.market_servlet.config.PropertyReceiver;
import com.zhuravel.market_servlet.dao.CategoryDao;
import com.zhuravel.market_servlet.dao.exception.DaoException;
import com.zhuravel.market_servlet.dao.impl.CategoryDaoImpl;
import com.zhuravel.market_servlet.model.entity.Category;
import com.zhuravel.market_servlet.model.pageable.Page;
import com.zhuravel.market_servlet.model.pageable.Pageable;
import com.zhuravel.market_servlet.service.CategoryService;
import com.zhuravel.market_servlet.service.dto.CategoryDto;
import com.zhuravel.market_servlet.service.dto.converter.CategoryToDtoConverter;
import com.zhuravel.market_servlet.service.exception.EntityNotFoundException;
import com.zhuravel.market_servlet.service.exception.PropertyNotFoundException;
import com.zhuravel.market_servlet.service.exception.ServiceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.List;

import static java.lang.String.format;

/**
 * @author Evgenii Zhuravel created on 19.05.2022
 * @version 1.0
 */

public class CategoryServiceImpl implements CategoryService {

    private static final Logger logger = LoggerFactory.getLogger(CategoryServiceImpl.class);

    private static final String defaultImagePath = "default.category.image.path";

    private final CategoryDao categoryDao;

    public CategoryServiceImpl() {
        this.categoryDao = new CategoryDaoImpl();
    }

    @Override
    public CategoryDto save(CategoryDto categoryDto) throws ServiceException {
        try {
            logger.debug("save categoryDto");

            Category category = categoryDao.save(CategoryToDtoConverter.convert(categoryDto));
            return CategoryToDtoConverter.convert(category);

        } catch (DaoException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public void update(CategoryDto category) throws ServiceException {
        try {
            logger.debug("update category");

            categoryDao.update(CategoryToDtoConverter.convert(category));
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public void delete(Long id) throws ServiceException {
        try {
            logger.debug("delete category with id: " + id);

            categoryDao.deleteById(id);
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public CategoryDto getById(Long id) throws ServiceException {
        try {
            logger.debug("get category with id: " + id);

            Category category = categoryDao.findById(id).orElseThrow(() ->
                    new EntityNotFoundException(format("LocalizedCategoryDto with %s not found", id), Category.class));

            return CategoryToDtoConverter.convert(category);
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public Page<CategoryDto> getAll(Pageable pageable) throws ServiceException {
        try {
            logger.debug("get all categories");

            Page<Category> categoryPage = categoryDao.findAll(pageable);

            return CategoryToDtoConverter.convert(categoryPage);

        } catch (DaoException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public Page<CategoryDto> getSubCategories(Long id, Pageable pageable) throws ServiceException {
        try {
            logger.debug("get sub categories with parent id: " + id);

            Page<Category> localizedCategories = categoryDao.findAllByParentCategoryId(id, pageable);

            setDefaultIcon(localizedCategories.getContent());

            return CategoryToDtoConverter.convert(localizedCategories);
        } catch (DaoException | PropertyNotFoundException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public Page<CategoryDto> getSubCategories(Long id, String filterEn, String filterUk, Pageable pageable) throws ServiceException {
        try {
            logger.debug("get sub categories with parent id: " + id);

            Page<Category> localizedCategories = categoryDao.findAllByParentCategoryId(id, filterEn, filterUk, pageable);

            setDefaultIcon(localizedCategories.getContent());

            return CategoryToDtoConverter.convert(localizedCategories);
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
    }

    private void setDefaultIcon(List<Category> categories) throws PropertyNotFoundException {
        logger.debug("set default icon for categories");

        for (Category category : categories) {
            String path = category.getImagePath();
            if (path == null || path.isEmpty()) {
                try {
                    String image = PropertyReceiver.get(defaultImagePath);
                    category.setImagePath(image);
                } catch (IOException e) {
                    throw new PropertyNotFoundException(format("Property '%s' not found", defaultImagePath));
                }
            }
        }
    }
}
