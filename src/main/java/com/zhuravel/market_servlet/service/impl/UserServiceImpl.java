package com.zhuravel.market_servlet.service.impl;

import com.zhuravel.market_servlet.dao.UserDao;
import com.zhuravel.market_servlet.dao.exception.DaoException;
import com.zhuravel.market_servlet.dao.impl.UserDaoImpl;
import com.zhuravel.market_servlet.model.LocaleType;
import com.zhuravel.market_servlet.model.entity.User;
import com.zhuravel.market_servlet.model.pageable.Page;
import com.zhuravel.market_servlet.model.pageable.Pageable;
import com.zhuravel.market_servlet.service.UserService;
import com.zhuravel.market_servlet.service.dto.UserDto;
import com.zhuravel.market_servlet.service.dto.converter.UserToDtoConverter;
import com.zhuravel.market_servlet.service.exception.ResourceNotFoundException;
import com.zhuravel.market_servlet.service.exception.ServiceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;

import static java.lang.String.format;

/**
 * @author Evgenii Zhuravel created on 19.05.2022
 * @version 1.0
 */
public class UserServiceImpl implements UserService {
    private static final Logger logger = LoggerFactory.getLogger(UserServiceImpl.class);

    private final UserDao userDao;

    public UserServiceImpl() {
        this.userDao = new UserDaoImpl();
    }

    @Override
    public UserDto save(UserDto user) throws ServiceException {
        try {
            User savedUser = userDao.save(UserToDtoConverter.convert(user));

            logger.debug("Successfully saved user: {}", user);

            return UserToDtoConverter.convert(savedUser);
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public void update(UserDto user) throws ServiceException {
        try {
            userDao.update(UserToDtoConverter.convert(user));

            logger.debug("Successfully updated user: {}", user);
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public void delete(Long id) throws ServiceException {
        try {
            userDao.deleteById(id);
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public UserDto getById(Long id) throws ServiceException {
        try {
            User user = userDao.findById(id).orElseThrow(() ->
                    new ResourceNotFoundException(format("User with %s not found", id), User.class));
            return UserToDtoConverter.convert(user);
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public Page<UserDto> getAll(Pageable pageable) throws ServiceException {
        try {
            return UserToDtoConverter.convert(userDao.findAll(pageable));
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public Page<UserDto> getAll(Map<String, Object> filters, Pageable page) throws ServiceException {
        try {
            return UserToDtoConverter.convert(userDao.findAll(filters, page));
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public UserDto getByLogin(String login) throws ServiceException {
        try {
            User user = userDao.findByLogin(login).orElseThrow(() ->
                    new ResourceNotFoundException(format("User with login '%s' not found", login), User.class));

            return UserToDtoConverter.convert(user);
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public void updateUserLocale(Long userId, LocaleType locale) throws ServiceException {
        UserDto user = getById(userId);

        user.setLocale(locale);

        save(user);
        logger.debug("In updateUserLocale - Successfully updated locale for user with id:[{}] to [{}]", userId, locale);
    }
}
