package com.zhuravel.market_servlet.service.impl;

import com.zhuravel.market_servlet.config.LocaleHolder;
import com.zhuravel.market_servlet.dao.ProductDao;
import com.zhuravel.market_servlet.dao.exception.DaoException;
import com.zhuravel.market_servlet.dao.impl.ProductDaoImpl;
import com.zhuravel.market_servlet.model.entity.Product;
import com.zhuravel.market_servlet.model.pageable.Page;
import com.zhuravel.market_servlet.model.pageable.Pageable;
import com.zhuravel.market_servlet.service.ParameterService;
import com.zhuravel.market_servlet.service.ProductService;
import com.zhuravel.market_servlet.service.dto.ParameterDto;
import com.zhuravel.market_servlet.service.dto.ProductDto;
import com.zhuravel.market_servlet.service.dto.ProductsCatalogDto;
import com.zhuravel.market_servlet.service.dto.converter.ProductToDtoConverter;
import com.zhuravel.market_servlet.service.exception.ResourceNotFoundException;
import com.zhuravel.market_servlet.service.exception.ServiceException;
import com.zhuravel.market_servlet.service.filter_criteria.ParameterSearchCriteria;

import java.time.LocalDateTime;
import java.util.stream.Collectors;

import static com.zhuravel.market_servlet.utils.DateTimeConverter.getMilliseconds;
import static java.lang.String.format;

/**
 * @author Evgenii Zhuravel created on 19.05.2022
 * @version 1.0
 */
public class ProductServiceImpl implements ProductService {

    private final ProductDao productDao;
    private final ParameterService parameterService;

    private final ProductToDtoConverter converter;

    public ProductServiceImpl() {
        this.productDao = new ProductDaoImpl();
        this.parameterService = new ParameterServiceImpl();
        this.converter = new ProductToDtoConverter();
    }

    public ProductServiceImpl(ProductDao productDao, ParameterService parameterService) {
        this.productDao = productDao;
        this.parameterService = parameterService;
        this.converter = new ProductToDtoConverter();
    }

    @Override
    public ProductDto save(ProductDto productDto) throws ServiceException {
        try {
            productDto.setCreateTime(getMilliseconds(LocalDateTime.now()));

            productDao.setLocale(LocaleHolder.INSTANCE.getStringLocale());

            ProductToDtoConverter converter = new ProductToDtoConverter();
            Product product = productDao.save(converter.convert(productDto));

            productDto.setId(product.getId());

            for (ParameterDto parameter : productDto.getParameters().values()) {
                parameterService.addToProduct(product.getId(), parameter);
            }

            return productDto;
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public void update(ProductDto productDto) throws ServiceException {
        try {
            productDao.setLocale(LocaleHolder.INSTANCE.getStringLocale());

            ProductToDtoConverter converter = new ProductToDtoConverter();

            Product product = converter.convert(productDto);

            productDao.update(product);

            parameterService.removeAllFromProduct(productDto.getId());

            for (ParameterDto parameter : productDto.getParameters().values()) {
                parameterService.addToProduct(product.getId(), parameter);
            }

        } catch (DaoException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public void delete(Long id) throws ServiceException {
        try {
            productDao.setLocale(LocaleHolder.INSTANCE.getStringLocale());

            productDao.deleteById(id);
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public ProductDto getById(Long id) throws ServiceException {
        try {
            productDao.setLocale(LocaleHolder.INSTANCE.getStringLocale());

            Product product = productDao.findById(id).orElseThrow(() ->
                    new ResourceNotFoundException(format("Product with %s not found", id), Product.class));

            return converter.convert(product);
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public Page<ProductDto> getAll(Pageable pageable) throws ServiceException {
        try {
            productDao.setLocale(LocaleHolder.INSTANCE.getStringLocale());

            Page<Product> products = productDao.findAll(pageable);

            return converter.convert(products);
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public Page<ProductDto> getAllByProducerName(String name, Pageable pageable) throws ServiceException {
        try {
            productDao.setLocale(LocaleHolder.INSTANCE.getStringLocale());

            Page<Product> products = productDao.findAllByProducerName(name, pageable);

            return converter.convert(products);
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public ProductsCatalogDto getAll(ParameterSearchCriteria criteria, String sort, Pageable pageable) throws ServiceException {
        try {
            productDao.setLocale(LocaleHolder.INSTANCE.getStringLocale());

            Page<Product> products = productDao.findAll(criteria, sort, pageable);
            Page<ProductDto> productsDtos = new ProductToDtoConverter().convert(products);

            ProductsCatalogDto productsCatalogDTO = new ProductsCatalogDto();
            productsCatalogDTO.setProducts(productsDtos);
            productsCatalogDTO.setProducers(productsDtos.getContent().stream().map(ProductDto::getProducer).collect(Collectors.toSet()));

            return productsCatalogDTO;
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
    }
}
