package com.zhuravel.market_servlet.service.impl;

import com.zhuravel.market_servlet.dao.RoleDao;
import com.zhuravel.market_servlet.dao.exception.DaoException;
import com.zhuravel.market_servlet.dao.impl.RoleDaoImpl;
import com.zhuravel.market_servlet.model.entity.Role;
import com.zhuravel.market_servlet.model.pageable.Page;
import com.zhuravel.market_servlet.model.pageable.Pageable;
import com.zhuravel.market_servlet.service.RoleService;
import com.zhuravel.market_servlet.service.exception.EntityNotFoundException;
import com.zhuravel.market_servlet.service.exception.ServiceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static java.lang.String.format;

/**
 * @author Evgenii Zhuravel created on 19.05.2022
 * @version 1.0
 */
public class RoleServiceImpl implements RoleService {

    private static final Logger logger = LoggerFactory.getLogger(RoleServiceImpl.class);

    private final RoleDao roleDao;

    public RoleServiceImpl() {
        this.roleDao = new RoleDaoImpl();
    }

    @Override
    public Role save(Role role) throws ServiceException {
        logger.debug("Save role");
        try {
            return roleDao.save(role);
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public void update(Role role) throws ServiceException {
        logger.debug("update role");
        try {
            roleDao.update(role);
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public void delete(Long id) throws ServiceException {
        logger.debug("delete role");
        try {
            roleDao.deleteById(id);
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public Role getById(Long id) throws ServiceException {
        logger.debug("getById role");
        try {
            return roleDao.findById(id).orElseThrow(() ->
                    new EntityNotFoundException(format("Role with id %s not found", id), Role.class));
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public Page<Role> getAll(Pageable page) throws ServiceException {
        try {
            return roleDao.findAll(page);
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
    }
}
