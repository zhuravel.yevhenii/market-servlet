package com.zhuravel.market_servlet.service.impl;

import com.zhuravel.market_servlet.dao.ShoppingCartDao;
import com.zhuravel.market_servlet.dao.exception.DaoException;
import com.zhuravel.market_servlet.dao.impl.ShoppingCartDaoImpl;
import com.zhuravel.market_servlet.model.OrderStatusType;
import com.zhuravel.market_servlet.model.entity.ShoppingCart;
import com.zhuravel.market_servlet.model.entity.User;
import com.zhuravel.market_servlet.model.pageable.Page;
import com.zhuravel.market_servlet.model.pageable.Pageable;
import com.zhuravel.market_servlet.service.ShoppingCartService;
import com.zhuravel.market_servlet.service.dto.ProductDto;
import com.zhuravel.market_servlet.service.dto.ShoppingCartDto;
import com.zhuravel.market_servlet.service.dto.builder.ShoppingCartDtoBuilder;
import com.zhuravel.market_servlet.service.exception.ResourceNotFoundException;
import com.zhuravel.market_servlet.service.exception.ServiceException;
import com.zhuravel.market_servlet.utils.DateTimeConverter;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

import static com.zhuravel.market_servlet.controller.PageableServlet.DEFAULT_PAGE;
import static java.lang.String.format;

/**
 * @author Evgenii Zhuravel created on 19.05.2022
 * @version 1.0
 */
public class ShoppingCartServiceImpl implements ShoppingCartService {

    private final ShoppingCartDao shoppingCartDao;

    public ShoppingCartServiceImpl() {
        this.shoppingCartDao = new ShoppingCartDaoImpl();
    }

    @Override
    public ShoppingCart save(ShoppingCart shoppingCart) throws ServiceException {
        try {
            return shoppingCartDao.save(shoppingCart);
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public void update(ShoppingCart shoppingCart) throws ServiceException {
        try {
            shoppingCartDao.update(shoppingCart);
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public void delete(Long id) throws ServiceException {
        try {
            shoppingCartDao.deleteById(id);
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public ShoppingCart getById(Long id) throws ServiceException {
        try {
            return shoppingCartDao.findById(id).orElseThrow(() ->
                    new ResourceNotFoundException(format("User with %s not found", id), User.class));
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public Page<ShoppingCart> getAll(Pageable pageable) throws ServiceException {
        try {
            return shoppingCartDao.findAll(pageable);
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public ShoppingCartDto getByUserId(Long userId) throws ServiceException {
        try {
            List<ShoppingCart> shoppingCart = shoppingCartDao.findAllByUserId(userId);

            ShoppingCartDtoBuilder shoppingCartDtoBuilder = new ShoppingCartDtoBuilder();

            return shoppingCartDtoBuilder.build(userId, shoppingCart);
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public void register(ShoppingCartDto shoppingCartDto) throws ServiceException {
        Map<Long, Integer> quantities = shoppingCartDto.getQuantitiesDto().getQuantities();

        Long currentTime = DateTimeConverter.getMilliseconds(LocalDateTime.now());

        for (Map.Entry<ProductDto, Integer> entry : shoppingCartDto.getProducts().entrySet()) {
            ProductDto productDto = entry.getKey();
            Integer quantity = entry.getValue();

            if (!quantities.get(productDto.getId()).equals(quantity)) {
                try {
                    if (quantities.get(productDto.getId()) == 0) {
                        shoppingCartDao.deleteByUserAndProductId(shoppingCartDto.getUserId(), productDto.getId());
                    } else {
                        ShoppingCart shoppingCart = new ShoppingCart();
                        shoppingCart.setUserId(shoppingCartDto.getUserId());
                        shoppingCart.setProductId(productDto.getId());
                        shoppingCart.setQuantity(quantities.get(productDto.getId()));
                        shoppingCart.setStatusId(OrderStatusType.REGISTERED.ordinal() + 1L);
                        shoppingCart.setCheckoutDateTime(currentTime);

                        shoppingCartDao.updateByUserAndProductId(shoppingCart.getUserId(), shoppingCart.getProductId(), shoppingCart);
                    }
                } catch (DaoException e) {
                    throw new ServiceException(e);
                }
            }
        }
    }

    @Override
    public void changeStatus(Long dateTime, Long newStatusId) throws ServiceException {
        try {
            Page<ShoppingCart> cartPage = shoppingCartDao.findAll(
                    dateTime, dateTime, 0L, new Pageable(Integer.parseInt(DEFAULT_PAGE), 100));

            for (ShoppingCart shoppingCart : cartPage.getContent()) {
                shoppingCart.setStatusId(newStatusId);
                shoppingCartDao.update(shoppingCart);
            }
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public void updateQuantity(ShoppingCartDto shoppingCartDto) throws ServiceException {
        Map<Long, Integer> quantities = shoppingCartDto.getQuantitiesDto().getQuantities();

        for (Map.Entry<ProductDto, Integer> entry : shoppingCartDto.getProducts().entrySet()) {
            ProductDto productDto = entry.getKey();
            Integer quantity = entry.getValue();

            if (!quantities.get(productDto.getId()).equals(quantity)) {
                try {
                    if (quantities.get(productDto.getId()) == 0) {
                        shoppingCartDao.deleteByUserAndProductId(shoppingCartDto.getUserId(), productDto.getId());
                    } else {
                        shoppingCartDao.updateQuantityByUserAndProductId(
                                shoppingCartDto.getUserId(), productDto.getId(), quantities.get(productDto.getId()));
                    }
                } catch (DaoException e) {
                    throw new ServiceException(e);
                }
            }
        }
    }

    @Override
    public void add(Long userId, Long productId) throws ServiceException {
        try {
            ShoppingCart shoppingCart = new ShoppingCart();

            shoppingCart.setUserId(userId);
            shoppingCart.setProductId(productId);
            shoppingCart.setQuantity(1);

            shoppingCartDao.save(shoppingCart);
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public ShoppingCartDto getAll(Long dateFrom, Long dateUntil, Long statusId, Pageable page) throws ServiceException {
        try {
            Page<ShoppingCart> shoppingCartPage = shoppingCartDao.findAll(dateFrom, dateUntil, statusId, page);

            ShoppingCartDtoBuilder shoppingCartDtoBuilder = new ShoppingCartDtoBuilder();

            return shoppingCartDtoBuilder.build(null, shoppingCartPage.getContent());
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
    }
}
