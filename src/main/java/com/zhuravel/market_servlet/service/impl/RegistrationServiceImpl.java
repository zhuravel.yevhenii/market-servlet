package com.zhuravel.market_servlet.service.impl;

import com.zhuravel.market_servlet.dao.UserDao;
import com.zhuravel.market_servlet.dao.exception.DaoException;
import com.zhuravel.market_servlet.dao.impl.UserDaoImpl;
import com.zhuravel.market_servlet.model.RoleType;
import com.zhuravel.market_servlet.model.UserStatusType;
import com.zhuravel.market_servlet.model.entity.Role;
import com.zhuravel.market_servlet.model.entity.User;
import com.zhuravel.market_servlet.model.entity.UserStatus;
import com.zhuravel.market_servlet.service.RegistrationService;
import com.zhuravel.market_servlet.service.dto.AccountForm;
import com.zhuravel.market_servlet.service.dto.UserDto;
import com.zhuravel.market_servlet.service.dto.converter.UserToAccountConverter;
import com.zhuravel.market_servlet.service.dto.converter.UserToDtoConverter;
import com.zhuravel.market_servlet.service.exception.EntityNotFoundException;
import com.zhuravel.market_servlet.service.exception.ServiceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static java.lang.String.format;

/**
 * @author Evgenii Zhuravel created on 19.05.2022
 * @version 1.0
 */
public class RegistrationServiceImpl implements RegistrationService {
    private static final Logger logger = LoggerFactory.getLogger(RegistrationServiceImpl.class);

    private final UserDao userDao;

    public RegistrationServiceImpl() {
        this.userDao = new UserDaoImpl();
    }

    @Override
    public UserDto save(AccountForm accountForm) throws ServiceException {
        try {
            User user = UserToAccountConverter.convert(accountForm);

            if (user.getRole() == null) {
                user.setRole(new Role(RoleType.ROLE_USER));
            }
            if (user.getStatus() == null) {
                user.setStatus(new UserStatus(UserStatusType.ACTIVE));
            }

            userDao.save(user);

            logger.debug("Successfully saved user: {}", user);

            return UserToDtoConverter.convert(user);
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public AccountForm getById(Long id) throws ServiceException {
        try {
            User user = userDao.findById(id).orElseThrow(() ->
                    new EntityNotFoundException(format("User with id %s not found", id), User.class));

            return UserToAccountConverter.convert(user);
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public AccountForm getByLogin(String login) throws ServiceException {
        try {
        User user = userDao.findByLogin(login).orElseThrow(() ->
                new EntityNotFoundException(format("User with login %s not found", login), User.class));

        return UserToAccountConverter.convert(user);
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
    }
}
