package com.zhuravel.market_servlet.service.impl;

import com.zhuravel.market_servlet.dao.UserDao;
import com.zhuravel.market_servlet.dao.exception.DaoException;
import com.zhuravel.market_servlet.dao.impl.UserDaoImpl;
import com.zhuravel.market_servlet.model.entity.User;
import com.zhuravel.market_servlet.service.LoginService;
import com.zhuravel.market_servlet.service.dto.LoginForm;
import com.zhuravel.market_servlet.service.dto.UserDto;
import com.zhuravel.market_servlet.service.dto.converter.UserToDtoConverter;
import com.zhuravel.market_servlet.service.exception.ResourceNotFoundException;
import com.zhuravel.market_servlet.service.exception.ServiceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Optional;

import static java.lang.String.format;

/**
 * @author Evgenii Zhuravel created on 19.05.2022
 * @version 1.0
 */
public class LoginServiceImpl implements LoginService {
    private static final Logger logger = LoggerFactory.getLogger(LoginServiceImpl.class);

    private final UserDao userDao;

    public LoginServiceImpl() {
        this.userDao = new UserDaoImpl();
    }

    @Override
    public Optional<UserDto> authenticate(LoginForm loginForm) throws ServiceException {
        logger.debug("authenticate user: " + loginForm.getLogin());
        try {
            User user = userDao.findByLogin(loginForm.getLogin()).orElseThrow(() ->
                    new ResourceNotFoundException(format("User with login %s not found", loginForm.getLogin()), LoginForm.class));

            return Optional.of(UserToDtoConverter.convert(user));
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
    }
}
