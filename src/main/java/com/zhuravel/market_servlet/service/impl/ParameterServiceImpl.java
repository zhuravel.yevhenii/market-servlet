package com.zhuravel.market_servlet.service.impl;

import com.zhuravel.market_servlet.dao.ParameterDao;
import com.zhuravel.market_servlet.dao.exception.DaoException;
import com.zhuravel.market_servlet.dao.impl.ParameterDaoImpl;
import com.zhuravel.market_servlet.model.entity.Parameter;
import com.zhuravel.market_servlet.model.pageable.Page;
import com.zhuravel.market_servlet.model.pageable.Pageable;
import com.zhuravel.market_servlet.service.ParameterService;
import com.zhuravel.market_servlet.service.dto.ParameterDto;
import com.zhuravel.market_servlet.service.dto.converter.ParameterToDtoConverter;
import com.zhuravel.market_servlet.service.exception.ResourceNotFoundException;
import com.zhuravel.market_servlet.service.exception.ServiceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

import static java.lang.String.format;

/**
 * @author Evgenii Zhuravel created on 19.05.2022
 * @version 1.0
 */
public class ParameterServiceImpl implements ParameterService {

    private static final Logger logger = LoggerFactory.getLogger(ParameterServiceImpl.class);

    private final ParameterDao parameterDao;

    public ParameterServiceImpl() {
        this.parameterDao = new ParameterDaoImpl();
    }

    public ParameterServiceImpl(ParameterDao parameterDao) {
        this.parameterDao = parameterDao;
    }

    @Override
    public ParameterDto save(ParameterDto parameter) throws ServiceException {
        logger.debug("Save parameter");
        try {
            ParameterToDtoConverter converter = new ParameterToDtoConverter();
            Parameter saved = parameterDao.save(converter.convert(parameter));

            parameter.setId(saved.getId());

            return parameter;
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public void update(ParameterDto parameter) throws ServiceException {
        logger.debug("update parameter");
        try {
            ParameterToDtoConverter converter = new ParameterToDtoConverter();

            parameterDao.update(converter.convert(parameter));
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public void delete(Long id) throws ServiceException {
        logger.debug("Delete by id: " + id);
        try {
            parameterDao.deleteById(id);
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public ParameterDto getById(Long id) throws ServiceException {
        logger.debug("Get by id: " + id);
        try {
            ParameterToDtoConverter converter = new ParameterToDtoConverter();

            Parameter parameter = parameterDao.findById(id).orElseThrow(() ->
                    new ResourceNotFoundException(format("Parameter with %s not found", id), Parameter.class));

            return converter.convert(parameter);
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public Page<ParameterDto> getAll(Pageable pageable) throws ServiceException {
        logger.debug("Get all");
        try {
            ParameterToDtoConverter converter = new ParameterToDtoConverter();

            return converter.convert(parameterDao.findAll(pageable));
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public void addToProduct(Long productId, ParameterDto parameter) throws ServiceException {
        logger.debug("Save parameter");
        try {
            ParameterToDtoConverter converter = new ParameterToDtoConverter();
            parameterDao.addToProduct(productId, converter.convert(parameter));
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public void removeAllFromProduct(Long productId) throws ServiceException {
        logger.debug("Save parameter");
        try {
            parameterDao.removeAllFromProduct(productId);
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public List<ParameterDto> getByProductId(Long id) throws ServiceException {
        logger.debug("Get by product id: " + id);
        try {
            ParameterToDtoConverter converter = new ParameterToDtoConverter();

            return converter.convert(parameterDao.findAllByProductId(id));
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public List<ParameterDto> getByTypeId(Long id) throws ServiceException {
        logger.debug("Get by type id: " + id);
        try {
            ParameterToDtoConverter converter = new ParameterToDtoConverter();

            return converter.convert(parameterDao.findByTypeId(id));
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
    }
}
