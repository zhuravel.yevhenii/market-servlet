package com.zhuravel.market_servlet.service.impl;

import com.zhuravel.market_servlet.config.LocaleHolder;
import com.zhuravel.market_servlet.dao.LocalizedParameterTypeDao;
import com.zhuravel.market_servlet.dao.exception.DaoException;
import com.zhuravel.market_servlet.dao.impl.LocalizedParameterTypeDaoImpl;
import com.zhuravel.market_servlet.model.entity.LocalizedParameterType;
import com.zhuravel.market_servlet.model.pageable.Page;
import com.zhuravel.market_servlet.model.pageable.Pageable;
import com.zhuravel.market_servlet.service.LocalizedParameterTypeService;
import com.zhuravel.market_servlet.service.exception.ResourceNotFoundException;
import com.zhuravel.market_servlet.service.exception.ServiceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static java.lang.String.format;

/**
 * @author Evgenii Zhuravel created on 19.05.2022
 * @version 1.0
 */
public class LocalizedParameterTypeServiceImpl implements LocalizedParameterTypeService {

    private static final Logger logger = LoggerFactory.getLogger(LocalizedParameterTypeServiceImpl.class);

    private final LocalizedParameterTypeDao localizedParameterTypeDao;

    public LocalizedParameterTypeServiceImpl() {
        this.localizedParameterTypeDao = new LocalizedParameterTypeDaoImpl();
    }

    @Override
    public LocalizedParameterType getById(Long id) throws ServiceException {
        logger.debug("getById with id {}", id);
        try {
            localizedParameterTypeDao.setLocale(LocaleHolder.INSTANCE.getStringLocale());

            return localizedParameterTypeDao.findById(id).orElseThrow(() ->
                    new ResourceNotFoundException(format("LocalizedParameterType with %s not found", id), LocalizedParameterType.class));
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public Page<LocalizedParameterType> getAllByCategoryId(Long id, Pageable pageable) throws ServiceException {
        logger.debug("getAllByCategoryId with CategoryId {}", id);
        try {
            localizedParameterTypeDao.setLocale(LocaleHolder.INSTANCE.getStringLocale());

            Page<LocalizedParameterType> categories = localizedParameterTypeDao.findAllByCategoryId(id, pageable);
            categories.getContent().forEach(parameterType -> {
                if (parameterType.getLocalizedName().isEmpty()) {
                    parameterType.setLocalizedName(parameterType.getName());
                }
            });
            return categories;
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
    }
}
