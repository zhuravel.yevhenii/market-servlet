package com.zhuravel.market_servlet.service;

import com.zhuravel.market_servlet.model.entity.ShoppingCart;
import com.zhuravel.market_servlet.model.pageable.Pageable;
import com.zhuravel.market_servlet.service.dto.ShoppingCartDto;
import com.zhuravel.market_servlet.service.exception.ServiceException;

/**
 * @author Evgenii Zhuravel created on 19.05.2022
 * @version 1.0
 */
public interface ShoppingCartService extends BaseService<ShoppingCart, Long> {

    ShoppingCartDto getByUserId(Long userId) throws ServiceException;

    void updateQuantity(ShoppingCartDto shoppingCartDto) throws ServiceException;

    void register(ShoppingCartDto shoppingCartDto) throws ServiceException;

    void changeStatus(Long dateTime, Long newStatusId) throws ServiceException;

    void add(Long userId, Long productId) throws ServiceException;

    ShoppingCartDto getAll(Long dateFrom, Long dateUntil, Long statusId, Pageable page) throws ServiceException;
}
