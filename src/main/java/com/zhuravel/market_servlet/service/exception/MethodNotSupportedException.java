package com.zhuravel.market_servlet.service.exception;

/**
 * @author Evgenii Zhuravel created on 27.05.2022
 * @version 1.0
 */
public class MethodNotSupportedException extends ServiceException {

    public MethodNotSupportedException(String message) {
        super(message);
    }

    public MethodNotSupportedException(String message, Class<?> resourceClass) {
        super(message, resourceClass);
    }

    public MethodNotSupportedException(String message, Throwable cause) {
        super(message, cause);
    }

    public MethodNotSupportedException(Throwable cause) {
        super(cause);
    }
}
