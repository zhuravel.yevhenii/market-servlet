package com.zhuravel.market_servlet.service.exception;

/**
 * @author Evgenii Zhuravel created on 27.05.2022
 * @version 1.0
 */
public class ServiceException extends Exception {

    private final Class<?> resourceClass;

    public ServiceException(String message) {
        super(message);
        this.resourceClass = null;
    }

    public ServiceException(String message, Class<?> resourceClass) {
        super(message);
        this.resourceClass = resourceClass;
    }

    public ServiceException(String message, Throwable cause) {
        super(message, cause);
        this.resourceClass = null;
    }

    public ServiceException(Throwable cause) {
        super(cause);
        this.resourceClass = null;
    }

    public Class<?> getResourceClass() {
        return resourceClass;
    }
}
