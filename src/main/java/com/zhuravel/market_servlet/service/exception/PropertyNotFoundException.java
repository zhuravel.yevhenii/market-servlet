package com.zhuravel.market_servlet.service.exception;

/**
 * @author Evgenii Zhuravel created on 27.05.2022
 * @version 1.0
 */
public class PropertyNotFoundException extends ServiceException {

    public PropertyNotFoundException(String message) {
        super(message);
    }

    public PropertyNotFoundException(String message, Class<?> resourceClass) {
        super(message, resourceClass);
    }

    public PropertyNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }

    public PropertyNotFoundException(Throwable cause) {
        super(cause);
    }
}
