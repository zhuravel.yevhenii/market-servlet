package com.zhuravel.market_servlet.service.exception;

/**
 * @author Evgenii Zhuravel created on 27.05.2022
 * @version 1.0
 */
public class EntityNotFoundException extends ServiceException {

    public EntityNotFoundException(String message) {
        super(message);
    }

    public EntityNotFoundException(String message, Class<?> resourceClass) {
        super(message, resourceClass);
    }

    public EntityNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }

    public EntityNotFoundException(Throwable cause) {
        super(cause);
    }
}
