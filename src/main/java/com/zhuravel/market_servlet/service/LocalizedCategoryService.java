package com.zhuravel.market_servlet.service;

import com.zhuravel.market_servlet.model.pageable.Page;
import com.zhuravel.market_servlet.model.pageable.Pageable;
import com.zhuravel.market_servlet.service.dto.LocalizedCategoryDto;
import com.zhuravel.market_servlet.service.exception.ServiceException;

/**
 * @author Evgenii Zhuravel created on 19.05.2022
 * @version 1.0
 */
public interface LocalizedCategoryService {

    LocalizedCategoryDto getById(Long id) throws ServiceException;

    Page<LocalizedCategoryDto> getSubCategories(Long id, Pageable pageable) throws ServiceException;

}
