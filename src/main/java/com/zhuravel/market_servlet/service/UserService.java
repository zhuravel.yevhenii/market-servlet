package com.zhuravel.market_servlet.service;

import com.zhuravel.market_servlet.model.LocaleType;
import com.zhuravel.market_servlet.model.pageable.Page;
import com.zhuravel.market_servlet.model.pageable.Pageable;
import com.zhuravel.market_servlet.service.dto.UserDto;
import com.zhuravel.market_servlet.service.exception.ServiceException;

import java.util.Map;

/**
 * @author Evgenii Zhuravel created on 19.05.2022
 * @version 1.0
 */
public interface UserService extends BaseService<UserDto, Long> {

    UserDto getByLogin(String login) throws ServiceException;

    void updateUserLocale(Long userId, LocaleType locale) throws ServiceException;

    Page<UserDto> getAll(Map<String, Object> filters, Pageable page) throws ServiceException;
}
