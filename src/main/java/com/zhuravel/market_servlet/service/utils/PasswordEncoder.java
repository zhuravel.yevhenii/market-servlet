package com.zhuravel.market_servlet.service.utils;

import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * @author Evgenii Zhuravel created on 20.06.2022
 * @version 1.0
 */
public class PasswordEncoder {

    public static String encode(String password) throws NoSuchAlgorithmException {
        return toHexString(getSHA(password));
    }

    public static boolean verify(String providedPassword, String securedPassword) throws NoSuchAlgorithmException {
        String newSecurePassword = encode(providedPassword);

        return newSecurePassword.equalsIgnoreCase(securedPassword);
    }

    private static byte[] getSHA(String input) throws NoSuchAlgorithmException {
        MessageDigest md = MessageDigest.getInstance("SHA-256");

        return md.digest(input.getBytes(StandardCharsets.UTF_8));
    }

    private static String toHexString(byte[] hash) {
        BigInteger number = new BigInteger(1, hash);

        StringBuilder hexString = new StringBuilder(number.toString(16));

        while (hexString.length() < 32)
        {
            hexString.insert(0, '0');
        }

        return hexString.toString();
    }
}
