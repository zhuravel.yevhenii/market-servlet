package com.zhuravel.market_servlet.service.validation;

import com.zhuravel.market_servlet.config.MessageResourceReceiver;
import com.zhuravel.market_servlet.config.PropertyReceiver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.lang.reflect.Field;
import java.util.Map;
import java.util.TreeMap;
import java.util.regex.Pattern;

/**
 * @author Evgenii Zhuravel created on 03.07.2022
 * @version 1.0
 */
public class Validator {
    private static final Logger logger = LoggerFactory.getLogger(Validator.class);

    private static Map<String, String> errors;

    public static Map<String, String> validate(Object object) {
        errors = new TreeMap<>();

        Field[] fields = object.getClass().getDeclaredFields();

        for (Field field : fields) {
            field.setAccessible(true);

            validateNotEmptyField(object, field);
            validateMinField(object, field);
            validateMaxField(object, field);
            validateEmailField(object, field);
        }

        validatePasswordField(object);

        return errors;
    }

    private static void validateNotEmptyField(Object object, Field field) {
        NotEmpty annotation = field.getAnnotation(NotEmpty.class);

        if (annotation != null) {
            Object o = null;
            try {
                o = field.get(object);
            } catch (IllegalAccessException e) {
                logger.debug(e.getMessage());
            }

            if (o == null || (o instanceof String && ((String) o).isEmpty())) {
                errors.put(field.getName(), getStringMessage(annotation.message()));
                System.out.println(errors);
            }
        }
    }

    private static void validateMinField(Object object, Field field) {
        Min annotation = field.getAnnotation(Min.class);

        if (annotation != null) {
            Object o = null;
            try {
                o = field.get(object);
            } catch (IllegalAccessException e) {
                logger.debug(e.getMessage());
            }

            Integer size = PropertyReceiver.get(annotation.value(), annotation.defaultValue());
            String message = getStringMessage(annotation.message());

            if (o != null) {
                if ((o instanceof String && ((String) o).length() < size) ||
                                (o instanceof Number && ((Number) o).longValue() < size)) {
                    errors.put(field.getName(), message);
                } else if (o instanceof Map) {
                    for (Map.Entry<?, ?> entry : ((Map<?, ?>) o).entrySet()) {
                        if ((entry.getValue() instanceof String && ((String) entry.getValue()).length() < size) ||
                                (entry.getValue() instanceof Number && ((Number) entry.getValue()).longValue() < size)) {
                            errors.put(field.getName() + "_" + entry.getKey(), message);
                        }
                    }
                }
            } else {
                errors.put(field.getName(), message);
            }
        }
    }

    private static void validateMaxField(Object object, Field field) {
        Max annotation = field.getAnnotation(Max.class);

        if (annotation != null) {
            Object o = null;
            try {
                o = field.get(object);
            } catch (IllegalAccessException e) {
                logger.debug(e.getMessage());
            }

            Integer size = PropertyReceiver.get(annotation.value(), annotation.defaultValue());

            if ((o != null) &&
                    ((o instanceof String && ((String) o).length() > size) ||
                    (o instanceof Number && ((Number) o).longValue() > size))) {

                String message = getStringMessage(annotation.message());
                errors.put(field.getName(), message);
            }
        }
    }

    private static void validateEmailField(Object object, Field field) {
        Email annotation = field.getAnnotation(Email.class);

        if (annotation != null) {
            Object o = null;
            try {
                o = field.get(object);
            } catch (IllegalAccessException e) {
                logger.debug(e.getMessage());
            }

            if (o != null) {
                String regexPattern;
                try {
                    regexPattern = PropertyReceiver.get(annotation.pattern());
                } catch (IOException e) {
                    e.printStackTrace();

                    regexPattern = annotation.pattern();
                }

                if (!patternMatches((String) o, regexPattern)) {
                    String message = getStringMessage(annotation.message());
                    errors.put(field.getName(), message);
                }
            }
        }
    }

    private static void validatePasswordField(Object object) {
        Field[] fields = object.getClass().getDeclaredFields();

        Field confirmField = null;
        ConfirmPassword confirmPassword = null;

        String password = "";
        String confirm = "";

        for (Field field : fields) {
            field.setAccessible(true);

            if (field.isAnnotationPresent(Password.class)) {
                try {
                    password = (String) field.get(object);
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
            } else if (field.isAnnotationPresent(ConfirmPassword.class)) {
                confirmField = field;
                confirmPassword = field.getAnnotation(ConfirmPassword.class);

                try {
                    confirm = (String) field.get(object);
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
            }
        }

        if (confirmField != null && confirmPassword != null) {
            if (!password.equals(confirm)) {
                errors.put(confirmField.getName(), getStringMessage(confirmPassword.message()));
            }
        }
    }

    private static boolean patternMatches(String string, String regexPattern) {
        return Pattern.compile(regexPattern)
                .matcher(string)
                .matches();
    }

    private static String getStringMessage(String annotationMessage) {
        try {
            return MessageResourceReceiver.getString(annotationMessage);
        } catch (Exception e) {
            return annotationMessage;
        }
    }
}
