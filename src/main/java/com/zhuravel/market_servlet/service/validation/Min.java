package com.zhuravel.market_servlet.service.validation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author Evgenii Zhuravel created on 03.07.2022
 * @version 1.0
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface Min {
    String value() default "";

    int defaultValue() default 0;

    String message() default "";
}
