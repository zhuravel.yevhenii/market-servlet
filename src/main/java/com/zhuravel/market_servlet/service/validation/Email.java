package com.zhuravel.market_servlet.service.validation;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * @author Evgenii Zhuravel created on 03.07.2022
 * @version 1.0
 */
@Retention(RetentionPolicy.RUNTIME)
public @interface Email {
    String pattern() default "^(?=.{1,64}@)[A-Za-z0-9_-]+(\\.[A-Za-z0-9_-]+)*@"
            + "[^-][A-Za-z0-9-]+(\\.[A-Za-z0-9-]+)*(\\.[A-Za-z]{2,})$";
    String message() default "";
}
