package com.zhuravel.market_servlet.service;

import com.zhuravel.market_servlet.service.dto.ParameterDto;
import com.zhuravel.market_servlet.service.exception.ServiceException;

import java.util.List;

/**
 * @author Evgenii Zhuravel created on 19.05.2022
 * @version 1.0
 */
public interface ParameterService extends BaseService<ParameterDto, Long> {

    void addToProduct(Long productId, ParameterDto e) throws ServiceException;

    void removeAllFromProduct(Long productId) throws ServiceException;

    List<ParameterDto> getByProductId(Long id) throws ServiceException;

    List<ParameterDto> getByTypeId(Long id) throws ServiceException;
}
