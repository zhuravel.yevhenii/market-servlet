package com.zhuravel.market_servlet.service;

import com.zhuravel.market_servlet.service.dto.AccountForm;
import com.zhuravel.market_servlet.service.dto.UserDto;
import com.zhuravel.market_servlet.service.exception.ServiceException;

/**
 * @author Evgenii Zhuravel created on 19.05.2022
 * @version 1.0
 */
public interface RegistrationService {

    AccountForm getById(Long id) throws ServiceException;

    AccountForm getByLogin(String login) throws ServiceException;

    UserDto save(AccountForm accountForm) throws ServiceException;
}
