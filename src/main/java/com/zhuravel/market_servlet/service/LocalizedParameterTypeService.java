package com.zhuravel.market_servlet.service;

import com.zhuravel.market_servlet.model.entity.LocalizedParameterType;
import com.zhuravel.market_servlet.model.pageable.Page;
import com.zhuravel.market_servlet.model.pageable.Pageable;
import com.zhuravel.market_servlet.service.exception.ServiceException;

/**
 * @author Evgenii Zhuravel created on 19.05.2022
 * @version 1.0
 */
public interface LocalizedParameterTypeService /*extends BaseService<LocalizedParameterType, Long>*/ {

    LocalizedParameterType getById(Long id) throws ServiceException;

    Page<LocalizedParameterType> getAllByCategoryId(Long id, Pageable pageable) throws ServiceException;
}
