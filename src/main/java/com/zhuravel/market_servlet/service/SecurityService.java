package com.zhuravel.market_servlet.service;

/**
 * @author Evgenii Zhuravel created on 22.05.2022
 * @version 1.0
 */
public interface SecurityService {

    String findLoggedInLogin();

    void autoLogin(String login, String password);
}
