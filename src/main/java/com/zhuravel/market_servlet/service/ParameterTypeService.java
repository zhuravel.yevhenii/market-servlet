package com.zhuravel.market_servlet.service;

import com.zhuravel.market_servlet.service.dto.ParameterTypeDto;
import com.zhuravel.market_servlet.service.exception.ServiceException;

import java.util.List;

/**
 * @author Evgenii Zhuravel created on 26.06.2022
 * @version 1.0
 */
public interface ParameterTypeService extends BaseService<ParameterTypeDto, Long> {

    List<ParameterTypeDto> getAllByCategoryId(Long id) throws ServiceException;
}
