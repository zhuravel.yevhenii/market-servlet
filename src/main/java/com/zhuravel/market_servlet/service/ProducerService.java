package com.zhuravel.market_servlet.service;

import com.zhuravel.market_servlet.model.entity.Producer;
import com.zhuravel.market_servlet.service.exception.ServiceException;

import java.util.List;

/**
 * @author Evgenii Zhuravel created on 19.05.2022
 * @version 1.0
 */
public interface ProducerService extends BaseService<Producer, Long> {

    List<Producer> getDistinctByCategoryId(Long categoryId) throws ServiceException;
}
