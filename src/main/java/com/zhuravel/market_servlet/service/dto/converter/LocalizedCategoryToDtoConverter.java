package com.zhuravel.market_servlet.service.dto.converter;

import com.zhuravel.market_servlet.model.pageable.Page;
import com.zhuravel.market_servlet.service.dto.LocalizedCategoryDto;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Evgenii Zhuravel created on 22.06.2022
 * @version 1.0
 */
public class LocalizedCategoryToDtoConverter {

    public static LocalizedCategoryDto convert(com.zhuravel.market_servlet.model.entity.LocalizedCategory category) {
        return new LocalizedCategoryDto(category.getId(),
                category.getName(),
                category.getParentCategoryId(),
                category.getImagePath(),
                category.getLocale(),
                category.getLocalizedName());
    }

    public static Page<LocalizedCategoryDto> convert(Page<com.zhuravel.market_servlet.model.entity.LocalizedCategory> categories) {
        List<LocalizedCategoryDto> collect = categories.getContent().stream().map(LocalizedCategoryToDtoConverter::convert).collect(Collectors.toList());
        return new Page<>(collect, categories.getTotalPages());
    }

    public static com.zhuravel.market_servlet.model.entity.LocalizedCategory convert(LocalizedCategoryDto localizedCategoryDto) {
        return new com.zhuravel.market_servlet.model.entity.LocalizedCategory(localizedCategoryDto.getId(),
                localizedCategoryDto.getName(),
                localizedCategoryDto.getParentCategoryId(),
                localizedCategoryDto.getImagePath(),
                localizedCategoryDto.getLocale(),
                localizedCategoryDto.getLocalizedName());
    }
}
