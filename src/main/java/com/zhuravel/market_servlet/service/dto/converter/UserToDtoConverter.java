package com.zhuravel.market_servlet.service.dto.converter;

import com.zhuravel.market_servlet.model.entity.User;
import com.zhuravel.market_servlet.model.pageable.Page;
import com.zhuravel.market_servlet.service.dto.UserDto;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Evgenii Zhuravel created on 04.06.2022
 * @version 1.0
 */
public class UserToDtoConverter {

    public static UserDto convert(User user) {
        return new UserDto(user.getId(),
                user.getLogin(),
                user.getFirstName(),
                user.getLastName(),
                user.getPassword(),
                user.getEmail(),
                user.getLocale(),
                user.getStatus(),
                user.getRole());
    }

    public static User convert(UserDto userDto) {
        return new User(userDto.getId(),
                userDto.getLogin(),
                userDto.getFirstName(),
                userDto.getLastName(),
                userDto.getPassword(),
                userDto.getEmail(),
                userDto.getLocale(),
                userDto.getStatus(),
                userDto.getRole());
    }

    public static Page<UserDto> convert(Page<User> users) {
        List<UserDto> collect = users.getContent().stream().map(UserToDtoConverter::convert).collect(Collectors.toList());
        return new Page<>(collect, users.getTotalPages());
    }
}
