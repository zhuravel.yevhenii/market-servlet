package com.zhuravel.market_servlet.service.dto;

import com.zhuravel.market_servlet.service.validation.NotEmpty;

import java.util.Objects;

/**
 * @author Evgenii Zhuravel created on 04.06.2022
 * @version 1.0
 */
public class ParameterDto implements Comparable<ParameterDto> {

    private Long id;

    private Long typeId;

    private String typeName;

    @NotEmpty(message = "message.value_must_not_be_empty")
    private String value;

    public ParameterDto() {
    }

    public ParameterDto(Long id, Long typeId, String typeName, String value) {
        this.id = id;
        this.typeId = typeId;
        this.typeName = typeName;
        this.value = value;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getTypeId() {
        return typeId;
    }

    public void setTypeId(Long typeId) {
        this.typeId = typeId;
    }

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ParameterDto that = (ParameterDto) o;
        return id.equals(that.id) && typeName.equals(that.typeName) && value.equals(that.value);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, typeName, value);
    }

    @Override
    public String toString() {
        return "ParameterDto{" +
                "id=" + id +
                ", typeId='" + typeId + '\'' +
                ", typeName='" + typeName + '\'' +
                ", value='" + value + '\'' +
                '}';
    }

    @Override
    public int compareTo(ParameterDto o) {
        return this.getValue().compareTo(o.getValue());
    }
}
