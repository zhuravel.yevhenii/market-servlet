package com.zhuravel.market_servlet.service.dto.converter;

import com.zhuravel.market_servlet.model.entity.Producer;
import com.zhuravel.market_servlet.model.entity.Product;
import com.zhuravel.market_servlet.model.entity.ProductTranslation;
import com.zhuravel.market_servlet.model.pageable.Page;
import com.zhuravel.market_servlet.service.ParameterService;
import com.zhuravel.market_servlet.service.ProducerService;
import com.zhuravel.market_servlet.service.dto.ParameterDto;
import com.zhuravel.market_servlet.service.dto.ProductDto;
import com.zhuravel.market_servlet.service.exception.ServiceException;
import com.zhuravel.market_servlet.service.impl.ParameterServiceImpl;
import com.zhuravel.market_servlet.service.impl.ProducerServiceImpl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.stream.Collectors;

/**
 * @author Evgenii Zhuravel created on 04.06.2022
 * @version 1.0
 */
public class ProductToDtoConverter {

    private final ProducerService producerService;
    private final ParameterService parameterService;

    public ProductToDtoConverter() {
        this.producerService = new ProducerServiceImpl();
        this.parameterService = new ParameterServiceImpl();
    }

    public ProductDto convert(Product product) throws ServiceException {
        ProductDto.Builder builder = ProductDto.newBuilder();

        List<ParameterDto> parameters = parameterService.getByProductId(product.getId());

        Map<Long, ParameterDto> parameterDtos = parameters.stream()
                .collect(Collectors.toMap(ParameterDto::getId, parameterDto -> parameterDto));

        Producer producer = producerService.getById(product.getProducerId());

        Map<String, String> localizedNames = new TreeMap<>();
        Map<String, String> localizedDescriptions = new TreeMap<>();

        for (ProductTranslation translation : product.getTranslations()) {
            localizedNames.put(translation.getLocale(), translation.getLocalizedName());
            localizedDescriptions.put(translation.getLocale(), translation.getLocalizedDescription());
        }

        builder.setId(product.getId())
                .setName(product.getName())
                .setNames(localizedNames)
                .setDescriptions(localizedDescriptions)
                .setCategoryId(product.getCategoryId())
                .setProducer(producer)
                .setPrice(product.getPrice())
                .setParameters(parameterDtos)
                .setImagePath(product.getImagePath())
                .setCreateTime(product.getCreateTime());

        return builder.build();
    }

    public Product convert(ProductDto productDto) {
        List<ProductTranslation> translations = new ArrayList<>();

        for (Map.Entry<String, String> localizedName : productDto.getLocalizedNames().entrySet()) {
            translations.add(new ProductTranslation(productDto.getId(),
                    localizedName.getKey(),
                    localizedName.getValue(),
                    productDto.getLocalizedDescriptions().get(localizedName.getKey())));
        }

        Product product = new Product();

        product.setId(productDto.getId());
        product.setName(productDto.getName());
        product.setTranslations(translations);
        product.setCategoryId(productDto.getCategoryId());
        product.setProducerId(productDto.getProducer().getId());
        product.setPrice(productDto.getPrice());
        product.setImagePath(productDto.getImagePath());
        product.setCreateTime(productDto.getCreateTime());

        return product;
    }

    public Page<ProductDto> convert(Page<Product> products) throws ServiceException {
        List<ProductDto> collect = new ArrayList<>();

        for (Product product : products.getContent()) {
            collect.add(this.convert(product));
        }
        return new Page<>(collect, products.getTotalPages());
    }
}
