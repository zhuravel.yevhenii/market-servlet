package com.zhuravel.market_servlet.service.dto;

import com.zhuravel.market_servlet.model.entity.BaseEntity;
import com.zhuravel.market_servlet.service.validation.Max;
import com.zhuravel.market_servlet.service.validation.Min;
import com.zhuravel.market_servlet.service.validation.NotEmpty;

import java.util.Map;

/**
 * @author Evgenii Zhuravel created on 26.06.2022
 * @version 1.0
 */
public class ParameterTypeDto extends BaseEntity implements Comparable<ParameterTypeDto> {

    private Long categoryId;

    @NotEmpty(message = "message.value_must_not_be_empty")
    @Min(value = "validation.type.length.min", defaultValue = 3, message = "message.field.size.min_3.max_32")
    @Max(value = "validation.type.length.max", defaultValue = 32, message = "message.field.size.min_3.max_32")
    private String name;

    @Min(value = "validation.type.length.min", defaultValue = 3, message = "message.field.size.min_3.max_32")
    @Max(value = "validation.type.length.max", defaultValue = 32, message = "message.field.size.min_3.max_32")
    private Map<String, String> translations;

    public ParameterTypeDto() {
    }

    public ParameterTypeDto(Long id, Long categoryId, String name, Map<String, String> translations) {
        this.id = id;
        this.categoryId = categoryId;
        this.name = name;
        this.translations = translations;
    }

    public Long getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Long categoryId) {
        this.categoryId = categoryId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Map<String, String> getTranslations() {
        return translations;
    }

    public void setTranslations(Map<String, String> translations) {
        this.translations = translations;
    }

    @Override
    public int compareTo(ParameterTypeDto o) {
        return name.compareTo(o.name);
    }

    @Override
    public String toString() {
        return "ParameterTypeDto{" +
                "id=" + id +
                ", categoryId=" + categoryId +
                ", name='" + name + '\'' +
                ", translations=" + translations +
                '}';
    }
}