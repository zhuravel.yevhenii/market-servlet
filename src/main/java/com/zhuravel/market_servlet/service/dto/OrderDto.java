package com.zhuravel.market_servlet.service.dto;

import com.zhuravel.market_servlet.model.OrderStatusType;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Evgenii Zhuravel created on 06.07.2022
 * @version 1.0
 */
public class OrderDto {

    private Long registrationDateTime;

    private Map<ProductDto, Integer> products = new HashMap<>();

    private OrderStatusType status;

    public Long getRegistrationDateTime() {
        return registrationDateTime;
    }

    public void setRegistrationDateTime(Long registrationDateTime) {
        this.registrationDateTime = registrationDateTime;
    }

    public Map<ProductDto, Integer> getProducts() {
        return products;
    }

    public void setProducts(Map<ProductDto, Integer> products) {
        this.products = products;
    }

    public OrderStatusType getStatus() {
        return status;
    }

    public void setStatus(OrderStatusType status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "OrderDto{" +
                "registrationDateTime='" + registrationDateTime + '\'' +
                ", products=" + products +
                ", status=" + status +
                '}';
    }
}
