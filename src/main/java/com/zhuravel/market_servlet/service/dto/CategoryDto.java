package com.zhuravel.market_servlet.service.dto;

import com.zhuravel.market_servlet.service.validation.Min;

import java.util.Map;
import java.util.Objects;

/**
 * @author Evgenii Zhuravel created on 19.05.2022
 * @version 1.0
 */
public class CategoryDto {

    private Long id;

    @Min(value = "validation.name.length.min", defaultValue = 5, message = "message.field.size")
    private String name;

    private Long parentCategoryId;

    private String imagePath;

    @Min(value = "validation.name.length.min", defaultValue = 5, message = "message.field.size")
    private Map<String, String> localizedNames;

    public CategoryDto() {
    }

    public CategoryDto(Long id, String name, Long parentCategoryId, String imagePath, Map<String, String> localizedNames) {
        this.id = id;
        this.name = name;
        this.parentCategoryId = parentCategoryId;
        this.imagePath = imagePath;
        this.localizedNames = localizedNames;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getParentCategoryId() {
        return parentCategoryId;
    }

    public void setParentCategoryId(Long parentCategoryId) {
        this.parentCategoryId = parentCategoryId;
    }

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    public Map<String, String> getLocalizedNames() {
        return localizedNames;
    }

    public void setLocalizedNames(Map<String, String> localizedNames) {
        this.localizedNames = localizedNames;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        CategoryDto that = (CategoryDto) o;
        return id.equals(that.id) && name.equals(that.name) && parentCategoryId.equals(that.parentCategoryId) && Objects.equals(imagePath, that.imagePath) && Objects.equals(localizedNames, that.localizedNames);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, parentCategoryId, imagePath, localizedNames);
    }

    @Override
    public String toString() {
        return "LocalizedCategoryDto{"
                + "id=" + id
                + ", name='" + name + '\''
                + ", parentCategoryId=" + parentCategoryId
                + ", imagePath='" + imagePath + '\''
                + ", localizedName='" + localizedNames + '\''
                + '}';
    }
}
