package com.zhuravel.market_servlet.service.dto.builder;

import com.zhuravel.market_servlet.dao.ProductDao;
import com.zhuravel.market_servlet.dao.exception.DaoException;
import com.zhuravel.market_servlet.dao.impl.ProductDaoImpl;
import com.zhuravel.market_servlet.model.OrderStatusType;
import com.zhuravel.market_servlet.model.entity.ParameterType;
import com.zhuravel.market_servlet.model.entity.Product;
import com.zhuravel.market_servlet.model.entity.ShoppingCart;
import com.zhuravel.market_servlet.service.dto.OrderDto;
import com.zhuravel.market_servlet.service.dto.ProductDto;
import com.zhuravel.market_servlet.service.dto.ProductQuantitiesDto;
import com.zhuravel.market_servlet.service.dto.ShoppingCartDto;
import com.zhuravel.market_servlet.service.dto.converter.ProductToDtoConverter;
import com.zhuravel.market_servlet.service.exception.ResourceNotFoundException;
import com.zhuravel.market_servlet.service.exception.ServiceException;

import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.stream.Collectors;

import static java.lang.String.format;

/**
 * @author Evgenii Zhuravel created on 06.06.2022
 * @version 1.0
 */
public class ShoppingCartDtoBuilder {

    private final ProductDao productDao;

    public ShoppingCartDtoBuilder() {
        this.productDao = new ProductDaoImpl();
    }

    public ShoppingCartDto build(Long userId, List<ShoppingCart> products) throws DaoException, ServiceException {
        ShoppingCartDto shoppingCartDto = new ShoppingCartDto();

        Map<ProductDto, Integer> newProducts = new TreeMap<>();
        //Map<String, Map<ProductDto, Integer>> checkouts = new TreeMap<>();
        Map<Long, OrderDto> orders = new TreeMap<>();
        //List<OrderDto> orders = new ArrayList<>();

        ProductToDtoConverter productToDtoConverter = new ProductToDtoConverter();

        for(ShoppingCart cart: products) {
            if (cart.getCheckoutDateTime() == null || cart.getCheckoutDateTime() == 0) {
                newProducts.put(productToDtoConverter.convert(retrieveProduct(cart)), cart.getQuantity());
            } else {
                Long dateTime = cart.getCheckoutDateTime();

                if (orders.get(dateTime) == null) {
                    OrderDto order = new OrderDto();
                    order.setRegistrationDateTime(dateTime);
                    order.getProducts()
                            .put(productToDtoConverter.convert(retrieveProduct(cart)), cart.getQuantity());
                    order.setStatus(OrderStatusType.values()[cart.getStatusId().intValue()-1]);

                    orders.put(dateTime, order);
                } else {
                    orders.get(dateTime).getProducts()
                            .put(productToDtoConverter.convert(retrieveProduct(cart)), cart.getQuantity());
                }
            }
        }

        List<OrderDto> sortedOrders = orders.values().stream().sorted(Comparator.comparing(OrderDto::getStatus)).toList();

        shoppingCartDto.setUserId(userId);
        shoppingCartDto.setQuantitiesDto(initForm(newProducts));
        shoppingCartDto.setProducts(newProducts);
        shoppingCartDto.setOrders(sortedOrders);

        return shoppingCartDto;
    }

    private Product retrieveProduct(ShoppingCart cart) throws DaoException, ResourceNotFoundException {
        return productDao.findById(cart.getProductId()).orElseThrow(() ->
                new ResourceNotFoundException(format("Product with id:%s not found", cart.getProductId()), ParameterType.class));
    }

    private static ProductQuantitiesDto initForm(Map<ProductDto, Integer> products) {
        ProductQuantitiesDto form = new ProductQuantitiesDto();

        form.setQuantities(products.entrySet().stream()
                .collect(Collectors.toMap(entry -> entry.getKey().getId(), Map.Entry::getValue)));

        return form;
    }
}
