package com.zhuravel.market_servlet.service.dto.builder;

import com.zhuravel.market_servlet.config.MessageResourceReceiver;
import com.zhuravel.market_servlet.model.entity.LocalizedParameterType;
import com.zhuravel.market_servlet.model.entity.Producer;
import com.zhuravel.market_servlet.service.dto.ParameterDto;
import com.zhuravel.market_servlet.service.dto.SelectedParametersDto;
import com.zhuravel.market_servlet.service.exception.ServiceException;
import com.zhuravel.market_servlet.service.impl.ParameterServiceImpl;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.stream.Collectors;

/**
 * @author Evgenii Zhuravel created on 03.06.2022
 * @version 1.0
 */
public class SelectedParametersDtoBuilder {

    public static final String PROPERTY_PRODUCER = "label.producer";

    public static final String FILTER_PRODUCERS = "filter-prod";
    public static final String FILTER_PARAMETERS = "filter-par";

    private final ParameterServiceImpl parameterService;

    public SelectedParametersDtoBuilder() {
        this.parameterService = new ParameterServiceImpl();
    }

    public SelectedParametersDto build(List<LocalizedParameterType> localizedParameterTypes) throws ServiceException {
        SelectedParametersDto selectedParametersDto = new SelectedParametersDto();

        List<SelectedParametersDto.ParametersType> types = selectedParametersDto.getParameterTypes();

        for (LocalizedParameterType type: localizedParameterTypes) {
            List<SelectedParametersDto.SelectedParameter> parameters = new ArrayList<>();

            List<ParameterDto> parameterList = parameterService.getByTypeId(type.getId());

            for (ParameterDto parameter: parameterList) {
                parameters.add(new SelectedParametersDto.SelectedParameter(parameter.getId(), parameter.getValue()));
            }

            types.add(new SelectedParametersDto.ParametersType(type.getLocalizedName(), parameters));
        }

        return selectedParametersDto;
    }

    public static void setProducers(SelectedParametersDto selectedParametersDto, List<Producer> producers) {
        Set<SelectedParametersDto.SelectedParameter> parameters = new TreeSet<>();

        for (Producer producer: producers) {
            parameters.add(new SelectedParametersDto.SelectedParameter(producer.getId(), producer.getName()));
        }

        String producersName = MessageResourceReceiver.getString(PROPERTY_PRODUCER);

        selectedParametersDto.setProducers(new SelectedParametersDto.ParametersType(producersName, parameters.stream().toList()));
    }

    public static SelectedParametersDto build(HttpServletRequest request) {
        SelectedParametersDto selectedParametersDto = new SelectedParametersDto();

        Map<String, String[]> filtersMap = request.getParameterMap();

        selectedParametersDto.setProducers(buildProducers(filtersMap));
        selectedParametersDto.setParameterTypes(buildParametersType(filtersMap));
        return selectedParametersDto;
    }

    private static SelectedParametersDto.ParametersType buildProducers(Map<String, String[]> filtersMap) {

        Map<String, String[]> producers = filtersMap.entrySet().stream()
                .filter(stringEntry -> stringEntry.getKey().startsWith(FILTER_PRODUCERS))
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (o1, o2) -> o1, TreeMap::new));

        List<SelectedParametersDto.SelectedParameter> producersList = producers.values().stream()
                .map(value -> {
                    boolean selected = false;
                    if (value.length > 2) {
                        selected = Boolean.parseBoolean(value[2]);
                    }
                    return new SelectedParametersDto.SelectedParameter(Long.parseLong(value[0]), value[1], selected);
                }).sorted().collect(Collectors.toList());

        SelectedParametersDto.ParametersType producersType = new SelectedParametersDto.ParametersType();
        producersType.setName(MessageResourceReceiver.getString(PROPERTY_PRODUCER));
        producersType.setParameters(producersList);

        return producersType;
    }

    private static List<SelectedParametersDto.ParametersType> buildParametersType(Map<String, String[]> filtersMap) {
        List<SelectedParametersDto.ParametersType> parametersTypes = new ArrayList<>();

        Map<String, String[]> parameters = filtersMap.entrySet().stream()
                .filter(stringEntry -> stringEntry.getKey().startsWith(FILTER_PARAMETERS))
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));

        if (!parameters.isEmpty()) {
            Map<String, SelectedParametersDto.SelectedParameter> collect = parameters.entrySet().stream()
                    .collect(Collectors.toMap(Map.Entry::getKey, value -> {
                        boolean selected = false;
                        if (value.getValue().length > 2) {
                            selected = Boolean.parseBoolean(value.getValue()[2]);
                        }
                        return new SelectedParametersDto.SelectedParameter(Long.parseLong(value.getValue()[0]), value.getValue()[1], selected);
                    }, (o1, o2) -> o1, TreeMap::new));

            Map<String, List<SelectedParametersDto.SelectedParameter>> parametersTypesMap = new TreeMap<>();

            for (Map.Entry<String, SelectedParametersDto.SelectedParameter> entry: collect.entrySet()) {
                String key = entry.getKey().split("-")[2];

                if (parametersTypesMap.get(key) == null) {
                    parametersTypesMap.put(key, new ArrayList<>(List.of(entry.getValue())));
                } else {
                    parametersTypesMap.get(key).add(entry.getValue());
                }
            }

            for (Map.Entry<String, List<SelectedParametersDto.SelectedParameter>> entry: parametersTypesMap.entrySet()) {
                Collections.sort(entry.getValue());

                parametersTypes.add(new SelectedParametersDto.ParametersType(entry.getKey(), entry.getValue()));
            }
        }

        Collections.sort(parametersTypes);

        return parametersTypes;
    }
}
