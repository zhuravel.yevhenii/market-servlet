package com.zhuravel.market_servlet.service.dto;

import java.util.Objects;

/**
 * @author Evgenii Zhuravel created on 19.05.2022
 * @version 1.0
 */
public class LocalizedCategoryDto {

    private Long id;

    private String name;

    private Long parentCategoryId;

    private String imagePath;

    private String locale;

    private String localizedName;

    public LocalizedCategoryDto() {
    }

    public LocalizedCategoryDto(Long id, String name, Long parentCategoryId, String imagePath, String locale, String localizedName) {
        this.id = id;
        this.name = name;
        this.parentCategoryId = parentCategoryId;
        this.imagePath = imagePath;
        this.locale = locale;
        this.localizedName = localizedName;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getParentCategoryId() {
        return parentCategoryId;
    }

    public void setParentCategoryId(Long parentCategoryId) {
        this.parentCategoryId = parentCategoryId;
    }

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    public String getLocale() {
        return locale;
    }

    public void setLocale(String locale) {
        this.locale = locale;
    }

    public String getLocalizedName() {
        return localizedName;
    }

    public void setLocalizedName(String localizedName) {
        this.localizedName = localizedName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        LocalizedCategoryDto that = (LocalizedCategoryDto) o;
        return id.equals(that.id) && name.equals(that.name) && parentCategoryId.equals(that.parentCategoryId) && Objects.equals(imagePath, that.imagePath) && Objects.equals(locale, that.locale) && Objects.equals(localizedName, that.localizedName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, parentCategoryId, imagePath, locale, localizedName);
    }

    @Override
    public String toString() {
        return "LocalizedCategoryDto{"
                + "id=" + id
                + ", name='" + name + '\''
                + ", parentCategoryId=" + parentCategoryId
                + ", imagePath='" + imagePath + '\''
                + ", locale='" + locale + '\''
                + ", localizedName='" + localizedName + '\''
                + '}';
    }
}
