package com.zhuravel.market_servlet.service.dto.converter;

import com.zhuravel.market_servlet.model.UserStatusType;
import com.zhuravel.market_servlet.model.entity.User;
import com.zhuravel.market_servlet.model.entity.UserStatus;
import com.zhuravel.market_servlet.service.dto.AccountForm;

/**
 * @author Evgenii Zhuravel created on 04.06.2022
 * @version 1.0
 */
public class UserToAccountConverter {

    public static AccountForm convert(User user) {
        return new AccountForm(user.getId(),
                user.getLogin(),
                user.getFirstName(),
                user.getLastName(),
                user.getEmail(),
                user.getLocale(),
                user.getRole());
    }

    public static User convert(AccountForm accountForm) {
        return new User(accountForm.getId(),
                accountForm.getLogin(),
                accountForm.getFirstName(),
                accountForm.getLastName(),
                accountForm.getPassword().get(),
                accountForm.getEmail(),
                accountForm.getLocale(),
                new UserStatus(UserStatusType.ACTIVE),
                accountForm.getRole());
    }
}
