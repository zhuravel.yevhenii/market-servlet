package com.zhuravel.market_servlet.service.dto;

import com.zhuravel.market_servlet.model.LocaleType;
import com.zhuravel.market_servlet.model.entity.Role;
import com.zhuravel.market_servlet.model.entity.UserStatus;
import com.zhuravel.market_servlet.service.validation.Email;
import com.zhuravel.market_servlet.service.validation.Max;
import com.zhuravel.market_servlet.service.validation.Min;

/**
 * @author Evgenii Zhuravel created on 22.05.2022
 * @version 1.0
 */
public class UserDto {

    private Long id;

    @Min(value = "validation.login.length.min", defaultValue = 5, message = "message.userForm.loginForm.size")
    @Max(value = "validation.login.length.max", defaultValue = 32, message = "message.userForm.loginForm.size")
    private String login;

    @Min(value = "validation.name.length.min", defaultValue = 5, message = "message.userForm.firstName.size")
    @Max(value = "validation.name.length.max", defaultValue = 32, message = "message.userForm.firstName.size")
    private String firstName;

    private String lastName;

    @Email(pattern = "validation.email.regexp", message = "message.userForm.email.incorrect")
    private String email;

    private String password;

    private LocaleType locale;

    private UserStatus status;

    private Role role;

    public UserDto() {
    }

    public UserDto(Long id, String login, String firstName, String lastName, String password, String email, LocaleType locale, UserStatus status, Role role) {
        this.id = id;
        this.login = login;
        this.firstName = firstName;
        this.lastName = lastName;
        this.password = password;
        this.email = email;
        this.locale = locale;
        this.status = status;
        this.role = role;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public UserStatus getStatus() {
        return status;
    }

    public void setStatus(UserStatus status) {
        this.status = status;
    }

    public LocaleType getLocale() {
        return locale;
    }

    public void setLocale(LocaleType locale) {
        this.locale = locale;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", login='" + login + '\'' +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", email='" + email + '\'' +
                ", password='" + password + '\'' +
                ", locale=" + locale +
                ", status=" + status +
                ", role=" + role +
                '}';
    }
}
