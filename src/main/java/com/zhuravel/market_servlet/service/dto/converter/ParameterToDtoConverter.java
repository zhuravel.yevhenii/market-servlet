package com.zhuravel.market_servlet.service.dto.converter;

import com.zhuravel.market_servlet.dao.exception.DaoException;
import com.zhuravel.market_servlet.model.entity.LocalizedParameterType;
import com.zhuravel.market_servlet.model.entity.Parameter;
import com.zhuravel.market_servlet.model.pageable.Page;
import com.zhuravel.market_servlet.service.LocalizedParameterTypeService;
import com.zhuravel.market_servlet.service.dto.ParameterDto;
import com.zhuravel.market_servlet.service.exception.ServiceException;
import com.zhuravel.market_servlet.service.impl.LocalizedParameterTypeServiceImpl;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Evgenii Zhuravel created on 04.06.2022
 * @version 1.0
 */
public class ParameterToDtoConverter {

    private final LocalizedParameterTypeService localizedParameterTypeService;

    public ParameterToDtoConverter() {
        this.localizedParameterTypeService = new LocalizedParameterTypeServiceImpl();
    }

    public ParameterDto convert(Parameter parameter) throws ServiceException {

        LocalizedParameterType localizedParameterType = localizedParameterTypeService.getById(parameter.getTypeId());

        return new ParameterDto(parameter.getId(),
                parameter.getTypeId(),
                localizedParameterType.getLocalizedName(),
                parameter.getValue());
    }

    public Parameter convert(ParameterDto parameterDto) {
        return new Parameter(parameterDto.getId(),
                parameterDto.getTypeId(),
                parameterDto.getValue());
    }

    public Page<ParameterDto> convert(Page<Parameter> parameters) throws DaoException, ServiceException {
        List<ParameterDto> dtos = convert(parameters.getContent());

        return new Page<>(dtos, parameters.getTotalPages());
    }

    public List<ParameterDto> convert(List<Parameter> parameters) throws ServiceException {
        List<ParameterDto> dtos = new ArrayList<>();

        for (Parameter parameter: parameters) {
            dtos.add(convert(parameter));
        }
        return dtos;
    }
}
