package com.zhuravel.market_servlet.service.dto.converter;

/**
 * @author Evgenii Zhuravel created on 18.06.2022
 * @version 1.0
 */
public class CurrencyFormatter {

    public static String format(Long value) {
        return String.format("$%s.00", value);
    }
}
