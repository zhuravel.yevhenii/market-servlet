package com.zhuravel.market_servlet.service.dto;

import com.zhuravel.market_servlet.model.entity.Producer;
import com.zhuravel.market_servlet.model.pageable.Page;

import java.util.Objects;
import java.util.Set;
import java.util.TreeSet;

/**
 * @author Evgenii Zhuravel created on 30.05.2022
 * @version 1.0
 */
public class ProductsCatalogDto {

    private Page<ProductDto> products;

    private Set<Producer> producers;

    public Page<ProductDto> getProducts() {
        return products;
    }

    public void setProducts(Page<ProductDto> products) {
        this.products = products;
    }

    public Set<Producer> getProducers() {
        return producers;
    }

    public void setProducers(Set<Producer> producers) {
        this.producers = new TreeSet<>(producers);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ProductsCatalogDto that = (ProductsCatalogDto) o;
        return products.equals(that.products) && producers.equals(that.producers);
    }

    @Override
    public int hashCode() {
        return Objects.hash(products, producers);
    }

    @Override
    public String toString() {
        return "ProductsCatalogDto{" +
                "products=" + products +
                ", producers=" + producers +
                '}';
    }
}
