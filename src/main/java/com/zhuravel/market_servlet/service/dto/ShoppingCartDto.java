package com.zhuravel.market_servlet.service.dto;

import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/**
 * @author Evgenii Zhuravel created on 04.06.2022
 * @version 1.0
 */
public class ShoppingCartDto {

    private Long userId;

    private ProductQuantitiesDto quantitiesDto = new ProductQuantitiesDto();

    private Map<ProductDto, Integer> products;

    private List<OrderDto> orders;

    public ShoppingCartDto() {
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Map<ProductDto, Integer> getProducts() {
        return products;
    }

    public void setProducts(Map<ProductDto, Integer> products) {
        this.products = new TreeMap<>(products);
    }

    public ProductQuantitiesDto getQuantitiesDto() {
        return quantitiesDto;
    }

    public void setQuantitiesDto(ProductQuantitiesDto quantitiesDto) {
        this.quantitiesDto = quantitiesDto;
    }

    public List<OrderDto> getOrders() {
        return orders;
    }

    public void setOrders(List<OrderDto> orders) {
        this.orders = orders;
    }
}
