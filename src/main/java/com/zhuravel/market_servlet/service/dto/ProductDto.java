package com.zhuravel.market_servlet.service.dto;

import com.zhuravel.market_servlet.model.entity.Producer;
import com.zhuravel.market_servlet.service.dto.converter.CurrencyFormatter;
import com.zhuravel.market_servlet.service.validation.Min;

import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * @author Evgenii Zhuravel created on 04.06.2022
 * @version 1.0
 */
public class ProductDto implements Comparable<ProductDto> {

    private Long id;

    @Min(value = "validation.name.length.min", defaultValue = 5, message = "message.field.size")
    private String name;

    @Min(value = "validation.name.length.min", defaultValue = 5, message = "message.field.size")
    private Map<String, String> localizedNames;

    private Map<String, String> localizedDescriptions;

    private Long categoryId;

    private Producer producer;

    private Map<Long, ParameterDto> parameters;

    private Long price;

    private String imagePath;

    private Long createTime;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Map<String, String> getLocalizedNames() {
        return localizedNames;
    }

    public void setLocalizedNames(Map<String, String> localizedNames) {
        this.localizedNames = localizedNames;
    }

    public Map<String, String> getLocalizedDescriptions() {
        return localizedDescriptions;
    }

    public void setLocalizedDescriptions(Map<String, String> localizedDescriptions) {
        this.localizedDescriptions = localizedDescriptions;
    }

    public Long getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Long categoryId) {
        this.categoryId = categoryId;
    }

    public Producer getProducer() {
        return producer;
    }

    public void setProducer(Producer producer) {
        this.producer = producer;
    }

    public Map<Long, ParameterDto> getParameters() {
        return parameters;
    }

    public String getParametersString() {
        return parameters.values().stream().map(ParameterDto::getValue).collect(Collectors.joining(", "));
    }

    public void setParameters(Map<Long, ParameterDto> parameters) {
        this.parameters = parameters;
    }

    public Long getPrice() {
        return price;
    }

    public String getPriceString() {
        return CurrencyFormatter.format(price);
    }

    public void setPrice(Long price) {
        this.price = price;
    }

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    public Long getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Long createTime) {
        this.createTime = createTime;
    }

    @Override
    public int compareTo(ProductDto o) {
        return this.localizedNames.get("en").compareTo(o.getLocalizedNames().get("en"));
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ProductDto that = (ProductDto) o;
        return id.equals(that.id) &&
                localizedNames.equals(that.localizedNames) &&
                Objects.equals(localizedDescriptions, that.localizedDescriptions) &&
                categoryId.equals(that.categoryId) &&
                producer.equals(that.producer) &&
                Objects.equals(parameters, that.parameters) &&
                Objects.equals(price, that.price) &&
                Objects.equals(imagePath, that.imagePath) &&
                createTime.equals(that.createTime);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, localizedNames, localizedDescriptions, categoryId, producer, parameters, price, imagePath, createTime);
    }

    @Override
    public String toString() {
        return "ProductDto{" +
                "id=" + id +
                ", name='" + localizedNames + '\'' +
                ", description='" + localizedDescriptions + '\'' +
                ", categoryId=" + categoryId +
                ", producer=" + producer +
                ", parameters=" + parameters +
                ", price='" + price + '\'' +
                ", imagePath='" + imagePath + '\'' +
                ", createTime='" + createTime + '\'' +
                '}';
    }

    public static Builder newBuilder() {
        return new ProductDto().new Builder();
    }

    public class Builder {

        public Builder() {
        }

        public ProductDto build() {
            return ProductDto.this;
        }

        public Builder setId(Long id) {
            ProductDto.this.id = id;
            return this;
        }

        public Builder setName(String name) {
            ProductDto.this.name = name;
            return this;
        }

        public Builder setNames(Map<String, String> name) {
            ProductDto.this.localizedNames = name;
            return this;
        }

        public Builder setDescriptions(Map<String, String> description) {
            ProductDto.this.localizedDescriptions = description;
            return this;
        }

        public Builder setCategoryId(Long categoryId) {
            ProductDto.this.categoryId = categoryId;
            return this;
        }

        public Builder setProducer(Producer producer) {
            ProductDto.this.producer = producer;
            return this;
        }

        public Builder setParameters(Map<Long, ParameterDto> parameters) {
            ProductDto.this.parameters = parameters;
            return this;
        }

        public Builder setPrice(Long price) {
            ProductDto.this.price = price;
            return this;
        }

        public Builder setImagePath(String imagePath) {
            ProductDto.this.imagePath = imagePath;
            return this;
        }

        public Builder setCreateTime(Long createTime) {
            ProductDto.this.createTime = createTime;
            return this;
        }
    }
}
