package com.zhuravel.market_servlet.service.dto;

import com.zhuravel.market_servlet.model.entity.BaseEntity;

/**
 * @author Evgenii Zhuravel created on 22.05.2022
 * @version 1.0
 */
public class LoginForm extends BaseEntity {

    private String login;

    private String password;

    public LoginForm() {
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String toString() {
        return "LoginForm{"
                + "id=" + id
                + ", login='" + login + '\''
                + ", password='" + password + '\''
                + '}';
    }
}
