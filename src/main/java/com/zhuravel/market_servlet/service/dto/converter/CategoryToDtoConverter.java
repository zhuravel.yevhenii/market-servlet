package com.zhuravel.market_servlet.service.dto.converter;

import com.zhuravel.market_servlet.model.entity.Category;
import com.zhuravel.market_servlet.model.entity.CategoryTranslation;
import com.zhuravel.market_servlet.model.pageable.Page;
import com.zhuravel.market_servlet.service.dto.CategoryDto;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author Evgenii Zhuravel created on 22.06.2022
 * @version 1.0
 */
public class CategoryToDtoConverter {

    public static CategoryDto convert(Category category) {
        return new CategoryDto(category.getId(),
                category.getName(),
                category.getParentCategoryId(),
                category.getImagePath(),
                convertTranslations(category.getTranslations()));
    }

    public static Page<CategoryDto> convert(Page<Category> categories) {
        List<CategoryDto> collect = categories.getContent().stream().map(CategoryToDtoConverter::convert).collect(Collectors.toList());
        return new Page<>(collect, categories.getTotalPages());
    }

    public static Category convert(CategoryDto categoryDto) {
        return new Category(categoryDto.getId(),
                categoryDto.getName(),
                categoryDto.getParentCategoryId(),
                categoryDto.getImagePath(),
                convertTranslations(categoryDto.getLocalizedNames(), categoryDto.getId()));
    }

    private static Map<String, String> convertTranslations(List<CategoryTranslation> translations) {
        return translations.stream()
                .collect(Collectors.toMap(CategoryTranslation::getLocale, CategoryTranslation::getLocalizedName));
    }

    private static List<CategoryTranslation> convertTranslations(Map<String, String> translations, Long categoryId) {
        return translations.entrySet().stream()
                .map(entry -> new CategoryTranslation(categoryId, entry.getKey(), entry.getValue()))
                .collect(Collectors.toList());
    }
}
