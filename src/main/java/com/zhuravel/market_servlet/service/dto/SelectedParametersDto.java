package com.zhuravel.market_servlet.service.dto;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Evgenii Zhuravel created on 31.05.2022
 * @version 1.0
 */
public class SelectedParametersDto {

    private boolean langUpdated;

    private ParametersType producers;

    private List<ParametersType> parameterTypes = new ArrayList<>();

    public boolean isLangUpdated() {
        return langUpdated;
    }

    public void setLangUpdated(boolean langUpdated) {
        this.langUpdated = langUpdated;
    }

    public ParametersType getProducers() {
        return producers;
    }

    public void setProducers(ParametersType producers) {
        this.producers = producers;
    }

    public List<ParametersType> getParameterTypes() {
        return parameterTypes;
    }

    public void setParameterTypes(List<ParametersType> parameterTypes) {
        this.parameterTypes = parameterTypes;
    }

    public void addParameterTypes(ParametersType parametersType) {
        this.parameterTypes.add(parametersType);
    }

    @Override
    public String toString() {
        return "SelectedParametersDto{}";
    }

    public static class ParametersType implements Comparable<ParametersType> {
        String name;
        List<SelectedParameter> parameters;

        public ParametersType() {
        }

        public ParametersType(String name, List<SelectedParameter> parameters) {
            this.name = name;
            this.parameters = new ArrayList<>(parameters);
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public List<SelectedParameter> getParameters() {
            return parameters;
        }

        public void setParameters(List<SelectedParameter> parameters) {
            this.parameters = parameters;
        }

        public void addParameters(SelectedParameter parameter) {
            this.parameters.add(parameter);
        }

        @Override
        public int compareTo(ParametersType o) {
            return this.getName().compareTo(o.getName());
        }
    }

    public static class SelectedParameter implements Comparable<SelectedParameter> {
        Long id;
        String name;
        Boolean selected = false;

        public SelectedParameter() {
        }

        public SelectedParameter(Long id, String name) {
            this.id = id;
            this.name = name;
        }

        public SelectedParameter(Long id, String name, Boolean selected) {
            this.id = id;
            this.name = name;
            this.selected = selected;
        }

        public Long getId() {
            return id;
        }

        public void setId(Long id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public Boolean isSelected() {
            return selected;
        }

        public Boolean getSelected() {
            return selected;
        }

        public void setSelected(Boolean selected) {
            this.selected = selected;
        }

        @Override
        public String toString() {
            return "SelectedParameter{" +
                    "id=" + id +
                    ", name='" + name + '\'' +
                    ", selected=" + selected +
                    '}';
        }

        @Override
        public int compareTo(SelectedParameter o) {
            return this.getName().compareTo(o.getName());
        }
    }
}
