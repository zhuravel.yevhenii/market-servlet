package com.zhuravel.market_servlet.service.dto;

import com.zhuravel.market_servlet.model.LocaleType;
import com.zhuravel.market_servlet.model.entity.Role;
import com.zhuravel.market_servlet.service.validation.ConfirmPassword;
import com.zhuravel.market_servlet.service.validation.Email;
import com.zhuravel.market_servlet.service.validation.Max;
import com.zhuravel.market_servlet.service.validation.Min;
import com.zhuravel.market_servlet.service.validation.Password;

import java.util.Optional;

/**
 * @author Evgenii Zhuravel created on 20.06.2022
 * @version 1.0
 */
public class AccountForm {

    private Long id;

    @Min(value = "validation.login.length.min", defaultValue = 5, message = "message.userForm.loginForm.size")
    @Max(value = "validation.login.length.max", defaultValue = 32, message = "message.userForm.loginForm.size")
    private String login;

    @Min(value = "validation.name.length.min", defaultValue = 5, message = "message.userForm.firstName.size")
    @Max(value = "validation.name.length.max", defaultValue = 32, message = "message.userForm.firstName.size")
    private String firstName;

    private String lastName;

    @Email(pattern = "validation.email.regexp", message = "message.userForm.email.incorrect")
    private String email;

    @Password
    @Min(value = "validation.password.length.min", defaultValue = 8, message = "message.userForm.password.size")
    private String password;

    @ConfirmPassword(message = "message.passwords_do_not_match")
    private String confirmPassword;

    private LocaleType locale;

    private Role role;

    public AccountForm() {
    }

    public AccountForm(Long id, String login, String firstName, String lastName, String email, LocaleType locale, Role role) {
        this.id = id;
        this.login = login;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.locale = locale;
        this.role = role;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Optional<String> getPassword() {
        return Optional.ofNullable(password);
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getConfirmPassword() {
        return confirmPassword;
    }

    public void setConfirmPassword(String confirmPassword) {
        this.confirmPassword = confirmPassword;
    }

    public LocaleType getLocale() {
        return locale;
    }

    public void setLocale(LocaleType locale) {
        this.locale = locale;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }
}
