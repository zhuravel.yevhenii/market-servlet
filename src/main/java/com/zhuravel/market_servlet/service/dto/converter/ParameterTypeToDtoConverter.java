package com.zhuravel.market_servlet.service.dto.converter;

import com.zhuravel.market_servlet.model.entity.ParameterType;
import com.zhuravel.market_servlet.model.entity.ParameterTypeTranslation;
import com.zhuravel.market_servlet.model.pageable.Page;
import com.zhuravel.market_servlet.service.dto.ParameterTypeDto;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author Evgenii Zhuravel created on 22.06.2022
 * @version 1.0
 */
public class ParameterTypeToDtoConverter {

    public static ParameterTypeDto convert(ParameterType parameterType) {
        return new ParameterTypeDto(parameterType.getId(),
                parameterType.getCategoryId(),
                parameterType.getName(),
                convertTranslations(parameterType.getTranslations()));
    }

    public static Page<ParameterTypeDto> convert(Page<ParameterType> categories) {
        List<ParameterTypeDto> collect = categories.getContent().stream().map(ParameterTypeToDtoConverter::convert).collect(Collectors.toList());
        return new Page<>(collect, categories.getTotalPages());
    }

    public static List<ParameterTypeDto> convert(List<ParameterType> categories) {
        return categories.stream().map(ParameterTypeToDtoConverter::convert).collect(Collectors.toList());
    }

    public static ParameterType convert(ParameterTypeDto ParameterTypeDto) {
        return new ParameterType(ParameterTypeDto.getId(),
                ParameterTypeDto.getCategoryId(),
                ParameterTypeDto.getName(),
                convertTranslations(ParameterTypeDto.getTranslations(), ParameterTypeDto.getId()));
    }

    private static Map<String, String> convertTranslations(List<ParameterTypeTranslation> translations) {
        return translations.stream()
                .collect(Collectors.toMap(ParameterTypeTranslation::getLocale, ParameterTypeTranslation::getLocalizedName));
    }

    private static List<ParameterTypeTranslation> convertTranslations(Map<String, String> translations, Long categoryId) {
        return translations.entrySet().stream()
                .map(entry -> new ParameterTypeTranslation(categoryId, entry.getKey(), entry.getValue()))
                .collect(Collectors.toList());
    }
}
