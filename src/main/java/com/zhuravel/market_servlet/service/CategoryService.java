package com.zhuravel.market_servlet.service;

import com.zhuravel.market_servlet.model.pageable.Page;
import com.zhuravel.market_servlet.model.pageable.Pageable;
import com.zhuravel.market_servlet.service.dto.CategoryDto;
import com.zhuravel.market_servlet.service.exception.ServiceException;

/**
 * @author Evgenii Zhuravel created on 19.05.2022
 * @version 1.0
 */
public interface CategoryService extends BaseService<CategoryDto, Long> {

    Page<CategoryDto> getSubCategories(Long id, Pageable pageable) throws ServiceException;

    Page<CategoryDto> getSubCategories(Long id, String filterEn, String filterUk, Pageable pageable) throws ServiceException;
}
