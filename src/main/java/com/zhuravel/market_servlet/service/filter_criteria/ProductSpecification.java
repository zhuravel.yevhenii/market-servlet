package com.zhuravel.market_servlet.service.filter_criteria;

import com.zhuravel.market_servlet.dao.mapper.ProductMapper;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Evgenii Zhuravel created on 01.06.2022
 * @version 1.0
 */
public class ProductSpecification {

    public static String generateQuery(ParameterSearchCriteria criteria, String locale) {
        List<Predicate> predicates = new ArrayList<>();

        predicates.add(createCategoryPredicate(criteria.getCategoryId()));

        ParameterCounter parameterCounter = new ParameterCounter();

        criteria.getProducers().ifPresent(producers -> {
            Predicate predicate = createProducerPredicate(producers);
            if (predicate != null) {
                predicates.add(predicate);
            }
        });

        for (ParameterSearchCriteria.ParametersType parametersType : criteria.getParameterTypes()) {
            if (parametersType.getParameters().size() > 0) {
                Predicate predicate = createParameterPredicate(parametersType, parameterCounter);
                if (predicate != null) {
                    predicates.add(predicate);
                }
            }
        }

        Predicate predicate = CriteriaBuilder.and(predicates);
        ProductMapper mapper = new ProductMapper();
        String translation = ProductMapper.TABLE_TRANSLATION_NAME;

        return "SELECT DISTINCT " + mapper.getSelectedFields("product0_") +
                " FROM " + ProductMapper.TABLE_NAME +
                " product0_ " + predicate.joinQuery +
                " JOIN " + translation +
                " ON " + " product0_ " + "." + ProductMapper.FIELD_ID + "=" + translation + "." + ProductMapper.FIELD_ID +
                " WHERE " + predicate.whereQuery +
                " AND " + ProductMapper.TABLE_TRANSLATION_NAME + "." + ProductMapper.FIELD_TRANSLATION_LOCALE + "='" + locale + "'";
    }

    record Predicate(String joinQuery, String whereQuery) {}

    private static Predicate createParameterPredicate(ParameterSearchCriteria.ParametersType parametersType, ParameterCounter parameterCounter) {
        List<Predicate> parameterPredicates = new ArrayList<>();
        for (ParameterSearchCriteria.SelectedParameter parameter : parametersType.getParameters()) {
            parameterPredicates.add(generateParameterPredicate(parameter.getId(), parameterCounter));
        }
        if (parameterPredicates.size() > 0) {
            return CriteriaBuilder.or(parameterPredicates);
        }
        return null;
    }

    private static Predicate generateParameterPredicate(Long parameterId, ParameterCounter parameterCounter) {
        String parameter1 = "parameter_" + parameterCounter.getCount();
        String parameter2 = "parameter_" + parameterCounter.getCount();

        String where = parameter2 + ".id=" + parameterId;

        return new Predicate(generateJoinQuery(parameter1, parameter2), where);
    }

    private static String generateJoinQuery(String parameter1, String parameter2) {
        return " INNER JOIN product_product_parameter " + parameter1 +
                " ON product0_.id=" + parameter1 + ".product_id" +
                " INNER JOIN product_parameter " + parameter2 +
                " ON " + parameter1 + ".parameter_id=" + parameter2 + ".id";
    }

    private static Predicate createProducerPredicate(ParameterSearchCriteria.ParametersType producers) {
        List<Predicate> producerPredicates = new ArrayList<>();
        for (ParameterSearchCriteria.SelectedParameter producer : producers.getParameters()) {
            producerPredicates.add(generateProducerPredicate(producer.getId()));
        }
        if (producerPredicates.size() > 0) {
            return CriteriaBuilder.or(producerPredicates);
        }
        return null;
    }

    private static Predicate generateProducerPredicate(Long producerId) {
        String where = "product0_.producer_id=" + producerId;

        return new Predicate("", where);
    }

    private static Predicate createCategoryPredicate(Long categoryId) {
        String where = "product0_.category_id=" + categoryId;

        return new Predicate("", where);
    }

    private static class ParameterCounter {
        private int count = 1;

        public int getCount() {
            return count++;
        }
    }

    private static class CriteriaBuilder {

        public static Predicate or(List<Predicate> predicates) {
            return new Predicate(
                    predicates.stream()
                            .map(predicate -> predicate.joinQuery)
                            .collect(Collectors.joining(" ")),
                    predicates.stream()
                            .map(predicate -> predicate.whereQuery)
                            .collect(Collectors.joining(" OR ", "(", ")")));
        }

        public static Predicate and(List<Predicate> predicates) {
            return new Predicate(
                    predicates.stream()
                            .map(predicate -> predicate.joinQuery)
                            .collect(Collectors.joining(" ")),
                    predicates.stream()
                            .map(predicate -> predicate.whereQuery)
                            .collect(Collectors.joining(" AND ")));
        }
    }
}
