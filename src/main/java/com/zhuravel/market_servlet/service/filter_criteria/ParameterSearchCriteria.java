package com.zhuravel.market_servlet.service.filter_criteria;

import java.util.List;
import java.util.Optional;

/**
 * @author Evgenii Zhuravel created on 31.05.2022
 * @version 1.0
 */
public class ParameterSearchCriteria {

    private Long categoryId;

    private ParametersType producers;

    private List<ParametersType> parameterTypes;

    public ParameterSearchCriteria() {
    }

    public Long getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Long categoryId) {
        this.categoryId = categoryId;
    }

    public Optional<ParametersType> getProducers() {
        return Optional.ofNullable(producers);
    }

    public void setProducers(ParametersType producers) {
        this.producers = producers;
    }

    public List<ParametersType> getParameterTypes() {
        return parameterTypes;
    }

    public void setParameterTypes(List<ParametersType> parameterTypes) {
        this.parameterTypes = parameterTypes;
    }

    public void addParameterTypes(ParametersType parametersType) {
        this.parameterTypes.add(parametersType);
    }

    @Override
    public String toString() {
        return "SelectedParametersDto{}";
    }

    public static class ParametersType {
        String name;
        List<SelectedParameter> parameters;

        public ParametersType() {
        }

        public ParametersType(String name, List<SelectedParameter> parameters) {
            this.name = name;
            this.parameters = parameters;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public List<SelectedParameter> getParameters() {
            return parameters;
        }

        public void setParameters(List<SelectedParameter> parameters) {
            this.parameters = parameters;
        }

        public void addParameters(SelectedParameter parameter) {
            this.parameters.add(parameter);
        }
    }

    public static class SelectedParameter {
        Long id;
        String name;
        Boolean selected = false;

        public SelectedParameter() {
        }

        public SelectedParameter(Long id, String name) {
            this.id = id;
            this.name = name;
        }

        public SelectedParameter(Long id, String name, Boolean selected) {
            this.id = id;
            this.name = name;
            this.selected = selected;
        }

        public Long getId() {
            return id;
        }

        public void setId(Long id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public Boolean isSelected() {
            return selected;
        }

        public Boolean getSelected() {
            return selected;
        }

        public void setSelected(Boolean selected) {
            this.selected = selected;
        }

        @Override
        public String toString() {
            return "SelectedParameter{" +
                    "id=" + id +
                    ", name='" + name + '\'' +
                    ", selected=" + selected +
                    '}';
        }
    }
}
