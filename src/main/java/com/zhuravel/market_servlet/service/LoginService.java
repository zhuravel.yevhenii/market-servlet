package com.zhuravel.market_servlet.service;

import com.zhuravel.market_servlet.service.dto.LoginForm;
import com.zhuravel.market_servlet.service.dto.UserDto;
import com.zhuravel.market_servlet.service.exception.ServiceException;

import java.util.Optional;

/**
 * @author Evgenii Zhuravel created on 19.05.2022
 * @version 1.0
 */
public interface LoginService {

    Optional<UserDto> authenticate(LoginForm loginForm) throws ServiceException;
}
