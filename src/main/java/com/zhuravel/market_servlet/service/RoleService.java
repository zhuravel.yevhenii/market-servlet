package com.zhuravel.market_servlet.service;

import com.zhuravel.market_servlet.model.entity.Role;

/**
 * @author Evgenii Zhuravel created on 28.06.2022
 * @version 1.0
 */
public interface RoleService extends BaseService<Role, Long> {

}
