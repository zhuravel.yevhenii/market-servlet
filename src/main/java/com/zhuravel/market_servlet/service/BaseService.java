package com.zhuravel.market_servlet.service;

import com.zhuravel.market_servlet.model.pageable.Page;
import com.zhuravel.market_servlet.model.pageable.Pageable;
import com.zhuravel.market_servlet.service.exception.ServiceException;

/**
 * @author Evgenii Zhuravel created on 19.05.2022
 * @version 1.0
 */
public interface BaseService<E, I> {

    E save(E e) throws ServiceException;

    void update(E e) throws ServiceException;

    void delete(I id) throws ServiceException;

    E getById(I id) throws ServiceException;

    Page<E> getAll(Pageable page) throws ServiceException;
}
