package com.zhuravel.market_servlet.service;

import com.zhuravel.market_servlet.model.pageable.Page;
import com.zhuravel.market_servlet.model.pageable.Pageable;
import com.zhuravel.market_servlet.service.dto.ProductDto;
import com.zhuravel.market_servlet.service.dto.ProductsCatalogDto;
import com.zhuravel.market_servlet.service.exception.ServiceException;
import com.zhuravel.market_servlet.service.filter_criteria.ParameterSearchCriteria;

/**
 * @author Evgenii Zhuravel created on 19.05.2022
 * @version 1.0
 */
public interface ProductService extends BaseService<ProductDto, Long> {

    Page<ProductDto> getAllByProducerName(String name, Pageable pageable) throws ServiceException;

    ProductsCatalogDto getAll(ParameterSearchCriteria criteria, String sort, Pageable pageable) throws ServiceException;
}
