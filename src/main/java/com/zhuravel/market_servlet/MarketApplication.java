package com.zhuravel.market_servlet;

import com.zhuravel.market_servlet.config.MessageResourceReceiver;

public class MarketApplication {

    public static void main(String[] args) {
        String producersName = MessageResourceReceiver.getString("label.producer");
        System.out.println(producersName);
    }
}
