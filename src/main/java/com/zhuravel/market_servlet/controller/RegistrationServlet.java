package com.zhuravel.market_servlet.controller;

import com.zhuravel.market_servlet.config.LocaleHolder;
import com.zhuravel.market_servlet.config.MessageResourceReceiver;
import com.zhuravel.market_servlet.model.LocaleConvertor;
import com.zhuravel.market_servlet.model.RoleType;
import com.zhuravel.market_servlet.model.entity.Role;
import com.zhuravel.market_servlet.service.RegistrationService;
import com.zhuravel.market_servlet.service.dto.AccountForm;
import com.zhuravel.market_servlet.service.dto.UserDto;
import com.zhuravel.market_servlet.service.exception.ServiceException;
import com.zhuravel.market_servlet.service.impl.RegistrationServiceImpl;
import com.zhuravel.market_servlet.service.utils.PasswordEncoder;
import com.zhuravel.market_servlet.service.validation.Validator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.util.Map;
import java.util.TreeMap;

import static com.zhuravel.market_servlet.config.Constants.Mapping.MAPPING_REGISTRATION;
import static com.zhuravel.market_servlet.config.Constants.SessionAttributes.ATTRIBUTE_USER;

/**
 * @author Evgenii Zhuravel created on 22.05.2022
 * @version 1.0
 */
@WebServlet(name = "Registration", urlPatterns = MAPPING_REGISTRATION)
public class RegistrationServlet extends BaseServlet {

    private static final Logger logger = LoggerFactory.getLogger(RegistrationServlet.class);

    private static final String PAGE_REGISTRATION = "/WEB-INF/templates/registration.jsp";

    private static final String ATTRIBUTE_ACCOUNT_FORM = "accountForm";
    private static final String ATTRIBUTE_ERRORS = "errors";

    private RegistrationService registrationService;

    @Override
    public void init() {
        this.registrationService = new RegistrationServiceImpl();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        logger.debug("Method Get for " + MAPPING_REGISTRATION);

        req.getRequestDispatcher(PAGE_REGISTRATION).forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        logger.debug("Method Post for " + MAPPING_REGISTRATION);
        Map<String, String> errors = new TreeMap<>();

        AccountForm accountForm = new AccountForm();
        accountForm.setLogin(req.getParameter("login"));
        accountForm.setFirstName(req.getParameter("firstName"));
        accountForm.setLastName(req.getParameter("lastName"));
        accountForm.setEmail(req.getParameter("email"));

        try {
            accountForm.setConfirmPassword(PasswordEncoder.encode(req.getParameter("password")));
            accountForm.setPassword(PasswordEncoder.encode(req.getParameter("confirmPassword")));
        } catch (NoSuchAlgorithmException e) {
            logger.error(e.getLocalizedMessage());

            errors.put("password", e.getLocalizedMessage());
            req.setAttribute(ATTRIBUTE_ERRORS, errors);

            req.getRequestDispatcher(PAGE_REGISTRATION).forward(req, resp);
        }

        accountForm.setRole(new Role(RoleType.ROLE_USER));
        accountForm.setLocale(LocaleConvertor.getLocaleType(LocaleHolder.INSTANCE.getLocale()));

        errors.putAll(Validator.validate(accountForm));

        if (!errors.isEmpty()) {
            req.setAttribute(ATTRIBUTE_ACCOUNT_FORM, accountForm);
            req.setAttribute(ATTRIBUTE_ERRORS, errors);

            req.getRequestDispatcher(PAGE_REGISTRATION).forward(req, resp);
        } else {
            try {
                UserDto user = registrationService.save(accountForm);

                req.getSession().setAttribute(ATTRIBUTE_USER, user);

                showSuccessMessage(req, resp, MessageResourceReceiver.getString("message.register.success"));
            } catch (ServiceException e) {
                logger.error(e.getMessage());
                showErrorMessage(req, resp, MessageResourceReceiver.getString("message.register.error"));
            }
        }
    }
}
