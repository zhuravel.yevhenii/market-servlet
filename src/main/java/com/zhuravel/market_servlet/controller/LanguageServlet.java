package com.zhuravel.market_servlet.controller;

import com.zhuravel.market_servlet.config.LocaleHolder;
import com.zhuravel.market_servlet.config.MessageResourceReceiver;
import com.zhuravel.market_servlet.model.LocaleType;
import com.zhuravel.market_servlet.service.UserService;
import com.zhuravel.market_servlet.service.dto.SelectedParametersDto;
import com.zhuravel.market_servlet.service.dto.UserDto;
import com.zhuravel.market_servlet.service.exception.ServiceException;
import com.zhuravel.market_servlet.service.impl.UserServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static com.zhuravel.market_servlet.config.Constants.Attributes.FILTER_PARAMETERS;
import static com.zhuravel.market_servlet.config.Constants.Parameters.PARAMETER_LANGUAGE;
import static com.zhuravel.market_servlet.config.Constants.SessionAttributes.ATTRIBUTE_USER;

/**
 * @author Evgenii Zhuravel created on 21.06.2022
 * @version 1.0
 */
@WebServlet(urlPatterns = {"/language"})
public class LanguageServlet extends BaseServlet {
    private static final Logger logger = LoggerFactory.getLogger(LanguageServlet.class);

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
        try {
            String returnUrl = req.getHeader("referer");

            String language = LocaleHolder.INSTANCE.receiveStringLocale(req);

            if (req.getSession().getAttribute(ATTRIBUTE_USER) != null) {
                UserDto user = (UserDto) req.getSession().getAttribute(ATTRIBUTE_USER);
                user.setLocale(LocaleType.valueOf(language));

                UserService userService = new UserServiceImpl();
                userService.update(user);
            }

            SelectedParametersDto selectedParameters = (SelectedParametersDto) req.getSession().getAttribute(FILTER_PARAMETERS);
            if (selectedParameters != null) {
                selectedParameters.setLangUpdated(true);
            }

            req.getSession().setAttribute(PARAMETER_LANGUAGE, language);

            resp.sendRedirect(returnUrl);
        } catch (ServiceException e) {
            logger.error(e.getMessage());
            showErrorMessage(req, resp, MessageResourceReceiver.getString("message.error.change.language"));
        }
    }
}
