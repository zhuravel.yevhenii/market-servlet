package com.zhuravel.market_servlet.controller;

import com.zhuravel.market_servlet.config.LocaleHolder;
import com.zhuravel.market_servlet.config.security.LoginUtil;
import com.zhuravel.market_servlet.model.RoleType;
import com.zhuravel.market_servlet.service.LoginService;
import com.zhuravel.market_servlet.service.ShoppingCartService;
import com.zhuravel.market_servlet.service.dto.LoginForm;
import com.zhuravel.market_servlet.service.dto.ShoppingCartDto;
import com.zhuravel.market_servlet.service.dto.UserDto;
import com.zhuravel.market_servlet.service.exception.ServiceException;
import com.zhuravel.market_servlet.service.impl.LoginServiceImpl;
import com.zhuravel.market_servlet.service.impl.ShoppingCartServiceImpl;
import com.zhuravel.market_servlet.service.utils.PasswordEncoder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.TreeMap;

import static com.zhuravel.market_servlet.config.Constants.Attributes.ATTRIBUTE_SELECTED_PRODUCTS;
import static com.zhuravel.market_servlet.config.Constants.Mapping.MAPPING_CATALOG;
import static com.zhuravel.market_servlet.config.Constants.Mapping.MAPPING_LOGIN;
import static com.zhuravel.market_servlet.config.Constants.Mapping.MAPPING_REGISTRATION;
import static com.zhuravel.market_servlet.config.Constants.Parameters.PARAMETER_LANGUAGE;

/**
 * @author Evgenii Zhuravel created on 22.05.2022
 * @version 1.0
 */
@WebServlet(name = "LoginForm", urlPatterns = MAPPING_LOGIN)
public class LoginServlet extends HttpServlet {

    private static final Logger logger = LoggerFactory.getLogger(LoginServlet.class);

    private static final String PAGE_LOGIN = "/WEB-INF/templates/login.jsp";

    private static final String ATTRIBUTE_ERRORS = "errors";

    private ShoppingCartService shoppingCartService;
    private LoginService loginService;

    @Override
    public void init() {
        this.loginService = new LoginServiceImpl();
        this.shoppingCartService = new ShoppingCartServiceImpl();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        logger.debug("Method Get for " + MAPPING_REGISTRATION);

        req.getRequestDispatcher(PAGE_LOGIN).forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        logger.debug("Method Post for " + MAPPING_REGISTRATION);

        LoginForm loginForm = new LoginForm();

        loginForm.setLogin(req.getParameter("login"));
        try {
            loginForm.setPassword(PasswordEncoder.encode(req.getParameter("password")));

            Optional<UserDto> user = loginService.authenticate(loginForm);
            if (user.isPresent()) {
                if (user.get().getPassword().equals(loginForm.getPassword())) {
                    LoginUtil.storeLoginedUser(req.getSession(), user.get());

                    String language = user.get().getLocale().name();

                    LocaleHolder.INSTANCE.setLocale(language);
                    req.getSession().setAttribute(PARAMETER_LANGUAGE, language);

                    if (user.get().getRole().getName().equals(RoleType.ROLE_USER.name())) {
                        ShoppingCartDto products = shoppingCartService.getByUserId(user.get().getId());
                        req.getSession().setAttribute(ATTRIBUTE_SELECTED_PRODUCTS, products.getQuantitiesDto());
                    }

                    int redirectId = -1;
                    try {
                        redirectId = Integer.parseInt(req.getParameter("redirectId"));
                    } catch (Exception ignored){}

                    String requestUri = LoginUtil.getRedirectUrlAfterLogin(redirectId);
                    resp.sendRedirect(Objects.requireNonNullElseGet(requestUri, () -> req.getContextPath() + MAPPING_CATALOG));
                } else {
                    String message = "Password or login is wrong";
                    logger.debug(message);

                    sendErrorMessage(req, resp, "password", message);
                }
            } else {
                String message = "User with login " + loginForm.getLogin() + " not found";
                logger.debug(message);

                sendErrorMessage(req, resp, "login", message);
            }
        } catch (NoSuchAlgorithmException | ServiceException e) {
            logger.error(e.getLocalizedMessage());
            sendErrorMessage(req, resp, "password", e.getLocalizedMessage());
        }
    }

    private void sendErrorMessage(HttpServletRequest req, HttpServletResponse resp, String fieldId, String message) throws ServletException, IOException {
        req.setAttribute("login", req.getParameter("login"));

        Map<String, String> errors = new TreeMap<>();
        errors.put(fieldId, message);
        req.setAttribute(ATTRIBUTE_ERRORS, errors);

        req.getRequestDispatcher(PAGE_LOGIN).forward(req, resp);
    }
}
