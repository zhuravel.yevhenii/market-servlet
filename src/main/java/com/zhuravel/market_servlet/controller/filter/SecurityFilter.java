package com.zhuravel.market_servlet.controller.filter;

import com.zhuravel.market_servlet.config.MessageResourceReceiver;
import com.zhuravel.market_servlet.config.security.LoginUtil;
import com.zhuravel.market_servlet.config.security.SecurityUtils;
import com.zhuravel.market_servlet.config.security.UserRoleRequestWrapper;
import com.zhuravel.market_servlet.controller.BaseServlet;
import com.zhuravel.market_servlet.service.dto.UserDto;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static com.zhuravel.market_servlet.config.Constants.Mapping.MAPPING_LOGIN;

/**
 * @author Evgenii Zhuravel created on 03.07.2022
 * @version 1.0
 */
@WebFilter("/*")
public class SecurityFilter extends BaseServlet implements Filter {

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest req = (HttpServletRequest) servletRequest;
        HttpServletResponse resp = (HttpServletResponse) servletResponse;

        String servletPath = req.getServletPath();

        if (servletPath.equals(MAPPING_LOGIN)) {
            filterChain.doFilter(req, resp);
            return;
        }

        UserDto user = LoginUtil.getLoginedUser(req.getSession());

        HttpServletRequest wrapRequest = req;

        if (user != null) {
            String login = user.getLogin();
            String roleName = user.getRole().getName();

            wrapRequest = new UserRoleRequestWrapper(login, roleName, req);
        }

        if (SecurityUtils.isSecurityPage(req)) {
            if (user == null) {
                String requestUri = req.getRequestURI();
                String queryString = req.getQueryString();
                if (queryString != null) {
                    requestUri = requestUri + "?" + queryString;
                }

                int redirectId = LoginUtil.storeRedirectUrlAfterLogin(requestUri);

                resp.sendRedirect(wrapRequest.getContextPath() + MAPPING_LOGIN + "?redirectId=" + redirectId);
                return;
            }

            boolean hasPermission = SecurityUtils.hasPermission(wrapRequest);

            if (!hasPermission) {
                showErrorMessage(req, resp, MessageResourceReceiver.getString("message.error.access_denied"));
                return;
            }
        }

        filterChain.doFilter(wrapRequest, resp);
    }

    @Override
    public void init(FilterConfig filterConfig) {

    }

    @Override
    public void destroy() {

    }
}
