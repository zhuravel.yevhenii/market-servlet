package com.zhuravel.market_servlet.controller;

import com.zhuravel.market_servlet.model.pageable.Pageable;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * @author Evgenii Zhuravel created on 25.06.2022
 * @version 1.0
 */
public interface PageableServlet {

    String PARAMETER_CURRENT_PAGE = "currentPage";
    String PARAMETER_PAGE_SIZE = "pageSize";

    String DEFAULT_PAGE = "1";
    String DEFAULT_SIZE = "6";

    String ATTRIBUTE_PAGE_NUMBERS = "pageNumbers";

    default Pageable getRequestPage(HttpServletRequest request) {
        Map<String, String[]> parameterMap = request.getParameterMap();

        String pageParameter = request.getParameter(PARAMETER_CURRENT_PAGE);
        String sizeParameter = request.getParameter(PARAMETER_PAGE_SIZE);

        if (pageParameter == null || parameterMap.isEmpty()) {
            pageParameter = DEFAULT_PAGE;
        }
        if (sizeParameter == null || sizeParameter.isEmpty()) {
            sizeParameter = DEFAULT_SIZE;
        }
        return new Pageable(Integer.parseInt(pageParameter), Integer.parseInt(sizeParameter));
    }

    default void setRequestPage(HttpServletRequest request, Pageable page, int totalPages) {
        List<Integer> pageNumbers = new ArrayList<>();

        if (totalPages > 0) {
            pageNumbers = IntStream.rangeClosed(1, totalPages)
                    .boxed()
                    .collect(Collectors.toList());
        }

        request.setAttribute(PARAMETER_CURRENT_PAGE, page.getNumber()+1);
        request.setAttribute(ATTRIBUTE_PAGE_NUMBERS, pageNumbers);
        request.setAttribute(PARAMETER_PAGE_SIZE, page.getSize());
    }
}
