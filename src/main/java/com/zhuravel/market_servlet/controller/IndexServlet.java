package com.zhuravel.market_servlet.controller;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static com.zhuravel.market_servlet.config.Constants.Mapping.MAPPING_CATALOG;

/**
 * @author Evgenii Zhuravel created on 19.06.2022
 * @version 1.0
 */
public class IndexServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getRequestDispatcher(MAPPING_CATALOG).forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getRequestDispatcher(MAPPING_CATALOG).forward(req, resp);
    }
}
