package com.zhuravel.market_servlet.controller;

import com.zhuravel.market_servlet.config.MessageResourceReceiver;
import com.zhuravel.market_servlet.service.ProductService;
import com.zhuravel.market_servlet.service.dto.ProductDto;
import com.zhuravel.market_servlet.service.exception.ServiceException;
import com.zhuravel.market_servlet.service.impl.ProductServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static com.zhuravel.market_servlet.config.Constants.Mapping.MAPPING_PRODUCT_PAGE;

/**
 * @author Evgenii Zhuravel created on 22.06.2022
 * @version 1.0
 */
@WebServlet(urlPatterns = MAPPING_PRODUCT_PAGE)
public class ProductPageServlet extends BaseServlet {
    private static final Logger logger = LoggerFactory.getLogger(ProductPageServlet.class);

    private static final String PAGE_PRODUCT_PAGE = "/WEB-INF/templates/product-page.jsp";

    public static final String ATTRIBUTE_PRODUCT = "product";

    private ProductService productService;

    @Override
    public void init() {
        productService = new ProductServiceImpl();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        logger.debug("Get product cart");

        if (req.getParameter("productId") != null) {
            try {
                Long productId = Long.parseLong(req.getParameter("productId"));

                ProductDto product = productService.getById(productId);

                req.setAttribute(ATTRIBUTE_PRODUCT, product);

                req.getRequestDispatcher(PAGE_PRODUCT_PAGE).forward(req, resp);
            } catch (NumberFormatException | ServiceException e) {
                showErrorMessage(req, resp, MessageResourceReceiver.getString("message.error.retrieving_product_page"));
            }
        } else {
            showErrorMessage(req, resp, MessageResourceReceiver.getString("message.error.retrieving_product_page"));
        }
    }
}
