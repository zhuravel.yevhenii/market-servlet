package com.zhuravel.market_servlet.controller;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import java.io.IOException;

import static com.zhuravel.market_servlet.config.Constants.Mapping.MAPPING_CATALOG;
import static com.zhuravel.market_servlet.config.Constants.Mapping.MAPPING_LOGIN;
import static com.zhuravel.market_servlet.config.Constants.Mapping.MAPPING_LOGOUT;
import static com.zhuravel.market_servlet.config.Constants.Mapping.MAPPING_SHOPPING_CART;

/**
 * @author Evgenii Zhuravel created on 21.06.2022
 * @version 1.0
 */
@WebServlet(name="Logout", urlPatterns = MAPPING_LOGOUT)
public class LogoutServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        HttpSession session = req.getSession(false);

        if (session != null) {
            String returnUrl = getReturnUrl(req);

            session.invalidate();

            resp.sendRedirect(req.getContextPath() + returnUrl);
        }
    }

    private String getReturnUrl(HttpServletRequest req) {
        String returnUrl = req.getHeader("referer");

        if (returnUrl == null ||
                returnUrl.equals(MAPPING_LOGIN) ||
                returnUrl.equals(MAPPING_SHOPPING_CART)) {
            returnUrl = MAPPING_CATALOG;
        }
        return returnUrl;
    }
}
