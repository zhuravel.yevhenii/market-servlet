package com.zhuravel.market_servlet.controller.seller;

import com.zhuravel.market_servlet.config.MessageResourceReceiver;
import com.zhuravel.market_servlet.config.security.Authenticated;
import com.zhuravel.market_servlet.controller.BaseServlet;
import com.zhuravel.market_servlet.model.RoleType;
import com.zhuravel.market_servlet.service.ProductService;
import com.zhuravel.market_servlet.service.exception.ServiceException;
import com.zhuravel.market_servlet.service.impl.ProductServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static com.zhuravel.market_servlet.config.Constants.Mapping.MAPPING_CATALOG;
import static com.zhuravel.market_servlet.config.Constants.Mapping.MAPPING_CATALOG_DELETE;

/**
 * @author Evgenii Zhuravel created on 29.06.2022
 * @version 1.0
 */
@Authenticated(role = RoleType.ROLE_SELLER)
@WebServlet(urlPatterns = MAPPING_CATALOG_DELETE)
public class DeleteProductServlet extends BaseServlet {
    private static final Logger logger = LoggerFactory.getLogger(DeleteProductServlet.class);

    private ProductService productService;

    @Override
    public void init() {
        this.productService = new ProductServiceImpl();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String id = req.getParameter("id");
        String product = req.getParameter("productId");
        try {
            long categoryId = Long.parseLong(id);

            if (product != null) {
                long productId = Long.parseLong(product);
                productService.delete(productId);
            }

            resp.sendRedirect(req.getContextPath() + MAPPING_CATALOG + "?id=" + categoryId);
        } catch (NumberFormatException | ServiceException e) {
            logger.error(e.getMessage());
            showErrorMessage(req, resp, MessageResourceReceiver.getString("message.error.retrieve_edit_product_page"));
        }
    }
}
