package com.zhuravel.market_servlet.controller.seller;

import com.zhuravel.market_servlet.config.MessageResourceReceiver;
import com.zhuravel.market_servlet.config.security.Authenticated;
import com.zhuravel.market_servlet.controller.BaseServlet;
import com.zhuravel.market_servlet.model.RoleType;
import com.zhuravel.market_servlet.model.entity.LocalizedParameterType;
import com.zhuravel.market_servlet.model.entity.Producer;
import com.zhuravel.market_servlet.model.pageable.Page;
import com.zhuravel.market_servlet.model.pageable.Pageable;
import com.zhuravel.market_servlet.service.LocalizedCategoryService;
import com.zhuravel.market_servlet.service.LocalizedParameterTypeService;
import com.zhuravel.market_servlet.service.ProducerService;
import com.zhuravel.market_servlet.service.ProductService;
import com.zhuravel.market_servlet.service.dto.LocalizedCategoryDto;
import com.zhuravel.market_servlet.service.dto.ParameterDto;
import com.zhuravel.market_servlet.service.dto.ProductDto;
import com.zhuravel.market_servlet.service.dto.SelectedParametersDto;
import com.zhuravel.market_servlet.service.dto.builder.SelectedParametersDtoBuilder;
import com.zhuravel.market_servlet.service.exception.ServiceException;
import com.zhuravel.market_servlet.service.impl.LocalizedCategoryServiceImpl;
import com.zhuravel.market_servlet.service.impl.LocalizedParameterTypeServiceImpl;
import com.zhuravel.market_servlet.service.impl.ProducerServiceImpl;
import com.zhuravel.market_servlet.service.impl.ProductServiceImpl;
import com.zhuravel.market_servlet.service.validation.Validator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

import static com.zhuravel.market_servlet.config.Constants.Attributes.ATTRIBUTE_CATEGORY;
import static com.zhuravel.market_servlet.config.Constants.Attributes.ATTRIBUTE_CATEGORY_ID;
import static com.zhuravel.market_servlet.config.Constants.Attributes.ATTRIBUTE_PARAMETER_TYPES;
import static com.zhuravel.market_servlet.config.Constants.Attributes.ATTRIBUTE_PRODUCERS;
import static com.zhuravel.market_servlet.config.Constants.Attributes.ATTRIBUTE_PRODUCT;
import static com.zhuravel.market_servlet.config.Constants.Attributes.ATTRIBUTE_PRODUCT_ID;
import static com.zhuravel.market_servlet.config.Constants.Attributes.FILTER_PARAMETERS;
import static com.zhuravel.market_servlet.config.Constants.Mapping.MAPPING_CATALOG;
import static com.zhuravel.market_servlet.config.Constants.Mapping.MAPPING_CATALOG_EDIT;
import static com.zhuravel.market_servlet.config.Constants.SessionAttributes.ATTRIBUTE_ERRORS;

/**
 * @author Evgenii Zhuravel created on 29.06.2022
 * @version 1.0
 */
@Authenticated(role = RoleType.ROLE_SELLER)
@WebServlet(urlPatterns = MAPPING_CATALOG_EDIT)
public class EditProductServlet extends BaseServlet {
    private static final Logger logger = LoggerFactory.getLogger(EditProductServlet.class);

    private static final String PAGE_PRODUCT_ADD = "/WEB-INF/templates/seller/product_add.jsp";

    public static final String TRANSLATION_NAME = "name-";

    public static final String TRANSLATION_DESC = "desc-";

    private ProductService productService;

    private LocalizedCategoryService categoryService;

    private ProducerService producerService;

    private LocalizedParameterTypeService localizedParameterTypeService;

    @Override
    public void init() {
        this.productService = new ProductServiceImpl();
        this.categoryService = new LocalizedCategoryServiceImpl();
        this.producerService = new ProducerServiceImpl();
        this.localizedParameterTypeService = new LocalizedParameterTypeServiceImpl();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            String productIdParameter = req.getParameter(ATTRIBUTE_PRODUCT_ID);
            String id = req.getParameter("id");

            long categoryId;
            if (id != null) {
                categoryId = Long.parseLong(id);
            } else {
                categoryId = (Long) req.getAttribute("id");
            }

            if (productIdParameter != null && req.getAttribute(ATTRIBUTE_ERRORS) == null) {
                long productId = Long.parseLong(productIdParameter);
                ProductDto product = productService.getById(productId);
                req.setAttribute(ATTRIBUTE_PRODUCT, product);
            }

            LocalizedCategoryDto category = categoryService.getById(categoryId);
            Page<Producer> producerPage = producerService.getAll(new Pageable());
            SelectedParametersDto parameterTypes = initParameters(req, categoryId);

            req.setAttribute(ATTRIBUTE_CATEGORY, category);
            req.setAttribute(ATTRIBUTE_PRODUCERS, producerPage.getContent());
            req.setAttribute(ATTRIBUTE_PARAMETER_TYPES, parameterTypes.getParameterTypes());

            req.getRequestDispatcher(PAGE_PRODUCT_ADD).forward(req, resp);

        } catch (NumberFormatException | ServiceException e) {
            logger.error(e.getMessage());
            showErrorMessage(req, resp, MessageResourceReceiver.getString("message.error.retrieve_edit_product_page"));
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
        logger.debug("Method Post for " + MAPPING_CATALOG_EDIT);
        try {
            req.setCharacterEncoding("UTF-8");

            String productIdParameter = req.getParameter(ATTRIBUTE_PRODUCT_ID);
            Long productId = (Long) req.getAttribute(ATTRIBUTE_PRODUCT_ID);
            if (productId == null && (productIdParameter != null && !productIdParameter.isEmpty())) {
                productId = Long.parseLong(productIdParameter);
            }

            long categoryId = Long.parseLong(req.getParameter(ATTRIBUTE_CATEGORY_ID));

            Map<String, String[]> parameterMap = req.getParameterMap();
            Map<Long, ParameterDto> parameters = initParameters(parameterMap);

            Map<String, String> localizedNames = retrieveNameTranslations(parameterMap);
            Map<String, String> localizedDesc = retrieveDescTranslations(parameterMap);

            Producer producer = new Producer();
            producer.setId(Long.parseLong(req.getParameter("producer")));

            ProductDto productDto = new ProductDto();
            productDto.setId(productId);
            productDto.setName(req.getParameter("name"));
            productDto.setLocalizedNames(localizedNames);
            productDto.setLocalizedDescriptions(localizedDesc);
            productDto.setCategoryId(categoryId);
            productDto.setProducer(producer);
            productDto.setParameters(parameters);
            productDto.setPrice(Long.parseLong(req.getParameter("price")));
            productDto.setImagePath("/img/tech/laptop1.jpg");

            Map<String, String> errors = Validator.validate(productDto);

            if (!errors.isEmpty()) {
                req.setAttribute(ATTRIBUTE_ERRORS, errors);
                req.setAttribute("id", categoryId);
                req.setAttribute(ATTRIBUTE_PRODUCT, productDto);

                this.doGet(req, resp);
            } else {
                if (productId != null) {
                    productService.update(productDto);
                } else {
                    productService.save(productDto);
                }

                resp.sendRedirect(req.getContextPath() + MAPPING_CATALOG + "?id=" + categoryId);
            }
        } catch (ServiceException e) {
            logger.error(e.getMessage());
            showErrorMessage(req, resp, MessageResourceReceiver.getString("message.error.retrieve_edit_product_page"));
        }
    }

    private SelectedParametersDto initParameters(HttpServletRequest req, Long categoryId) throws ServiceException {
        SelectedParametersDto selectedParameters = (SelectedParametersDto) req.getSession().getAttribute(FILTER_PARAMETERS);

        if (selectedParameters == null || selectedParameters.isLangUpdated()) {
            Page<LocalizedParameterType> parameterTypes = localizedParameterTypeService.getAllByCategoryId(categoryId, new Pageable());

            selectedParameters = new SelectedParametersDtoBuilder().build(parameterTypes.getContent());
        }

        req.getSession().setAttribute(FILTER_PARAMETERS, selectedParameters);

        return selectedParameters;
    }

    private Map<Long, ParameterDto> initParameters(Map<String, String[]> filtersMap) {
        Map<String, String[]> parameters = filtersMap.entrySet().stream()
                .filter(stringEntry -> stringEntry.getKey().startsWith("parameter-"))
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));

        Map<Long, ParameterDto> parametersSet = new HashMap<>();
        parameters.forEach((key, value) -> {
            ParameterDto parameterDto = new ParameterDto();

            parameterDto.setId(Long.parseLong(value[0]));

            parametersSet.put(parameterDto.getId(), parameterDto);
        });

        return parametersSet;
    }

    private static Map<String, String> retrieveNameTranslations(Map<String, String[]> filtersMap) {
        return filtersMap.entrySet().stream()
                .filter(stringEntry -> stringEntry.getKey().startsWith(TRANSLATION_NAME))
                .collect(Collectors.toMap(stringEntry -> stringEntry.getKey().split("-")[1], stringEntry -> stringEntry.getValue()[0]));
    }

    private static Map<String, String> retrieveDescTranslations(Map<String, String[]> filtersMap) {
        return filtersMap.entrySet().stream()
                .filter(stringEntry -> stringEntry.getKey().startsWith(TRANSLATION_DESC))
                .collect(Collectors.toMap(stringEntry -> stringEntry.getKey().split("-")[1], stringEntry -> stringEntry.getValue()[0]));
    }
}
