package com.zhuravel.market_servlet.controller.user;

import com.zhuravel.market_servlet.config.MessageResourceReceiver;
import com.zhuravel.market_servlet.config.security.Authenticated;
import com.zhuravel.market_servlet.controller.BaseServlet;
import com.zhuravel.market_servlet.model.RoleType;
import com.zhuravel.market_servlet.service.ShoppingCartService;
import com.zhuravel.market_servlet.service.dto.ProductQuantitiesDto;
import com.zhuravel.market_servlet.service.dto.ShoppingCartDto;
import com.zhuravel.market_servlet.service.dto.UserDto;
import com.zhuravel.market_servlet.service.exception.ServiceException;
import com.zhuravel.market_servlet.service.impl.ShoppingCartServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Map;
import java.util.stream.Collectors;

import static com.zhuravel.market_servlet.config.Constants.Attributes.ATTRIBUTE_ORDERS;
import static com.zhuravel.market_servlet.config.Constants.Attributes.ATTRIBUTE_SELECTED_PRODUCTS;
import static com.zhuravel.market_servlet.config.Constants.Mapping.MAPPING_SHOPPING_CART;
import static com.zhuravel.market_servlet.config.Constants.SessionAttributes.ATTRIBUTE_USER;

/**
 * @author Evgenii Zhuravel created on 01.07.2022
 * @version 1.0
 */
@Authenticated(role = RoleType.ROLE_USER)
@WebServlet(urlPatterns = MAPPING_SHOPPING_CART)
public class ShoppingCartServlet extends BaseServlet {
    private static final Logger logger = LoggerFactory.getLogger(ShoppingCartServlet.class);

    private static final String PAGE_SHOPPING_CART = "/WEB-INF/templates/shopping_cart.jsp";

    public static final String ATTRIBUTE_PRODUCT_CART = "productCart";

    public static final String QUANTITY = "quantity_";

    private ShoppingCartService shoppingCartService;

    @Override
    public void init() {
        this.shoppingCartService = new ShoppingCartServiceImpl();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            UserDto user = (UserDto) req.getSession().getAttribute(ATTRIBUTE_USER);
            ShoppingCartDto products = shoppingCartService.getByUserId(user.getId());

            req.getSession().setAttribute(ATTRIBUTE_PRODUCT_CART, products);
            req.setAttribute(ATTRIBUTE_SELECTED_PRODUCTS, products.getQuantitiesDto());
            req.setAttribute(ATTRIBUTE_ORDERS, products.getOrders());

            req.getRequestDispatcher(PAGE_SHOPPING_CART).forward(req, resp);
        } catch (ServiceException e) {
            logger.error(e.getMessage());
            showErrorMessage(req, resp, MessageResourceReceiver.getString("message.shopping_cart.retrieve.error"));
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
        try {
            ShoppingCartDto cartDto = (ShoppingCartDto) req.getSession().getAttribute(ATTRIBUTE_PRODUCT_CART);

            Map<Long, Integer> quantities = retrieveQuantities(req);

            ProductQuantitiesDto productQuantitiesDto = new ProductQuantitiesDto();
            productQuantitiesDto.setQuantities(quantities);

            cartDto.setQuantitiesDto(productQuantitiesDto);

            if (req.getParameter("save") != null) {
                shoppingCartService.updateQuantity(cartDto);

                req.setAttribute(ATTRIBUTE_SELECTED_PRODUCTS, cartDto.getQuantitiesDto());
                req.setAttribute(ATTRIBUTE_ORDERS, cartDto.getOrders());

                req.getRequestDispatcher(PAGE_SHOPPING_CART).forward(req, resp);
            } else if (req.getParameter("register") != null) {
                shoppingCartService.register(cartDto);
                showSuccessMessage(req, resp, MessageResourceReceiver.getString("message.shopping_cart.registration.success"));
            }
        } catch (ServiceException e) {
            showErrorMessage(req, resp, MessageResourceReceiver.getString("message.shopping_cart.registration.error"));
        }
    }

    private static Map<Long, Integer> retrieveQuantities(HttpServletRequest request) {
        Map<String, String[]> filtersMap = request.getParameterMap();

        return filtersMap.entrySet().stream()
                .filter(stringEntry -> stringEntry.getKey().startsWith(QUANTITY))
                .collect(Collectors.toMap(stringEntry -> Long.parseLong(stringEntry.getKey().split("_")[1]), stringEntry -> Integer.parseInt(stringEntry.getValue()[0])));
    }
}
