package com.zhuravel.market_servlet.controller.user;

import com.zhuravel.market_servlet.config.MessageResourceReceiver;
import com.zhuravel.market_servlet.config.security.Authenticated;
import com.zhuravel.market_servlet.controller.BaseServlet;
import com.zhuravel.market_servlet.model.RoleType;
import com.zhuravel.market_servlet.service.ShoppingCartService;
import com.zhuravel.market_servlet.service.dto.ProductQuantitiesDto;
import com.zhuravel.market_servlet.service.dto.UserDto;
import com.zhuravel.market_servlet.service.impl.ShoppingCartServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static com.zhuravel.market_servlet.config.Constants.Attributes.ATTRIBUTE_PRODUCT_ID;
import static com.zhuravel.market_servlet.config.Constants.Attributes.ATTRIBUTE_SELECTED_PRODUCTS;
import static com.zhuravel.market_servlet.config.Constants.Mapping.MAPPING_SHOPPING_CART_ADD;
import static com.zhuravel.market_servlet.config.Constants.SessionAttributes.ATTRIBUTE_USER;

/**
 * @author Evgenii Zhuravel created on 04.07.2022
 * @version 1.0
 */
@Authenticated(role = RoleType.ROLE_USER)
@WebServlet(urlPatterns = MAPPING_SHOPPING_CART_ADD)
public class ShoppingCartAddServlet extends BaseServlet {
    private static final Logger logger = LoggerFactory.getLogger(ShoppingCartAddServlet.class);

    private ShoppingCartService shoppingCartService;

    @Override
    public void init() {
        this.shoppingCartService = new ShoppingCartServiceImpl();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        UserDto user = (UserDto) req.getSession().getAttribute(ATTRIBUTE_USER);

        try {
            Long productId = Long.parseLong(req.getParameter(ATTRIBUTE_PRODUCT_ID));

            shoppingCartService.add(user.getId(), productId);

            ProductQuantitiesDto selectedProducts = (ProductQuantitiesDto) req.getSession().getAttribute(ATTRIBUTE_SELECTED_PRODUCTS);
            if (selectedProducts == null) {
                selectedProducts = new ProductQuantitiesDto();
            }
            selectedProducts.getQuantities().put(productId, 1);

            String returnUrl = req.getHeader("referer");

            resp.sendRedirect(req.getContextPath() + returnUrl);
        } catch (Exception e) {
            logger.error(e.getMessage());
            showErrorMessage(req, resp, MessageResourceReceiver.getString("message.error.shopping_cart.add"));
        }
    }
}
