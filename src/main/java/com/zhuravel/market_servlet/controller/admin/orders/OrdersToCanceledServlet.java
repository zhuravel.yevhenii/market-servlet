package com.zhuravel.market_servlet.controller.admin.orders;

import com.zhuravel.market_servlet.config.MessageResourceReceiver;
import com.zhuravel.market_servlet.config.security.Authenticated;
import com.zhuravel.market_servlet.model.OrderStatusType;
import com.zhuravel.market_servlet.model.RoleType;
import com.zhuravel.market_servlet.service.ShoppingCartService;
import com.zhuravel.market_servlet.service.exception.ServiceException;
import com.zhuravel.market_servlet.service.impl.ShoppingCartServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static com.zhuravel.market_servlet.config.Constants.Mapping.MAPPING_ORDERS_CANCELED;

/**
 * @author Evgenii Zhuravel created on 06.07.2022
 * @version 1.0
 */
@Authenticated(role = RoleType.ROLE_ADMIN)
@WebServlet(urlPatterns = MAPPING_ORDERS_CANCELED)
public class OrdersToCanceledServlet extends OrdersServlet {
    private static final Logger logger = LoggerFactory.getLogger(OrdersToCanceledServlet.class);

    private static final String PARAMETER_DATE = "date_time";

    private ShoppingCartService shoppingCartService;

    @Override
    public void init() {
        this.shoppingCartService = new ShoppingCartServiceImpl();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        logger.debug("doGet");

        try {
            String filter_date_from = req.getParameter(PARAMETER_DATE);
            if (filter_date_from != null && !filter_date_from.isEmpty()) {
                long dateTime = Long.parseLong(filter_date_from);

                shoppingCartService.changeStatus(dateTime, OrderStatusType.CANCELED.ordinal()+1L);
            }

            super.doGet(req, resp);
        } catch (ServiceException e) {
            logger.error(e.getMessage());
            showErrorMessage(req, resp, MessageResourceReceiver.getString("message.error.retrieving.orders"));
        }
    }
}
