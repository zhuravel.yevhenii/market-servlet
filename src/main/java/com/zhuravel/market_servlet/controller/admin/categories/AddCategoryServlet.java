package com.zhuravel.market_servlet.controller.admin.categories;

import com.zhuravel.market_servlet.config.MessageResourceReceiver;
import com.zhuravel.market_servlet.config.security.Authenticated;
import com.zhuravel.market_servlet.model.RoleType;
import com.zhuravel.market_servlet.service.dto.CategoryDto;
import com.zhuravel.market_servlet.service.exception.ServiceException;
import com.zhuravel.market_servlet.service.validation.Validator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Map;
import java.util.stream.Collectors;

import static com.zhuravel.market_servlet.config.Constants.Attributes.ATTRIBUTE_CATEGORY_FORM;
import static com.zhuravel.market_servlet.config.Constants.Mapping.MAPPING_CATEGORIES;
import static com.zhuravel.market_servlet.config.Constants.Mapping.MAPPING_CATEGORY_ADD;
import static com.zhuravel.market_servlet.config.Constants.SessionAttributes.ATTRIBUTE_ERRORS;

/**
 * @author Evgenii Zhuravel created on 23.06.2022
 * @version 1.0
 */
@Authenticated(role = RoleType.ROLE_ADMIN)
@WebServlet(urlPatterns = MAPPING_CATEGORY_ADD)
public class AddCategoryServlet extends CategoriesServlet {
    private static final Logger logger = LoggerFactory.getLogger(AddCategoryServlet.class);

    public static final String NAME_TRANSLATION = "name-";

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
        logger.debug("Method Post for " + MAPPING_CATEGORY_ADD);
        try {
            req.setCharacterEncoding("UTF-8");

            CategoryDto categoryForm = new CategoryDto();
            categoryForm.setName(req.getParameter("name"));
            categoryForm.setParentCategoryId(Long.parseLong(req.getParameter("parentCategoryId")));
            categoryForm.setImagePath(req.getParameter("imagePath"));
            categoryForm.setLocalizedNames(retrieveLocalizedNames(req));

            Map<String, String> errors = Validator.validate(categoryForm);
            if (!errors.isEmpty()) {
                req.setAttribute(ATTRIBUTE_ERRORS, errors);
                req.setAttribute(ATTRIBUTE_CATEGORY_FORM, categoryForm);

                super.doGet(req, resp);
            } else {
                super.categoryService.save(categoryForm);

                resp.sendRedirect(req.getContextPath() + MAPPING_CATEGORIES + "?id=" + categoryForm.getParentCategoryId());
            }
        } catch (ServiceException e) {
            logger.error(e.getMessage());
            showErrorMessage(req, resp, MessageResourceReceiver.getString("message.error.add.category"));
        }
    }

    private static Map<String, String> retrieveLocalizedNames(HttpServletRequest request) {
        Map<String, String[]> filtersMap = request.getParameterMap();

        return filtersMap.entrySet().stream()
                .filter(stringEntry -> stringEntry.getKey().startsWith(NAME_TRANSLATION))
                .collect(Collectors.toMap(stringEntry -> stringEntry.getKey().split("-")[1], stringEntry -> stringEntry.getValue()[0]));
    }
}
