package com.zhuravel.market_servlet.controller.admin.categories;

import com.zhuravel.market_servlet.config.MessageResourceReceiver;
import com.zhuravel.market_servlet.config.security.Authenticated;
import com.zhuravel.market_servlet.controller.BaseServlet;
import com.zhuravel.market_servlet.model.RoleType;
import com.zhuravel.market_servlet.service.ParameterTypeService;
import com.zhuravel.market_servlet.service.dto.ParameterTypeDto;
import com.zhuravel.market_servlet.service.exception.ServiceException;
import com.zhuravel.market_servlet.service.impl.ParameterTypeServiceImpl;
import com.zhuravel.market_servlet.service.validation.Validator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Map;
import java.util.stream.Collectors;

import static com.zhuravel.market_servlet.config.Constants.Attributes.ATTRIBUTE_CATEGORY_ID;
import static com.zhuravel.market_servlet.config.Constants.Attributes.ATTRIBUTE_PARAMETER_TYPE;
import static com.zhuravel.market_servlet.config.Constants.Mapping.MAPPING_CATEGORY_EDIT;
import static com.zhuravel.market_servlet.config.Constants.Mapping.MAPPING_PARAMETER_TYPE_EDIT;
import static com.zhuravel.market_servlet.config.Constants.Mapping.MAPPING_REGISTRATION;
import static com.zhuravel.market_servlet.config.Constants.SessionAttributes.ATTRIBUTE_ERRORS;

/**
 * @author Evgenii Zhuravel created on 27.06.2022
 * @version 1.0
 */
@Authenticated(role = RoleType.ROLE_ADMIN)
@WebServlet(urlPatterns = MAPPING_PARAMETER_TYPE_EDIT)
public class EditParameterTypeServlet extends BaseServlet {
    private static final Logger logger = LoggerFactory.getLogger(EditParameterTypeServlet.class);

    private static final String PAGE_PARAMETER_TYPE_EDIT = "/WEB-INF/templates/admin/admin-parameter-type-edit.jsp";

    public static final String NAME_TRANSLATION = "name-";

    private ParameterTypeService parameterTypeService;

    @Override
    public void init() {
        parameterTypeService = new ParameterTypeServiceImpl();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String id = req.getParameter("id");
        try {
            Long parameterTypeId = Long.parseLong(id);
            ParameterTypeDto parameterType = parameterTypeService.getById(parameterTypeId);

            req.setAttribute(ATTRIBUTE_PARAMETER_TYPE, parameterType);

            resp.setCharacterEncoding("UTF-8");
            req.getRequestDispatcher(PAGE_PARAMETER_TYPE_EDIT).forward(req, resp);
        } catch (NumberFormatException | ServiceException e) {
            showErrorMessage(req, resp, MessageResourceReceiver.getString("message.error.retrieve_edit_category_page"));
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
        logger.debug("Method Post for " + MAPPING_REGISTRATION);
        try {
            req.setCharacterEncoding("UTF-8");

            ParameterTypeDto parameterTypeDto = new ParameterTypeDto();
            parameterTypeDto.setId(Long.parseLong(req.getParameter("id")));
            parameterTypeDto.setCategoryId(Long.parseLong(req.getParameter(ATTRIBUTE_CATEGORY_ID)));
            parameterTypeDto.setName(req.getParameter("name"));
            parameterTypeDto.setTranslations(retrieveTranslations(req));

            Map<String, String> errors = Validator.validate(parameterTypeDto);

            if (!errors.isEmpty()) {
                req.setAttribute(ATTRIBUTE_PARAMETER_TYPE, parameterTypeDto);
                req.setAttribute(ATTRIBUTE_ERRORS, errors);

                req.getRequestDispatcher(PAGE_PARAMETER_TYPE_EDIT).forward(req, resp);
            } else {
                parameterTypeService.update(parameterTypeDto);

                resp.sendRedirect(req.getContextPath() + MAPPING_CATEGORY_EDIT + "?id=" + parameterTypeDto.getCategoryId());
            }
        } catch (ServiceException e) {
            logger.error(e.getMessage());
            showErrorMessage(req, resp, MessageResourceReceiver.getString("message.error.edit.parameter_type"));
        }
    }

    private static Map<String, String> retrieveTranslations(HttpServletRequest request) {
        Map<String, String[]> filtersMap = request.getParameterMap();

        return filtersMap.entrySet().stream()
                .filter(stringEntry -> stringEntry.getKey().startsWith(NAME_TRANSLATION))
                .collect(Collectors.toMap(stringEntry -> stringEntry.getKey().split("-")[1], stringEntry -> stringEntry.getValue()[0]));
    }
}
