package com.zhuravel.market_servlet.controller.admin.categories;

import com.zhuravel.market_servlet.config.MessageResourceReceiver;
import com.zhuravel.market_servlet.config.security.Authenticated;
import com.zhuravel.market_servlet.model.RoleType;
import com.zhuravel.market_servlet.service.ParameterService;
import com.zhuravel.market_servlet.service.ParameterTypeService;
import com.zhuravel.market_servlet.service.dto.CategoryDto;
import com.zhuravel.market_servlet.service.dto.ParameterDto;
import com.zhuravel.market_servlet.service.dto.ParameterTypeDto;
import com.zhuravel.market_servlet.service.exception.ServiceException;
import com.zhuravel.market_servlet.service.impl.ParameterServiceImpl;
import com.zhuravel.market_servlet.service.impl.ParameterTypeServiceImpl;
import com.zhuravel.market_servlet.service.validation.Validator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static com.zhuravel.market_servlet.config.Constants.Attributes.ATTRIBUTE_CATEGORY_FORM;
import static com.zhuravel.market_servlet.config.Constants.Attributes.ATTRIBUTE_PARAMETERS;
import static com.zhuravel.market_servlet.config.Constants.Attributes.ATTRIBUTE_PARAMETER_TYPES;
import static com.zhuravel.market_servlet.config.Constants.Mapping.MAPPING_CATEGORIES;
import static com.zhuravel.market_servlet.config.Constants.Mapping.MAPPING_CATEGORY_EDIT;
import static com.zhuravel.market_servlet.config.Constants.Mapping.MAPPING_REGISTRATION;
import static com.zhuravel.market_servlet.config.Constants.Pages.PAGE_CATEGORY_EDIT;
import static com.zhuravel.market_servlet.config.Constants.SessionAttributes.ATTRIBUTE_ERRORS;

/**
 * @author Evgenii Zhuravel created on 23.06.2022
 * @version 1.0
 */
@Authenticated(role = RoleType.ROLE_ADMIN)
@WebServlet(urlPatterns = MAPPING_CATEGORY_EDIT)
public class EditCategoryServlet extends CategoriesServlet {
    private static final Logger logger = LoggerFactory.getLogger(EditCategoryServlet.class);

    public static final String NAME_TRANSLATION = "name-";

    public static final String ATTRIBUTE_TYPE = "type";

    private ParameterTypeService parameterTypeService;

    private ParameterService parameterService;

    @Override
    public void init() throws ServletException {
        super.init();
        parameterTypeService = new ParameterTypeServiceImpl();
        parameterService = new ParameterServiceImpl();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String id = req.getParameter("id");
        try {
            Long categoryId = Long.parseLong(id);
            CategoryDto category = categoryService.getById(categoryId);
            List<ParameterTypeDto> parameterTypes = parameterTypeService.getAllByCategoryId(categoryId);

            req.setAttribute(ATTRIBUTE_CATEGORY_FORM, category);
            req.setAttribute(ATTRIBUTE_PARAMETER_TYPES, parameterTypes);

            String type = req.getParameter(ATTRIBUTE_TYPE);
            if (type != null) {
                List<ParameterDto> parameters = parameterService.getByTypeId(Long.parseLong(type));

                req.setAttribute(ATTRIBUTE_TYPE, type);
                req.setAttribute(ATTRIBUTE_PARAMETERS, parameters);
            }

            resp.setCharacterEncoding("UTF-8");
            req.getRequestDispatcher(PAGE_CATEGORY_EDIT).forward(req, resp);
        } catch (NumberFormatException | ServiceException e) {
            logger.error(e.getMessage());
            showErrorMessage(req, resp, MessageResourceReceiver.getString("message.error.retrieve_edit_category_page"));
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
        logger.debug("Method Post for " + MAPPING_REGISTRATION);
        try {
            req.setCharacterEncoding("UTF-8");

            CategoryDto categoryForm = new CategoryDto();
            categoryForm.setId(Long.parseLong(req.getParameter("id")));
            categoryForm.setName(req.getParameter("name"));
            categoryForm.setParentCategoryId(Long.parseLong(req.getParameter("parentCategoryId")));
            categoryForm.setImagePath(req.getParameter("imagePath"));
            categoryForm.setLocalizedNames(retrieveLocalizedNames(req));

            Map<String, String> errors = Validator.validate(categoryForm);

            if (!errors.isEmpty()) {
                List<ParameterTypeDto> parameterTypes = parameterTypeService.getAllByCategoryId(categoryForm.getId());

                req.setAttribute(ATTRIBUTE_ERRORS, errors);
                req.setAttribute(ATTRIBUTE_CATEGORY_FORM, categoryForm);
                req.setAttribute(ATTRIBUTE_PARAMETER_TYPES, parameterTypes);

                req.getRequestDispatcher(PAGE_CATEGORY_EDIT).forward(req, resp);

                super.doGet(req, resp);
            } else {
                categoryService.update(categoryForm);

                resp.sendRedirect(req.getContextPath() + MAPPING_CATEGORIES + "?id=" + categoryForm.getParentCategoryId());
            }
        } catch (ServiceException e) {
            logger.error(e.getMessage());
            showErrorMessage(req, resp, MessageResourceReceiver.getString("message.error.retrieve_edit_category_page"));
        }
    }

    private static Map<String, String> retrieveLocalizedNames(HttpServletRequest request) {
        Map<String, String[]> filtersMap = request.getParameterMap();

        return filtersMap.entrySet().stream()
                .filter(stringEntry -> stringEntry.getKey().startsWith(NAME_TRANSLATION))
                .collect(Collectors.toMap(stringEntry -> stringEntry.getKey().split("-")[1], stringEntry -> stringEntry.getValue()[0]));
    }
}
