package com.zhuravel.market_servlet.controller.admin.users;

import com.zhuravel.market_servlet.config.MessageResourceReceiver;
import com.zhuravel.market_servlet.config.security.Authenticated;
import com.zhuravel.market_servlet.controller.BaseServlet;
import com.zhuravel.market_servlet.controller.PageableServlet;
import com.zhuravel.market_servlet.model.RoleType;
import com.zhuravel.market_servlet.model.entity.Role;
import com.zhuravel.market_servlet.model.entity.UserStatus;
import com.zhuravel.market_servlet.model.pageable.Page;
import com.zhuravel.market_servlet.model.pageable.Pageable;
import com.zhuravel.market_servlet.service.RoleService;
import com.zhuravel.market_servlet.service.UserService;
import com.zhuravel.market_servlet.service.UserStatusService;
import com.zhuravel.market_servlet.service.dto.UserDto;
import com.zhuravel.market_servlet.service.exception.ServiceException;
import com.zhuravel.market_servlet.service.impl.RoleServiceImpl;
import com.zhuravel.market_servlet.service.impl.UserServiceImpl;
import com.zhuravel.market_servlet.service.impl.UserStatusServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import static com.zhuravel.market_servlet.config.Constants.Attributes.ATTRIBUTE_ROLES;
import static com.zhuravel.market_servlet.config.Constants.Attributes.ATTRIBUTE_STATUSES;
import static com.zhuravel.market_servlet.config.Constants.Mapping.MAPPING_USERS;

/**
 * @author Evgenii Zhuravel created on 28.06.2022
 * @version 1.0
 */
@Authenticated(role = RoleType.ROLE_ADMIN)
@WebServlet(urlPatterns = MAPPING_USERS)
public class UsersServlet extends BaseServlet implements PageableServlet {
    private static final Logger logger = LoggerFactory.getLogger(UsersServlet.class);

    private static final String PAGE_USERS = "/WEB-INF/templates/admin/admin-users.jsp";

    private static final String ATTRIBUTE_USERS = "users";

    private UserService userService;
    private RoleService roleService;
    private UserStatusService userStatusService;

    @Override
    public void init() {
        this.userService = new UserServiceImpl();
        roleService = new RoleServiceImpl();
        userStatusService = new UserStatusServiceImpl();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        logger.debug("doGet");

        try {
            Page<UserDto> users = userService.getAll(getRequestPage(req));

            List<Role> roles = new ArrayList<>();
            roles.add(new Role());
            roles.addAll(roleService.getAll(new Pageable()).getContent());

            List<UserStatus> statuses = new ArrayList<>();
            statuses.add(new UserStatus());
            statuses.addAll(userStatusService.getAll(new Pageable()).getContent());

            req.setAttribute(ATTRIBUTE_USERS, users.getContent());
            req.setAttribute(ATTRIBUTE_ROLES, roles);
            req.setAttribute(ATTRIBUTE_STATUSES, statuses);

            setRequestPage(req, getRequestPage(req), users.getTotalPages());

            req.getRequestDispatcher(PAGE_USERS).forward(req, resp);
        } catch (ServiceException e) {
            logger.error(e.getMessage());
            showErrorMessage(req, resp, MessageResourceReceiver.getString("message.error.retrieving.users"));
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        logger.debug("doGet");
        try {
            Page<UserDto> users = getFilteredUsers(req);

            List<Role> roles = new ArrayList<>();
            roles.add(new Role());
            roles.addAll(roleService.getAll(new Pageable()).getContent());

            List<UserStatus> statuses = new ArrayList<>();
            statuses.add(new UserStatus());
            statuses.addAll(userStatusService.getAll(new Pageable()).getContent());

            req.setAttribute(ATTRIBUTE_USERS, users.getContent());
            req.setAttribute(ATTRIBUTE_ROLES, roles);
            req.setAttribute(ATTRIBUTE_STATUSES, statuses);

            setRequestPage(req, getRequestPage(req), users.getTotalPages());

            req.getRequestDispatcher(PAGE_USERS).forward(req, resp);
        } catch (ServiceException e) {
            logger.error(e.getMessage());
            showErrorMessage(req, resp, MessageResourceReceiver.getString("message.error.retrieving.users"));
        }
    }

    private Page<UserDto> getFilteredUsers(HttpServletRequest req) throws ServiceException {
        Page<UserDto> usersPage;

        Map<String, String> filters = new TreeMap<>();
        filters.put("filter_login", req.getParameter("filter_login"));
        filters.put("filter_first_name", req.getParameter("filter_first_name"));
        filters.put("filter_last_name", req.getParameter("filter_last_name"));
        filters.put("filter_email", req.getParameter("filter_email"));
        filters.put("filter_locale", req.getParameter("filter_locale"));
        filters.put("filter_status_id", req.getParameter("filter_status_id"));
        filters.put("filter_role_id", req.getParameter("filter_role_id"));

        Map<String, Object> filterFields = new TreeMap<>();

        for (Map.Entry<String, String> entry : filters.entrySet()) {
            String value = "";
            if (entry.getValue() != null && !entry.getValue().isEmpty()) {
                value = entry.getValue();

                if (entry.getKey().contains("_id")) {
                    filterFields.put(entry.getKey().replace("filter_", ""), Long.parseLong(entry.getValue()));
                } else {
                    filterFields.put(entry.getKey().replace("filter_", ""), entry.getValue());
                }
            }
            req.setAttribute(entry.getKey(), value);
        }

        if (filterFields.size() > 0) {
            usersPage = userService.getAll(filterFields, getRequestPage(req));
        } else {
            usersPage = userService.getAll(getRequestPage(req));
        }

        return usersPage;
    }
}
