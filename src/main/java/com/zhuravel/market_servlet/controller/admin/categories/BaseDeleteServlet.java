package com.zhuravel.market_servlet.controller.admin.categories;

import com.zhuravel.market_servlet.config.MessageResourceReceiver;
import com.zhuravel.market_servlet.controller.BaseServlet;
import com.zhuravel.market_servlet.service.BaseService;
import com.zhuravel.market_servlet.service.exception.ServiceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author Evgenii Zhuravel created on 23.06.2022
 * @version 1.0
 */
public class BaseDeleteServlet<E> extends BaseServlet {
    private static final Logger logger = LoggerFactory.getLogger(BaseDeleteServlet.class);

    private BaseService<E, Long> service;

    public void init(BaseService<E, Long> service) {
        this.service = service;
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String id = req.getParameter("id");
        try {
            service.delete(Long.parseLong(id));

            String returnUrl = req.getHeader("referer");
            resp.sendRedirect(req.getContextPath() + returnUrl);
        } catch (NumberFormatException | ServiceException e) {
            logger.error(e.getMessage());
            showErrorMessage(req, resp, MessageResourceReceiver.getString("message.error.delete_category"));
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.doPost(req, resp);
    }
}
