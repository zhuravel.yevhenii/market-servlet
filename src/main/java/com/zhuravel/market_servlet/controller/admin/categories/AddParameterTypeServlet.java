package com.zhuravel.market_servlet.controller.admin.categories;

import com.zhuravel.market_servlet.config.MessageResourceReceiver;
import com.zhuravel.market_servlet.config.security.Authenticated;
import com.zhuravel.market_servlet.model.RoleType;
import com.zhuravel.market_servlet.service.ParameterTypeService;
import com.zhuravel.market_servlet.service.dto.CategoryDto;
import com.zhuravel.market_servlet.service.dto.ParameterTypeDto;
import com.zhuravel.market_servlet.service.exception.ServiceException;
import com.zhuravel.market_servlet.service.impl.ParameterTypeServiceImpl;
import com.zhuravel.market_servlet.service.validation.Validator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static com.zhuravel.market_servlet.config.Constants.Attributes.ATTRIBUTE_CATEGORY_FORM;
import static com.zhuravel.market_servlet.config.Constants.Attributes.ATTRIBUTE_CATEGORY_ID;
import static com.zhuravel.market_servlet.config.Constants.Attributes.ATTRIBUTE_PARAMETER_TYPE;
import static com.zhuravel.market_servlet.config.Constants.Attributes.ATTRIBUTE_PARAMETER_TYPES;
import static com.zhuravel.market_servlet.config.Constants.Mapping.MAPPING_CATEGORY_EDIT;
import static com.zhuravel.market_servlet.config.Constants.Mapping.MAPPING_PARAMETER_TYPE_ADD;
import static com.zhuravel.market_servlet.config.Constants.Pages.PAGE_CATEGORY_EDIT;
import static com.zhuravel.market_servlet.config.Constants.SessionAttributes.ATTRIBUTE_ERRORS;

/**
 * @author Evgenii Zhuravel created on 23.06.2022
 * @version 1.0
 */
@Authenticated(role = RoleType.ROLE_ADMIN)
@WebServlet(urlPatterns = MAPPING_PARAMETER_TYPE_ADD)
public class AddParameterTypeServlet extends EditCategoryServlet {
    private static final Logger logger = LoggerFactory.getLogger(AddParameterTypeServlet.class);

    public static final String NAME_TRANSLATION = "name-";

    private ParameterTypeService parameterTypeService;

    @Override
    public void init() throws ServletException {
        super.init();
        parameterTypeService = new ParameterTypeServiceImpl();
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
        logger.debug("Method Post for " + MAPPING_PARAMETER_TYPE_ADD);

        try {
            req.setCharacterEncoding("UTF-8");

            ParameterTypeDto parameterTypeDto = new ParameterTypeDto();
            parameterTypeDto.setCategoryId(Long.parseLong(req.getParameter(ATTRIBUTE_CATEGORY_ID)));
            parameterTypeDto.setTranslations(retrieveLocalizedNames(req));
            parameterTypeDto.setName(parameterTypeDto.getTranslations().get("en"));

            Map<String, String> errors = Validator.validate(parameterTypeDto);

            if (!errors.isEmpty()) {
                Map<String, String> typeErrors = new HashMap<>();

                for (Map.Entry<String, String> entry : errors.entrySet()) {
                    typeErrors.put("type_" + entry.getKey(), entry.getValue());
                }

                long categoryId = Long.parseLong(req.getParameter(ATTRIBUTE_CATEGORY_ID));

                CategoryDto category = categoryService.getById(categoryId);
                List<ParameterTypeDto> parameterTypes = parameterTypeService.getAllByCategoryId(categoryId);

                req.setAttribute(ATTRIBUTE_ERRORS, typeErrors);
                req.setAttribute(ATTRIBUTE_PARAMETER_TYPE, parameterTypeDto);

                req.setAttribute(ATTRIBUTE_CATEGORY_FORM, category);
                req.setAttribute(ATTRIBUTE_PARAMETER_TYPES, parameterTypes);

                req.getRequestDispatcher(PAGE_CATEGORY_EDIT).forward(req, resp);
            } else {
                parameterTypeService.save(parameterTypeDto);

                resp.sendRedirect(req.getContextPath() + MAPPING_CATEGORY_EDIT + "?id=" + parameterTypeDto.getCategoryId());
            }
        } catch (ServiceException e) {
            logger.error(e.getMessage());
            showErrorMessage(req, resp, MessageResourceReceiver.getString("message.error.add.parameter_type"));
        }
    }

    private static Map<String, String> retrieveLocalizedNames(HttpServletRequest request) {
        Map<String, String[]> filtersMap = request.getParameterMap();

        return filtersMap.entrySet().stream()
                .filter(stringEntry -> stringEntry.getKey().startsWith(NAME_TRANSLATION))
                .collect(Collectors.toMap(stringEntry -> stringEntry.getKey().split("-")[1], stringEntry -> stringEntry.getValue()[0]));
    }
}
