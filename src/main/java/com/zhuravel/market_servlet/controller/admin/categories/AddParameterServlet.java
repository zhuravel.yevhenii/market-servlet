package com.zhuravel.market_servlet.controller.admin.categories;

import com.zhuravel.market_servlet.config.MessageResourceReceiver;
import com.zhuravel.market_servlet.config.security.Authenticated;
import com.zhuravel.market_servlet.model.RoleType;
import com.zhuravel.market_servlet.service.ParameterService;
import com.zhuravel.market_servlet.service.ParameterTypeService;
import com.zhuravel.market_servlet.service.dto.CategoryDto;
import com.zhuravel.market_servlet.service.dto.ParameterDto;
import com.zhuravel.market_servlet.service.dto.ParameterTypeDto;
import com.zhuravel.market_servlet.service.exception.ServiceException;
import com.zhuravel.market_servlet.service.impl.ParameterServiceImpl;
import com.zhuravel.market_servlet.service.impl.ParameterTypeServiceImpl;
import com.zhuravel.market_servlet.service.validation.Validator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.Map;

import static com.zhuravel.market_servlet.config.Constants.Attributes.ATTRIBUTE_CATEGORY_FORM;
import static com.zhuravel.market_servlet.config.Constants.Attributes.ATTRIBUTE_CATEGORY_ID;
import static com.zhuravel.market_servlet.config.Constants.Attributes.ATTRIBUTE_PARAMETER;
import static com.zhuravel.market_servlet.config.Constants.Attributes.ATTRIBUTE_PARAMETERS;
import static com.zhuravel.market_servlet.config.Constants.Attributes.ATTRIBUTE_PARAMETER_TYPES;
import static com.zhuravel.market_servlet.config.Constants.Mapping.MAPPING_CATEGORY_EDIT;
import static com.zhuravel.market_servlet.config.Constants.Mapping.MAPPING_PARAMETER_ADD;
import static com.zhuravel.market_servlet.config.Constants.Pages.PAGE_CATEGORY_EDIT;
import static com.zhuravel.market_servlet.config.Constants.SessionAttributes.ATTRIBUTE_ERRORS;

/**
 * @author Evgenii Zhuravel created on 23.06.2022
 * @version 1.0
 */
@Authenticated(role = RoleType.ROLE_ADMIN)
@WebServlet(urlPatterns = MAPPING_PARAMETER_ADD)
public class AddParameterServlet extends EditCategoryServlet {
    private static final Logger logger = LoggerFactory.getLogger(AddParameterServlet.class);

    private ParameterTypeService parameterTypeService;

    private ParameterService parameterService;

    @Override
    public void init() throws ServletException {
        super.init();
        parameterTypeService = new ParameterTypeServiceImpl();
        parameterService = new ParameterServiceImpl();
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
        logger.debug("Method Post for " + MAPPING_PARAMETER_ADD);
        try {
            req.setCharacterEncoding("UTF-8");

            ParameterDto parameterDto = new ParameterDto();
            parameterDto.setTypeId(Long.parseLong(req.getParameter("parameterTypeId")));
            parameterDto.setValue(req.getParameter("value"));

            long categoryId = Long.parseLong(req.getParameter(ATTRIBUTE_CATEGORY_ID));

            Map<String, String> errors = Validator.validate(parameterDto);

            if (!errors.isEmpty()) {
                CategoryDto category = categoryService.getById(categoryId);
                List<ParameterTypeDto> parameterTypes = parameterTypeService.getAllByCategoryId(categoryId);

                String type = req.getParameter("parameterTypeId");
                List<ParameterDto> parameters = parameterService.getByTypeId(Long.parseLong(type));

                req.setAttribute(ATTRIBUTE_ERRORS, errors);
                req.setAttribute(ATTRIBUTE_PARAMETER, parameterDto);

                req.setAttribute(ATTRIBUTE_CATEGORY_FORM, category);
                req.setAttribute(ATTRIBUTE_PARAMETER_TYPES, parameterTypes);
                req.setAttribute(ATTRIBUTE_TYPE, type);
                req.setAttribute(ATTRIBUTE_PARAMETERS, parameters);

                req.getRequestDispatcher(PAGE_CATEGORY_EDIT).forward(req, resp);
            } else {
                parameterService.save(parameterDto);

                resp.sendRedirect(req.getContextPath() + MAPPING_CATEGORY_EDIT + "?id=" + categoryId + "&type=" + parameterDto.getTypeId());
            }

        } catch (ServiceException e) {
            logger.error(e.getMessage());
            showErrorMessage(req, resp, MessageResourceReceiver.getString("message.error.add.parameter"));
        }
    }
}
