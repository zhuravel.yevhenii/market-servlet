package com.zhuravel.market_servlet.controller.admin.users;

import com.zhuravel.market_servlet.config.MessageResourceReceiver;
import com.zhuravel.market_servlet.config.security.Authenticated;
import com.zhuravel.market_servlet.controller.BaseServlet;
import com.zhuravel.market_servlet.model.LocaleType;
import com.zhuravel.market_servlet.model.RoleType;
import com.zhuravel.market_servlet.model.entity.Role;
import com.zhuravel.market_servlet.model.entity.UserStatus;
import com.zhuravel.market_servlet.model.pageable.Pageable;
import com.zhuravel.market_servlet.service.RoleService;
import com.zhuravel.market_servlet.service.UserService;
import com.zhuravel.market_servlet.service.UserStatusService;
import com.zhuravel.market_servlet.service.dto.UserDto;
import com.zhuravel.market_servlet.service.exception.ServiceException;
import com.zhuravel.market_servlet.service.impl.RoleServiceImpl;
import com.zhuravel.market_servlet.service.impl.UserServiceImpl;
import com.zhuravel.market_servlet.service.impl.UserStatusServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

import static com.zhuravel.market_servlet.config.Constants.Attributes.ATTRIBUTE_ROLES;
import static com.zhuravel.market_servlet.config.Constants.Attributes.ATTRIBUTE_STATUSES;
import static com.zhuravel.market_servlet.config.Constants.Attributes.ATTRIBUTE_USER;
import static com.zhuravel.market_servlet.config.Constants.Mapping.MAPPING_USERS;
import static com.zhuravel.market_servlet.config.Constants.Mapping.MAPPING_USERS_EDIT;

/**
 * @author Evgenii Zhuravel created on 28.06.2022
 * @version 1.0
 */
@Authenticated(role = RoleType.ROLE_ADMIN)
@WebServlet(urlPatterns = MAPPING_USERS_EDIT)
public class EditUserServlet extends BaseServlet {
    private static final Logger logger = LoggerFactory.getLogger(EditUserServlet.class);

    private static final String PAGE_USER_EDIT = "/WEB-INF/templates/admin/admin-user-edit.jsp";

    private UserService userService;

    private RoleService roleService;

    private UserStatusService userStatusService;

    @Override
    public void init() {
        userService = new UserServiceImpl();
        roleService = new RoleServiceImpl();
        userStatusService = new UserStatusServiceImpl();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String id = req.getParameter("id");
        try {
            Long userId = Long.parseLong(id);
            UserDto userDto = userService.getById(userId);

            List<Role> roles = roleService.getAll(new Pageable()).getContent();
            List<UserStatus> statuses = userStatusService.getAll(new Pageable()).getContent();

            req.setAttribute(ATTRIBUTE_USER, userDto);
            req.setAttribute(ATTRIBUTE_ROLES, roles);
            req.setAttribute(ATTRIBUTE_STATUSES, statuses);

            req.getRequestDispatcher(PAGE_USER_EDIT).forward(req, resp);
        } catch (NumberFormatException | ServiceException e) {
            logger.error(e.getMessage());
            showErrorMessage(req, resp, MessageResourceReceiver.getString("message.error.retrieve_edit_category_page"));
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
        logger.debug("Method Post for " + MAPPING_USERS_EDIT);
        try {
            req.setCharacterEncoding("UTF-8");

            Role role = new Role();
            role.setId(Long.parseLong(req.getParameter("role")));
            UserStatus userStatus = new UserStatus();
            userStatus.setId(Long.parseLong(req.getParameter("status")));

            UserDto userDto = new UserDto();
            userDto.setId(Long.parseLong(req.getParameter("id")));
            userDto.setLogin(req.getParameter("login"));
            userDto.setFirstName(req.getParameter("firstName"));
            userDto.setLastName(req.getParameter("lastName"));
            userDto.setPassword(req.getParameter("password"));
            userDto.setEmail(req.getParameter("email"));
            userDto.setLocale(LocaleType.valueOf(req.getParameter("locale")));
            userDto.setStatus(userStatus);
            userDto.setRole(role);

            userService.update(userDto);

            resp.sendRedirect(req.getContextPath() + MAPPING_USERS);
        } catch (NumberFormatException | ServiceException e) {
            logger.error(e.getMessage());
            showErrorMessage(req, resp, MessageResourceReceiver.getString("message.error.edit.user"));
        }
    }
}
