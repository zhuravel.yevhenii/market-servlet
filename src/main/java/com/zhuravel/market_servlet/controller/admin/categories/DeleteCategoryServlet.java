package com.zhuravel.market_servlet.controller.admin.categories;

import com.zhuravel.market_servlet.config.security.Authenticated;
import com.zhuravel.market_servlet.model.RoleType;
import com.zhuravel.market_servlet.service.dto.CategoryDto;
import com.zhuravel.market_servlet.service.impl.CategoryServiceImpl;

import javax.servlet.annotation.WebServlet;

import static com.zhuravel.market_servlet.config.Constants.Mapping.MAPPING_CATEGORY_DELETE;

/**
 * @author Evgenii Zhuravel created on 23.06.2022
 * @version 1.0
 */
@Authenticated(role = RoleType.ROLE_ADMIN)
@WebServlet(urlPatterns = MAPPING_CATEGORY_DELETE)
public class DeleteCategoryServlet extends BaseDeleteServlet<CategoryDto> {

    @Override
    public void init() {
        super.init(new CategoryServiceImpl());
    }
}
