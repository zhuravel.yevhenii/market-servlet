package com.zhuravel.market_servlet.controller.admin.categories;

import com.zhuravel.market_servlet.config.security.Authenticated;
import com.zhuravel.market_servlet.model.RoleType;
import com.zhuravel.market_servlet.service.dto.ParameterDto;
import com.zhuravel.market_servlet.service.impl.ParameterServiceImpl;

import javax.servlet.annotation.WebServlet;

import static com.zhuravel.market_servlet.config.Constants.Mapping.MAPPING_PARAMETER_DELETE;

/**
 * @author Evgenii Zhuravel created on 23.06.2022
 * @version 1.0
 */
@Authenticated(role = RoleType.ROLE_ADMIN)
@WebServlet(urlPatterns = MAPPING_PARAMETER_DELETE)
public class DeleteParameterServlet extends BaseDeleteServlet<ParameterDto> {

    @Override
    public void init() {
        super.init(new ParameterServiceImpl());
    }
}
