package com.zhuravel.market_servlet.controller.admin.orders;

import com.zhuravel.market_servlet.config.MessageResourceReceiver;
import com.zhuravel.market_servlet.config.security.Authenticated;
import com.zhuravel.market_servlet.controller.BaseServlet;
import com.zhuravel.market_servlet.controller.PageableServlet;
import com.zhuravel.market_servlet.dao.exception.DaoException;
import com.zhuravel.market_servlet.model.OrderStatusType;
import com.zhuravel.market_servlet.model.RoleType;
import com.zhuravel.market_servlet.model.entity.ShoppingCart;
import com.zhuravel.market_servlet.model.pageable.Page;
import com.zhuravel.market_servlet.model.pageable.Pageable;
import com.zhuravel.market_servlet.service.ShoppingCartService;
import com.zhuravel.market_servlet.service.dto.OrderDto;
import com.zhuravel.market_servlet.service.dto.ShoppingCartDto;
import com.zhuravel.market_servlet.service.dto.builder.ShoppingCartDtoBuilder;
import com.zhuravel.market_servlet.service.exception.ServiceException;
import com.zhuravel.market_servlet.service.impl.ShoppingCartServiceImpl;
import com.zhuravel.market_servlet.utils.DateTimeConverter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

import static com.zhuravel.market_servlet.config.Constants.Attributes.ATTRIBUTE_ORDERS;
import static com.zhuravel.market_servlet.config.Constants.Attributes.ATTRIBUTE_STATUSES;
import static com.zhuravel.market_servlet.config.Constants.Mapping.MAPPING_ORDERS;

/**
 * @author Evgenii Zhuravel created on 06.07.2022
 * @version 1.0
 */
@Authenticated(role = RoleType.ROLE_ADMIN)
@WebServlet(urlPatterns = MAPPING_ORDERS)
public class OrdersServlet extends BaseServlet implements PageableServlet {
    private static final Logger logger = LoggerFactory.getLogger(OrdersServlet.class);

    private static final String PAGE_ORDERS = "/WEB-INF/templates/admin/admin-orders.jsp";

    private static final String PARAMETER_CURRENT_PAGE_NAVIGATOR = "currentPageNavigator";

    private static final String ATTRIBUTE_DATES = "dates";

    private ShoppingCartService shoppingCartService;

    @Override
    public void init() {
        this.shoppingCartService = new ShoppingCartServiceImpl();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        logger.debug("doGet");

        try {
            Page<ShoppingCart> cartPage = shoppingCartService.getAll(getProductsPage(req));

            ShoppingCartDtoBuilder shoppingCartDtoBuilder = new ShoppingCartDtoBuilder();

            ShoppingCartDto products = shoppingCartDtoBuilder.build(null, cartPage.getContent());

            setRequestAttributesAndForward(req, resp, products);
        } catch (ServiceException | DaoException e) {
            logger.error(e.getMessage());
            showErrorMessage(req, resp, MessageResourceReceiver.getString("message.error.retrieving.orders"));
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        logger.debug("doGet");
        try {
            ShoppingCartDto products = getFilteredOrders(req);

            setRequestAttributesAndForward(req, resp, products);
        } catch (ServiceException e) {
            logger.error(e.getMessage());
            showErrorMessage(req, resp, MessageResourceReceiver.getString("message.error.retrieving.users"));
        }
    }

    private void setRequestAttributesAndForward(HttpServletRequest req, HttpServletResponse resp, ShoppingCartDto products) throws ServletException, IOException {
        List<Long> dates = products.getOrders().stream()
                .map(OrderDto::getRegistrationDateTime)
                .collect(Collectors.toList());

        req.setAttribute(ATTRIBUTE_DATES, dates);
        req.setAttribute(ATTRIBUTE_STATUSES, List.of(OrderStatusType.values()));
        req.setAttribute(ATTRIBUTE_ORDERS, products.getOrders());

        req.getRequestDispatcher(PAGE_ORDERS).forward(req, resp);
    }

    private ShoppingCartDto getFilteredOrders(HttpServletRequest req) throws ServiceException {
        long dateFrom = 0L;
        String filter_date_from = req.getParameter("filter_date_from");
        if (filter_date_from != null && !filter_date_from.isEmpty()) {
            dateFrom = Long.parseLong(filter_date_from);
            req.setAttribute("filter_date_from", dateFrom);
        }

        long dateUntil = DateTimeConverter.getMilliseconds(LocalDateTime.now());
        String filter_date_until = req.getParameter("filter_date_until");
        if (filter_date_until != null && !filter_date_until.isEmpty()) {
            dateUntil = Long.parseLong(filter_date_until);
            req.setAttribute("filter_date_until", dateUntil);
        }

        long statusId = 0L;
        String filter_status_id = req.getParameter("filter_status");
        if (filter_status_id != null && !filter_status_id.isEmpty()) {
            statusId = OrderStatusType.valueOf(filter_status_id).ordinal() + 1;
            req.setAttribute("filter_status", filter_status_id);
        }

        return shoppingCartService.getAll(dateFrom, dateUntil, statusId, getProductsPage(req));
    }

    private Pageable getProductsPage(HttpServletRequest request) {
        String pageParameter = request.getParameter(PARAMETER_CURRENT_PAGE_NAVIGATOR);

        if (pageParameter == null) {
            pageParameter = DEFAULT_PAGE;
        }
        return new Pageable(Integer.parseInt(pageParameter), 100);
    }
}
