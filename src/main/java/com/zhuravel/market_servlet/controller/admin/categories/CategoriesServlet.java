package com.zhuravel.market_servlet.controller.admin.categories;

import com.zhuravel.market_servlet.config.MessageResourceReceiver;
import com.zhuravel.market_servlet.config.security.Authenticated;
import com.zhuravel.market_servlet.controller.BaseServlet;
import com.zhuravel.market_servlet.controller.PageableServlet;
import com.zhuravel.market_servlet.model.RoleType;
import com.zhuravel.market_servlet.model.pageable.Page;
import com.zhuravel.market_servlet.model.pageable.Pageable;
import com.zhuravel.market_servlet.service.CategoryService;
import com.zhuravel.market_servlet.service.LocalizedCategoryService;
import com.zhuravel.market_servlet.service.dto.CategoryDto;
import com.zhuravel.market_servlet.service.dto.LocalizedCategoryDto;
import com.zhuravel.market_servlet.service.exception.ServiceException;
import com.zhuravel.market_servlet.service.impl.CategoryServiceImpl;
import com.zhuravel.market_servlet.service.impl.LocalizedCategoryServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static com.zhuravel.market_servlet.config.Constants.Attributes.ATTRIBUTE_CATEGORY;
import static com.zhuravel.market_servlet.config.Constants.Attributes.ATTRIBUTE_PARENT_CATEGORIES;
import static com.zhuravel.market_servlet.config.Constants.Attributes.ATTRIBUTE_PARENT_CATEGORY;
import static com.zhuravel.market_servlet.config.Constants.Mapping.MAPPING_CATEGORIES;
import static com.zhuravel.market_servlet.config.Constants.Parameters.PARAMETER_ID;

/**
 * @author Evgenii Zhuravel created on 22.06.2022
 * @version 1.0
 */
@Authenticated(role = RoleType.ROLE_ADMIN)
@WebServlet(urlPatterns = MAPPING_CATEGORIES)
public class CategoriesServlet extends BaseServlet implements PageableServlet {
    private static final Logger logger = LoggerFactory.getLogger(CategoriesServlet.class);

    private static final String PAGE_CATEGORIES = "/WEB-INF/templates/admin/admin-categories.jsp";

    private static final String PARAMETER_PAGE_NUMBER_NAVIGATOR = "currentPageNavigator";

    private static final String ATTRIBUTE_CATEGORIES = "categories";

    protected LocalizedCategoryService localizedCategoryService;

    protected CategoryService categoryService;

    @Override
    public void init() throws ServletException {
        this.localizedCategoryService = new LocalizedCategoryServiceImpl();
        this.categoryService = new CategoryServiceImpl();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            String id = req.getParameter(PARAMETER_ID);

            long categoryId;
            try {
                categoryId = Long.parseLong(id);
            } catch (NumberFormatException e) {
                categoryId = 0L;
            }

            if (categoryId != 0L) {
                prepareSubCategory(categoryId, req, resp);
            } else {
                prepareRootCategory(req, resp);
            }
        } catch (ServiceException ex) {
            logger.error(ex.getMessage());
            showErrorMessage(req, resp, MessageResourceReceiver.getString("message.error.retrieving_catalog_page"));
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setCharacterEncoding("UTF-8");

        this.doGet(req, resp);
    }

    private void prepareRootCategory(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException, ServiceException {
        logger.info("Get root Categories");

        LocalizedCategoryDto category = localizedCategoryService.getById(0L);

        Page<CategoryDto> categories = getFilteredCategories(0L, req);

        Page<LocalizedCategoryDto> upperCategories = localizedCategoryService.getSubCategories(0L, getNavigatorRequestPage(req));

        setRequestPage(req, getRequestPage(req), categories.getTotalPages());

        req.setAttribute(ATTRIBUTE_CATEGORY, category);
        req.setAttribute(ATTRIBUTE_PARENT_CATEGORIES, upperCategories.getContent());
        req.setAttribute(ATTRIBUTE_CATEGORIES, categories.getContent());

        req.getRequestDispatcher(PAGE_CATEGORIES).forward(req, resp);
    }

    private void prepareSubCategory(Long categoryId, HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException, ServiceException {
        LocalizedCategoryDto category = localizedCategoryService.getById(categoryId);

        Page<CategoryDto> categories = getFilteredCategories(categoryId, req);

        Long parentCategoryId = category.getParentCategoryId();

        Page<LocalizedCategoryDto> subCategories = localizedCategoryService.getSubCategories(parentCategoryId, getNavigatorRequestPage(req));

        req.setAttribute(ATTRIBUTE_CATEGORY, category);
        req.setAttribute(ATTRIBUTE_PARENT_CATEGORIES, subCategories.getContent());

        if (parentCategoryId != 0) {
            req.setAttribute(ATTRIBUTE_PARENT_CATEGORY, localizedCategoryService.getById(parentCategoryId));
        }

        setRequestPage(req, getRequestPage(req), categories.getTotalPages());
        req.setAttribute(ATTRIBUTE_CATEGORIES, categories.getContent());

        req.getRequestDispatcher(PAGE_CATEGORIES).forward(req, resp);
    }

    private Page<CategoryDto> getFilteredCategories(Long categoryId, HttpServletRequest req) throws ServiceException {
        Page<CategoryDto> categories;
        String filterEn = req.getParameter("filter_en");
        String filterUk = req.getParameter("filter_uk");

        if (filterEn != null && !filterEn.isEmpty()
                || filterUk != null && !filterUk.isEmpty()) {

            if (filterEn == null) {
                filterEn = "";
            }
            if (filterUk == null) {
                filterUk = "";
            }
            req.setAttribute("filter_en", filterEn);
            req.setAttribute("filter_uk", filterUk);

            categories = categoryService.getSubCategories(categoryId, filterEn, filterUk, getRequestPage(req));
        } else {
            categories = categoryService.getSubCategories(categoryId, getRequestPage(req));
        }
        return categories;
    }

    private Pageable getNavigatorRequestPage(HttpServletRequest request) {
        String pageParameter = request.getParameter(PARAMETER_PAGE_NUMBER_NAVIGATOR);

        if (pageParameter == null) {
            pageParameter = DEFAULT_PAGE;
        }
        return new Pageable(Integer.parseInt(pageParameter), 20);
    }
}
