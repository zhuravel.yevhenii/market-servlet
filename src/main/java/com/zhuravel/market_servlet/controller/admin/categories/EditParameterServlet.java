package com.zhuravel.market_servlet.controller.admin.categories;

import com.zhuravel.market_servlet.config.MessageResourceReceiver;
import com.zhuravel.market_servlet.config.security.Authenticated;
import com.zhuravel.market_servlet.controller.BaseServlet;
import com.zhuravel.market_servlet.model.RoleType;
import com.zhuravel.market_servlet.service.ParameterService;
import com.zhuravel.market_servlet.service.dto.ParameterDto;
import com.zhuravel.market_servlet.service.exception.ServiceException;
import com.zhuravel.market_servlet.service.impl.ParameterServiceImpl;
import com.zhuravel.market_servlet.service.validation.Validator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Map;

import static com.zhuravel.market_servlet.config.Constants.Attributes.ATTRIBUTE_CATEGORY_ID;
import static com.zhuravel.market_servlet.config.Constants.Attributes.ATTRIBUTE_PARAMETER;
import static com.zhuravel.market_servlet.config.Constants.Mapping.MAPPING_CATEGORY_EDIT;
import static com.zhuravel.market_servlet.config.Constants.Mapping.MAPPING_PARAMETER_EDIT;
import static com.zhuravel.market_servlet.config.Constants.Mapping.MAPPING_REGISTRATION;
import static com.zhuravel.market_servlet.config.Constants.SessionAttributes.ATTRIBUTE_ERRORS;

/**
 * @author Evgenii Zhuravel created on 28.06.2022
 * @version 1.0
 */
@Authenticated(role = RoleType.ROLE_ADMIN)
@WebServlet(urlPatterns = MAPPING_PARAMETER_EDIT)
public class EditParameterServlet extends BaseServlet {
    private static final Logger logger = LoggerFactory.getLogger(EditParameterServlet.class);

    private static final String PAGE_PARAMETER_EDIT = "/WEB-INF/templates/admin/admin-parameter-edit.jsp";

    private ParameterService parameterService;

    @Override
    public void init() {
        parameterService = new ParameterServiceImpl();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String id = req.getParameter("id");
        try {
            Long parameterId = Long.parseLong(id);
            ParameterDto parameter = parameterService.getById(parameterId);

            Long categoryId = Long.parseLong(req.getParameter(ATTRIBUTE_CATEGORY_ID));

            req.setAttribute(ATTRIBUTE_CATEGORY_ID, categoryId);
            req.setAttribute(ATTRIBUTE_PARAMETER, parameter);

            req.getRequestDispatcher(PAGE_PARAMETER_EDIT).forward(req, resp);
        } catch (NumberFormatException | ServiceException e) {
            logger.error(e.getMessage());
            showErrorMessage(req, resp, MessageResourceReceiver.getString("message.error.retrieve_edit_category_page"));
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
        logger.debug("Method Post for " + MAPPING_REGISTRATION);
        try {
            req.setCharacterEncoding("UTF-8");

            ParameterDto parameterDto = new ParameterDto();
            parameterDto.setId(Long.parseLong(req.getParameter("id")));
            parameterDto.setTypeId(Long.parseLong(req.getParameter("typeId")));
            parameterDto.setTypeName(req.getParameter("typeName"));
            parameterDto.setValue(req.getParameter("value"));

            Map<String, String> errors = Validator.validate(parameterDto);

            String categoryId = req.getParameter(ATTRIBUTE_CATEGORY_ID);

            if (!errors.isEmpty()) {
                req.setAttribute(ATTRIBUTE_ERRORS, errors);
                req.setAttribute(ATTRIBUTE_PARAMETER, parameterDto);
                req.setAttribute("id", req.getParameter("id"));
                req.setAttribute(ATTRIBUTE_CATEGORY_ID, categoryId);

                req.getRequestDispatcher(PAGE_PARAMETER_EDIT).forward(req, resp);
            } else {
                parameterService.update(parameterDto);

                resp.sendRedirect(req.getContextPath() + MAPPING_CATEGORY_EDIT
                        + "?id=" + categoryId
                        + "&type=" + parameterDto.getTypeId());
            }
        } catch (ServiceException e) {
            logger.error(e.getMessage());
            showErrorMessage(req, resp, MessageResourceReceiver.getString("message.error.save.parameter"));
        }
    }
}
