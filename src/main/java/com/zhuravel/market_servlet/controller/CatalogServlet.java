package com.zhuravel.market_servlet.controller;

import com.zhuravel.market_servlet.config.MessageResourceReceiver;
import com.zhuravel.market_servlet.dao.mapper.ProductMapper;
import com.zhuravel.market_servlet.model.entity.LocalizedParameterType;
import com.zhuravel.market_servlet.model.pageable.Page;
import com.zhuravel.market_servlet.model.pageable.Pageable;
import com.zhuravel.market_servlet.service.LocalizedCategoryService;
import com.zhuravel.market_servlet.service.LocalizedParameterTypeService;
import com.zhuravel.market_servlet.service.ProducerService;
import com.zhuravel.market_servlet.service.ProductService;
import com.zhuravel.market_servlet.service.dto.LocalizedCategoryDto;
import com.zhuravel.market_servlet.service.dto.ProductDto;
import com.zhuravel.market_servlet.service.dto.ProductsCatalogDto;
import com.zhuravel.market_servlet.service.dto.SelectedParametersDto;
import com.zhuravel.market_servlet.service.dto.builder.SelectedParametersDtoBuilder;
import com.zhuravel.market_servlet.service.exception.ServiceException;
import com.zhuravel.market_servlet.service.filter_criteria.ParameterSearchCriteria;
import com.zhuravel.market_servlet.service.filter_criteria.ParameterSearchCriteriaBuilder;
import com.zhuravel.market_servlet.service.impl.LocalizedCategoryServiceImpl;
import com.zhuravel.market_servlet.service.impl.LocalizedParameterTypeServiceImpl;
import com.zhuravel.market_servlet.service.impl.ProducerServiceImpl;
import com.zhuravel.market_servlet.service.impl.ProductServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static com.zhuravel.market_servlet.config.Constants.Attributes.ATTRIBUTE_CATEGORIES;
import static com.zhuravel.market_servlet.config.Constants.Attributes.ATTRIBUTE_CATEGORY;
import static com.zhuravel.market_servlet.config.Constants.Attributes.ATTRIBUTE_PARENT_CATEGORIES;
import static com.zhuravel.market_servlet.config.Constants.Attributes.ATTRIBUTE_PARENT_CATEGORY;
import static com.zhuravel.market_servlet.config.Constants.Attributes.ATTRIBUTE_PRODUCT_LIST;
import static com.zhuravel.market_servlet.config.Constants.Attributes.FILTER_PARAMETERS;
import static com.zhuravel.market_servlet.config.Constants.Mapping.MAPPING_CATALOG;

/**
 * @author Evgenii Zhuravel created on 19.05.2022
 * @version 1.0
 */
@WebServlet(name = "Catalog", urlPatterns = MAPPING_CATALOG)
public class CatalogServlet extends BaseServlet implements PageableServlet {

    private static final Logger logger = LoggerFactory.getLogger(CatalogServlet.class);

    private static final String PAGE_CATEGORIES = "/WEB-INF/templates/categories.jsp";

    private static final String PAGE_CATALOG = "/WEB-INF/templates/catalog.jsp";

    private static final String PARAMETER_CURRENT_PAGE_NAVIGATOR = "currentPageNavigator";

    private LocalizedCategoryService categoryService;

    private ProductService productService;

    private ProducerService producerService;

    private LocalizedParameterTypeService localizedParameterTypeService;

    @Override
    public void init() {
        this.categoryService = new LocalizedCategoryServiceImpl();
        this.productService = new ProductServiceImpl();
        this.producerService = new ProducerServiceImpl();
        this.localizedParameterTypeService = new LocalizedParameterTypeServiceImpl();
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String id = request.getParameter("id");
        try {
            long categoryId = Long.parseLong(id);

            if (categoryId != 0L) {
                request.getSession().removeAttribute(FILTER_PARAMETERS);

                prepareSubCategory(categoryId, request, response);
            } else {
                prepareRootCategory(request, response);
            }
        } catch (NumberFormatException e) {
            try {
                prepareRootCategory(request, response);
            } catch (ServiceException ex) {
                logger.error(e.getMessage());
                showErrorMessage(request, response, MessageResourceReceiver.getString("message.error.retrieving_catalog_page"));
            }
        } catch (ServiceException e) {
            logger.error(e.getMessage());
            showErrorMessage(request, response, MessageResourceReceiver.getString("message.error.retrieving_catalog_page"));
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            request.setCharacterEncoding("UTF-8");

            String id = request.getParameter("id");
            if (id != null) {
                Long categoryId = Long.parseLong(id);

                prepareSubCategory(categoryId, request, response);
            } else {
                prepareRootCategory(request, response);
            }
        } catch (ServiceException e) {
            logger.error(e.getMessage());
            showErrorMessage(request, response, MessageResourceReceiver.getString("message.error.retrieving_catalog_page"));
        }
    }

    public void prepareRootCategory(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, ServiceException {
        logger.info("Get root Categories");

        LocalizedCategoryDto category = categoryService.getById(0L);
        Page<LocalizedCategoryDto> categories = categoryService.getSubCategories(0L, getRequestPage(request));

        request.setAttribute(ATTRIBUTE_CATEGORY, category);
        request.setAttribute(ATTRIBUTE_PARENT_CATEGORIES, categories.getContent());
        request.setAttribute(ATTRIBUTE_CATEGORIES, categories.getContent());

        request.getRequestDispatcher(PAGE_CATEGORIES).forward(request, response);
    }

    private void prepareSubCategory(Long categoryId, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, ServiceException {
        LocalizedCategoryDto category = categoryService.getById(categoryId);

        Page<LocalizedCategoryDto> categories = categoryService.getSubCategories(categoryId, getRequestPage(request));

        Long parentCategoryId = category.getParentCategoryId();

        request.setAttribute(ATTRIBUTE_CATEGORY, category);
        request.setAttribute(ATTRIBUTE_PARENT_CATEGORIES, categoryService.getSubCategories(parentCategoryId, getNavigatorRequestPage(request)).getContent());

        if (parentCategoryId != 0) {
            request.setAttribute(ATTRIBUTE_PARENT_CATEGORY, categoryService.getById(parentCategoryId));
        }

        if (categories.getContent().size() > 0) {
            request.setAttribute(ATTRIBUTE_CATEGORIES, categories.getContent());

            request.getRequestDispatcher(PAGE_CATEGORIES).forward(request, response);
        } else {
            prepareCatalogPage(request, response, categoryId);
        }
    }

    private void prepareCatalogPage(HttpServletRequest request, HttpServletResponse response, Long categoryId) throws ServletException, IOException, ServiceException {

        SelectedParametersDto selectedParameters = initSelectedParameters(request, categoryId);

        ParameterSearchCriteria criteria = ParameterSearchCriteriaBuilder.build(categoryId, selectedParameters);

        Pageable page = getRequestPage(request);

        ProductsCatalogDto productsCatalogDTO = productService.getAll(criteria, initSortingField(request), page);

        Page<ProductDto> products = productsCatalogDTO.getProducts();

        setRequestPage(request, page, products.getTotalPages());

        request.setAttribute(ATTRIBUTE_PRODUCT_LIST, products.getContent());

        request.getRequestDispatcher(PAGE_CATALOG).forward(request, response);
    }

    private Pageable getNavigatorRequestPage(HttpServletRequest request) {
        String pageParameter = request.getParameter(PARAMETER_CURRENT_PAGE_NAVIGATOR);

        if (pageParameter == null) {
            pageParameter = DEFAULT_PAGE;
        }
        return new Pageable(Integer.parseInt(pageParameter), 20);
    }

    private SelectedParametersDto initSelectedParameters(HttpServletRequest req, Long categoryId) throws ServiceException {
        SelectedParametersDto selectedParameters = SelectedParametersDtoBuilder.build(req);

        if (selectedParameters.getParameterTypes().size() == 0) {
            selectedParameters = (SelectedParametersDto) req.getSession().getAttribute(FILTER_PARAMETERS);

            if (selectedParameters == null || selectedParameters.isLangUpdated()) {
                Page<LocalizedParameterType> parameterTypes = localizedParameterTypeService.getAllByCategoryId(categoryId, new Pageable());

                selectedParameters = new SelectedParametersDtoBuilder().build(parameterTypes.getContent());

                SelectedParametersDtoBuilder.setProducers(selectedParameters, producerService.getDistinctByCategoryId(categoryId));
            }
        }
        req.getSession().setAttribute(FILTER_PARAMETERS, selectedParameters);

        return selectedParameters;
    }

    private String initSortingField(HttpServletRequest req) {
        String query = "";

        String sorting = req.getParameter("sorting");

        if (sorting != null) {
            switch (sorting.split("_")[0]) {
                case "name" -> query += ProductMapper.FIELD_NAME;
                case "price" -> query += ProductMapper.FIELD_PRICE;
                case "newest" -> query += ProductMapper.FIELD_CREATE_TIME + " DESC";
                case "oldest" -> query += ProductMapper.FIELD_CREATE_TIME;
                default -> throw new IllegalStateException("Unexpected value: " + sorting.split("_")[0]);
            }
            if (sorting.split("_").length > 1 && sorting.split("_")[1].equals("desc")) {
                query += " DESC";
            }

            req.setAttribute("sorting", sorting);
        }

        return query;
    }
}
