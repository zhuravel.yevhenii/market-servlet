package com.zhuravel.market_servlet.controller;

import com.zhuravel.market_servlet.config.MessageResourceReceiver;
import com.zhuravel.market_servlet.config.security.Authenticated;
import com.zhuravel.market_servlet.model.RoleType;
import com.zhuravel.market_servlet.service.UserService;
import com.zhuravel.market_servlet.service.dto.UserDto;
import com.zhuravel.market_servlet.service.exception.ServiceException;
import com.zhuravel.market_servlet.service.impl.UserServiceImpl;
import com.zhuravel.market_servlet.service.validation.Validator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Map;

import static com.zhuravel.market_servlet.config.Constants.Mapping.MAPPING_PROFILE;
import static com.zhuravel.market_servlet.config.Constants.Mapping.MAPPING_REGISTRATION;
import static com.zhuravel.market_servlet.config.Constants.SessionAttributes.ATTRIBUTE_USER;

/**
 * @author Evgenii Zhuravel created on 22.05.2022
 * @version 1.0
 */
@WebServlet(urlPatterns = MAPPING_PROFILE)
@Authenticated(role = {RoleType.ROLE_ADMIN, RoleType.ROLE_SELLER, RoleType.ROLE_USER})
public class UserProfileServlet extends BaseServlet {

    private static final Logger logger = LoggerFactory.getLogger(UserProfileServlet.class);

    private static final String PAGE_REGISTRATION = "/WEB-INF/templates/profile_edit.jsp";

    private static final String ATTRIBUTE_ACCOUNT_FORM = "accountForm";
    private static final String ATTRIBUTE_ERRORS = "errors";

    private UserService userService;

    @Override
    public void init() {
        this.userService = new UserServiceImpl();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        logger.debug("Method Get for " + MAPPING_REGISTRATION);

        UserDto user = (UserDto) req.getSession().getAttribute(ATTRIBUTE_USER);

        req.setAttribute(ATTRIBUTE_ACCOUNT_FORM, user);

        req.getRequestDispatcher(PAGE_REGISTRATION).forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        logger.debug("Method Post for " + MAPPING_REGISTRATION);
        try {
            UserDto accountForm = new UserDto();
            accountForm.setLogin(req.getParameter("login"));
            accountForm.setFirstName(req.getParameter("firstName"));
            accountForm.setLastName(req.getParameter("lastName"));
            accountForm.setEmail(req.getParameter("email"));

            Map<String, String> errors = Validator.validate(accountForm);

            if (!errors.isEmpty()) {
                req.setAttribute(ATTRIBUTE_ACCOUNT_FORM, accountForm);
                req.setAttribute(ATTRIBUTE_ERRORS, errors);

                req.getRequestDispatcher(PAGE_REGISTRATION).forward(req, resp);
            } else {
                UserDto user = (UserDto) req.getSession().getAttribute(ATTRIBUTE_USER);
                user.setLogin(accountForm.getLogin());
                user.setFirstName(accountForm.getFirstName());
                user.setLastName(accountForm.getLastName());
                user.setEmail(accountForm.getEmail());

                userService.save(user);

                req.getSession().setAttribute(ATTRIBUTE_USER, user);

                showSuccessMessage(req, resp, MessageResourceReceiver.getString("message.profile.edit.success"));
            }
        } catch (ServiceException e) {
            logger.error(e.getMessage());
            showErrorMessage(req, resp, MessageResourceReceiver.getString("message.profile.edit.error"));
        }
    }
}
