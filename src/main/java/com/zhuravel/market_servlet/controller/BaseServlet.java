package com.zhuravel.market_servlet.controller;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.IOException;

import static com.zhuravel.market_servlet.config.Constants.Attributes.ATTRIBUTE_MESSAGE;
import static com.zhuravel.market_servlet.config.Constants.Pages.PAGE_ERROR;
import static com.zhuravel.market_servlet.config.Constants.Pages.PAGE_SUCCESS;

/**
 * @author Evgenii Zhuravel created on 03.07.2022
 * @version 1.0
 */
public class BaseServlet extends HttpServlet {

    protected void showSuccessMessage(HttpServletRequest req, HttpServletResponse resp, String message) throws ServletException, IOException {
        req.setAttribute(ATTRIBUTE_MESSAGE, message);

        req.getServletContext().getRequestDispatcher(PAGE_SUCCESS).forward(req, resp);
    }

    protected void showErrorMessage(HttpServletRequest req, HttpServletResponse resp, String message) throws ServletException, IOException {
        req.setAttribute(ATTRIBUTE_MESSAGE, message);

        req.getServletContext().getRequestDispatcher(PAGE_ERROR).forward(req, resp);
    }
}
