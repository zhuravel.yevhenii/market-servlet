package com.zhuravel.market_servlet.dao;

import com.zhuravel.market_servlet.dao.exception.DaoException;
import com.zhuravel.market_servlet.model.entity.Category;
import com.zhuravel.market_servlet.model.pageable.Page;
import com.zhuravel.market_servlet.model.pageable.Pageable;

import java.util.Optional;

/**
 * @author Evgenii Zhuravel created on 20.05.2022
 * @version 1.0
 */
public interface CategoryDao extends BaseDao<Category> {

    Page<Category> findAllByParentCategoryId(Long parentCategoryId, Pageable pageable) throws DaoException;

    Page<Category> findAllByParentCategoryId(Long parentCategoryId, String filterEn, String filterUk, Pageable pageable) throws DaoException;

    Optional<Category> findById(Long id) throws DaoException;
}
