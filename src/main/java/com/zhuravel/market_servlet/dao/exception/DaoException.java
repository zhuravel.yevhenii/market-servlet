package com.zhuravel.market_servlet.dao.exception;

/**
 * @author Evgenii Zhuravel created on 08.06.2022
 * @version 1.0
 */
public class DaoException extends Exception {

    public DaoException(String message) {
        super(message);
    }
}
