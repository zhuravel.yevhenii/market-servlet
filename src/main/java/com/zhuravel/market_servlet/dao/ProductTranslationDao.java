package com.zhuravel.market_servlet.dao;

import com.zhuravel.market_servlet.model.entity.ProductTranslation;

/**
 * @author Evgenii Zhuravel created on 20.05.2022
 * @version 1.0
 */
public interface ProductTranslationDao extends BaseDao<ProductTranslation> {

}
