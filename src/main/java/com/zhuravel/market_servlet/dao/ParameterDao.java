package com.zhuravel.market_servlet.dao;

import com.zhuravel.market_servlet.dao.exception.DaoException;
import com.zhuravel.market_servlet.model.entity.Parameter;

import java.util.List;

/**
 * @author Evgenii Zhuravel created on 20.05.2022
 * @version 1.0
 */
public interface ParameterDao extends BaseDao<Parameter> {

    void addToProduct(Long productId, Parameter entity) throws DaoException;

    void removeAllFromProduct(Long productId) throws DaoException;

    List<Parameter> findAllByProductId(Long id) throws DaoException;

    List<Parameter> findByTypeId(Long id) throws DaoException;
}
