package com.zhuravel.market_servlet.dao;

import com.zhuravel.market_servlet.dao.exception.DaoException;
import com.zhuravel.market_servlet.model.entity.BaseEntity;
import com.zhuravel.market_servlet.model.pageable.Page;
import com.zhuravel.market_servlet.model.pageable.Pageable;

import java.util.List;
import java.util.Optional;

/**
 * @author Evgenii Zhuravel created on 08.06.2022
 * @version 1.0
 */
public interface BaseDao<T extends BaseEntity> {

    List<T> findAll() throws DaoException;

    Page<T> findAll(Pageable pageable) throws DaoException;

    List<T> findAll(String query, List<Object> values) throws DaoException;

    Page<T> findAll(String query, List<Object> values, Pageable pageable) throws DaoException;

    Optional<T> findById(Long id) throws DaoException;

    List<T> findALLById(Long id) throws DaoException;

    T save(T entity) throws DaoException;

    T update(T entity) throws DaoException;

    void deleteById(Long id) throws DaoException;
}
