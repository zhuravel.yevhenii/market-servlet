package com.zhuravel.market_servlet.dao;

import com.zhuravel.market_servlet.dao.exception.DaoException;
import com.zhuravel.market_servlet.model.entity.User;
import com.zhuravel.market_servlet.model.pageable.Page;
import com.zhuravel.market_servlet.model.pageable.Pageable;

import java.util.Map;
import java.util.Optional;

/**
 * @author Evgenii Zhuravel created on 20.05.2022
 * @version 1.0
 */
public interface UserDao extends BaseDao<User> {

    Optional<User> findByLogin(String login) throws DaoException;

    Page<User> findAll(Map<String, Object> filters, Pageable page) throws DaoException;
}
