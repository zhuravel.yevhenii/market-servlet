package com.zhuravel.market_servlet.dao;

import com.zhuravel.market_servlet.dao.exception.DaoException;
import com.zhuravel.market_servlet.model.entity.Product;
import com.zhuravel.market_servlet.model.pageable.Page;
import com.zhuravel.market_servlet.model.pageable.Pageable;
import com.zhuravel.market_servlet.service.filter_criteria.ParameterSearchCriteria;

/**
 * @author Evgenii Zhuravel created on 20.05.2022
 * @version 1.0
 */
public interface ProductDao extends BaseDao<Product> {

    void setLocale(String locale);

    Page<Product> findAllByProducerName(String name, Pageable pageable) throws DaoException;

    Page<Product> findAll(ParameterSearchCriteria criteria, String sort, Pageable pageable) throws DaoException;
}
