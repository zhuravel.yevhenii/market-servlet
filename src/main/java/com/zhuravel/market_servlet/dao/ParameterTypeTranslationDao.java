package com.zhuravel.market_servlet.dao;

import com.zhuravel.market_servlet.dao.exception.DaoException;
import com.zhuravel.market_servlet.model.entity.ParameterTypeTranslation;

import java.util.List;

/**
 * @author Evgenii Zhuravel created on 26.06.2022
 * @version 1.0
 */
public interface ParameterTypeTranslationDao extends BaseDao<ParameterTypeTranslation> {

    List<ParameterTypeTranslation> findAllByParameterTypeId(Long parameterTypeId) throws DaoException;
}
