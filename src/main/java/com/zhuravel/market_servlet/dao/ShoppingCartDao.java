package com.zhuravel.market_servlet.dao;

import com.zhuravel.market_servlet.dao.exception.DaoException;
import com.zhuravel.market_servlet.model.entity.ShoppingCart;
import com.zhuravel.market_servlet.model.pageable.Page;
import com.zhuravel.market_servlet.model.pageable.Pageable;

import java.util.List;

/**
 * @author Evgenii Zhuravel created on 20.05.2022
 * @version 1.0
 */
public interface ShoppingCartDao extends BaseDao<ShoppingCart> {

    List<ShoppingCart> findAllByUserId(Long userId) throws DaoException;

    Page<ShoppingCart> findAll(Long dateFrom, Long dateUntil, Long statusId, Pageable pageable) throws DaoException;

    void deleteByUserAndProductId(Long userId, Long productId) throws DaoException;

    void updateByUserAndProductId(Long userId, Long productId, ShoppingCart shoppingCart) throws DaoException;

    void updateQuantityByUserAndProductId(Long userId, Long productId, Integer quantity) throws DaoException;
}
