package com.zhuravel.market_servlet.dao.impl;

import com.zhuravel.market_servlet.config.DataSource;
import com.zhuravel.market_servlet.dao.CategoryTranslationDao;
import com.zhuravel.market_servlet.dao.exception.DaoException;
import com.zhuravel.market_servlet.dao.mapper.CategoryTranslationMapper;
import com.zhuravel.market_servlet.model.entity.CategoryTranslation;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Evgenii Zhuravel created on 19.05.2022
 */
public class CategoryTranslationDaoImpl extends BaseDaoImpl<CategoryTranslation, CategoryTranslationMapper> implements CategoryTranslationDao {

    public CategoryTranslationDaoImpl() {
        super(CategoryTranslationMapper.TABLE_NAME, new CategoryTranslationMapper());
    }

    @Override
    public List<CategoryTranslation> findAllByCategoryId(Long categoryId) throws DaoException {
        String query = "SELECT * FROM " + CategoryTranslationMapper.TABLE_NAME
                + " WHERE " + CategoryTranslationMapper.FIELD_ID + "=?";

        try (Connection con = DataSource.getConnection();
             PreparedStatement pst = con.prepareStatement(query)) {
            pst.setLong(1, categoryId);

            try (ResultSet result = pst.executeQuery()) {
                List<CategoryTranslation> translations = new ArrayList<>();

                while (result.next()) {
                    translations.add(mapper.of(result));
                }
                return translations;
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
            throw new DaoException(ex.getMessage());
        }
    }

    @Override
    public CategoryTranslation save(CategoryTranslation entity) throws DaoException {
        logger.debug("save with id " + entity.getId());

        String fields = mapper.getSavedFields(entity);

        String query = "INSERT INTO " + tableName + fields;

        try (Connection conn = DataSource.getConnection();
             PreparedStatement insertStatement = conn.prepareStatement(query)) {

            mapper.setSaveStatement(insertStatement, entity);

            insertStatement.executeUpdate();

            return entity;
        } catch (SQLException ex) {
            ex.printStackTrace();
            throw new DaoException(ex.getMessage());
        }
    }

    @Override
    public CategoryTranslation update(CategoryTranslation entity) throws DaoException {
        String fields = mapper.getUpdatedFields(entity);

        String query = "UPDATE " + tableName + " SET " + fields + WHERE + ID + "=? AND " + CategoryTranslationMapper.FIELD_LOCALE + " LIKE ?";

        try (Connection conn = DataSource.getConnection();
             PreparedStatement pst = conn.prepareStatement(query)) {

            Integer nextIndex = mapper.setUpdateStatement(pst, entity);

            pst.setLong(nextIndex++, entity.getId());
            pst.setString(nextIndex, entity.getLocale());

            pst.executeUpdate();

            return entity;
        } catch (SQLException ex) {
            ex.printStackTrace();
            throw new DaoException(ex.getMessage());
        }
    }
}
