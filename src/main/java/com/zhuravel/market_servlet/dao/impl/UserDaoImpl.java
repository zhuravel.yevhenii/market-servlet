package com.zhuravel.market_servlet.dao.impl;

import com.zhuravel.market_servlet.config.DataSource;
import com.zhuravel.market_servlet.dao.RoleDao;
import com.zhuravel.market_servlet.dao.UserDao;
import com.zhuravel.market_servlet.dao.UserStatusDao;
import com.zhuravel.market_servlet.dao.exception.DaoException;
import com.zhuravel.market_servlet.dao.mapper.UserMapper;
import com.zhuravel.market_servlet.model.entity.Role;
import com.zhuravel.market_servlet.model.entity.User;
import com.zhuravel.market_servlet.model.entity.UserStatus;
import com.zhuravel.market_servlet.model.pageable.Page;
import com.zhuravel.market_servlet.model.pageable.Pageable;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.StringJoiner;

/**
 * @author Evgenii Zhuravel created on 08.06.2022
 * @version 1.0
 */
public class UserDaoImpl extends BaseDaoImpl<User, UserMapper> implements UserDao {

    private final RoleDao roleDao;
    private final UserStatusDao userStatusDao;

    public UserDaoImpl() {
        super(UserMapper.TABLE_NAME, new UserMapper());

        this.roleDao = new RoleDaoImpl();
        this.userStatusDao = new UserStatusDaoImpl();
    }

    @Override
    public Optional<User> findById(Long id) throws DaoException {
        logger.debug("findById " + id);

        Optional<User> optionalUser = super.findById(id);
        if (optionalUser.isPresent()) {
            initUser(optionalUser.get());

            return optionalUser;
        } else {
            return Optional.empty();
        }
    }

    @Override
    public Optional<User> findByLogin(String login) throws DaoException {
        logger.debug("findByLogin " + login);

        String query = selectFromTable + WHERE + UserMapper.FIELD_LOGIN + "=?";
        try (Connection con = DataSource.getConnection();
             PreparedStatement pst = con.prepareStatement(query)) {
            pst.setString(1, login);

            try (ResultSet result = pst.executeQuery()) {
                if (result.next()) {
                    return Optional.of(initUser(mapper.of(result)));
                }
            }

            return Optional.empty();
        } catch (SQLException ex) {
            ex.printStackTrace();
            throw new DaoException(ex.getMessage());
        }
    }

    @Override
    public Page<User> findAll(Pageable pageable) throws DaoException {
        Page<User> userPage = super.findAll(pageable);

        initUsers(userPage.getContent());

        return userPage;
    }

    @Override
    public Page<User> findAll(Map<String, Object> filters, Pageable pageable) throws DaoException {
        StringJoiner joiner = new StringJoiner(" AND ");

        List<Object> values = new ArrayList<>();

        for (Map.Entry<String, Object> entry : filters.entrySet()) {
            if (entry.getValue() instanceof String) {
                joiner.add("(" + entry.getKey() + " LIKE '%?%') ");
                values.add(entry.getValue());
            }
            if (entry.getValue() instanceof Long) {
                joiner.add("(" + entry.getKey() + " = ?) ");
                values.add(entry.getValue());
            }
        }

        String query = "SELECT DISTINCT " + mapper.getSelectedFields() + " FROM " + tableName +
                " WHERE " + joiner;

        Page<User> userPage = super.findAll(query, values, pageable);

        initUsers(userPage.getContent());

        return userPage;
    }

    private void initUsers(List<User> users) throws DaoException {
        for (User user : users) {
            initUser(user);
        }
    }

    private User initUser(User user) throws DaoException {
        UserStatus status = userStatusDao.findById(user.getStatus().getId()).orElseThrow(() ->
                new DaoException("UserStatus with id " + user.getStatus().getId() + " not found!"));

        Role role = roleDao.findById(user.getRole().getId()).orElseThrow(() ->
                new DaoException("Role with id " + user.getRole().getId() + " not found!"));

        user.setStatus(status);
        user.setRole(role);

        return user;
    }
}
