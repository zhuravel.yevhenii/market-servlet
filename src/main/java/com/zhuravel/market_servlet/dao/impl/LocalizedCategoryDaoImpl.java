package com.zhuravel.market_servlet.dao.impl;

import com.zhuravel.market_servlet.config.DataSource;
import com.zhuravel.market_servlet.dao.LocalizedCategoryDao;
import com.zhuravel.market_servlet.dao.exception.DaoException;
import com.zhuravel.market_servlet.dao.mapper.LocalizedCategoryMapper;
import com.zhuravel.market_servlet.model.entity.LocalizedCategory;
import com.zhuravel.market_servlet.model.pageable.Page;
import com.zhuravel.market_servlet.model.pageable.Pageable;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Evgenii Zhuravel created on 19.05.2022
 */
public class LocalizedCategoryDaoImpl extends BaseDaoImpl<LocalizedCategory, LocalizedCategoryMapper> implements LocalizedCategoryDao {

    private String locale;

    public LocalizedCategoryDaoImpl() {
        super(LocalizedCategoryMapper.TABLE_NAME, new LocalizedCategoryMapper());
    }

    @Override
    public void setLocale(String locale) {
        this.locale = locale;
    }

    @Override
    public Page<LocalizedCategory> findAllByParentCategoryId(Long parentCategoryId, Pageable pageable) throws DaoException {
        String category = LocalizedCategoryMapper.TABLE_NAME;

        String query = getSelectQueryByLocale(category)
                + " AND " + category + "." + LocalizedCategoryMapper.FIELD_PARENT_CATEGORY_ID + "=?"
                + " LIMIT " + pageable.getSize() + " OFFSET " + pageable.getNumber() * pageable.getSize();

        try (Connection con = DataSource.getConnection();
             PreparedStatement pst = con.prepareStatement(query)) {
            pst.setString(1, locale);
            pst.setLong(2, parentCategoryId);

            try (ResultSet result = pst.executeQuery()) {
                List<LocalizedCategory> categories = new ArrayList<>();

                while (result.next()) {
                    categories.add(mapper.of(result));
                }
                return pageable.buildPage(categories, getTotalCount(query, List.of("'" + locale + "'", parentCategoryId)));
            }

        } catch (SQLException ex) {
            ex.printStackTrace();
            throw new DaoException(ex.getMessage());
        }
    }

    @Override
    public Optional<LocalizedCategory> findById(Long id) throws DaoException {
        logger.debug("findById " + id);

        String category = LocalizedCategoryMapper.TABLE_NAME;

        String query = getSelectQueryByLocale(category)
                + " AND " + category + "." + LocalizedCategoryMapper.FIELD_ID + "=?";

        try (Connection con = DataSource.getConnection();
             PreparedStatement pst = con.prepareStatement(query)) {
            pst.setString(1, locale);
            pst.setLong(2, id);

            try (ResultSet result = pst.executeQuery()) {
                if (result.next()) {
                    return Optional.of(mapper.of(result));
                }
            }

            return Optional.empty();
        } catch (SQLException ex) {
            ex.printStackTrace();
            throw new DaoException(ex.getMessage());
        }
    }

    private String getSelectQueryByLocale(String category) {
        String translation = LocalizedCategoryMapper.TABLE_NAME_TRANSLATION;

        return "SELECT " + mapper.getSelectedFields() + " FROM " + category
                + " INNER JOIN " + translation
                + " ON " + category + "." + LocalizedCategoryMapper.FIELD_ID + "=" + translation + "." + LocalizedCategoryMapper.FIELD_ID
                + " WHERE " + translation + "." + LocalizedCategoryMapper.FIELD_T_LOCALE + " LIKE ?";
    }
}
