package com.zhuravel.market_servlet.dao.impl;

import com.zhuravel.market_servlet.dao.ProducerDao;
import com.zhuravel.market_servlet.dao.exception.DaoException;
import com.zhuravel.market_servlet.dao.mapper.ProducerMapper;
import com.zhuravel.market_servlet.dao.mapper.ProductMapper;
import com.zhuravel.market_servlet.model.entity.Producer;

import java.util.List;

/**
 * @author Evgenii Zhuravel created on 09.06.2022
 * @version 1.0
 */
public class ProducerDaoImpl extends BaseDaoImpl<Producer, ProducerMapper> implements ProducerDao {

    public ProducerDaoImpl() {
        super(ProducerMapper.TABLE_NAME, new ProducerMapper());
    }

    @Override
    public List<Producer> findDistinctByCategoryId(Long categoryId) throws DaoException {
        String producer = ProducerMapper.TABLE_NAME;
        String product = ProductMapper.TABLE_NAME;

        String query = "SELECT DISTINCT * FROM " + tableName +
                " INNER JOIN " + product +
                " ON " + product + "." + ProductMapper.FIELD_PRODUCER_ID + "=" + producer + "." + ProducerMapper.FIELD_ID +
                WHERE + product + "." + ProductMapper.FIELD_CATEGORY_ID + "=?";

        return super.findAll(query, List.of(categoryId));
    }
}
