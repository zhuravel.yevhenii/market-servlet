package com.zhuravel.market_servlet.dao.impl;

import com.zhuravel.market_servlet.dao.RoleDao;
import com.zhuravel.market_servlet.dao.mapper.RoleMapper;
import com.zhuravel.market_servlet.model.entity.Role;

/**
 * @author Evgenii Zhuravel created on 09.06.2022
 * @version 1.0
 */
public class RoleDaoImpl extends BaseDaoImpl<Role, RoleMapper> implements RoleDao {

    public RoleDaoImpl() {
        super(RoleMapper.TABLE_NAME, new RoleMapper());
    }
}
