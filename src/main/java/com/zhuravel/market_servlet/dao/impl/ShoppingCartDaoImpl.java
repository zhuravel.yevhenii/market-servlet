package com.zhuravel.market_servlet.dao.impl;

import com.zhuravel.market_servlet.config.DataSource;
import com.zhuravel.market_servlet.dao.ShoppingCartDao;
import com.zhuravel.market_servlet.dao.exception.DaoException;
import com.zhuravel.market_servlet.dao.mapper.ShoppingCartMapper;
import com.zhuravel.market_servlet.model.entity.ShoppingCart;
import com.zhuravel.market_servlet.model.pageable.Page;
import com.zhuravel.market_servlet.model.pageable.Pageable;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Evgenii Zhuravel created on 08.06.2022
 * @version 1.0
 */
public class ShoppingCartDaoImpl extends BaseDaoImpl<ShoppingCart, ShoppingCartMapper> implements ShoppingCartDao {

    public ShoppingCartDaoImpl() {
        super(ShoppingCartMapper.TABLE_NAME, new ShoppingCartMapper());
    }

    @Override
    public Page<ShoppingCart> findAll(Pageable pageable) throws DaoException {
        logger.debug("findAll");
        return findAll(selectFromTable + " ORDER BY " + tableName + "." + ShoppingCartMapper.FIELD_STATUS_ID + " ASC", new ArrayList<>(), pageable);
    }

    @Override
    public List<ShoppingCart> findAllByUserId(Long userId) throws DaoException {
        logger.debug("findAllByUserId " + userId);

        String query = selectFromTable + WHERE + ShoppingCartMapper.FIELD_USER_ID + "=?";

        findAll(query, List.of(userId));
        
        try (Connection con = DataSource.getConnection();
             PreparedStatement pst = con.prepareStatement(query)) {
            pst.setLong(1, userId);

            List<ShoppingCart> shoppingCarts = new ArrayList<>();
            try (ResultSet result = pst.executeQuery()) {

                while (result.next()) {
                    shoppingCarts.add(mapper.of(result));
                }

            }

            return shoppingCarts;
        } catch (SQLException ex) {
            ex.printStackTrace();
            throw new DaoException(ex.getMessage());
        }
    }

    @Override
    public Page<ShoppingCart> findAll(Long dateFrom, Long dateUntil, Long statusId, Pageable pageable) throws DaoException {
        logger.debug("findAll");
        List<Object> values = new ArrayList<>();

        String query = selectFromTable + " WHERE " + ShoppingCartMapper.FIELD_CHECKOUT_DATE_TIME + ">=? AND " +
                ShoppingCartMapper.FIELD_CHECKOUT_DATE_TIME + "<=?";
        values.add(dateFrom);
        values.add(dateUntil);

        if (statusId != null && statusId > 0L) {
            query += " AND " + ShoppingCartMapper.FIELD_STATUS_ID + "=?";
            values.add(statusId);
        }

        return findAll(query, values, pageable);
    }

    @Override
    public void deleteByUserAndProductId(Long userId, Long productId) throws DaoException {
        logger.debug("deleteByUser: " + userId + " AndProductId: " + productId);

        String query = "DELETE FROM " + tableName +
                WHERE + ShoppingCartMapper.FIELD_USER_ID + "=? AND " +
                ShoppingCartMapper.FIELD_PRODUCT_ID + "=?";

        try (Connection con = DataSource.getConnection();
             PreparedStatement pst = con.prepareStatement(query)) {
            pst.setLong(1, userId);
            pst.setLong(2, productId);
            pst.executeUpdate();

        } catch (SQLException ex) {
            ex.printStackTrace();
            throw new DaoException(ex.getMessage());
        }
    }

    @Override
    public void updateByUserAndProductId(Long userId, Long productId, ShoppingCart shoppingCart) throws DaoException {
        String fields = mapper.getUpdatedFields(new ShoppingCart());

        String query = "UPDATE " + tableName + " SET " + fields +
                WHERE + ShoppingCartMapper.FIELD_USER_ID + "=? AND " +
                ShoppingCartMapper.FIELD_PRODUCT_ID + "=?";

        try (Connection conn = DataSource.getConnection();
             PreparedStatement pst = conn.prepareStatement(query)) {

            Integer nextIndex = mapper.setUpdateStatement(pst, shoppingCart);

            pst.setLong(nextIndex++, userId);
            pst.setLong(nextIndex, productId);

            pst.executeUpdate();

        } catch (SQLException ex) {
            ex.printStackTrace();
            throw new DaoException(ex.getMessage());
        }
    }

    @Override
    public void updateQuantityByUserAndProductId(Long userId, Long productId, Integer quantity) throws DaoException {
        String query = "UPDATE " + tableName + " SET " + ShoppingCartMapper.FIELD_QUANTITY + "=? " +
                WHERE + ShoppingCartMapper.FIELD_USER_ID + "=? AND " +
                ShoppingCartMapper.FIELD_PRODUCT_ID + "=?";

        try (Connection conn = DataSource.getConnection();
             PreparedStatement pst = conn.prepareStatement(query)) {

            pst.setLong(1, quantity);
            pst.setLong(2, userId);
            pst.setLong(3, productId);

            pst.executeUpdate();

        } catch (SQLException ex) {
            ex.printStackTrace();
            throw new DaoException(ex.getMessage());
        }
    }
}
