package com.zhuravel.market_servlet.dao.impl;

import com.zhuravel.market_servlet.config.DataSource;
import com.zhuravel.market_servlet.dao.ParameterDao;
import com.zhuravel.market_servlet.dao.exception.DaoException;
import com.zhuravel.market_servlet.dao.mapper.ParameterMapper;
import com.zhuravel.market_servlet.dao.mapper.ProductMapper;
import com.zhuravel.market_servlet.dao.mapper.ProductParameterMapper;
import com.zhuravel.market_servlet.model.entity.Parameter;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Evgenii Zhuravel created on 08.06.2022
 * @version 1.0
 */
public class ParameterDaoImpl extends BaseDaoImpl<Parameter, ParameterMapper> implements ParameterDao {

    public ParameterDaoImpl() {
        super(ParameterMapper.TABLE_NAME, new ParameterMapper());
    }

    @Override
    public void addToProduct(Long productId, Parameter entity) throws DaoException {
        ProductParameterMapper productParameterMapper = new ProductParameterMapper();
        String fields = productParameterMapper.getInsertedFields();

        String query = "INSERT INTO " + ProductParameterMapper.TABLE_NAME + fields;

        try (Connection conn = DataSource.getConnection();
             PreparedStatement insertStatement = conn.prepareStatement(query)) {

            productParameterMapper.setSaveStatement(insertStatement, productId, entity.getId());

            insertStatement.executeUpdate();

        } catch (SQLException ex) {
            ex.printStackTrace();
            throw new DaoException(ex.getMessage());
        }
    }

    @Override
    public void removeAllFromProduct(Long productId) throws DaoException {
        logger.debug("removeFromProduct " + productId);

        String query = "DELETE FROM " + ProductParameterMapper.TABLE_NAME +
                WHERE + ProductParameterMapper.FIELD_PRODUCT_ID + "=?";

        try (Connection con = DataSource.getConnection();
             PreparedStatement pst = con.prepareStatement(query)) {
            pst.setLong(1, productId);
            pst.executeUpdate();

        } catch (SQLException ex) {
            ex.printStackTrace();
            throw new DaoException(ex.getMessage());
        }
    }

    @Override
    public List<Parameter> findAllByProductId(Long productId) throws DaoException {
        String parameter = ParameterMapper.TABLE_NAME;
        String product_parameter = ProductParameterMapper.TABLE_NAME;
        String product = ProductMapper.TABLE_NAME;

        String query = selectFromTable +
                " INNER JOIN " + product_parameter +
                " ON " + parameter + "." + ParameterMapper.FIELD_ID + "=" + product_parameter + "." + ProductParameterMapper.FIELD_PARAMETER_ID +
                " INNER JOIN " + product +
                " ON " + product + "." + ProductMapper.FIELD_ID + "=" + product_parameter + "." + ProductParameterMapper.FIELD_PRODUCT_ID +
                WHERE + product + "." + ProductMapper.FIELD_ID + "=?";

        try (Connection con = DataSource.getConnection();
             PreparedStatement pst = con.prepareStatement(query)) {
            pst.setLong(1, productId);

            try (ResultSet result = pst.executeQuery()) {
                List<Parameter> parameters = new ArrayList<>();

                while (result.next()) {
                    parameters.add(mapper.of(result));
                }
                return parameters;
            }

        } catch (SQLException ex) {
            ex.printStackTrace();
            throw new DaoException(ex.getMessage());
        }
    }

    @Override
    public List<Parameter> findByTypeId(Long id) throws DaoException {
        logger.debug("findByTypeId " + id);

        String query = selectFromTable + WHERE + ParameterMapper.FIELD_PARAMETER_TYPE_ID + "=?";
        try (Connection con = DataSource.getConnection();
             PreparedStatement pst = con.prepareStatement(query)) {
            pst.setLong(1, id);

            List<Parameter> parameters = new ArrayList<>();

            try (ResultSet result = pst.executeQuery()) {
                while (result.next()) {
                    parameters.add(mapper.of(result));
                }
            }

            return parameters;
        } catch (SQLException ex) {
            ex.printStackTrace();
            throw new DaoException(ex.getMessage());
        }
    }
}
