package com.zhuravel.market_servlet.dao.impl;

import com.zhuravel.market_servlet.dao.UserStatusDao;
import com.zhuravel.market_servlet.dao.mapper.UserStatusMapper;
import com.zhuravel.market_servlet.model.entity.UserStatus;

/**
 * @author Evgenii Zhuravel created on 09.06.2022
 * @version 1.0
 */
public class UserStatusDaoImpl extends BaseDaoImpl<UserStatus, UserStatusMapper> implements UserStatusDao {

    public UserStatusDaoImpl() {
        super(UserStatusMapper.TABLE_NAME, new UserStatusMapper());
    }
}
