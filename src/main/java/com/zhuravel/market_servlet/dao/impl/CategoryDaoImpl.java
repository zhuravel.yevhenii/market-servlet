package com.zhuravel.market_servlet.dao.impl;

import com.zhuravel.market_servlet.config.DataSource;
import com.zhuravel.market_servlet.dao.CategoryDao;
import com.zhuravel.market_servlet.dao.CategoryTranslationDao;
import com.zhuravel.market_servlet.dao.exception.DaoException;
import com.zhuravel.market_servlet.dao.mapper.CategoryMapper;
import com.zhuravel.market_servlet.dao.mapper.CategoryTranslationMapper;
import com.zhuravel.market_servlet.model.entity.Category;
import com.zhuravel.market_servlet.model.entity.CategoryTranslation;
import com.zhuravel.market_servlet.model.pageable.Page;
import com.zhuravel.market_servlet.model.pageable.Pageable;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.StringJoiner;

/**
 * Evgenii Zhuravel created on 19.05.2022
 */
public class CategoryDaoImpl extends BaseDaoImpl<Category, CategoryMapper> implements CategoryDao {

    private final CategoryTranslationDao categoryTranslationDao;

    public CategoryDaoImpl() {
        super(CategoryMapper.TABLE_NAME, new CategoryMapper());
        this.categoryTranslationDao = new CategoryTranslationDaoImpl();
    }

    @Override
    public Page<Category> findAllByParentCategoryId(Long parentCategoryId, Pageable pageable) throws DaoException {
        String query = selectFromTable
                + " WHERE " + CategoryMapper.FIELD_PARENT_CATEGORY_ID + "=?";

        Page<Category> results = super.findAll(query, List.of(parentCategoryId), pageable);

        for (Category category : results.getContent()) {
            category.setTranslations(categoryTranslationDao.findAllByCategoryId(category.getId()));
        }
        return results;
    }

    @Override
    public Page<Category> findAllByParentCategoryId(Long parentCategoryId, String filterEn, String filterUk, Pageable pageable) throws DaoException {
        String categoryTable = CategoryMapper.TABLE_NAME;
        String translationTable = CategoryTranslationMapper.TABLE_NAME;

        List<Object> values = new ArrayList<>();
        values.add(parentCategoryId);

        StringJoiner joiner = new StringJoiner(" OR ");
        if (!filterEn.isEmpty()) {
            joiner.add("(" + translationTable + "." + CategoryTranslationMapper.FIELD_LOCALE + " LIKE 'en'"
                    + " AND " + translationTable + "." + CategoryTranslationMapper.FIELD_LOCALIZED_NAME + " LIKE '%" + filterEn + "%') ");
        }
        if (!filterUk.isEmpty()) {
            joiner.add("(" + translationTable + "." + CategoryTranslationMapper.FIELD_LOCALE + " LIKE 'uk'"
                    + " AND " + translationTable + "." + CategoryTranslationMapper.FIELD_LOCALIZED_NAME + " LIKE '%" + filterUk + "%') ");
        }

        String query = "SELECT DISTINCT " + mapper.getSelectedFields(tableName) + " FROM " + tableName
                + " JOIN " + translationTable
                + " ON " + categoryTable + "." + CategoryMapper.FIELD_ID + "=" + translationTable + "." + CategoryTranslationMapper.FIELD_ID
                + " WHERE " + CategoryMapper.FIELD_PARENT_CATEGORY_ID + "=?"
                + " AND (" + joiner + ") ";

        Page<Category> results = super.findAll(query, values, pageable);

        for (Category category : results.getContent()) {
            category.setTranslations(categoryTranslationDao.findAllByCategoryId(category.getId()));
        }
        return results;
    }

    @Override
    public Optional<Category> findById(Long id) throws DaoException {
        logger.debug("findById " + id);

        Optional<Category> result = super.findById(id);

        if (result.isPresent()) {
            result.get().setTranslations(categoryTranslationDao.findAllByCategoryId(id));
        }
        return result;
    }

    @Override
    public List<Category> findAll() throws DaoException {
        logger.debug("findAllByLocale with query");

        List<Category> results = super.findAll();

        for (Category category : results) {
            category.setTranslations(categoryTranslationDao.findAllByCategoryId(category.getId()));
        }
        return results;
    }

    @Override
    public Category save(Category entity) throws DaoException {
        logger.debug("save with id " + entity.getId());

        super.save(entity);

        for (CategoryTranslation translation : entity.getTranslations()) {
            categoryTranslationDao.save(translation);
        }
        return entity;
    }

    public Category update(Category entity) throws DaoException {
        logger.debug("save with id " + entity.getId());

        String fields = mapper.getUpdatedFields(entity);

        String query = "UPDATE " + tableName + " SET " + fields + WHERE + ID + "=?";

        try (Connection conn = DataSource.getConnection();
             PreparedStatement pst = conn.prepareStatement(query)) {

            Integer nextIndex = mapper.setUpdateStatement(pst, entity);

            pst.setLong(nextIndex, entity.getId());

            pst.executeUpdate();

            for (CategoryTranslation translation : entity.getTranslations()) {
                categoryTranslationDao.update(translation);
            }

            return entity;
        } catch (SQLException ex) {
            ex.printStackTrace();
            throw new DaoException(ex.getMessage());
        }
    }
}
