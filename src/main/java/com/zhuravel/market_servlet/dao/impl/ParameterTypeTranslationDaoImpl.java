package com.zhuravel.market_servlet.dao.impl;

import com.zhuravel.market_servlet.config.DataSource;
import com.zhuravel.market_servlet.dao.ParameterTypeTranslationDao;
import com.zhuravel.market_servlet.dao.exception.DaoException;
import com.zhuravel.market_servlet.dao.mapper.ParameterTypeTranslationMapper;
import com.zhuravel.market_servlet.model.entity.ParameterTypeTranslation;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Evgenii Zhuravel created on 19.05.2022
 */
public class ParameterTypeTranslationDaoImpl extends BaseDaoImpl<ParameterTypeTranslation, ParameterTypeTranslationMapper> implements ParameterTypeTranslationDao {

    public ParameterTypeTranslationDaoImpl() {
        super(ParameterTypeTranslationMapper.TABLE_NAME, new ParameterTypeTranslationMapper());
    }

    @Override
    public List<ParameterTypeTranslation> findAllByParameterTypeId(Long parameterTypeId) throws DaoException {
        String query = selectFromTable +
                " WHERE " + ParameterTypeTranslationMapper.FIELD_ID + "=?";

        try (Connection con = DataSource.getConnection();
             PreparedStatement pst = con.prepareStatement(query)) {
            pst.setLong(1, parameterTypeId);

            try (ResultSet result = pst.executeQuery()) {
                List<ParameterTypeTranslation> translations = new ArrayList<>();

                while (result.next()) {
                    translations.add(mapper.of(result));
                }
                return translations;
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
            throw new DaoException(ex.getMessage());
        }
    }

    @Override
    public ParameterTypeTranslation save(ParameterTypeTranslation entity) throws DaoException {
        logger.debug("save with id " + entity.getId());

        String fields = mapper.getSavedFields(entity);

        String query = "INSERT INTO " + tableName + fields;

        try (Connection conn = DataSource.getConnection();
             PreparedStatement insertStatement = conn.prepareStatement(query)) {

            mapper.setSaveStatement(insertStatement, entity);

            insertStatement.executeUpdate();

            return entity;
        } catch (SQLException ex) {
            ex.printStackTrace();
            throw new DaoException(ex.getMessage());
        }
    }

    @Override
    public ParameterTypeTranslation update(ParameterTypeTranslation entity) throws DaoException {
        String fields = mapper.getUpdatedFields(entity);

        String query = "UPDATE " + tableName + " SET " + fields + WHERE + ID + "=? AND " + ParameterTypeTranslationMapper.FIELD_LOCALE + " LIKE ?";

        try (Connection conn = DataSource.getConnection();
             PreparedStatement pst = conn.prepareStatement(query)) {

            Integer nextIndex = mapper.setUpdateStatement(pst, entity);

            pst.setLong(nextIndex++, entity.getId());
            pst.setString(nextIndex, entity.getLocale());

            pst.executeUpdate();

            return entity;
        } catch (SQLException ex) {
            ex.printStackTrace();
            throw new DaoException(ex.getMessage());
        }
    }
}
