package com.zhuravel.market_servlet.dao.impl;

import com.zhuravel.market_servlet.config.DataSource;
import com.zhuravel.market_servlet.dao.LocalizedParameterTypeDao;
import com.zhuravel.market_servlet.dao.exception.DaoException;
import com.zhuravel.market_servlet.dao.mapper.LocalizedParameterTypeMapper;
import com.zhuravel.market_servlet.model.entity.LocalizedParameterType;
import com.zhuravel.market_servlet.model.pageable.Page;
import com.zhuravel.market_servlet.model.pageable.Pageable;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * @author Evgenii Zhuravel created on 08.06.2022
 * @version 1.0
 */
public class LocalizedParameterTypeDaoImpl extends BaseDaoImpl<LocalizedParameterType, LocalizedParameterTypeMapper> implements LocalizedParameterTypeDao {

    private String locale;

    public LocalizedParameterTypeDaoImpl() {
        super(LocalizedParameterTypeMapper.TABLE_NAME, new LocalizedParameterTypeMapper());
    }

    @Override
    public void setLocale(String locale) {
        this.locale = locale;
    }

    @Override
    public Page<LocalizedParameterType> findAllByCategoryId(Long categoryId, Pageable pageable) throws DaoException {
        logger.debug("findAllByCategoryId " + categoryId);

        String parameterType = LocalizedParameterTypeMapper.TABLE_NAME;

        String query = getSelectQueryByLocale(parameterType)
                + " AND " + parameterType + "." + LocalizedParameterTypeMapper.FIELD_CATEGORY_ID + "=?"
                + " LIMIT " + pageable.getSize() + " OFFSET " + pageable.getNumber() * pageable.getSize();

        try (Connection con = DataSource.getConnection();
             PreparedStatement pst = con.prepareStatement(query)) {
            pst.setString(1, locale);
            pst.setLong(2, categoryId);

            List<LocalizedParameterType> localizedParameterTypes = new ArrayList<>();

            try (ResultSet result = pst.executeQuery()) {
                while (result.next()) {
                    localizedParameterTypes.add(mapper.of(result));
                }
            }

            return pageable.buildPage(localizedParameterTypes, super.getTotalCount(query, List.of(locale, categoryId)));
        } catch (SQLException ex) {
            ex.printStackTrace();
            throw new DaoException(ex.getMessage());
        }
    }

    @Override
    public Optional<LocalizedParameterType> findById(Long id) throws DaoException {
        logger.debug("findById " + id);

        String parameterType = LocalizedParameterTypeMapper.TABLE_NAME;

        String query = getSelectQueryByLocale(parameterType)
                + " AND " + parameterType + "." + LocalizedParameterTypeMapper.FIELD_ID + "=?";

        try (Connection con = DataSource.getConnection();
             PreparedStatement pst = con.prepareStatement(query)) {
            pst.setString(1, locale);
            pst.setLong(2, id);

            try (ResultSet result = pst.executeQuery()) {
                if (result.next()) {
                    return Optional.of(mapper.of(result));
                }
            }

            return Optional.empty();
        } catch (SQLException ex) {
            ex.printStackTrace();
            throw new DaoException(ex.getMessage());
        }
    }

    private String getSelectQueryByLocale(String parameterType) {
        String translation = LocalizedParameterTypeMapper.TABLE_NAME_TRANSLATION;

        return "SELECT " + mapper.getSelectedFields() + " FROM " + parameterType
                + " LEFT JOIN " + translation
                + " ON " + parameterType + "." + LocalizedParameterTypeMapper.FIELD_ID + "=" + translation + "." + LocalizedParameterTypeMapper.FIELD_ID
                + " WHERE " + translation + "." + LocalizedParameterTypeMapper.FIELD_T_LOCALE + " LIKE ?";
    }
}
