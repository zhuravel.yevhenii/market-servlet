package com.zhuravel.market_servlet.dao.impl;

import com.zhuravel.market_servlet.config.DataSource;
import com.zhuravel.market_servlet.dao.ProductTranslationDao;
import com.zhuravel.market_servlet.dao.exception.DaoException;
import com.zhuravel.market_servlet.dao.mapper.ProductTranslationMapper;
import com.zhuravel.market_servlet.model.entity.ProductTranslation;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 * Evgenii Zhuravel created on 19.05.2022
 */
public class ProductTranslationDaoImpl extends BaseDaoImpl<ProductTranslation, ProductTranslationMapper> implements ProductTranslationDao {

    public ProductTranslationDaoImpl() {
        super(ProductTranslationMapper.TABLE_NAME, new ProductTranslationMapper());
    }

    @Override
    public ProductTranslation save(ProductTranslation entity) throws DaoException {
        logger.debug("save with id " + entity.getId());

        String fields = mapper.getSavedFields(entity);

        String query = "INSERT INTO " + tableName + fields;

        try (Connection conn = DataSource.getConnection();
             PreparedStatement insertStatement = conn.prepareStatement(query)) {

            mapper.setSaveStatement(insertStatement, entity);

            insertStatement.executeUpdate();

            return entity;
        } catch (SQLException ex) {
            ex.printStackTrace();
            throw new DaoException(ex.getMessage());
        }
    }

    @Override
    public ProductTranslation update(ProductTranslation entity) throws DaoException {
        String fields = mapper.getUpdatedFields(entity);

        String query = "UPDATE " + tableName + " SET " + fields + WHERE + ID + "=? AND " + ProductTranslationMapper.FIELD_LOCALE + " LIKE ?";

        try (Connection conn = DataSource.getConnection();
             PreparedStatement pst = conn.prepareStatement(query)) {

            Integer nextIndex = mapper.setUpdateStatement(pst, entity);

            pst.setLong(nextIndex++, entity.getId());
            pst.setString(nextIndex, entity.getLocale());

            pst.executeUpdate();

            return entity;
        } catch (SQLException ex) {
            ex.printStackTrace();
            throw new DaoException(ex.getMessage());
        }
    }
}
