package com.zhuravel.market_servlet.dao.impl;

import com.zhuravel.market_servlet.config.DataSource;
import com.zhuravel.market_servlet.dao.ProductDao;
import com.zhuravel.market_servlet.dao.ProductTranslationDao;
import com.zhuravel.market_servlet.dao.exception.DaoException;
import com.zhuravel.market_servlet.dao.mapper.ProducerMapper;
import com.zhuravel.market_servlet.dao.mapper.ProductMapper;
import com.zhuravel.market_servlet.model.entity.Product;
import com.zhuravel.market_servlet.model.entity.ProductTranslation;
import com.zhuravel.market_servlet.model.pageable.Page;
import com.zhuravel.market_servlet.model.pageable.Pageable;
import com.zhuravel.market_servlet.service.filter_criteria.ParameterSearchCriteria;
import com.zhuravel.market_servlet.service.filter_criteria.ProductSpecification;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * @author Evgenii Zhuravel created on 08.06.2022
 * @version 1.0
 */
public class ProductDaoImpl extends BaseDaoImpl<Product, ProductMapper> implements ProductDao {

    private String locale;

    private final ProductTranslationDao productTranslationDao;

    public ProductDaoImpl() {
        super(ProductMapper.TABLE_NAME, new ProductMapper());
        this.productTranslationDao = new ProductTranslationDaoImpl();
    }

    public void setLocale(String locale) {
        this.locale = locale;
    }

    @Override
    public Optional<Product> findById(Long id) throws DaoException {
        Optional<Product> product = super.findById(id);

        if (product.isPresent()) {
            List<ProductTranslation> translations = productTranslationDao.findALLById(product.get().getId());
            product.get().setTranslations(translations);
        }
        return product;
    }

    @Override
    public Page<Product> findAll(Pageable pageable) throws DaoException {
        Page<Product> productPage = super.findAll(pageable);
        for (Product product : productPage.getContent()) {
            List<ProductTranslation> translations = productTranslationDao.findALLById(product.getId());
            product.setTranslations(translations);
        }
        return productPage;
    }

    @Override
    public Page<Product> findAllByProducerName(String name, Pageable pageable) throws DaoException {
        String productTable = ProductMapper.TABLE_NAME;
        String producerTable = ProducerMapper.TABLE_NAME;

        String query = "SELECT " + mapper.getSelectedFields(ProductMapper.TABLE_NAME) + " FROM " + tableName +
                " JOIN " + producerTable +
                " ON " + productTable + "." + ProductMapper.FIELD_PRODUCER_ID + "=" + producerTable + "." + ProducerMapper.FIELD_ID +
                WHERE + producerTable + "." + ProducerMapper.FIELD_NAME + "=?" +
                " LIMIT " + pageable.getSize() + " OFFSET " + pageable.getNumber() * pageable.getSize();

        try (Connection con = DataSource.getConnection();
             PreparedStatement pst = con.prepareStatement(query)) {
            pst.setString(1, name);

            try (ResultSet result = pst.executeQuery()) {
                List<Product> products = new ArrayList<>();

                while (result.next()) {
                    Product product = mapper.of(result);

                    List<ProductTranslation> translations = productTranslationDao.findALLById(product.getId());

                    product.setTranslations(translations);

                    products.add(product);
                }
                return pageable.buildPage(products, getTotalCount(query, List.of(name)));
            }

        } catch (SQLException ex) {
            ex.printStackTrace();
            throw new DaoException(ex.getMessage());
        }
    }

    @Override
    public Page<Product> findAll(ParameterSearchCriteria criteria, String sort, Pageable pageable) throws DaoException {
        String filterQuery = ProductSpecification.generateQuery(criteria, locale);

        if (sort != null && !sort.isEmpty()) {
            filterQuery += " ORDER BY " + sort;
        }
        Page<Product> productPage = findAll(filterQuery, new ArrayList<>(), pageable);

        for (Product product : productPage.getContent()) {
            List<ProductTranslation> translations = productTranslationDao.findALLById(product.getId());
            product.setTranslations(translations);
        }
        return productPage;
    }

    @Override
    public Product save(Product entity) throws DaoException {
        Product product = super.save(entity);

        for (ProductTranslation translation : product.getTranslations()) {
            translation.setId(product.getId());
            productTranslationDao.save(translation);
        }

        return product;
    }

    @Override
    public Product update(Product entity) throws DaoException {
        Product product = super.update(entity);

        for (ProductTranslation translation : product.getTranslations()) {
            translation.setId(product.getId());
            productTranslationDao.update(translation);
        }

        return product;
    }
}
