package com.zhuravel.market_servlet.dao.impl;

import com.zhuravel.market_servlet.dao.ParameterTypeDao;
import com.zhuravel.market_servlet.dao.ParameterTypeTranslationDao;
import com.zhuravel.market_servlet.dao.exception.DaoException;
import com.zhuravel.market_servlet.dao.mapper.ParameterTypeMapper;
import com.zhuravel.market_servlet.model.entity.ParameterType;
import com.zhuravel.market_servlet.model.entity.ParameterTypeTranslation;
import com.zhuravel.market_servlet.model.pageable.Page;
import com.zhuravel.market_servlet.model.pageable.Pageable;

import java.util.List;
import java.util.Optional;

/**
 * @author Evgenii Zhuravel created on 08.06.2022
 * @version 1.0
 */
public class ParameterTypeDaoImpl extends BaseDaoImpl<ParameterType, ParameterTypeMapper> implements ParameterTypeDao {

    private final ParameterTypeTranslationDao parameterTypeTranslationDao;

    public ParameterTypeDaoImpl() {
        super(ParameterTypeMapper.TABLE_NAME, new ParameterTypeMapper());

        this.parameterTypeTranslationDao = new ParameterTypeTranslationDaoImpl();
    }

    @Override
    public Page<ParameterType> findAll(Pageable pageable) throws DaoException {

        Page<ParameterType> parameterTypes = super.findAll(pageable);

        for (ParameterType parameterType : parameterTypes.getContent()) {
            parameterType.setTranslations(parameterTypeTranslationDao.findAllByParameterTypeId(parameterType.getId()));
        }

        return parameterTypes;
    }

    @Override
    public List<ParameterType> findAllByCategoryId(Long categoryId) throws DaoException {
        logger.debug("findAllByCategoryId " + categoryId);

        String query = selectFromTable +
                " WHERE " + ParameterTypeMapper.FIELD_CATEGORY_ID + "=?";

        List<ParameterType> parameterTypes = super.findAll(query, List.of(categoryId));

        for (ParameterType parameterType : parameterTypes) {
            parameterType.setTranslations(parameterTypeTranslationDao.findAllByParameterTypeId(parameterType.getId()));
        }

        return parameterTypes;
    }

    @Override
    public Optional<ParameterType> findById(Long id) throws DaoException {
        logger.debug("findById " + id);

        Optional<ParameterType> result = super.findById(id);

        if (result.isPresent()) {
            result.get().setTranslations(parameterTypeTranslationDao.findAllByParameterTypeId(id));
        }

        return result;
    }

    @Override
    public ParameterType save(ParameterType entity) throws DaoException {
        logger.debug("save with id " + entity.getId());

        super.save(entity);

        for (ParameterTypeTranslation translation: entity.getTranslations()) {
            translation.setId(entity.getId());
            parameterTypeTranslationDao.save(translation);
        }

        return entity;
    }

    @Override
    public ParameterType update(ParameterType entity) throws DaoException {
        logger.debug("update with id " + entity.getId());

        super.update(entity);

        for (ParameterTypeTranslation translation: entity.getTranslations()) {
            translation.setId(entity.getId());
            parameterTypeTranslationDao.update(translation);
        }

        return entity;
    }
}
