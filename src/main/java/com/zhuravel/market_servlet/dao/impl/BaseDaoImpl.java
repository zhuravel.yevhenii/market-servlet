package com.zhuravel.market_servlet.dao.impl;

import com.zhuravel.market_servlet.config.DataSource;
import com.zhuravel.market_servlet.dao.BaseDao;
import com.zhuravel.market_servlet.model.pageable.Page;
import com.zhuravel.market_servlet.model.pageable.Pageable;
import com.zhuravel.market_servlet.dao.exception.DaoException;
import com.zhuravel.market_servlet.dao.mapper.EntityMapper;
import com.zhuravel.market_servlet.model.entity.BaseEntity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author Evgenii Zhuravel created on 08.06.2022
 * @version 1.0
 */
public abstract class BaseDaoImpl<T extends BaseEntity, M extends EntityMapper<T>> implements BaseDao<T> {

    protected static final Logger logger = LoggerFactory.getLogger(BaseDaoImpl.class);

    protected final String tableName;

    protected static final String ID = "id";

    protected static final String WHERE = " WHERE ";

    protected final String selectFromTable;

    protected final String deleteFromTable;

    protected final M mapper;

    public BaseDaoImpl(String tableName, M mapper) {
        this.tableName = tableName;
        this.selectFromTable = "SELECT * FROM " + tableName;
        this.deleteFromTable = "DELETE FROM " + tableName + WHERE + ID + "=?";
        this.mapper = mapper;
    }

    @Override
    public List<T> findAll() throws DaoException {
        logger.debug("findAll");
        return findAll(selectFromTable, new ArrayList<>());
    }

    @Override
    public Page<T> findAll(Pageable pageable) throws DaoException {
        logger.debug("findAll");
        return findAll(selectFromTable, new ArrayList<>(), pageable);
    }

    @Override
    public Page<T> findAll(String query, List<Object> value, Pageable pageable) throws DaoException {
        logger.debug("findAll");
        query = query + " LIMIT " + pageable.getSize() + " OFFSET " + (pageable.getNumber() * pageable.getSize());

        int totalCount = getTotalCount(query, value);

        List<T> content = findAll(query, value);

        return pageable.buildPage(content, totalCount);
    }

    @Override
    public List<T> findAll(String query, List<Object> values) throws DaoException {
        List<T> results = new ArrayList<>();
        try (Connection con = DataSource.getConnection();
             PreparedStatement pst = con.prepareStatement(query)) {

            setPreparedStatement(pst, values);

            ResultSet result = pst.executeQuery();
            while (result.next()) {
                results.add(mapper.of(result));
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
            throw new DaoException(ex.getMessage());
        }

        return results;
    }

    @Override
    public Optional<T> findById(Long id) throws DaoException {
        logger.debug("findById " + id);

        String query = selectFromTable + WHERE + ID + "=?";
        try (Connection con = DataSource.getConnection();
             PreparedStatement pst = con.prepareStatement(query)) {
            pst.setLong(1, id);

            try (ResultSet result = pst.executeQuery()) {
                if (result.next()) {
                    return Optional.of(mapper.of(result));
                }
            }

            return Optional.empty();
        } catch (SQLException ex) {
            throw new DaoException(ex.getMessage());
        }
    }

    @Override
    public List<T> findALLById(Long id) throws DaoException {
        logger.debug("findById " + id);
        List<T> results = new ArrayList<>();

        String query = selectFromTable + WHERE + ID + "=?";
        try (Connection con = DataSource.getConnection();
             PreparedStatement pst = con.prepareStatement(query)) {
            pst.setLong(1, id);

            try (ResultSet result = pst.executeQuery()) {
                while (result.next()) {
                    results.add(mapper.of(result));
                }
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
            throw new DaoException(ex.getMessage());
        }
        return results;
    }

    @Override
    public T save(T entity) throws DaoException {
        logger.debug("save with id " + entity.getId());

        String fields = mapper.getSavedFields(entity);

        String query = "INSERT INTO " + tableName + fields;

        try (Connection conn = DataSource.getConnection();
             PreparedStatement insertStatement = conn.prepareStatement(query, Statement.RETURN_GENERATED_KEYS)) {

            mapper.setSaveStatement(insertStatement, entity);

            insertStatement.executeUpdate();

            ResultSet generatedKeys = insertStatement.getGeneratedKeys();

            if (generatedKeys.next()) {
                entity.setId(generatedKeys.getLong(1));
            }
            return entity;
        } catch (SQLException ex) {
            throw new DaoException(ex.getMessage());
        }
    }

    @Override
    public T update(T entity) throws DaoException {
        String fields = mapper.getUpdatedFields(entity);

        String query = "UPDATE " + tableName + " SET " + fields + WHERE + ID + "=?";

        try (Connection conn = DataSource.getConnection();
             PreparedStatement pst = conn.prepareStatement(query)) {

            Integer nextIndex = mapper.setUpdateStatement(pst, entity);

            pst.setLong(nextIndex, entity.getId());

            pst.executeUpdate();

            return entity;
        } catch (SQLException ex) {
            throw new DaoException(ex.getMessage());
        }
    }

    @Override
    public void deleteById(Long id) throws DaoException {
        logger.debug("deleteById " + id);

        try (Connection con = DataSource.getConnection();
             PreparedStatement pst = con.prepareStatement(deleteFromTable)) {
            pst.setLong(1, id);
            pst.executeUpdate();

        } catch (SQLException ex) {
            throw new DaoException(ex.getMessage());
        }
    }

    protected int getTotalCount(String query, List<Object> values) throws DaoException {
        logger.debug("getTotalCount");

        Pattern pattern = Pattern.compile("(?= FROM).+?(?=( ORDER )|( LIMIT ))");
        Matcher matcher = pattern.matcher(query);
        String s;
        if (matcher.find()) {
            s = query.substring(matcher.start(), matcher.end());
        } else {
            s = query.substring(query.indexOf(" FROM "));
        }
        String queryCount = "SELECT COUNT(*)" + s;

        try (Connection con = DataSource.getConnection();
             PreparedStatement pst = con.prepareStatement(queryCount)) {

            setPreparedStatement(pst, values);

            ResultSet result = pst.executeQuery();

            if (result.next()) {
                return result.getInt(1);
            }
            return 0;
        } catch (SQLException ex) {
            ex.printStackTrace();
            throw new DaoException(ex.getMessage());
        }
    }

    private void setPreparedStatement(PreparedStatement pst, List<Object> values) throws SQLException {
        for (int i = 0; i < values.size(); i++) {
            if (values.get(i) instanceof Long) {
                pst.setLong(i + 1, (Long) values.get(i));
            }
            if (values.get(i) instanceof String) {
                pst.setString(i + 1, (String) values.get(i));
            }
        }
    }
}
