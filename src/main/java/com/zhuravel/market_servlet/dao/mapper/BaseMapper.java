package com.zhuravel.market_servlet.dao.mapper;

import com.zhuravel.market_servlet.model.entity.Column;

import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.StringJoiner;

/**
 * @author Evgenii Zhuravel created on 08.06.2022
 * @version 1.0
 */
public abstract class BaseMapper<T> implements EntityMapper<T> {

    @Override
    public String getSelectedFields() {
        StringJoiner fields = new StringJoiner(", ");

        for (int i = 0; i < getFieldNames().length; i++) {
            fields.add(getFieldNames()[i]);
        }

        return fields.toString();
    }

    public String getSelectedFields(String tableName) {
        StringJoiner fields = new StringJoiner(", ");

        for (int i = 0; i < getFieldNames().length; i++) {
            fields.add(tableName + "." + getFieldNames()[i]);
        }

        return fields.toString();
    }

    @Override
    public String getSavedFields(T entity) {
        return "(" + getFieldNames(entity)[0] + ", " + getFields(entity)
                + ") VALUES (default"
                + ", ?".repeat(Math.max(0, getFieldNames(entity).length - 1))
                + ")";
    }

    @Override
    public String getUpdatedFields(T entity) {
        StringJoiner fields = new StringJoiner("=?, ", " ", "=? ");

        for (int i = 1; i < getFieldNames(entity).length; i++) {
            fields.add(getFieldNames(entity)[i]);
        }

        return fields.toString();
    }

    @Override
    public Integer setUpdateStatement(PreparedStatement pst, T entity) throws SQLException {
        return setSaveStatement(pst, entity);
    }

    @Override
    public Integer setSaveStatement(PreparedStatement pst, T entity) throws SQLException {
        int i = 1;
        Class<T> persistentClass = (Class<T>)
                ((ParameterizedType)getClass().getGenericSuperclass())
                        .getActualTypeArguments()[0];
        try {
            for (Field field : persistentClass.getDeclaredFields()) {
                field.setAccessible(true);

                if (field.get(entity) != null && field.isAnnotationPresent(Column.class)) {
                    if (field.getType().isAssignableFrom(Long.class)) {
                        pst.setLong(i++, (Long) field.get(entity));
                    } else if (field.getType().isAssignableFrom(String.class)) {
                        pst.setString(i++, (String) field.get(entity));
                    } else if (field.getType().isAssignableFrom(Integer.class)) {
                        pst.setInt(i++, (Integer) field.get(entity));
                    }
                }
            }
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        return i;
    }

    protected abstract String[] getFieldNames();

    protected String[] getFieldNames(T entity) {
        Class<T> persistentClass = (Class<T>)
                ((ParameterizedType)getClass().getGenericSuperclass())
                        .getActualTypeArguments()[0];

        List<String> fieldNames = new ArrayList<>();
        fieldNames.add("id");
        try {
            for (Field field : persistentClass.getDeclaredFields()) {
                field.setAccessible(true);

                if (field.get(entity) != null && field.isAnnotationPresent(Column.class)) {
                    fieldNames.add(field.getAnnotation(Column.class).name());
                }
            }
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }

        String[] array = new String[fieldNames.size()];
        return fieldNames.toArray(array);
    }

    protected String getFields(T entity) {
        StringJoiner fields = new StringJoiner(", ");

        for (int i = 1; i < getFieldNames(entity).length; i++) {
            fields.add(getFieldNames(entity)[i]);
        }
        return fields.toString();
    }
}
