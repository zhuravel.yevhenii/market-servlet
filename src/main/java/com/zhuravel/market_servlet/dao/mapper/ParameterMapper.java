package com.zhuravel.market_servlet.dao.mapper;

import com.zhuravel.market_servlet.model.entity.Parameter;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * @author Evgenii Zhuravel created on 08.06.2022
 * @version 1.0
 */
public class ParameterMapper extends BaseMapper<Parameter> {

    public static final String TABLE_NAME = "product_parameter";

    public static final String FIELD_ID = "id";
    public static final String FIELD_PARAMETER_TYPE_ID = "parameter_type_id";
    public static final String FIELD_VALUE = "sense";

    @Override
    public Parameter of(ResultSet resultSet) throws SQLException {
        return new Parameter(resultSet.getLong(1),
                resultSet.getLong(2),
                resultSet.getString(3));
    }

    @Override
    protected String[] getFieldNames() {
        return new String[]{FIELD_ID, FIELD_PARAMETER_TYPE_ID, FIELD_VALUE};
    }
}
