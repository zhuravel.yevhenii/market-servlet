package com.zhuravel.market_servlet.dao.mapper;

import com.zhuravel.market_servlet.model.LocaleConvertor;
import com.zhuravel.market_servlet.model.entity.Role;
import com.zhuravel.market_servlet.model.entity.User;
import com.zhuravel.market_servlet.model.entity.UserStatus;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * @author Evgenii Zhuravel created on 08.06.2022
 * @version 1.0
 */
public class UserMapper extends BaseMapper<User> {

    public static final String TABLE_NAME = "users";

    public static final String FIELD_ID = "id";
    public static final String FIELD_LOGIN = "login";
    public static final String FIELD_FIRST_NAME = "first_name";
    public static final String FIELD_LAST_NAME = "last_name";
    public static final String FIELD_PASSWORD = "password";
    public static final String FIELD_EMAIL = "email";
    public static final String FIELD_LOCALE = "locale";
    public static final String FIELD_STATUS = "status_id";
    public static final String FIELD_ROLE = "role_id";

    @Override
    protected String[] getFieldNames() {
        return new String[]{FIELD_ID,
                FIELD_LOGIN,
                FIELD_FIRST_NAME,
                FIELD_LAST_NAME,
                FIELD_EMAIL,
                FIELD_PASSWORD,
                FIELD_LOCALE,
                FIELD_STATUS,
                FIELD_ROLE};
    }

    @Override
    public User of(ResultSet resultSet) throws SQLException {
        UserStatus status = new UserStatus();
        status.setId(resultSet.getLong(8));

        Role role = new Role();
        role.setId(resultSet.getLong(9));

        User user = new User();
        user.setId(resultSet.getLong(1));
        user.setLogin(resultSet.getString(2));
        user.setFirstName(resultSet.getString(3));
        user.setLastName(resultSet.getString(4));
        user.setEmail(resultSet.getString(5));
        user.setPassword(resultSet.getString(6));
        user.setLocale(LocaleConvertor.getLocaleType(resultSet.getString(7)));
        user.setStatus(status);
        user.setRole(role);

        return user;
    }
}
