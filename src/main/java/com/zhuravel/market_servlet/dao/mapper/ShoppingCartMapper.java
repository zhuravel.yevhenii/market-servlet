package com.zhuravel.market_servlet.dao.mapper;

import com.zhuravel.market_servlet.model.entity.ShoppingCart;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * @author Evgenii Zhuravel created on 08.06.2022
 * @version 1.0
 */
public class ShoppingCartMapper extends BaseMapper<ShoppingCart> {

    public static final String TABLE_NAME = "shopping_cart";

    public static final String FIELD_ID = "id";
    public static final String FIELD_USER_ID = "user_id";
    public static final String FIELD_PRODUCT_ID = "product_id";
    public static final String FIELD_QUANTITY = "quantity";
    public static final String FIELD_STATUS_ID = "status_id";
    public static final String FIELD_CHECKOUT_DATE_TIME = "registration_date_time";

    @Override
    protected String[] getFieldNames() {
        return new String[]{FIELD_ID,
                FIELD_USER_ID,
                FIELD_PRODUCT_ID,
                FIELD_QUANTITY,
                FIELD_STATUS_ID,
                FIELD_CHECKOUT_DATE_TIME};
    }

    @Override
    public ShoppingCart of(ResultSet resultSet) throws SQLException {
        return new ShoppingCart(resultSet.getLong(1),
                resultSet.getLong(2),
                resultSet.getLong(3),
                resultSet.getInt(4),
                resultSet.getLong(5),
                resultSet.getLong(6));
    }
}
