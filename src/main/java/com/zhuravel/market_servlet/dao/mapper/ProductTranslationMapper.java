package com.zhuravel.market_servlet.dao.mapper;

import com.zhuravel.market_servlet.model.entity.ProductTranslation;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.StringJoiner;

/**
 * @author Evgenii Zhuravel created on 08.06.2022
 * @version 1.0
 */
public class ProductTranslationMapper extends BaseMapper<ProductTranslation> {

    public static final String TABLE_NAME = "product_translation";

    public static final String FIELD_ID = "id";
    public static final String FIELD_LOCALE = "locale";
    public static final String FIELD_LOCALIZED_NAME = "localized_name";
    public static final String FIELD_LOCALIZED_DESC = "localized_desc";

    @Override
    public ProductTranslation of(ResultSet resultSet) throws SQLException {
        return new ProductTranslation(resultSet.getLong(1),
                resultSet.getString(2),
                resultSet.getString(3),
                resultSet.getString(4));
    }

    @Override
    public String getUpdatedFields(ProductTranslation product) {
        StringJoiner fields = new StringJoiner("=?, ", " ", "=? ");

        fields.add(FIELD_LOCALIZED_NAME);
        fields.add(FIELD_LOCALIZED_DESC);

        return fields.toString();
    }

    @Override
    protected String[] getFieldNames() {
        return new String[]{FIELD_ID,
                FIELD_LOCALE,
                FIELD_LOCALIZED_NAME,
                FIELD_LOCALIZED_DESC};
    }

    @Override
    public String getSavedFields(ProductTranslation entity) {
        return "(" + getFieldNames(entity)[0] +
                ", " +
                getFields(entity) +
                ") VALUES (?" +
                ", ?".repeat(Math.max(0, getFieldNames(entity).length - 1)) +
                ")";
    }

    @Override
    public Integer setSaveStatement(PreparedStatement pst, ProductTranslation product) throws SQLException {
        pst.setLong(1, product.getId());
        pst.setString(2, product.getLocale());
        pst.setString(3, product.getLocalizedName());
        pst.setString(4, product.getLocalizedDescription());
        return 5;
    }

    @Override
    public Integer setUpdateStatement(PreparedStatement pst, ProductTranslation product) throws SQLException {
        pst.setString(1, product.getLocalizedName());
        pst.setString(2, product.getLocalizedDescription());
        return 3;
    }
}
