package com.zhuravel.market_servlet.dao.mapper;

import com.zhuravel.market_servlet.model.entity.ParameterType;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.StringJoiner;

/**
 * @author Evgenii Zhuravel created on 08.06.2022
 * @version 1.0
 */
public class ParameterTypeMapper extends BaseMapper<ParameterType> {

    public static final String TABLE_NAME = "product_parameter_type";

    public static final String FIELD_ID = "id";
    public static final String FIELD_CATEGORY_ID = "category_id";
    public static final String FIELD_NAME = "name";

    @Override
    protected String[] getFieldNames() {
        return new String[]{FIELD_ID,
                FIELD_CATEGORY_ID,
                FIELD_NAME};
    }

    @Override
    public ParameterType of(ResultSet resultSet) throws SQLException {
        return new ParameterType(resultSet.getLong(1),
                resultSet.getLong(2),
                resultSet.getString(3),
                new ArrayList<>());
    }

    @Override
    public String getSelectedFields() {
        StringJoiner fields = new StringJoiner(", ");

        fields.add(FIELD_ID);
        fields.add(FIELD_CATEGORY_ID);
        fields.add(FIELD_NAME);

        return fields.toString();
    }
}
