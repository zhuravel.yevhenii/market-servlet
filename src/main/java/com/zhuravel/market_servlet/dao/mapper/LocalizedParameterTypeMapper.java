package com.zhuravel.market_servlet.dao.mapper;

import com.zhuravel.market_servlet.model.entity.LocalizedParameterType;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.StringJoiner;

/**
 * @author Evgenii Zhuravel created on 08.06.2022
 * @version 1.0
 */
public class LocalizedParameterTypeMapper extends BaseMapper<LocalizedParameterType> {

    public static final String TABLE_NAME = "product_parameter_type";

    public static final String TABLE_NAME_TRANSLATION = ParameterTypeTranslationMapper.TABLE_NAME;

    public static final String FIELD_ID = "id";

    public static final String FIELD_CATEGORY_ID = "category_id";

    public static final String FIELD_NAME = "name";

    public static final String FIELD_T_LOCALE = ParameterTypeTranslationMapper.FIELD_LOCALE;

    public static final String FIELD_T_LOCALIZED_NAME = ParameterTypeTranslationMapper.FIELD_LOCALIZED_NAME;

    @Override
    protected String[] getFieldNames() {
        return new String[]{FIELD_ID,
                            FIELD_CATEGORY_ID,
                            FIELD_NAME};
    }

    @Override
    public LocalizedParameterType of(ResultSet resultSet) throws SQLException {
        return new LocalizedParameterType(resultSet.getLong(1),
                resultSet.getLong(2),
                resultSet.getString(3),
                resultSet.getString(4),
                resultSet.getString(5));
    }

    @Override
    public String getSelectedFields() {
        StringJoiner fields = new StringJoiner(", ");

        fields.add(TABLE_NAME + "." + FIELD_ID);
        fields.add(TABLE_NAME + "." + FIELD_CATEGORY_ID);
        fields.add(TABLE_NAME + "." + FIELD_NAME);
        fields.add(TABLE_NAME_TRANSLATION + "." + FIELD_T_LOCALE);
        fields.add(TABLE_NAME_TRANSLATION + "." + FIELD_T_LOCALIZED_NAME);

        return fields.toString();
    }
}
