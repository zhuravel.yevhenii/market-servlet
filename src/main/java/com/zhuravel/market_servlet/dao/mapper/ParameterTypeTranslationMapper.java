package com.zhuravel.market_servlet.dao.mapper;

import com.zhuravel.market_servlet.model.entity.ParameterTypeTranslation;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.StringJoiner;

/**
 * @author Evgenii Zhuravel created on 08.06.2022
 * @version 1.0
 */
public class ParameterTypeTranslationMapper extends BaseMapper<ParameterTypeTranslation> {

    public static final String TABLE_NAME = "product_parameter_type_translation";

    public static final String FIELD_ID = "id";
    public static final String FIELD_LOCALE = "locale";
    public static final String FIELD_LOCALIZED_NAME = "localized_name";

    @Override
    public ParameterTypeTranslation of(ResultSet resultSet) throws SQLException {
        return new ParameterTypeTranslation(resultSet.getLong(1),
                resultSet.getString(2),
                resultSet.getString(3));
    }

    @Override
    public String getSavedFields(ParameterTypeTranslation ParameterType) {
        return "(" + getFieldNames(ParameterType)[0] +
                ", " +
                getFields(ParameterType) +
                ") VALUES (?" +
                ", ?".repeat(Math.max(0, getFieldNames(ParameterType).length - 1)) +
                ")";
    }

    @Override
    public String getUpdatedFields(ParameterTypeTranslation ParameterType) {
        StringJoiner fields = new StringJoiner("=?, ", " ", "=? ");

        fields.add(FIELD_LOCALIZED_NAME);

        return fields.toString();
    }

    @Override
    protected String[] getFieldNames() {
        return new String[]{FIELD_ID,
            FIELD_LOCALE,
            FIELD_LOCALIZED_NAME};
    }

    @Override
    public Integer setSaveStatement(PreparedStatement pst, ParameterTypeTranslation ParameterType) throws SQLException {
        pst.setLong(1, ParameterType.getId());
        pst.setString(2, ParameterType.getLocale());
        pst.setString(3, ParameterType.getLocalizedName());
        return 4;
    }

    @Override
    public Integer setUpdateStatement(PreparedStatement pst, ParameterTypeTranslation ParameterType) throws SQLException {
        pst.setString(1, ParameterType.getLocalizedName());
        return 2;
    }
}
