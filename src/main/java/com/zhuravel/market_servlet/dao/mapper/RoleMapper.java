package com.zhuravel.market_servlet.dao.mapper;

import com.zhuravel.market_servlet.model.entity.Role;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * @author Evgenii Zhuravel created on 08.06.2022
 * @version 1.0
 */
public class RoleMapper extends BaseMapper<Role> {

    public static final String TABLE_NAME = "roles";

    public static final String FIELD_ID = "id";
    public static final String FIELD_NAME = "name";

    @Override
    protected String[] getFieldNames() {
        return new String[]{FIELD_ID,
                FIELD_NAME};
    }

    @Override
    public Role of(ResultSet resultSet) throws SQLException {
        return new Role(resultSet.getLong(1),
                resultSet.getString(2));
    }
}
