package com.zhuravel.market_servlet.dao.mapper;

import com.zhuravel.market_servlet.model.entity.UserStatus;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * @author Evgenii Zhuravel created on 08.06.2022
 * @version 1.0
 */
public class UserStatusMapper extends BaseMapper<UserStatus> {

    public static final String TABLE_NAME = "user_status";

    public static final String FIELD_ID = "id";
    public static final String FIELD_NAME = "name";

    @Override
    protected String[] getFieldNames() {
        return new String[]{FIELD_ID,
        FIELD_NAME};
    }

    @Override
    public UserStatus of(ResultSet resultSet) throws SQLException {
        return new UserStatus(resultSet.getLong(1),
                resultSet.getString(2));
    }
}
