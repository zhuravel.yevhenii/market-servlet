package com.zhuravel.market_servlet.dao.mapper;

import com.zhuravel.market_servlet.model.entity.Product;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.StringJoiner;

/**
 * @author Evgenii Zhuravel created on 08.06.2022
 * @version 1.0
 */
public class ProductMapper extends BaseMapper<Product> {

    public static final String TABLE_NAME = "product";
    public static final String TABLE_TRANSLATION_NAME = "product_translation";

    public static final String FIELD_ID = "id";
    public static final String FIELD_NAME = "name";
    public static final String FIELD_CATEGORY_ID = "category_id";
    public static final String FIELD_PRODUCER_ID = "producer_id";
    public static final String FIELD_PRICE = "price";
    public static final String FIELD_IMAGE = "image";
    public static final String FIELD_CREATE_TIME = "create_time";

    public static final String FIELD_TRANSLATION_LOCALE = "locale";

    @Override
    protected String[] getFieldNames() {
        return new String[]{FIELD_ID,
                FIELD_NAME,
                FIELD_CATEGORY_ID,
                FIELD_PRODUCER_ID,
                FIELD_PRICE,
                FIELD_IMAGE,
                FIELD_CREATE_TIME};
    }

    @Override
    public Product of(ResultSet resultSet) throws SQLException {
        return new Product(resultSet.getLong(1),
                resultSet.getString(2),
                resultSet.getLong(3),
                resultSet.getLong(4),
                resultSet.getLong(5),
                resultSet.getString(6),
                resultSet.getLong(7));
    }

    public String getSelectedFields(String tableName) {
        StringJoiner fields = new StringJoiner(", ");

        fields.add(tableName + "." + FIELD_ID);
        fields.add(tableName + "." + FIELD_NAME);
        fields.add(tableName + "." + FIELD_CATEGORY_ID);
        fields.add(tableName + "." + FIELD_PRODUCER_ID);
        fields.add(tableName + "." + FIELD_PRICE);
        fields.add(tableName + "." + FIELD_IMAGE);
        fields.add(tableName + "." + FIELD_CREATE_TIME);

        return fields.toString();
    }
}
