package com.zhuravel.market_servlet.dao.mapper;

import com.zhuravel.market_servlet.model.entity.Category;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 * @author Evgenii Zhuravel created on 08.06.2022
 * @version 1.0
 */
public class CategoryMapper extends BaseMapper<Category> {
    public static final String TABLE_NAME = "category";

    public static final String FIELD_ID = "id";

    public static final String FIELD_NAME = "name";

    public static final String FIELD_PARENT_CATEGORY_ID = "parent_category_id";

    public static final String FIELD_IMAGE_PATH = "image";

    @SuppressWarnings("checkstyle:Indentation")
    @Override
    protected String[] getFieldNames() {
        return new String[]{FIELD_ID,
                            FIELD_NAME,
                            FIELD_PARENT_CATEGORY_ID,
                            FIELD_IMAGE_PATH};
    }

    @Override
    public Category of(ResultSet resultSet) throws SQLException {
        return new Category(resultSet.getLong(1),
                resultSet.getString(2),
                resultSet.getLong(3),
                resultSet.getString(4),
                new ArrayList<>());
    }
}
