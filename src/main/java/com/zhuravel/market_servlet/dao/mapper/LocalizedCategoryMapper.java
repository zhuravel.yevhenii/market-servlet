package com.zhuravel.market_servlet.dao.mapper;

import com.zhuravel.market_servlet.model.entity.LocalizedCategory;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * @author Evgenii Zhuravel created on 08.06.2022
 * @version 1.0
 */
public class LocalizedCategoryMapper extends BaseMapper<LocalizedCategory> {

    public static final String TABLE_NAME = CategoryMapper.TABLE_NAME;

    public static final String TABLE_NAME_TRANSLATION = CategoryTranslationMapper.TABLE_NAME;

    public static final String FIELD_ID = CategoryMapper.FIELD_ID;

    public static final String FIELD_NAME = CategoryMapper.FIELD_NAME;

    public static final String FIELD_PARENT_CATEGORY_ID = CategoryMapper.FIELD_PARENT_CATEGORY_ID;

    public static final String FIELD_IMAGE_PATH = CategoryMapper.FIELD_IMAGE_PATH;

    public static final String FIELD_T_LOCALE = CategoryTranslationMapper.FIELD_LOCALE;

    public static final String FIELD_T_LOCALIZED_NAME = CategoryTranslationMapper.FIELD_LOCALIZED_NAME;

    public LocalizedCategory of(ResultSet resultSet) throws SQLException {
        return new LocalizedCategory(resultSet.getLong(1),
                resultSet.getString(2),
                resultSet.getLong(3),
                resultSet.getString(4),
                resultSet.getString(5),
                resultSet.getString(6));
    }

    @Override
    protected String[] getFieldNames() {
        return new String[]{TABLE_NAME + "." + FIELD_ID,
            TABLE_NAME + "." + FIELD_NAME,
            TABLE_NAME + "." + FIELD_PARENT_CATEGORY_ID,
            TABLE_NAME + "." + FIELD_IMAGE_PATH,
            TABLE_NAME_TRANSLATION + "." + FIELD_T_LOCALE,
            TABLE_NAME_TRANSLATION + "." + FIELD_T_LOCALIZED_NAME};
    }

    @Override
    public String getSavedFields(LocalizedCategory entity) {
        return "(" + getFieldNames(entity)[0]
                + ", "
                + getFields(entity)
                + ") VALUES (default"
                + ", ?".repeat(Math.max(0, getFieldNames(entity).length - 1))
                + ")";
    }
}
