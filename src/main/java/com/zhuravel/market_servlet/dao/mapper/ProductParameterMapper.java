package com.zhuravel.market_servlet.dao.mapper;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.StringJoiner;

/**
 * @author Evgenii Zhuravel created on 08.06.2022
 * @version 1.0
 */
public class ProductParameterMapper {

    public static final String TABLE_NAME = "product_product_parameter";

    public static final String FIELD_ID = "id";
    public static final String FIELD_PRODUCT_ID = "product_id";
    public static final String FIELD_PARAMETER_ID = "parameter_id";

    public String getInsertedFields() {
        return "(" + getFieldNames()[0] +
                ", " +
                getFields() +
                ") VALUES (default" +
                ", ?".repeat(Math.max(0, getFieldNames().length - 1)) +
                ")";
    }

    public Integer setSaveStatement(PreparedStatement pst, Long productId, Long parameterId) throws SQLException {
        pst.setLong(1, productId);
        pst.setLong(2, parameterId);
        return 3;
    }

    protected String[] getFieldNames() {
        return new String[]{FIELD_ID,
                FIELD_PRODUCT_ID,
                FIELD_PARAMETER_ID};
    }

    protected String getFields() {
        StringJoiner fields = new StringJoiner(", ");

        for (int i = 1; i < getFieldNames().length; i++) {
            fields.add(getFieldNames()[i]);
        }
        return fields.toString();
    }

    public String getUpdatedFields() {
        StringJoiner fields = new StringJoiner("=?, ", " ", "=? ");

        fields.add(FIELD_PARAMETER_ID);

        return fields.toString();
    }

    public Integer setUpdateStatement(PreparedStatement pst, Long parameterId) throws SQLException {
        pst.setLong(1, parameterId);
        return 3;
    }
}
