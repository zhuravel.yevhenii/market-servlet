package com.zhuravel.market_servlet.dao.mapper;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * @author Evgenii Zhuravel created on 08.06.2022
 * @version 1.0
 */
public interface EntityMapper<T> {

    T of(ResultSet resultSet) throws SQLException;

    String getSavedFields(T entity);

    String getUpdatedFields(T entity);

    String getSelectedFields();

    Integer setSaveStatement(PreparedStatement pst, T entity) throws SQLException;

    Integer setUpdateStatement(PreparedStatement pst, T entity) throws SQLException;
}
