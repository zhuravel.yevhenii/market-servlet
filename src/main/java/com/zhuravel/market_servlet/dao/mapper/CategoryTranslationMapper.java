package com.zhuravel.market_servlet.dao.mapper;

import com.zhuravel.market_servlet.model.entity.CategoryTranslation;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.StringJoiner;

/**
 * @author Evgenii Zhuravel created on 08.06.2022
 * @version 1.0
 */
public class CategoryTranslationMapper extends BaseMapper<CategoryTranslation> {

    public static final String TABLE_NAME = "category_translation";

    public static final String FIELD_ID = "id";

    public static final String FIELD_LOCALE = "locale";

    public static final String FIELD_LOCALIZED_NAME = "localized_name";

    @Override
    public CategoryTranslation of(ResultSet resultSet) throws SQLException {
        return new CategoryTranslation(resultSet.getLong(1),
                resultSet.getString(2),
                resultSet.getString(3));
    }

    @Override
    public String getSavedFields(CategoryTranslation translation) {
        return "(" + getFieldNames(translation)[0]
                + ", "
                + getFields(translation)
                + ") VALUES (?, ?".repeat(Math.max(0, getFieldNames(translation).length - 1))
                + ")";
    }

    @Override
    public String getUpdatedFields(CategoryTranslation translation) {
        StringJoiner fields = new StringJoiner("=?, ", " ", "=? ");

        fields.add(FIELD_LOCALIZED_NAME);

        return fields.toString();
    }

    @Override
    protected String[] getFieldNames() {
        return new String[]{FIELD_ID,
            FIELD_LOCALE,
            FIELD_LOCALIZED_NAME};
    }

    @Override
    public Integer setSaveStatement(PreparedStatement pst, CategoryTranslation category) throws SQLException {
        pst.setLong(1, category.getId());
        pst.setString(2, category.getLocale());
        pst.setString(3, category.getLocalizedName());
        return 4;
    }

    @Override
    public Integer setUpdateStatement(PreparedStatement pst, CategoryTranslation category) throws SQLException {
        pst.setString(1, category.getLocalizedName());
        return 2;
    }
}
