package com.zhuravel.market_servlet.dao;

import com.zhuravel.market_servlet.dao.exception.DaoException;
import com.zhuravel.market_servlet.model.entity.LocalizedCategory;
import com.zhuravel.market_servlet.model.pageable.Page;
import com.zhuravel.market_servlet.model.pageable.Pageable;

import java.util.Optional;

/**
 * @author Evgenii Zhuravel created on 20.05.2022
 * @version 1.0
 */
public interface LocalizedCategoryDao {

    void setLocale(String locale);

    Optional<LocalizedCategory> findById(Long id) throws DaoException;

    Page<LocalizedCategory> findAllByParentCategoryId(Long parentCategoryId, Pageable pageable) throws DaoException;
}
