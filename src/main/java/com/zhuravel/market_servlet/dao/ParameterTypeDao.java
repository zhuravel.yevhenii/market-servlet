package com.zhuravel.market_servlet.dao;

import com.zhuravel.market_servlet.dao.exception.DaoException;
import com.zhuravel.market_servlet.model.entity.ParameterType;

import java.util.List;
import java.util.Optional;

/**
 * @author Evgenii Zhuravel created on 20.05.2022
 * @version 1.0
 */
public interface ParameterTypeDao extends BaseDao<ParameterType> {

    Optional<ParameterType> findById(Long id) throws DaoException;

    List<ParameterType> findAllByCategoryId(Long id) throws DaoException;
}
