package com.zhuravel.market_servlet.dao;

import com.zhuravel.market_servlet.dao.exception.DaoException;
import com.zhuravel.market_servlet.model.entity.LocalizedParameterType;
import com.zhuravel.market_servlet.model.pageable.Page;
import com.zhuravel.market_servlet.model.pageable.Pageable;

import java.util.Optional;

/**
 * @author Evgenii Zhuravel created on 20.05.2022
 * @version 1.0
 */
public interface LocalizedParameterTypeDao /*extends BaseDao<LocalizedParameterType>*/ {

    void setLocale(String locale);

    Optional<LocalizedParameterType> findById(Long id) throws DaoException;

    Page<LocalizedParameterType> findAllByCategoryId(Long id, Pageable pageable) throws DaoException;
}
