package com.zhuravel.market_servlet.dao;

import com.zhuravel.market_servlet.dao.exception.DaoException;
import com.zhuravel.market_servlet.model.entity.Producer;

import java.util.List;

/**
 * @author Evgenii Zhuravel created on 20.05.2022
 * @version 1.0
 */
public interface ProducerDao extends BaseDao<Producer> {

    List<Producer> findDistinctByCategoryId(Long categoryId) throws DaoException;
}
