package com.zhuravel.market_servlet.model;

import java.util.Locale;

/**
 * @author Evgenii Zhuravel created on 03.06.2022
 * @version 1.0
 */
public class LocaleConvertor {

    public static LocaleType getLocaleType(Locale locale) {
        return LocaleType.valueOf(locale.getLanguage());
    }

    public static LocaleType getLocaleType(String language) {
        return LocaleType.valueOf(language);
    }
}
