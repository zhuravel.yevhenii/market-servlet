package com.zhuravel.market_servlet.model;

/**
 * @author Evgenii Zhuravel created on 05.07.2022
 * @version 1.0
 */
public enum OrderStatusType {
    REGISTERED,
    PAID,
    CANCELED
}
