package com.zhuravel.market_servlet.model;

/**
 * @author Evgenii Zhuravel created on 27.05.2022
 * @version 1.0
 */
public enum LocaleType {
    en,
    uk
}
