package com.zhuravel.market_servlet.model;

/**
 * @author Evgenii Zhuravel created on 23.05.2022
 * @version 1.0
 */
public enum UserStatusType {
    ACTIVE,
    BLOCKED,
    DELETED
}
