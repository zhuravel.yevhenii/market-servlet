package com.zhuravel.market_servlet.model;

/**
 * @author Evgenii Zhuravel created on 22.05.2022
 * @version 1.0
 */
public enum RoleType {
    ROLE_ADMIN,
    ROLE_SELLER,
    ROLE_USER
}
