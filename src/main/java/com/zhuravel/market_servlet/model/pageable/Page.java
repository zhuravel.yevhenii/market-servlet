package com.zhuravel.market_servlet.model.pageable;

import java.util.List;

/**
 * @author Evgenii Zhuravel created on 08.06.2022
 * @version 1.0
 */
public class Page<T> {

    private int totalPages;

    private List<T> content;

    public Page() {
    }

    public Page(List<T> content, int totalPages) {
        this.totalPages = totalPages;
        this.content = content;
    }

    public int getTotalPages() {
        return totalPages;
    }

    public void setTotalPages(int totalPages) {
        this.totalPages = totalPages;
    }

    public List<T> getContent() {
        return content;
    }

    public void setContent(List<T> content) {
        this.content = content;
    }

    @Override
    public String toString() {
        return "Page{" +
                "totalPages=" + totalPages +
                ", content=" + content +
                '}';
    }
}
