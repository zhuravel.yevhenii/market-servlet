package com.zhuravel.market_servlet.model.pageable;


import java.util.List;

/**
 * @author Evgenii Zhuravel created on 08.06.2022
 * @version 1.0
 */
public class Pageable {

    private int number = 0;
    private int size = 501;

    public Pageable() {
    }

    public Pageable(int number, int size) {
        this.number = number-1;
        this.size = size;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public <T> Page<T> buildPage(List<T> content, int totalCount) {
        int pageCount = (int) Math.ceil((double) totalCount / size);

        return new Page<>(content, pageCount);
    }
}
