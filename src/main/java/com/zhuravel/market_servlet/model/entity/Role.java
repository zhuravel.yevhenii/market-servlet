package com.zhuravel.market_servlet.model.entity;

import com.zhuravel.market_servlet.model.RoleType;

/**
 * @author Evgenii Zhuravel created on 22.05.2022
 * @version 1.0
 */
public class Role extends BaseEntity {

    @Column(name = "name")
    private String name;

    public Role() {
    }

    public Role(Long id, String name) {
        this.id = id;
        this.name = name;
    }

    public Role(RoleType roleType) {
        this.id = roleType.ordinal() + 1L;
        this.name = roleType.name();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Role{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}
