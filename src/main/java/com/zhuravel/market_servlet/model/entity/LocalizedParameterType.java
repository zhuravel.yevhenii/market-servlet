package com.zhuravel.market_servlet.model.entity;

/**
 * @author Evgenii Zhuravel created on 29.05.2022
 * @version 1.0
 */
public class LocalizedParameterType extends BaseEntity implements Comparable<LocalizedParameterType> {

    @Column(name = "category_id")
    private Long categoryId;

    @Column(name = "name")
    private String name;

    @Column(name = "locale")
    private String locale;

    @Column(name = "localized_name")
    private String localizedName;

    public LocalizedParameterType() {
    }

    public LocalizedParameterType(Long id, Long categoryId, String name, String locale, String localizedName) {
        this.id = id;
        this.categoryId = categoryId;
        this.name = name;
        this.locale = locale;
        this.localizedName = localizedName;
    }

    public Long getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Long categoryId) {
        this.categoryId = categoryId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLocale() {
        return locale;
    }

    public void setLocale(String locale) {
        this.locale = locale;
    }

    public String getLocalizedName() {
        return localizedName;
    }

    public void setLocalizedName(String localizedName) {
        this.localizedName = localizedName;
    }

    @Override
    public int compareTo(LocalizedParameterType o) {
        return name.compareTo(o.name);
    }

    @Override
    public String toString() {
        return "LocalizedParameterType{"
                + "id=" + id
                + ", categoryId=" + categoryId
                + ", name='" + name + '\''
                + ", localizedName='" + localizedName + '\''
                + '}';
    }
}