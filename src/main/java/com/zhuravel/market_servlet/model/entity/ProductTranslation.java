package com.zhuravel.market_servlet.model.entity;

import java.util.Objects;

/**
 * @author Evgenii Zhuravel created on 29.06.2022
 * @version 1.0
 */
public class ProductTranslation extends BaseEntity implements Comparable<ProductTranslation> {

    @Column(name = "locale")
    private String locale;

    @Column(name = "localized_name")
    private String localizedName;

    @Column(name = "localized_desc")
    private String localizedDescription;

    public ProductTranslation() {
    }

    public ProductTranslation(Long id, String locale, String localizedName, String localizedDescription) {
        this.id = id;
        this.locale = locale;
        this.localizedName = localizedName;
        this.localizedDescription = localizedDescription;
    }

    @Override
    public void setId(Long id) {
        super.setId(id);
    }

    public String getLocale() {
        return locale;
    }

    public void setLocale(String locale) {
        this.locale = locale;
    }

    public String getLocalizedName() {
        return localizedName;
    }

    public void setLocalizedName(String localizedName) {
        this.localizedName = localizedName;
    }

    public String getLocalizedDescription() {
        return localizedDescription;
    }

    public void setLocalizedDescription(String localizedDescription) {
        this.localizedDescription = localizedDescription;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        ProductTranslation that = (ProductTranslation) o;
        return locale.equals(that.locale) && Objects.equals(localizedName, that.localizedName) && Objects.equals(localizedDescription, that.localizedDescription);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), locale, localizedName, localizedDescription);
    }

    @Override
    public String toString() {
        return "CategoryTranslation{" +
                "id=" + id +
                ", locale='" + locale + '\'' +
                ", localizedName='" + localizedName + '\'' +
                ", localizedDescription='" + localizedDescription + '\'' +
                '}';
    }

    @Override
    public int compareTo(ProductTranslation o) {
        return this.localizedName.compareTo(o.localizedName);
    }
}
