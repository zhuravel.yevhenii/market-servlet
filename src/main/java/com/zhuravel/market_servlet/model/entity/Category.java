package com.zhuravel.market_servlet.model.entity;

import java.util.List;
import java.util.Objects;

/**
 * @author Evgenii Zhuravel created on 19.05.2022
 * @version 1.0
 */
public class Category extends BaseEntity {

    @Column(name = "name")
    private String name;

    @Column(name = "parent_category_id")
    private Long parentCategoryId;

    @Column(name = "image")
    private String imagePath;

    private List<CategoryTranslation> translations;

    public Category() {
    }

    public Category(Long id, String name, Long parentCategoryId, String imagePath, List<CategoryTranslation> translations) {
        this.id = id;
        this.name = name;
        this.parentCategoryId = parentCategoryId;
        this.imagePath = imagePath;
        this.translations = translations;
    }

    @Override
    public void setId(Long id) {
        super.setId(id);
        for (CategoryTranslation translation: translations) {
            translation.setId(id);
        }
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getParentCategoryId() {
        return parentCategoryId;
    }

    public void setParentCategoryId(Long parentCategoryId) {
        this.parentCategoryId = parentCategoryId;
    }

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    public List<CategoryTranslation> getTranslations() {
        return translations;
    }

    public void setTranslations(List<CategoryTranslation> translations) {
        this.translations = translations;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }
        Category category = (Category) o;
        return name.equals(category.name) && Objects.equals(parentCategoryId, category.parentCategoryId) && Objects.equals(imagePath, category.imagePath) && Objects.equals(translations, category.translations);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), name, parentCategoryId, imagePath, translations);
    }

    @Override
    public String toString() {
        return "LocalizedCategoryDto{"
                + "id=" + id
                + ", name='" + name + '\''
                + ", parentCategoryId=" + parentCategoryId
                + ", imagePath='" + imagePath + '\''
                + ", localizedName='" + translations + '\''
                + '}';
    }
}
