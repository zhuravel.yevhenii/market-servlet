package com.zhuravel.market_servlet.model.entity;

/**
 * @author Evgenii Zhuravel created on 25.05.2022
 * @version 1.0
 */

public class Producer extends BaseEntity implements Comparable<Producer> {

    @Column(name = "name")
    private String name;

    public Producer() {
    }

    public Producer(Long id, String name) {
        this.id = id;
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public int compareTo(Producer o) {
        return name.compareTo(o.name);
    }

    @Override
    public String toString() {
        return "Producer{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}