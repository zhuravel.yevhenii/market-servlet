package com.zhuravel.market_servlet.model.entity;

import java.util.Objects;

/**
 * @author Evgenii Zhuravel created on 19.05.2022
 * @version 1.0
 */
public class LocalizedCategory extends BaseEntity {

    @Column(name = "name")
    private String name;

    @Column(name = "parent_category_id")
    private Long parentCategoryId;

    @Column(name = "image")
    private String imagePath;

    @Column(name = "locale")
    private String locale;

    @Column(name = "localized_name")
    private String localizedName;

    public LocalizedCategory() {
    }

    public LocalizedCategory(Long id, String name, Long parentCategoryId, String imagePath, String locale, String localizedName) {
        this.id = id;
        this.name = name;
        this.parentCategoryId = parentCategoryId;
        this.imagePath = imagePath;
        this.locale = locale;
        this.localizedName = localizedName;
    }

    @Override
    public void setId(Long id) {
        super.setId(id);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getParentCategoryId() {
        return parentCategoryId;
    }

    public void setParentCategoryId(Long parentCategoryId) {
        this.parentCategoryId = parentCategoryId;
    }

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    public String getLocale() {
        return locale;
    }

    public void setLocale(String locale) {
        this.locale = locale;
    }

    public String getLocalizedName() {
        return localizedName;
    }

    public void setLocalizedName(String localizedName) {
        this.localizedName = localizedName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }
        LocalizedCategory category = (LocalizedCategory) o;
        return name.equals(category.name) && Objects.equals(parentCategoryId, category.parentCategoryId) && Objects.equals(imagePath, category.imagePath) && locale.equals(category.locale) && Objects.equals(localizedName, category.localizedName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), name, parentCategoryId, imagePath, locale, localizedName);
    }

    @Override
    public String toString() {
        return "LocalizedCategoryDto{"
                + "id=" + id
                + ", name='" + name + '\''
                + ", parentCategoryId=" + parentCategoryId
                + ", imagePath='" + imagePath + '\''
                + ", locale='" + locale + '\''
                + ", localizedName='" + localizedName + '\''
                + '}';
    }
}
