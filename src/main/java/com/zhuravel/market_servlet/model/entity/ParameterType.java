package com.zhuravel.market_servlet.model.entity;

import java.util.List;

/**
 * @author Evgenii Zhuravel created on 29.05.2022
 * @version 1.0
 */
public class ParameterType extends BaseEntity implements Comparable<ParameterType> {

    @Column(name = "category_id")
    private Long categoryId;

    @Column(name = "name")
    private String name;

    private List<ParameterTypeTranslation> translations;

    public ParameterType() {
    }

    public ParameterType(Long id, Long categoryId, String name, List<ParameterTypeTranslation> translations) {
        this.id = id;
        this.categoryId = categoryId;
        this.name = name;
        this.translations = translations;
    }

    public Long getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Long categoryId) {
        this.categoryId = categoryId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<ParameterTypeTranslation> getTranslations() {
        return translations;
    }

    public void setTranslations(List<ParameterTypeTranslation> translations) {
        this.translations = translations;
    }

    @Override
    public int compareTo(ParameterType o) {
        return name.compareTo(o.name);
    }

    @Override
    public String toString() {
        return "ParameterTypeDto{" +
                "id=" + id +
                ", categoryId=" + categoryId +
                ", name='" + name + '\'' +
                ", translations=" + translations +
                '}';
    }
}