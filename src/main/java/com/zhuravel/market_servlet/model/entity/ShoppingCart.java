package com.zhuravel.market_servlet.model.entity;

/**
 * @author Evgenii Zhuravel created on 05.06.2022
 * @version 1.0
 */

public class ShoppingCart extends BaseEntity {

    @Column(name = "user_id")
    private Long userId;

    @Column(name = "product_id")
    private Long productId;

    @Column(name = "quantity")
    private Integer quantity;

    @Column(name = "status_id")
    private Long statusId;

    @Column(name = "registration_date_time")
    private Long checkoutDateTime;

    public ShoppingCart() {
    }

    public ShoppingCart(Long id, Long userId, Long productId, Integer quantity, Long statusId, Long checkoutDateTime) {
        this.id = id;
        this.userId = userId;
        this.productId = productId;
        this.quantity = quantity;
        this.statusId = statusId;
        this.checkoutDateTime = checkoutDateTime;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getProductId() {
        return productId;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public Long getStatusId() {
        return statusId;
    }

    public void setStatusId(Long statusId) {
        this.statusId = statusId;
    }

    public Long getCheckoutDateTime() {
        return checkoutDateTime;
    }

    public void setCheckoutDateTime(Long checkoutDateTime) {
        this.checkoutDateTime = checkoutDateTime;
    }
}
