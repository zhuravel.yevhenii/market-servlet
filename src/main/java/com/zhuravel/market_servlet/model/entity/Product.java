package com.zhuravel.market_servlet.model.entity;

import java.util.List;
import java.util.Objects;

/**
 * @author Evgenii Zhuravel created on 25.05.2022
 * @version 1.0
 */
public class Product extends BaseEntity implements Comparable<Product> {

    @Column(name = "name")
    private String name;

    private List<ProductTranslation> translations;

    @Column(name = "category_id")
    private Long categoryId;

    @Column(name = "producer_id")
    private Long producerId;

    @Column(name = "price")
    private Long price;

    @Column(name = "image")
    private String imagePath;

    @Column(name = "create_time")
    private Long createTime;

    public Product() {
    }

    public Product(Long id, String name, List<ProductTranslation> translations, Long categoryId, Long producerId, Long price, String imagePath, Long createTime) {
        this.id = id;
        this.name = name;
        this.translations = translations;
        this.categoryId = categoryId;
        this.producerId = producerId;
        this.price = price;
        this.imagePath = imagePath;
        this.createTime = createTime;
    }

    public Product(Long id, String name, Long categoryId, Long producerId, Long price, String imagePath, Long createTime) {
        this.id = id;
        this.name = name;
        this.categoryId = categoryId;
        this.producerId = producerId;
        this.price = price;
        this.imagePath = imagePath;
        this.createTime = createTime;
    }

    @Override
    public void setId(Long id) {
        super.setId(id);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<ProductTranslation> getTranslations() {
        return translations;
    }

    public void setTranslations(List<ProductTranslation> translations) {
        this.translations = translations;
    }

    public Long getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Long categoryId) {
        this.categoryId = categoryId;
    }

    public Long getProducerId() {
        return producerId;
    }

    public void setProducerId(Long producerId) {
        this.producerId = producerId;
    }

    public Long getPrice() {
        return price;
    }

    public void setPrice(Long price) {
        this.price = price;
    }

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    public Long getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Long createTime) {
        this.createTime = createTime;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        Product product = (Product) o;
        return name.equals(product.name) &&
                Objects.equals(translations, product.translations) &&
                categoryId.equals(product.categoryId) &&
                producerId.equals(product.producerId) &&
                Objects.equals(price, product.price) &&
                Objects.equals(imagePath, product.imagePath) &&
                createTime.equals(product.createTime);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), name, translations, categoryId, producerId, price, imagePath);
    }

    @Override
    public int compareTo(Product o) {
        return this.name.compareTo(o.name);
    }

    @Override
    public String toString() {
        return "Product{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", translations=" + translations +
                ", categoryId=" + categoryId +
                ", producerId=" + producerId +
                ", price=" + price +
                ", imagePath='" + imagePath +
                ", createTime='" + createTime + '\'' +
                '}';
    }
}