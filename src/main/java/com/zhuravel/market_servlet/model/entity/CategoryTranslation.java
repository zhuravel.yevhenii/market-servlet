package com.zhuravel.market_servlet.model.entity;

import java.util.Objects;

/**
 * @author Evgenii Zhuravel created on 19.05.2022
 * @version 1.0
 */
public class CategoryTranslation extends BaseEntity {

    @Column(name = "locale")
    private String locale;

    @Column(name = "localized_name")
    private String localizedName;

    public CategoryTranslation() {
    }

    public CategoryTranslation(Long id, String locale, String localizedName) {
        this.id = id;
        this.locale = locale;
        this.localizedName = localizedName;
    }

    @Override
    public void setId(Long id) {
        super.setId(id);
    }

    public String getLocale() {
        return locale;
    }

    public void setLocale(String locale) {
        this.locale = locale;
    }

    public String getLocalizedName() {
        return localizedName;
    }

    public void setLocalizedName(String localizedName) {
        this.localizedName = localizedName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }
        CategoryTranslation category = (CategoryTranslation) o;
        return locale.equals(category.locale) && Objects.equals(localizedName, category.localizedName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), locale, localizedName);
    }

    @Override
    public String toString() {
        return "CategoryTranslation{"
                + "id=" + id
                + ", locale='" + locale + '\''
                + ", localizedName='" + localizedName + '\''
                + '}';
    }
}
