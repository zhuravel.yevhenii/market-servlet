package com.zhuravel.market_servlet.model.entity;

import java.util.Objects;

/**
 * @author Evgenii Zhuravel created on 29.05.2022
 * @version 1.0
 */
public class Parameter extends BaseEntity implements Comparable<Parameter> {

    @Column(name = "parameter_type_id")
    private Long typeId;

    @Column(name = "sense")
    private String value;

    public Parameter() {
    }

    public Parameter(Long id, Long typeId, String value) {
        this.id = id;
        this.typeId = typeId;
        this.value = value;
    }

    public Long getTypeId() {
        return typeId;
    }

    public void setTypeId(Long typeId) {
        this.typeId = typeId;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        Parameter parameter = (Parameter) o;
        return typeId.equals(parameter.typeId) && value.equals(parameter.value);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), typeId, value);
    }

    @Override
    public int compareTo(Parameter o) {
        return value.compareTo(o.value);
    }

    @Override
    public String toString() {
        return "Parameter{" +
                "id=" + id +
                ", typeId=" + typeId +
                ", value='" + value + '\'' +
                '}';
    }
}
