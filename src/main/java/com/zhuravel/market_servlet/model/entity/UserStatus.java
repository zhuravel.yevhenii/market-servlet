package com.zhuravel.market_servlet.model.entity;

import com.zhuravel.market_servlet.model.UserStatusType;

/**
 * @author Evgenii Zhuravel created on 24.05.2022
 * @version 1.0
 */
public class UserStatus extends BaseEntity {

    @Column(name = "name")
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public UserStatus() {
    }

    public UserStatus(Long id, String name) {
        this.id = id;
        this.name = name;
    }

    public UserStatus(UserStatusType statusType) {
        this.id = statusType.ordinal() + 1L;
        this.name = statusType.name();
    }

    @Override
    public String toString() {
        return "UserStatusType{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}
