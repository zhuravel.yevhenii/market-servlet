package com.zhuravel.market_servlet.model.entity;

import java.util.Objects;

/**
 * @author Evgenii Zhuravel created on 08.06.2022
 * @version 1.0
 */
public class BaseEntity {

    @Column(name = "id")
    protected Long id;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        BaseEntity that = (BaseEntity) o;
        return id.equals(that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
