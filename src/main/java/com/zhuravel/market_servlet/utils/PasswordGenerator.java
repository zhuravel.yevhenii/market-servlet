package com.zhuravel.market_servlet.utils;

import com.zhuravel.market_servlet.service.utils.PasswordEncoder;

import java.security.NoSuchAlgorithmException;

/**
 * @author Evgenii Zhuravel created on 21.06.2022
 * @version 1.0
 */
public class PasswordGenerator {

    public static void main(String[] args) throws NoSuchAlgorithmException {

        System.out.println("Encrypted Admin Password: " + PasswordEncoder.encode("Admin"));
        System.out.println("Encrypted Seller Password: " + PasswordEncoder.encode("Seller"));
        System.out.println("Encrypted ActiveUser Password: " + PasswordEncoder.encode("ActiveUser"));
        System.out.println("Encrypted BlockedUser Password: " + PasswordEncoder.encode("BlockedUser"));
        System.out.println("Encrypted DeletedUser Password: " + PasswordEncoder.encode("DeletedUser"));
    }
}
