<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<c:set var="contextPath" value="${pageContext.request.contextPath}" />
<c:set var="language" value="${not empty sessionScope.language ? sessionScope.language : not empty language ? language : pageContext.request.locale}" scope="session" />
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="messages"/>

<!DOCTYPE html>
<html>
    <jsp:include page="../fragments/header.jsp"/>
<body>
    <jsp:include page="../fragments/navbar.jsp"/>

    <main class="page gallery-page">
        <section class="clean-block clean-gallery dark">
            <div class="container_">
                <div class="block-heading">
                    <h2><a class="text-info"><fmt:message key="label.users_list"/></a></h2>
                </div>
                <div class="row">
                    <div class="col-md">
                        <div class="products">
                                <div class="table-responsive table mt-2" id="dataTable" role="grid" aria-describedby="dataTable_info">
                                    <div class="col-md-11">
                                    <form class="nav-item form-select border-0" name="form" action="${contextPath}/admin/users" method="POST">
                                        <table class="table dataTable my-0">
                                            <tbody>
                                            <tr>
                                                <td>
                                                    <div class="row" aria-controls="dataTable">
                                                        <label for="pageSize_" class="col-md-3"><fmt:message key="label.show"/></label>
                                                        <form class="nav-item form-select border-0" name="form" action="${contextPath}/admin/users" method="POST">
                                                            <select id="pageSize_" name="pageSize" class="form-control custom-select" onchange="document.form.submit();">
                                                                <option value="6" ${pageSize == 6 ? 'selected' : ''}>6</option>
                                                                <option value="9" ${pageSize == 9 ? 'selected' : ''}>9</option>
                                                                <option value="12" ${pageSize == 12 ? 'selected' : ''}>12</option>
                                                                <option value="24" ${pageSize == 24 ? 'selected' : ''}>24</option>
                                                            </select>
                                                        </form>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td><div class="text-md dataTables_filter">
                                                    <label><input type="search" class="form-control form-control-sm" name="filter_login" value="${filter_login}"
                                                               aria-controls="dataTable" placeholder="<fmt:message key="label.login"/>">
                                                    </label>
                                                    </div>
                                                </td>
                                                <td><div class="text-md dataTables_filter">
                                                    <label><input type="search" class="form-control form-control-sm" name="filter_first_name" value="${filter_first_name}"
                                                               aria-controls="dataTable" placeholder="<fmt:message key="label.first_name"/>">
                                                    </label>
                                                    </div>
                                                </td>
                                                <td><div class="text-md dataTables_filter">
                                                    <label><input type="search" class="form-control form-control-sm" name="filter_last_name" value="${filter_last_name}"
                                                               aria-controls="dataTable" placeholder="<fmt:message key="label.last_name"/>">
                                                    </label>
                                                    </div>
                                                </td>
                                                <td><div class="text-md dataTables_filter">
                                                    <label><input type="search" class="form-control form-control-sm" name="filter_email" value="${filter_email}"
                                                               aria-controls="dataTable" placeholder="<fmt:message key="label.email"/>">
                                                    </label>
                                                    </div>
                                                </td>
                                                <td><div class="text-md dataTables_filter">
                                                    <label><input type="search" class="form-control form-control-sm" name="filter_locale" value="${filter_locale}"
                                                               aria-controls="dataTable" placeholder="<fmt:message key="label.language"/>">
                                                    </label>
                                                    </div>
                                                </td>
                                                <td><div class="text-md dataTables_filter">
                                                    <select id="status" name="filter_status_id" class="form-control custom-select">
                                                        <c:forEach var="status" items="${statuses}">
                                                            <option value="${status.id}" ${filter_status_id == status.id ? 'selected' : ''}>${status.name}</option>
                                                        </c:forEach>
                                                    </select>
                                                    </div>
                                                </td>
                                                <td><div class="text-md dataTables_filter">
                                                    <select id="role" name="filter_role_id" class="form-control custom-select">
                                                        <c:forEach var="role" items="${roles}">
                                                            <option value="${role.id}" ${filter_role_id == role.id ? 'selected' : ''}>${role.name}</option>
                                                        </c:forEach>
                                                    </select>
                                                    </div>
                                                </td>

                                                <td>
                                                    <button class="btn btn-outline-dark btn-block" type="submit"><fmt:message key="label.search"/></button>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </form>
                                    </div>
                                </div>
                            <jsp:useBean id="users" scope="request" type="java.util.List"/>
                                <div class="table-responsive table mt-2" id="dataTable" role="grid" aria-describedby="dataTable_info">
                                    <table class="table dataTable my-0">
                                        <thead>
                                        <tr>
                                            <th>
                                                <div class="row">
                                                    <div class="col-md">
                                                        <fmt:message key="label.login"/>
                                                    </div>
                                                </div>
                                            </th>
                                            <th>
                                                <fmt:message key="label.first_name"/>
                                            </th>
                                            <th><fmt:message key="label.last_name"/></th>
                                            <th><fmt:message key="label.email"/></th>
                                            <th><fmt:message key="label.language"/></th>
                                            <th><fmt:message key="label.status"/></th>
                                            <th><fmt:message key="label.role"/></th>
                                            <th><fmt:message key="label.edit"/></th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <c:forEach var="user" items="${users}">
                                            <tr>
                                                <td>${user.login}</td>
                                                <td>${user.firstName}</td>
                                                <td>${user.lastName}</td>
                                                <td>${user.email}</td>
                                                <td>${user.locale}</td>
                                                <td>${user.status.name}</td>
                                                <td>${user.role.name}</td>
                                                <td><a class="btn btn-primary" type="button" href="${contextPath}/admin/users/edit?id=${user.id}">
                                                        <fmt:message key="label.edit"/></a>
                                                </td>
                                            </tr>
                                        </c:forEach>
                                        </tbody>
                                    </table>
                                </div>
                                <nav>
                                    <c:if test="${pageNumbers.size() > 0}">
                                        <ul class="pagination pagination-sm">
                                            <c:forEach var="page" items="${pageNumbers}">
                                                <li class="page-item ${page==currentPage ? 'active' : ''}">
                                                    <a class="page-link" href="${contextPath}/admin/users?pageSize=${pageSize}&currentPage=${page}">
                                                            ${page}</a>
                                                </li>
                                            </c:forEach>
                                        </ul>
                                    </c:if>
                                </nav>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </main>

    <jsp:include page="../fragments/footer.jsp"/>

</body>
</html>