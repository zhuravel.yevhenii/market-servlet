<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="ex" uri="/WEB-INF/date_time_tag.tld"%>

<c:set var="contextPath" value="${pageContext.request.contextPath}" />
<c:set var="language" value="${not empty sessionScope.language ? sessionScope.language : not empty language ? language : pageContext.request.locale}" scope="session" />
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="messages"/>

<!DOCTYPE html>
<html>
    <jsp:include page="../fragments/header.jsp"/>
<body>
    <jsp:include page="../fragments/navbar.jsp"/>

    <main class="page gallery-page">
        <section class="clean-block clean-gallery dark">
            <div class="container_">
                <div class="block-heading">
                    <h2><a class="text-info"><fmt:message key="label.orders_list"/></a></h2>
                </div>
                <div class="row">
                    <div class="col-md">
                        <div class="products">
                                <div class="table-responsive table mt-2" id="dataTable" role="grid" aria-describedby="dataTable_info">
                                    <div class="col-md-11">
                                    <form class="nav-item form-select border-0" name="form" action="${contextPath}/admin/orders" method="POST">
                                        <table class="table dataTable my-0">
                                            <tbody>
                                            <tr>
                                                <td><div class="text-md dataTables_filter">
                                                    <select name="filter_date_from" class="form-control custom-select">
                                                        <option value="" ${filter_date_from == null ? 'selected' : ''}></option>
                                                        <c:forEach var="date" items="${dates}">
                                                            <option value="${date}" ${filter_date_from == date ? 'selected' : ''}><ex:DateTime>${date}</ex:DateTime></option>
                                                        </c:forEach>
                                                    </select>
                                                    </div>
                                                </td>
                                                <td><div class="text-md dataTables_filter">
                                                    <select name="filter_date_until" class="form-control custom-select">
                                                        <option value="" ${filter_date_until == null ? 'selected' : ''}></option>
                                                        <c:forEach var="date" items="${dates}">
                                                            <option value="${date}" ${filter_date_until == date ? 'selected' : ''}><ex:DateTime>${date}</ex:DateTime></option>
                                                        </c:forEach>
                                                    </select>
                                                    </div>
                                                </td>
                                                <td><div class="text-md dataTables_filter">
                                                    <select id="status" name="filter_status" class="form-control custom-select">
                                                        <option value="" ${filter_status == null ? 'selected' : ''}></option>
                                                        <c:forEach var="status" items="${statuses}">
                                                            <option value="${status}" ${filter_status == status ? 'selected' : ''}>${status}</option>
                                                        </c:forEach>
                                                    </select>
                                                    </div>
                                                </td>

                                                <td>
                                                    <button class="btn btn-outline-dark btn-block" type="submit"><fmt:message key="label.search"/></button>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </form>
                                    </div>
                                </div>
                                <div class="table-responsive table mt-2" role="grid" aria-describedby="dataTable_info">
                                    <table class="table dataTable my-0" id="1">
                                        <thead>
                                        <tr>
                                            <th style="width: 60%"><fmt:message key="label.date_time"/></th>
                                            <th style="width: 10%"><fmt:message key="label.quantity"/></th>
                                            <th style="width: 10%"><fmt:message key="label.status"/></th>
                                            <th style="width: 10%"></th>
                                            <th style="width: 10%"></th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <c:forEach var="order" items="${orders}" varStatus="item">
                                            <tr>
                                                <td colspan="4">
                                                    <table class="table dataTable my-0" id="3">
                                                        <tbody>
                                                        <tr>
                                                            <td style="width: 60%"><a data-bs-toggle="collapse" href="#collapse_${item.index}" role="button" aria-expanded="false" aria-controls="collapse_${item.index}">
                                                                <ex:DateTime>${order.registrationDateTime}</ex:DateTime></a></td>
                                                            <td style="width: 10%">${order.products.size()}</td>
                                                            <td style="width: 10%">${order.status}</td>
                                                            <td style="width: 10%">
                                                                <a class="btn btn-outline-dark btn-block" href="${contextPath}/admin/orders/paid?date_time=${order.registrationDateTime}">
                                                                    <fmt:message key="label.paid"/></a>
                                                            </td>
                                                            <td style="width: 10%">
                                                                <a class="btn btn-outline-dark btn-block" href="${contextPath}/admin/orders/canceled?date_time=${order.registrationDateTime}">
                                                                    <fmt:message key="label.canceled"/></a>
                                                            </td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                    <div class="collapse" id="collapse_${item.index}">
                                                        <div class="card card-body">
                                                            <table class="table dataTable my-0" id="2">
                                                                <thead>
                                                                <tr>
                                                                    <th><fmt:message key="label.producer"/></th>
                                                                    <th><fmt:message key="label.name"/></th>
                                                                    <th><fmt:message key="label.quantity"/></th>
                                                                </tr>
                                                                </thead>
                                                                <tbody>
                                                                <c:forEach var="product" items="${order.products}">
                                                                    <tr>
                                                                        <td>${product.key.producer.name}</td>
                                                                        <td>${product.key.name}</td>
                                                                        <td>${product.value}</td>
                                                                    </tr>
                                                                </c:forEach>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                        </c:forEach>
                                        </tbody>
                                        <tfoot>
                                        <tr>
                                            <td style="width: 60%"><strong><fmt:message key="label.date_time"/></strong></td>
                                            <td style="width: 10%"><strong><fmt:message key="label.quantity"/></strong></td>
                                            <td style="width: 10%"><strong><fmt:message key="label.status"/></strong></td>
                                            <td style="width: 10%"><strong></strong></td>
                                            <td style="width: 10%"><strong></strong></td>
                                        </tr>
                                        </tfoot>
                                    </table>
                                </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </main>

    <jsp:include page="../fragments/footer.jsp"/>

</body>
</html>