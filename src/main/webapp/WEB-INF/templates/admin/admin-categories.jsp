<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<c:set var="contextPath" value="${pageContext.request.contextPath}" />
<c:set var="language" value="${not empty sessionScope.language ? sessionScope.language : not empty language ? language : pageContext.request.locale}" scope="session" />
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="messages"/>

<!DOCTYPE html>
<html>
    <jsp:include page="../fragments/header.jsp"/>
<body>
    <jsp:include page="../fragments/navbar.jsp"/>

    <main class="page gallery-page">
        <section class="clean-block clean-gallery dark">
            <div class="container_">
                <div class="block-heading">
                    <jsp:useBean id="category" scope="request" type="com.zhuravel.market_servlet.service.dto.LocalizedCategoryDto"/>
                    <c:if test="${category != null}">
                        <h2><a class="text-info" href="${contextPath}/admin/categories?id=${category.parentCategoryId}">${category.localizedName}</a></h2>
                    </c:if>
                </div>
                <div class="row">
                    <div class="col-md-2">
                        <jsp:include page="../fragments/categorybar.jsp">
                            <jsp:param name="currentId" value="${category.id}"/>
                            <jsp:param name="servletUrl" value="${contextPath}/admin/categories"/>
                        </jsp:include>
                    </div>
                    <div class="col-md-10">
                        <div class="products">
                            <jsp:useBean id="categories" scope="request" type="java.util.List"/>
                            <c:if test="${categories.size() > 0}">
                                <div class="table-responsive table mt-2" id="dataTable" role="grid" aria-describedby="dataTable_info">
                                    <table class="table dataTable my-0">
                                        <tbody>
                                        <tr>
                                            <td>
                                                <div class="row" aria-controls="dataTable">
                                                    <label for="pageSize_" class="col-md-3"><fmt:message key="label.show"/></label>
                                                    <form class="nav-item form-select border-0" name="formPage" action="${contextPath}/admin/categories?id=${category.id}" method="POST">
                                                        <select id="pageSize_" name="pageSize" class="form-control custom-select" onchange="document.formPage.submit();">
                                                            <option value="6" ${pageSize == 6 ? 'selected' : ''}>6</option>
                                                            <option value="9" ${pageSize == 9 ? 'selected' : ''}>9</option>
                                                            <option value="12" ${pageSize == 12 ? 'selected' : ''}>12</option>
                                                            <option value="24" ${pageSize == 24 ? 'selected' : ''}>24</option>
                                                        </select>
                                                    </form>
                                                </div>
                                            </td>
                                            <form class="nav-item form-select border-0" name="form" action="${contextPath}/admin/categories?id=${category.id}" method="POST">
                                                <td>
                                                    <div class="text-md dataTables_filter">
                                                    <label>
                                                        <input type="search" class="form-control form-control-sm" name="filter_en" value="${filter_en}" aria-controls="dataTable" placeholder="EN <fmt:message key="label.search"/>">
                                                    </label>
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="text-md dataTables_filter">
                                                    <label>
                                                        <input type="search" class="form-control form-control-sm" name="filter_uk" value="${filter_uk}" aria-controls="dataTable" placeholder="UK <fmt:message key="label.search"/>">
                                                    </label>
                                                    </div>
                                                </td>

                                                <td>
                                                    <button class="btn btn-outline-dark btn-block" type="submit"><fmt:message key="label.search"/></button>
                                                </td>
                                                <td>
                                                    <div class="text-md-2">
                                                    </div>
                                                </td>
                                            </form>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="table-responsive table mt-2" id="dataTable" role="grid" aria-describedby="dataTable_info">
                                    <table class="table dataTable my-0">
                                        <thead>
                                        <tr>
                                            <th><fmt:message key="label.name"/></th>
                                            <th>EN</th>
                                            <th>UK</th>
                                            <th><fmt:message key="label.edit"/></th>
                                            <th><fmt:message key="label.delete"/></th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <c:forEach var="category" items="${categories}">
                                            <tr>
                                                <td>
                                                    <a class="lightbox" href="${contextPath}/admin/categories?id=${category.id}">
                                                        <img class="rounded-circle mr-2" width="30" height="30" src="/resources/static${category.imagePath}" alt="">
                                                        ${category.name}
                                                    </a>
                                                </td>
<%--                                                <c:forEach var="localizedNames" items="${category.localizedNames}">--%>
                                                    <td>${category.localizedNames['en']}</td>
                                                    <td>${category.localizedNames['uk']}</td>
<%--                                                </c:forEach>--%>
                                                <td>
                                                    <a class="btn btn-primary" type="button" href="${contextPath}/admin/categories/edit?id=${category.id}">
                                                        <fmt:message key="label.edit"/>
                                                    </a>
                                                </td>
                                                <td>
                                                    <a class="btn btn-danger" type="button" href="${contextPath}/admin/categories/delete?id=${category.id}">
                                                        <fmt:message key="label.delete"/>
                                                    </a>
                                                </td>
                                            </tr>
                                        </c:forEach>
                                        </tbody>
                                        <tfoot>
                                        <tr>
                                            <th><fmt:message key="label.name"/></th>
                                            <th>EN</th>
                                            <th>UK</th>
                                            <th><fmt:message key="label.edit"/></th>
                                            <th><fmt:message key="label.delete"/></th>
                                        </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </c:if>
                            <c:if test="${categories.size() == 0}">
                                <h3><fmt:message key="label.categories_not_found"/></h3>
                            </c:if>
                            <div class="table-responsive table mt-2" role="grid" aria-describedby="dataTable_info">
                            <form action="${pageContext.request.contextPath}/admin/categories/add" method="post">
                                <input type="hidden" id="parentCategoryId" name="parentCategoryId" value="${category.id}">
                                <table class="table dataTable my-0">
                                    <tbody>
                                    <tr>
                                        <td>
                                            <label>
                                                <input type="text" class="form-control form-control-sm" name="name" aria-controls="dataTable"
                                                       value="${categoryForm.name}"
                                                       placeholder=<fmt:message key="label.name"/>>
                                            </label>
                                            <div>
                                                <span style="color:red">${errors.name}</span>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="text-md dataTables_filter">
                                                <label>
                                                    <input type="text" class="form-control form-control-sm" name="name-en" aria-controls="dataTable"
                                                           value="${categoryForm.localizedNames['en']}"
                                                           placeholder="EN">
                                                </label>
                                                <div>
                                                    <span style="color:red">${errors.localizedNames_en}</span>
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="text-md dataTables_filter">
                                                <label>
                                                    <input type="text" class="form-control form-control-sm" name="name-uk" aria-controls="dataTable"
                                                           value="${categoryForm.localizedNames['uk']}"
                                                           placeholder="UK">
                                                </label>
                                                <div>
                                                    <span style="color:red">${errors.localizedNames_uk}</span>
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                            <button class="btn btn-primary btn-block" type="submit"><fmt:message key="label.add"/></button>
                                        </td>
                                        <td>
                                            <div class="text-md-2">
                                            </div>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </form>
                            </div>
                            <nav>
                                <c:if test="${pageNumbers.size() > 0}">
                                    <ul class="pagination pagination-sm">
                                        <c:forEach var="page" items="${pageNumbers}">
                                            <li class="page-item ${page==currentPage ? 'active' : ''}">
                                                <a class="page-link" href="${contextPath}/admin/categories?id=${category.id}&pageSize=${pageSize}&currentPage=${page}">
                                                        ${page}</a>
                                            </li>
                                        </c:forEach>
                                    </ul>
                                </c:if>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </main>

    <jsp:include page="../fragments/footer.jsp"/>

<%--    <footer replace="/WEB-INF/templates/fragments/footer:: footer(_)"></footer>--%>

</body>
</html>