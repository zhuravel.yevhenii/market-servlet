<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<c:set var="contextPath" value="${pageContext.request.contextPath}" />

<c:set var="language" value="${not empty sessionScope.language ? sessionScope.language : not empty language ? language : pageContext.request.locale}" scope="session" />
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="messages"/>

<!DOCTYPE html>
<html>
    <jsp:include page="../fragments/header.jsp"/>
<body>
    <jsp:include page="../fragments/navbar.jsp"/>

    <main class="page gallery-page">
        <section class="clean-block clean-gallery dark">
            <div class="container">
                <div class="block-heading">
                    <jsp:useBean id="parameterType" scope="request" type="com.zhuravel.market_servlet.service.dto.ParameterTypeDto"/>
                    <c:if test="${parameterType != null}">
                        <h2><a class="text-info" href="${contextPath}/admin/categories?id=${parameterType.categoryId}">${parameterType.name}</a></h2>
                    </c:if>
                </div>
                <div class="row">
                    <form class="col-md-3" action="${pageContext.request.contextPath}/admin/parameter-type/edit" method="post">
                        <input type="hidden" id="id" name="id" value="${parameterType.id}">
                        <input type="hidden" id="categoryId" name="categoryId" value="${parameterType.categoryId}">
                        <div class="form-group col-md-10">
                            <label for="name">* <fmt:message key="label.name"/></label>
                            <input class="form-control" type="text" id="name" name="name" value="${parameterType.name}">
                            <span style="color:red">${errors.name}</span>
                        </div>
                        <div class="form-group col-md-10">
                            <label for="name-en">* EN</label>
                            <input class="form-control" type="text" id="name-en" name="name-en" value="${parameterType.translations.en}">
                            <span style="color:red">${errors.translations_en}</span>
                        </div>
                        <div class="form-group col-md-10">
                            <label for="name-uk">* UK</label>
                            <input class="form-control" type="text" id="name-uk" name="name-uk" value="${parameterType.translations.uk}">
                            <span style="color:red">${errors.translations_uk}</span>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <button class="btn btn-primary btn-block" type="submit"><fmt:message key="label.save"/></button>
                            </div>
                            <div class="col-md-6">
                                <a class="btn btn-outline-dark btn-block" href="../categories/edit?id=${parameterType.categoryId}"><fmt:message key="label.back"/></a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </section>
    </main>

    <jsp:include page="../fragments/footer.jsp"/>

</body>
</html>