<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<c:set var="contextPath" value="${pageContext.request.contextPath}" />
<jsp:useBean id="parameterTypes" scope="request" type="java.util.List"/>

<c:set var="language" value="${not empty sessionScope.language ? sessionScope.language : not empty language ? language : pageContext.request.locale}" scope="session" />
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="messages"/>

<!DOCTYPE html>
<html>
    <jsp:include page="../fragments/header.jsp"/>
<body>
    <jsp:include page="../fragments/navbar.jsp"/>

    <main class="page gallery-page">
        <section class="clean-block clean-gallery dark">
            <div class="container_">
                <div class="block-heading">
                    <jsp:useBean id="categoryForm" scope="request" type="com.zhuravel.market_servlet.service.dto.CategoryDto"/>
                    <c:if test="${categoryForm != null}">
                        <h2><a class="text-info" href="${contextPath}/admin/categories?id=${categoryForm.parentCategoryId}">${categoryForm.name}</a></h2>
                    </c:if>
                </div>
                <div class="row">
                    <form class="col-md-3" action="${pageContext.request.contextPath}/admin/categories/edit" method="post">
                        <input type="hidden" id="id" name="id" value="${categoryForm.id}">
                        <input type="hidden" id="parentCategoryId" name="parentCategoryId" value="${categoryForm.parentCategoryId}">
                        <input type="hidden" id="imagePath" name="imagePath" value="${categoryForm.imagePath}">
                        <div class="form-group col-md-10">
                            <img class="img-fluid d-block mx-auto" src="${contextPath}/resources/static${categoryForm.imagePath}" alt="">
                        </div>
                        <div class="form-group col-md-10">
                            <label for="name">* <fmt:message key="label.name"/></label>
                            <input class="form-control" type="text" id="name" name="name" value="${categoryForm.name}">
                            <span style="color:red">${errors.name}</span>
                        </div>
                        <div class="form-group col-md-10">
                            <label for="name-en">* EN</label>
                            <input class="form-control" type="text" id="name-en" name="name-en" value="${categoryForm.localizedNames['en']}">
                            <span style="color:red">${errors.localizedNames_en}</span>
                        </div>
                        <div class="form-group col-md-10">
                            <label for="name-uk">* UK</label>
                            <input class="form-control" type="text" id="name-uk" name="name-uk" value="${categoryForm.localizedNames['uk']}">
                            <span style="color:red">${errors.localizedNames_uk}</span>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <button class="btn btn-primary btn-block" type="submit"><fmt:message key="label.save"/></button>
                            </div>
                            <div class="col-md-6">
                                <a class="btn btn-outline-dark btn-block" href="../categories"><fmt:message key="label.back"/></a>
                            </div>
                        </div>
                    </form>

                    <div class="col-md-5">
                        <h3><fmt:message key="label.parameterTypes"/></h3>
                        <div class="table-responsive table mt-2" role="grid" aria-describedby="dataTable_info">
                            <table class="table dataTable my-0">
                                <thead>
                                <tr>
                                    <th>EN</th>
                                    <th>UK</th>
                                    <th><fmt:message key="label.edit"/></th>
                                    <th><fmt:message key="label.delete"/></th>
                                </tr>
                                </thead>
                                <tbody>
                                <c:forEach var="parameterType" items="${parameterTypes}">
                                    <tr class="${type == parameterType.id ? 'table-primary' : ''}">
                                        <td><a href="${contextPath}/admin/categories/edit?id=${categoryForm.id}&type=${parameterType.id}">
                                                    ${parameterType.translations['en']}</a>
                                        </td>
                                        <td><a href="${contextPath}/admin/categories/edit?id=${categoryForm.id}&type=${parameterType.id}">
                                                    ${parameterType.translations['uk']}</a>
                                        </td>
                                        <td><a class="btn btn-primary" type="button" href="${contextPath}/admin/parameter-type/edit?id=${parameterType.id}">
                                                <fmt:message key="label.edit"/></a>
                                        </td>
                                        <td><a class="btn btn-danger" type="button" href="${contextPath}/admin/parameter-type/delete?id=${parameterType.id}">
                                                <fmt:message key="label.delete"/></a>
                                        </td>
                                    </tr>
                                </c:forEach>
                                </tbody>
                            </table>
                        </div>
                        <div class="table-responsive table mt-2" role="grid" aria-describedby="dataTable_info">
                            <form action="${contextPath}/admin/parameter-type/add" method="post">
                                <input type="hidden" name="categoryId" value="${categoryForm.id}">
                                <table class="table dataTable my-0">
                                    <tbody>
                                    <tr>
                                        <td>
                                            <div class="text-md">
                                                <label>
                                                    <input type="text" class="form-control form-control-sm" name="name-en" aria-controls="dataTable" placeholder="EN"
                                                        value="${parameterType.translations.en}">
                                                </label>
                                                <div>
                                                    <span style="color:red">${errors.type_translations_en}</span>
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="text-md">
                                                <label>
                                                    <input type="text" class="form-control form-control-sm" name="name-uk" aria-controls="dataTable" placeholder="UK"
                                                           value="${parameterType.translations.uk}">
                                                </label>
                                                <div>
                                                    <span style="color:red">${errors.type_translations_uk}</span>
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                            <button class="btn btn-primary btn-block" type="submit"><fmt:message key="label.add"/></button>
                                        </td>
                                        <td>
                                            <div class="text-md-2">
                                            </div>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </form>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <h3><fmt:message key="label.parameters"/></h3>
                        <div class="table-responsive table mt-2" role="grid" aria-describedby="dataTable_info">
                            <c:if test="${categoryForm != null}">
                                <table class="table dataTable my-0">
                                    <thead>
                                    <tr>
                                        <th><fmt:message key="label.value"/></th>
                                        <th><fmt:message key="label.edit"/></th>
                                        <th><fmt:message key="label.delete"/></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <c:forEach var="parameter" items="${parameters}">
                                        <tr>
                                            <td>${parameter.value}</td>
                                            <td>
                                                <a class="btn btn-primary" type="button" href="${contextPath}/admin/parameter/edit?id=${parameter.id}&categoryId=${categoryForm.id}">
                                                    <fmt:message key="label.edit"/>
                                                </a>
                                            </td>
                                            <td>
                                                <a class="btn btn-danger" type="button" href="${contextPath}/admin/parameter/delete?id=${parameter.id}">
                                                    <fmt:message key="label.delete"/>
                                                </a>
                                            </td>
                                        </tr>
                                    </c:forEach>
                                    </tbody>
                                </table>

                                <c:if test="${type != null}">
                                <div class="table-responsive table mt-2" role="grid" aria-describedby="dataTable_info">
                                    <form action="${contextPath}/admin/parameter/add" method="post">
                                        <input type="hidden" name="categoryId" value="${categoryForm.id}">
                                        <input type="hidden" id="parameterTypeId" name="parameterTypeId" value="${type}">
                                        <table class="table dataTable my-0">
                                            <tbody>
                                            <tr>
                                                <td>
                                                    <div class="text-md dataTables_filter">
                                                        <label>
                                                            <input type="text" class="form-control form-control-sm" name="value" aria-controls="dataTable" placeholder=""
                                                                   value="${parameter.value}">
                                                        </label>
                                                        <div>
                                                            <span style="color:red">${errors.value}</span>
                                                        </div>
                                                    </div>
                                                </td>
                                                <td>
                                                    <button class="btn btn-primary btn-block" type="submit"><fmt:message key="label.add"/></button>
                                                </td>
                                                <td>
                                                    <div class="text-md-2">
                                                    </div>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </form>
                                </div>
                                </c:if>
                            </c:if>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </main>

    <jsp:include page="../fragments/footer.jsp"/>

</body>
</html>