<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<c:set var="contextPath" value="${pageContext.request.contextPath}" />

<c:set var="language" value="${not empty sessionScope.language ? sessionScope.language : not empty language ? language : pageContext.request.locale}" scope="session" />
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="messages"/>

<!DOCTYPE html>
<html>
    <jsp:include page="../fragments/header.jsp"/>
<body>
    <jsp:include page="../fragments/navbar.jsp"/>

    <main class="page gallery-page">
        <section class="clean-block clean-gallery dark">
            <div class="container">
                <div class="block-heading">
                    <jsp:useBean id="user" scope="request" type="com.zhuravel.market_servlet.service.dto.UserDto"/>
                    <c:if test="${user != null}">
                        <h2><a class="text-info" href="${contextPath}/admin/users"><fmt:message key="label.edition_user"/></a></h2>
                    </c:if>
                </div>
                <div class="row">
                    <form class="col-md-12" name="userForm" action="${pageContext.request.contextPath}/admin/users/edit" method="post">
                        <input type="hidden" id="id" name="id" value="${user.id}">
                        <input type="hidden" id="password" name="password" value="${user.password}">
                        <div class="table-responsive table" id="dataTable" role="grid" aria-describedby="dataTable_info">
                            <table class="table dataTable my-0">
                                <tbody>
                                <tr>
                                    <td>
                                        <label><fmt:message key="label.login"/></label>
                                    </td>
                                    <td>
                                        <input type="hidden" id="login" name="login" value="${user.login}">
                                        <span>${user.login}</span>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <label><fmt:message key="label.first_name"/></label>
                                    </td>
                                    <td>
                                        <input type="hidden" id="firstName" name="firstName" value="${user.firstName}">
                                        <span>${user.firstName}</span>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <label><fmt:message key="label.last_name"/></label>
                                    </td>
                                    <td>
                                        <input type="hidden" id="lastName" name="lastName" value="${user.lastName}">
                                        <span>${user.lastName}</span>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <label><fmt:message key="label.email"/></label>
                                    </td>
                                    <td>
                                        <input type="hidden" id="email" name="email" value="${user.email}">
                                        <span>${user.email}</span>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <label><fmt:message key="label.language"/></label>
                                    </td>
                                    <td>
                                        <input type="hidden" id="locale" name="locale" value="${user.locale}">
                                        <span>${user.locale}</span>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <label for="status"><fmt:message key="label.status"/></label>
                                    </td>
                                    <td>
                                        <select id="status" name="status" class="form-control custom-select">
                                            <c:forEach var="status" items="${statuses}">
                                                <option value="${status.id}" ${user.status.id == status.id ? 'selected' : ''}>${status.name}</option>
                                            </c:forEach>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <label for="role"><fmt:message key="label.role"/></label>
                                    </td>
                                    <td>
                                        <select id="role" name="role" class="form-control custom-select">
                                            <c:forEach var="role" items="${roles}">
                                                <option value="${role.id}" ${user.role.id == role.id ? 'selected' : ''}>${role.name}</option>
                                            </c:forEach>
                                        </select>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="row md-8">
                            <div class="col-md-6">
                                <button class="btn btn-primary btn-block" type="submit"><fmt:message key="label.save"/></button>
                            </div>
                            <div class="col-md-6">
                                <a class="btn btn-outline-dark btn-block" href="${contextPath}/admin/users"><fmt:message key="label.back"/></a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </section>
    </main>

    <jsp:include page="../fragments/footer.jsp"/>

</body>
</html>