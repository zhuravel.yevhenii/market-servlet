<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<c:set var="contextPath" value="${pageContext.request.contextPath}" />

<c:set var="language" value="${not empty sessionScope.language ? sessionScope.language : not empty language ? language : pageContext.request.locale}" scope="session" />
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="messages"/>

<!DOCTYPE html>
<html>
    <jsp:include page="../fragments/header.jsp"/>
<body>
    <jsp:include page="../fragments/navbar.jsp"/>

    <main class="page gallery-page">
        <section class="clean-block clean-gallery dark">
            <div class="container">
                <div class="block-heading">
                    <jsp:useBean id="parameter" scope="request" type="com.zhuravel.market_servlet.service.dto.ParameterDto"/>
                    <c:if test="${parameter != null}">
                        <h2><a class="text-info" href="${contextPath}/admin/categories/edit?id=${categoryId}&type=${parameter.typeId}"><fmt:message key="label.edition_parameter_for_type"/>${parameter.typeName}</a></h2>
                    </c:if>
                </div>
                <div class="row">
                    <form class="col-md-3" action="${pageContext.request.contextPath}/admin/parameter/edit" method="post">
                        <input type="hidden" id="id" name="id" value="${parameter.id}">
                        <input type="hidden" id="typeId" name="typeId" value="${parameter.typeId}">
                        <input type="hidden" id="typeName" name="typeName" value="${parameter.typeName}">
                        <input type="hidden" id="categoryId" name="categoryId" value="${categoryId}">
                        <div class="form-group col-md-10">
                            <label for="value">* <fmt:message key="label.value"/></label>
                            <input class="form-control" type="text" id="value" name="value" value="${parameter.value}">
                            <span style="color:red">${errors.value}</span>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <button class="btn btn-primary btn-block" type="submit"><fmt:message key="label.save"/></button>
                            </div>
                            <div class="col-md-6">
                                <a class="btn btn-outline-dark btn-block" href="${contextPath}/admin/categories/edit?id=${categoryId}&type=${parameter.typeId}"><fmt:message key="label.back"/></a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </section>
    </main>

    <jsp:include page="../fragments/footer.jsp"/>

</body>
</html>