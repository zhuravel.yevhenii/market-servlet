<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="ex" uri="/WEB-INF/date_time_tag.tld" %>

<c:set var="contextPath" value="${pageContext.request.contextPath}"/>

<c:set var="language"
       value="${not empty sessionScope.language ? sessionScope.language : not empty language ? language : pageContext.request.locale}"
       scope="session"/>
<fmt:setLocale value="${language}"/>
<fmt:setBundle basename="messages"/>

<jsp:useBean id="currentPage" scope="request" type="java.lang.Integer"/>

<!DOCTYPE html>
<html lang="en">

    <jsp:include page="fragments/header.jsp"/>

    <body>
        <jsp:include page="fragments/navbar.jsp"/>

        <main class="page catalog-page">
            <section class="clean-block clean-catalog dark">
                <div class="container">
                    <div class="block-heading">
                        <c:if test="${category != null}">
                            <h2><a class="text-info" href="${contextPath}/catalog?id=${category.parentCategoryId}">
                                    ${category.localizedName}</a></h2>
                        </c:if>
                    </div>
                    <div class="content">
                        <div class="row">
                            <form class="col-md-3" method="POST" action="${contextPath}/catalog?id=${category.id}">
                                <div class="d-none d-md-block">
                                    <div class="filters">
                                        <fieldset>
                                            <div class="filter-item">
                                                <h3><fmt:message key="label.producer"/></h3>
                                                <c:forEach var="producer"
                                                           items="${filterCriteria.producers.parameters}">
                                                    <div class="form-check">
                                                        <input type="hidden" id="producerId"
                                                               name="filter-prod-${producer.id}" value="${producer.id}">
                                                        <input type="hidden" id="producerName"
                                                               name="filter-prod-${producer.id}"
                                                               value="${producer.name}">
                                                        <input class="form-check-input" type="checkbox"
                                                               id="filter-prod-${producer.id}"
                                                               name="filter-prod-${producer.id}"
                                                               value="true" ${producer.selected == true ? 'checked' : ''}>
                                                        <label for="filter-prod-${producer.id}"
                                                               class="form-check-label">${producer.name}</label>
                                                    </div>
                                                </c:forEach>
                                            </div>
                                            <div class="filter-item">
                                                <c:forEach var="parameterType" items="${filterCriteria.parameterTypes}">
                                                    <div class="filter-item">
                                                        <h3>${parameterType.name}</h3>
                                                        <c:forEach var="type" items="${parameterType.parameters}">
                                                            <div class="form-check">
                                                                <input type="hidden"
                                                                       name="filter-par-${parameterType.name}-${type.id}"
                                                                       value="${type.id}">
                                                                <input type="hidden"
                                                                       name="filter-par-${parameterType.name}-${type.id}"
                                                                       value="${type.name}">
                                                                <input class="form-check-input" type="checkbox"
                                                                       id="filter-par-${parameterType.name}-${type.id}"
                                                                       name="filter-par-${parameterType.name}-${type.id}"
                                                                       value="true" ${type.selected == true ? 'checked' : ''}>
                                                                <label for="filter-par-${parameterType.name}-${type.id}"
                                                                       class="form-check-label">${type.name}</label>
                                                            </div>
                                                        </c:forEach>
                                                    </div>
                                                </c:forEach>
                                            </div>
                                            <div class="input-group-append">
                                                <button class="btn btn-outline-secondary" type="submit"><fmt:message
                                                        key="label.filter"/></button>
                                            </div>
                                        </fieldset>
                                    </div>
                                </div>
                            </form>

                            <div class="col-md-9">
                                <div class="row" aria-controls="dataTable">
                                    <label for="pageSize_" class="col-md-3"><fmt:message key="label.show"/></label>
                                    <form class="nav-item form-select border-0" name="form"
                                          action="${contextPath}/catalog?id=${category.id}" method="POST">
                                        <select id="pageSize_" name="pageSize" class="form-control custom-select"
                                                onchange="document.form.submit();">
                                            <option value="6" ${pageSize == 6 ? 'selected' : ''}>6</option>
                                            <option value="9" ${pageSize == 9 ? 'selected' : ''}>9</option>
                                            <option value="12" ${pageSize == 12 ? 'selected' : ''}>12</option>
                                            <option value="24" ${pageSize == 24 ? 'selected' : ''}>24</option>
                                        </select>
                                    </form>
                                    <label for="sorting" class="col-md-3"><fmt:message key="label.sorting"/></label>
                                    <form class="nav-item form-select border-0" name="formSorting"
                                          action="${contextPath}/catalog?id=${category.id}" method="POST">
                                        <select id="sorting" name="sorting" class="form-control custom-select"
                                                onchange="document.formSorting.submit();">
                                            <option value="" ${sorting == '' ? 'selected' : ''}></option>
                                            <option value="name_asc" ${sorting == 'name_asc' ? 'selected' : ''}>
                                                <fmt:message key="label.name_asc"/></option>
                                            <option value="name_desc" ${sorting == 'name_desc' ? 'selected' : ''}>
                                                <fmt:message key="label.name_asc"/></option>
                                            <option value="price_asc" ${sorting == 'price_asc' ? 'selected' : ''}>
                                                <fmt:message key="label.price_asc"/></option>
                                            <option value="price_desc" ${sorting == 'price_desc' ? 'selected' : ''}>
                                                <fmt:message key="label.price_desc"/></option>
                                            <option value="newest" ${sorting == 'newest' ? 'selected' : ''}><fmt:message
                                                    key="label.newest"/></option>
                                            <option value="oldest" ${sorting == 'oldest' ? 'selected' : ''}><fmt:message
                                                    key="label.oldest"/></option>
                                        </select>
                                    </form>
                                </div>
                                <div class="products">
                                    <div class="row no-gutters">
                                        <jsp:useBean id="productList" scope="request" type="java.util.List"/>
                                        <c:forEach var="productItem" items="${productList}">
                                            <div class="col-12 col-md-6 col-lg-4">
                                                <div class="clean-product-item">
                                                    <c:if test="${sessionScope.user.role.name == 'ROLE_USER'}">
                                                        <div class="float-right">
                                                            <c:if test="${sessionScope.selectedProducts.quantities.containsKey(productItem.id)}">
                                                                <a class="btn btn-warning" type="button"
                                                                   href="${contextPath}/shopping-cart/add?productId=${productItem.id}&url=${httpServletRequest.requestURI}">
                                                                    <img class="img-fluid d-block mx-auto"
                                                                         src="${contextPath}/resources/static/img/shopping-cart.png"
                                                                         alt="">
                                                                </a>
                                                            </c:if>
                                                            <c:if test="${!sessionScope.selectedProducts.quantities.containsKey(productItem.id)}">
                                                                <a class="btn btn-light" type="button"
                                                                   href="${contextPath}/shopping-cart/add?productId=${productItem.id}&url=${httpServletRequest.requestURI}">
                                                                    <img class="img-fluid d-block mx-auto"
                                                                         src="${contextPath}/resources/static/img/shopping-cart.png"
                                                                         alt="">
                                                                </a>
                                                            </c:if>
                                                        </div>
                                                    </c:if>
                                                    <c:if test="${sessionScope.user.role.name == 'ROLE_SELLER'}">
                                                        <div class="float-right">
                                                            <a class="btn btn-warning" type="button"
                                                               href="${contextPath}/catalog/delete?id=${category.id}&productId=${productItem.id}">
                                                                <fmt:message key="label.delete"/>
                                                            </a>
                                                        </div>
                                                        <div class="image">
                                                            <a href="${contextPath}/catalog/edit?id=${category.id}&productId=${productItem.id}">
                                                                <img class="img-fluid d-block mx-auto"
                                                                     src="${contextPath}/resources/static${productItem.imagePath}"
                                                                     alt="">
                                                            </a>
                                                        </div>
                                                        <a href="${contextPath}/catalog/edit?id=${category.id}&productId=${productItem.id}">
                                                                ${productItem.producer.name} ${productItem.name}</a>
                                                    </c:if>
                                                    <c:if test="${sessionScope.user.role.name != 'ROLE_SELLER'}">
                                                        <div class="image">
                                                            <a href="../product-page?productId=${productItem.id}">
                                                                <img class="img-fluid d-block mx-auto"
                                                                     src="${contextPath}/resources/static${productItem.imagePath}"
                                                                     alt="">
                                                            </a>
                                                        </div>
                                                        <div class="product-name">
                                                            <a href="../product-page?productId=${productItem.id}">
                                                                    ${productItem.producer.name} ${productItem.name}</a>
                                                        </div>
                                                    </c:if>
                                                    <div>
                                                        <small>${productItem.parametersString}</small>
                                                    </div>
                                                    <div>
                                                        <small><ex:DateTime>${productItem.createTime}</ex:DateTime></small>
                                                    </div>
                                                    <div class="about">
                                                        <div class="price float-right">
                                                            <h3>${productItem.priceString}</h3>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                        </c:forEach>

                                        <c:if test="${sessionScope.user.role.name == 'ROLE_SELLER'}">
                                            <a class="btn btn-primary btn-block"
                                               href="${contextPath}/catalog/edit?id=${category.id}"><fmt:message
                                                    key="label.add"/></a>
                                        </c:if>
                                    </div>
                                    <nav>
                                        <c:if test="${pageNumbers.size() > 0}">
                                            <ul class="pagination pagination-sm">
                                                <c:forEach var="page" items="${pageNumbers}">
                                                    <li class="page-item ${page==currentPage ? 'active' : ''}">
                                                        <a class="page-link"
                                                           href="${contextPath}/catalog?id=${category.id}&pageSize=${pageSize}&currentPage=${page}">
                                                                ${page}</a>
                                                    </li>
                                                </c:forEach>
                                            </ul>
                                        </c:if>
                                    </nav>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </main>

        <jsp:include page="fragments/footer.jsp"/>

        <script src="${contextPath}/resources/static/assets/js/jquery.min.js"></script>
        <script src="${contextPath}/resources/static/assets/bootstrap/js/bootstrap.min.js"></script>
        <script src="${contextPath}/resources/static/assets/js/chart.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.4.1/jquery.easing.js"></script>
        <script src="${contextPath}/resources/static/assets/js/script.min.js"></script>
    </body>

</html>