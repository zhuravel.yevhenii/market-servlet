<%@ page pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<c:set var="contextPath" value="${pageContext.request.contextPath}" />

<c:set var="language" value="${not empty sessionScope.language ? sessionScope.language : not empty language ? language : pageContext.request.locale}" scope="session" />
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="messages"/>

<jsp:useBean id="product" scope="request" type="com.zhuravel.market_servlet.service.dto.ProductDto"/>
<!DOCTYPE html>
<html>

    <jsp:include page="fragments/header.jsp"/>

<body>
    <jsp:include page="fragments/navbar.jsp"/>

    <main class="page product-page">
        <section class="clean-block clean-product dark">
            <div class="container">
                <div class="block-heading">
                </div>
                <div class="block-content">
                    <div class="product-info">
                        <div class="row">
                            <div class="col-md-6">
                                <img class="img-fluid d-block mx-auto" src="${contextPath}/resources/static${product.imagePath}" alt="">
                            </div>
                            <div class="col-md-6">
                                <div class="info">
                                    <h3>${product.producer.name} ${product.localizedNames}</h3>
                                    <div class="price">
                                        <h3>${product.priceString}</h3>
                                    </div>
                                    <c:if test="${not empty sessionScope.user}">
                                        <a class="btn btn-primary" type="button" href="../shopping-cart/add/{productId}(productId=${product.id})">
                                            <i class="icon-basket"></i>
                                            <fmt:message key="label.add_to_cart"/>
                                        </a>
                                    </c:if>
                                    <div class="summary">
                                        <p>${product.localizedDescriptions}</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="product-info">
                        <div>
                            <ul class="nav nav-tabs" id="myTab">
                                <li class="nav-item"><a class="nav-link active" role="tab" data-toggle="tab" id="specifications-tabs" href="#specifications"><fmt:message key="label.specifications"/></a></li>
                            </ul>
                            <div class="tab-content" id="myTabContent">
                                <div class="tab-pane active fade show specifications" role="tabpanel" id="specifications">
                                    <div class="table-responsive table-bordered">
                                        <table class="table">
                                            <tbody>
                                                <c:forEach items="${product.parameters}" var="type">
                                                    <tr>
                                                        <td class="stat">${type.typeName}</td>
                                                        <td>${type.value}</td>
                                                    </tr>
                                                </c:forEach>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </main>

    <jsp:include page="fragments/footer.jsp"/>
</body>

</html>