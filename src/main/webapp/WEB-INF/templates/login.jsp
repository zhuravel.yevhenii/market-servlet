<%@ page pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<c:set var="language" value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}" scope="session" />
<c:set var="contextPath" value="${pageContext.request.contextPath}" />

<fmt:setLocale value="${language}" />
<fmt:setBundle basename="messages"/>

<!DOCTYPE html>
<html lang="${language}">
    <jsp:include page="fragments/header.jsp"/>

    <body>
        <main class="page login-page">
            <section class="clean-block clean-form dark">
                <div class="container">
                    <div class="block-heading">
                        <h2 class="text-info">Log In</h2>
                    </div>
                    <form name='f' action="${contextPath}/login" method="post">
                        <input type="hidden" name="redirectId" value="${param.redirectId}" />
                        <div class="form-group">
                            <label for="login">Login</label>
                            <input class="form-control item" type="text" name="login" id="login" value="${login}">
                            <span style="color:red">${errors.login}</span>
                        </div>
                        <div class="form-group">
                            <label for="password" ><fmt:message key="label.password"/></label>
                            <input class="form-control" type="password" name="password" id="password">
                            <span style="color:red">${errors.password}</span>
                        </div>
                        <button class="btn btn-primary btn-block" type="submit"><fmt:message key="label.log-in"/></button>

                        <div class="row">
                            <label class="col item"><fmt:message key="label.dont-have-account"/></label>
                            <a class="col-lg-auto item" href="${contextPath}/registration"><fmt:message key="label.sign-up"/></a>
                        </div>
                    </form>
                </div>
            </section>
        </main>
        <jsp:include page="fragments/footer_bottom_fixed.jsp"/>
    </body>
</html>