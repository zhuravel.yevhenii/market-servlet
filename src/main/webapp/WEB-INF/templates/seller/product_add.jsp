<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<c:set var="contextPath" value="${pageContext.request.contextPath}" />

<c:set var="language" value="${not empty sessionScope.language ? sessionScope.language : not empty language ? language : pageContext.request.locale}" scope="session" />
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="messages"/>

<!DOCTYPE html>
<html>
    <jsp:include page="../fragments/header.jsp"/>
    <body>
        <jsp:include page="../fragments/navbar.jsp"/>

        <main class="page gallery-page">
            <section class="clean-block clean-gallery dark">
                <div class="container">
                    <div class="block-heading">
<%--                        <c:if test="${product != null}">--%>
                            <h2><a class="text-info" href="${contextPath}/catalog?id=${category.id}">${category.localizedName}</a></h2>
                            <h4><fmt:message key="label.edit_product"/></h4>
<%--                        </c:if>--%>
                    </div>
                    <div class="row">
                        <form class="col-md-12" name="productForm" action="${contextPath}/catalog/edit" method="post">
                            <input type="hidden" id="productId" name="productId" value="${product.id}">
                            <input type="hidden" id="categoryId" name="categoryId" value="${category.id}">
                            <div class="table-responsive table" id="dataTable" role="grid" aria-describedby="dataTable_info">
                                <table class="table dataTable my-0">
                                    <tbody>
                                    <tr><td>
                                            <label><fmt:message key="label.name"/></label></td>
                                        <td>
                                            <input type="text" id="name" name="name" value="${product.name}">
                                            <div>
                                                <span style="color:red">${errors.name}</span>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td>
                                            <table class="table dataTable my-0">
                                                <thead>
                                                    <tr>
                                                        <th></th>
                                                        <th>EN</th>
                                                        <th>UK</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                <tr><td><label><fmt:message key="label.name"/></label></td>
                                                    <td><input type="text" id="name-en" name="name-en" value="${product.localizedNames['en']}">
                                                        <div>
                                                            <span style="color:red">${errors.localizedNames_en}</span>
                                                        </div>
                                                    </td>
                                                    <td><input type="text" id="name-uk" name="name-uk" value="${product.localizedNames['uk']}">
                                                        <div>
                                                            <span style="color:red">${errors.localizedNames_uk}</span>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr><td><label><fmt:message key="label.description"/></label></td>
                                                    <td><textarea type="text" name="desc-en" cols="40" rows="3">${product.localizedDescriptions['en']}</textarea></td>
                                                    <td><textarea type="text" name="desc-uk" cols="40" rows="3">${product.localizedDescriptions['uk']}</textarea></td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr><td>
                                            <label><fmt:message key="label.producer"/></label></td>
                                        <td>
                                            <select id="producer" name="producer" class="form-control custom-select">
                                                <c:forEach var="producer" items="${producers}">
                                                    <option value="${producer.id}" ${product.producer.id == producer.id ? 'selected' : ''}>${producer.name}</option>
                                                </c:forEach>
                                            </select>
                                    </tr>
                                    <tr><td>
                                            <label><fmt:message key="label.parameters"/></label></td>
                                        <td>
                                            <c:forEach var="parameterType" items="${parameterTypes}">
                                                <h5>${parameterType.name}</h5>

                                                <select id="parameter" name="parameter-${parameterType.name}" class="form-control custom-select">
                                                    <c:forEach var="parameter" items="${parameterType.parameters}">
                                                        <option value="${parameter.id}" ${product.parameters[parameter.id] != null  ? 'selected' : ''}>${parameter.name}</option>
                                                    </c:forEach>
                                                </select>
                                            </c:forEach>
                                    </tr>
                                    <tr>
                                        <td>
                                            <label><fmt:message key="label.price"/></label>
                                        </td>
                                        <td>
                                            <input type="number" id="price" name="price" min="0" value="${product.price}">
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="row md-8">
                                <div class="col-md-6">
                                    <button class="btn btn-primary btn-block" type="submit"><fmt:message key="label.save"/></button>
                                </div>
                                <div class="col-md-6">
                                    <a class="btn btn-outline-dark btn-block" href="${contextPath}/catalog?id=${category.id}"><fmt:message key="label.back"/></a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </section>
        </main>

        <jsp:include page="../fragments/footer.jsp"/>

    </body>
</html>