<%@ page pageEncoding="UTF-8" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<c:set var="user" value="${sessionScope.user}" />
<c:set var="language" value="${not empty sessionScope.language ? sessionScope.language : not empty language ? language : pageContext.request.locale}" scope="session" />
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="messages"/>

<!DOCTYPE html>
<html>
    <jsp:include page="fragments/header.jsp"/>
<body>
    <jsp:include page="fragments/navbar.jsp"/>

    <main class="page registration-page">
        <section class="clean-block clean-form dark">
            <div class="container">
                <div class="block-heading">
                    <h2 class="text-info"><fmt:message key="label.registration"/></h2>
                </div>
                <form action="${pageContext.request.contextPath}/registration" method="post">
                    <div class="form-group">
                        <label for="login">* <fmt:message key="label.loginForm"/></label>
                        <input class="form-control item" type="text" id="login" name="login" value="${accountForm.login}">
                        <span style="color:red">${errors.login}</span>
                    </div>
                    <div class="form-group">
                        <label for="firstName">* <fmt:message key="label.first_name"/></label>
                        <input class="form-control item" type="text" id="firstName" name="firstName" value="${accountForm.firstName}">
                        <span style="color:red">${errors.firstName}</span>
                    </div>
                    <div class="form-group">
                        <label for="lastName"><fmt:message key="label.last_name"/></label>
                        <input class="form-control item" type="text" id="lastName" name="lastName" value="${accountForm.lastName}">
                        <span style="color:red">${errors.lastName}</span>
                    </div>
                    <div class="form-group">
                        <label for="email"><fmt:message key="label.email"/></label>
                        <input class="form-control item" type="text" id="email" name="email" value="${accountForm.email}">
                        <span style="color:red">${errors.email}</span>
                    </div>
                    <div class="form-group">
                        <label for="password">* <fmt:message key="label.password"/></label>
                        <input class="form-control item" type="password" id="password" name="password">
                        <span style="color:red">${errors.password}</span>
                    </div>
                    <div class="form-group">
                        <label for="confirmPassword">* <fmt:message key="label.password_confirm"/></label>
                        <input class="form-control item" type="password" id="confirmPassword" name="confirmPassword">
                        <span style="color:red">${errors.confirmPassword}</span>
                    </div>
                    <button class="btn btn-primary btn-block" type="submit"><fmt:message key="label.sign-up"/></button>
                </form>
            </div>
        </section>
    </main>

<%--    <footer th:replace="fragments/footer:: footer (fixed = true)"></footer>--%>
    <jsp:include page="fragments/footer.jsp"/>
</body>

</html>