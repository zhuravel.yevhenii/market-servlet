<%@ page pageEncoding="UTF-8" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<c:set var="language" value="${not empty sessionScope.language ? sessionScope.language : not empty language ? language : pageContext.request.locale}" scope="session" />
<fmt:setLocale value="${language}" />

<!DOCTYPE html>
<html>
    <jsp:include page="fragments/header.jsp"/>
<body>
    <jsp:include page="fragments/navbar.jsp"/>

    <jsp:useBean id="message" scope="request" type="java.lang.String"/>

    <main class="page registration-page">
        <section class="clean-block clean-form dark">
            <div class="container">
                <div class="block-heading">
                    <h3 class="text-danger" style="color:red">${message}</h3>
                </div>
            </div>
        </section>
    </main>

    <jsp:include page="fragments/footer_bottom_fixed.jsp"/>
</body>

</html>