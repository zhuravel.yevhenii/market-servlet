<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<c:set var="contextPath" value="${pageContext.request.contextPath}" />
<c:set var="language" value="${not empty sessionScope.language ? sessionScope.language : not empty language ? language : pageContext.request.locale}" scope="session" />
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="messages"/>

<nav class="navbar navbar-light navbar-expand-lg fixed-top bg-white clean-navbar"
     xmlns="http://www.w3.org/1999/html">

    <div class="container">
        <a class="navbar-brand logo" href="${contextPath}/catalog">
            <fmt:message key="label.app.name"/>
        </a>

        <button data-toggle="collapse" class="navbar-toggler" data-target="#navcol-1">
            <span class="sr-only">Toggle navigation</span>
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse"
             id="navcol-1">
            <ul class="nav navbar-nav ml-auto">
                <c:if test="${empty sessionScope.user}">
                    <li class="nav-item">
                        <a class="nav-link" href="${contextPath}/login"><fmt:message key="label.log-in"/></a>
                    </li>
                </c:if>
                <c:if test="${not empty sessionScope.user}">
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDarkDropdownMenuLink" role="button"
                           data-bs-toggle="dropdown" aria-expanded="false">${sessionScope.user.login}</a>
                        <ul class="dropdown-menu dropdown-menu-dark" aria-labelledby="navbarDarkDropdownMenuLink">
                            <li><a class="dropdown-item" href="${contextPath}/profile-edit"><fmt:message key="label.edit_profile"/></a></li>
                            <li><a class="dropdown-item" href="${contextPath}/logout"><fmt:message key="label.log-out"/></a></li>
                        </ul>
                    </li>
                </c:if>
                <c:if test="${sessionScope.user.role.name == 'ROLE_ADMIN'}">
                    <li class="nav-item">
                        <div class="row">
                            <a class="nav-link" href="${contextPath}/admin/categories"><fmt:message key="label.categories"/></a>
                        </div>
                    </li>
                    <li class="nav-item">
                        <div class="row">
                            <a class="nav-link" href="${contextPath}/admin/users"><fmt:message key="label.users"/></a>
                        </div>
                    </li>
                    <li class="nav-item">
                        <div class="row">
                            <a class="nav-link" href="${contextPath}/admin/orders"><fmt:message key="label.orders"/></a>
                        </div>
                    </li>
                </c:if>
                <c:if test="${sessionScope.user.role.name == 'ROLE_USER'}">
                    <li class="nav-item">
                        <div class="row">
                            <a class="nav-link" href="${contextPath}/shopping-cart"><fmt:message key="label.shopping_cart"/></a>
                            <c:if test="${session.selectedProducts != null}">
                                <a class="nav-link" href="${contextPath}/shopping-cart">( + ${session.selectedProducts.quantities.size()} + )</a>
                            </c:if>
                        </div>
                    </li>
                </c:if>
                <form class="nav-item form-select border-0" name="form1" action="${pageContext.request.contextPath}/language" method="POST">
                    <label for="language"></label>
                    <select id="language" data-width="fit" name="language" onchange="document.form1.submit();">
                        <option value="en" ${language == 'en' ? 'selected' : ''}><fmt:message key="label.lang.english"/></option>
                        <option value="uk" ${language == 'uk' ? 'selected' : ''}><fmt:message key="label.lang.ukrainian"/></option>
                    </select>
                </form>
            </ul>
        </div>
    </div>
</nav>
