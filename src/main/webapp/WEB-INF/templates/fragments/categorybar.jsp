<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<c:if test="${not empty upperCategories}">
<div>
    <c:if test="${not empty parentCategory}">
        <div class="list-group-item list-group-item-action list-group-item-primary">
            <a class="list-group-item-first-child-font" href="${param.servletUrl}?id=${parentCategory.id}">
                <<  ${parentCategory.localizedName}  <<
            </a>
        </div>
    </c:if>
    <c:forEach var="item" items="${upperCategories}">
        <c:if test="${item.id == param.currentId}">
            <div class="list-group-item list-group-item-action active">
               <a href="${param.servletUrl}?id=${item.id}">
                       ${item.localizedName}
               </a>
            </div>
        </c:if>
        <c:if test="${item.id != param.currentId}">
            <div class="list-group-item list-group-item-action">
               <a href="${param.servletUrl}?id=${item.id}">
                       ${item.localizedName}
               </a>
            </div>
        </c:if>
    </c:forEach>
</div>
</c:if>