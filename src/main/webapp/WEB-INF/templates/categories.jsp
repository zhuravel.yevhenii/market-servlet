<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<c:set var="contextPath" value="${pageContext.request.contextPath}" />

<!DOCTYPE html>
<html>
    <jsp:include page="fragments/header.jsp"/>
<body>
    <jsp:include page="fragments/navbar.jsp"/>

    <main class="page gallery-page">
        <section class="clean-block clean-gallery dark">
            <div class="container">
                <div class="block-heading">
                    <jsp:useBean id="category" scope="request" type="com.zhuravel.market_servlet.service.dto.LocalizedCategoryDto"/>
                    <c:if test="${category != null}">
                        <h2><a class="text-info" href="${contextPath}/catalog?id=${category.parentCategoryId}">${category.localizedName}</a></h2>
                    </c:if>
                </div>
                <div class="row">
                    <div class="col-md-3">
                        <jsp:include page="fragments/categorybar.jsp">
                            <jsp:param name="currentId" value="${category.id}"/>
                            <jsp:param name="servletUrl" value="${contextPath}/catalog"/>
                        </jsp:include>
                    </div>
                    <div class="col-md-9">
                        <div class="products">
                            <jsp:useBean id="categories" scope="request" type="java.util.List"/>
                            <c:if test="${categories != null}">
                                <div class="row">
                                    <c:forEach var="type" items="${categories}">
                                        <div class="col-md-6 col-lg-4 item">
                                            <a class="lightbox" href="${contextPath}/catalog?id=${type.id}">
                                                <img class="img-thumbnail img-fluid image" src="/resources/static${type.imagePath}" alt="">
                                                <label>${type.localizedName}</label>
                                            </a>
                                        </div>
                                    </c:forEach>
                                </div>
                            </c:if>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </main>

    <jsp:include page="fragments/footer.jsp"/>

<%--    <footer replace="/WEB-INF/templates/fragments/footer:: footer(_)"></footer>--%>

</body>
</html>