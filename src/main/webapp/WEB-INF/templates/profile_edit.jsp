<%@ page pageEncoding="UTF-8" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<jsp:useBean id="accountForm" scope="request" type="com.zhuravel.market_servlet.service.dto.UserDto"/>
<fmt:setBundle basename="messages"/>
<c:set var="user" value="${sessionScope.user}" />

<!DOCTYPE html>
<html>
    <jsp:include page="fragments/header.jsp"/>
<body>
    <jsp:include page="fragments/navbar.jsp"/>

    <main class="page registration-page">
        <section class="clean-block clean-form dark">
            <div class="container">
                <div class="block-heading">
                    <h2 class="text-info"><fmt:message key="label.registration"/></h2>
                </div>
                <form action="${pageContext.request.contextPath}/profile-edit" method="post">
                    <div class="form-group">
                        <label for="login">* <fmt:message key="label.loginForm"/></label>
                        <input class="form-control item" type="text" id="login" name="login" value="${accountForm.login}">
                        <span style="color:red">${errors.login}</span>
                    </div>
                    <div class="form-group">
                        <label for="firstName">* <fmt:message key="label.first_name"/></label>
                        <input class="form-control item" type="text" id="firstName" name="firstName" value="${accountForm.firstName}">
                        <span style="color:red">${errors.firstName}</span>
                    </div>
                    <div class="form-group">
                        <label for="lastName"><fmt:message key="label.last_name"/></label>
                        <input class="form-control item" type="text" id="lastName" name="lastName" value="${accountForm.lastName}">
                        <span style="color:red">${errors.lastName}</span>
                    </div>
                    <div class="form-group">
                        <label for="email"><fmt:message key="label.email"/></label>
                        <input class="form-control item" type="text" id="email" name="email" value="${accountForm.email}">
                        <span style="color:red">${errors.email}</span>
                    </div>
                    <button class="btn btn-primary btn-block" type="submit"><fmt:message key="label.save"/></button>
                </form>
            </div>
        </section>
    </main>

<%--    <footer th:replace="fragments/footer:: footer (fixed = true)"></footer>--%>
    <jsp:include page="fragments/footer.jsp"/>
</body>

</html>