<%@ page pageEncoding="UTF-8" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="ex" uri="/WEB-INF/date_time_tag.tld"%>

<c:set var="contextPath" value="${pageContext.request.contextPath}"/>
<c:set var="language" value="${not empty sessionScope.language ? sessionScope.language : not empty language ? language : pageContext.request.locale}" scope="session" />
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="messages"/>

<!DOCTYPE html>
<html>
    <jsp:include page="fragments/header.jsp"/>
<body>
    <jsp:include page="fragments/navbar.jsp"/>

    <main class="page shopping-cart-page">
        <section class="clean-block clean-cart dark">
            <div class="container">
                <div class="block-heading">
                    <h2 class="text-info"><fmt:message key="label.shopping_cart"/></h2>
                </div>
                <div class="content">
                    <c:if test="${sessionScope.productCart.products.size() > 0}">
                    <form class="row no-gutters" method="POST" id="shopping-cart"
                              action="${contextPath}/shopping-cart">
                        <div class="col-md-12 col-lg-8">
                            <div class="items">
                                <c:forEach var="product" items="${sessionScope.productCart.products.entrySet()}">
                                <div class="product">
                                    <div class="row justify-content-center align-items-center">
                                        <div class="col-md-3">
                                            <div class="product-image"><img class="img-fluid d-block mx-auto image" src="${contextPath}/resources/static${product.key.imagePath}" alt=""></div>
                                        </div>
                                        <div class="col-md-5 product-info">
                                            <a class="product-name" href="${contextPath}/product-page?prodId = ${product.key.id})">
                                                    ${product.key.producer.name} ${product.key.name}</a>
                                            <div class="product-specs">
                                                <c:forEach var="parameter" items="${product.key.parameters.values()}">
                                                <div>
                                                    <span>${parameter.typeName}: </span>
                                                    <span class="value">${parameter.value}</span></div>
                                                </c:forEach>
                                            </div>
                                        </div>
                                        <div class="col-6 col-md-2 quantity">
                                            <label class="d-none d-md-block" for="number"><fmt:message key="label.quantity"/></label>
                                            <input type="number" id="number" class="form-control quantity-input" min="0"
                                                       name="quantity_${product.key.id}"
                                                       value="${selectedProducts.quantities[product.key.id]}"></div>
                                        <div class="col-6 col-md-2 price" id="price">
                                            <span hidden class="unit-price">${product.key.price}</span>
                                            <span class="quantity-price">${product.key.price}</span>
                                        </div>
                                    </div>
                                </div>
                                </c:forEach>
                                <button class="btn btn-primary btn-block btn-lg" type="submit" name="save"><fmt:message key="label.save_changes"/></button>
                            </div>
                        </div>
                        <div class="col-md-12 col-lg-4">
                            <div class="summary">
                                <h3><fmt:message key="label.summary"/></h3>
                                <h4><span class="text total">$360</span></h4>
                                <button class="btn btn-primary btn-block btn-lg" type="submit" name="register"><fmt:message key="label.to_order"/></button>
                            </div>
                        </div>
                    </form>
                    </c:if>
                    <c:if test="${sessionScope.productCart.products.size() <= 0}">
                    <div class="row no-gutters">
                            <h3 class="summary"><fmt:message key="label.shopping_cart_empty"/></h3>
                    </div>
                    </c:if>
                </div>
                <div class="card shadow">
                    <div class="card-header py-3">
                        <p class="text-primary m-0 font-weight-bold">Employee Info</p>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive table mt-2" id="dataTable" role="grid" aria-describedby="dataTable_info">
                            <table class="table dataTable my-0" id="1">
                                <thead>
                                    <tr>
                                        <th><fmt:message key="label.date_time"/></th>
                                        <th><fmt:message key="label.quantity"/></th>
                                        <th><fmt:message key="label.status"/></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <c:forEach var="order" items="${orders}" varStatus="item">
                                    <tr>
                                        <td colspan="4">
                                            <table class="table dataTable my-0" id="3">
                                                <tbody>
                                                <tr>
                                                    <td>
                                                        <a data-bs-toggle="collapse" href="#collapse_${item.index}" role="button" aria-expanded="false" aria-controls="collapse_${item.index}">
                                                            <ex:DateTime>${order.registrationDateTime}</ex:DateTime></a>
                                                    </td>
                                                    <td>${order.products.size()}</td>
                                                    <td>${order.status}</td>
                                                </tr>
                                                </tbody>
                                            </table>
                                            <div class="collapse" id="collapse_${item.index}">
                                                <div class="card card-body">
                                                    <table class="table dataTable my-0" id="2">
                                                        <thead>
                                                            <tr>
                                                                <th><fmt:message key="label.producer"/></th>
                                                                <th><fmt:message key="label.name"/></th>
                                                                <th><fmt:message key="label.quantity"/></th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                        <c:forEach var="product" items="${order.products}">
                                                        <tr>
                                                            <td>${product.key.producer.name}</td>
                                                            <td>${product.key.name}</td>
                                                            <td>${product.value}</td>
                                                        </tr>
                                                        </c:forEach>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    </c:forEach>
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <td><strong>Date and Time</strong></td>
                                        <td><strong>Quantity</strong></td>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </main>

    <jsp:include page="fragments/footer.jsp"/>

    <script src="${contextPath}/resources/static/js/shopping-cart.js"></script>
</body>

</html>