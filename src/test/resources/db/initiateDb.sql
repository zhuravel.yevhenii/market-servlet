CREATE SCHEMA IF NOT EXISTS marketdb;

CREATE TABLE IF NOT EXISTS category (
    id                 integer auto_increment,
    name               VARCHAR(100) NOT NULL,
    parent_category_id BIGINT,
    image              VARCHAR(255)
--     CONSTRAINT FK_category_category FOREIGN KEY (parent_category_id) REFERENCES category (id)
);

CREATE TABLE IF NOT EXISTS category_translation (
    id             BIGINT       NOT NULL,
    locale         VARCHAR(10)  NOT NULL,
    localized_name VARCHAR(100) DEFAULT '',
    CONSTRAINT PK_category_translation PRIMARY KEY (id, locale),
    CONSTRAINT FK_translation_category FOREIGN KEY (id) REFERENCES category (id)
);

CREATE TABLE IF NOT EXISTS producer (
    id   BIGINT PRIMARY KEY,
    name VARCHAR(100)
);

CREATE TABLE IF NOT EXISTS product (
    id          BIGINT PRIMARY KEY,
    name        VARCHAR(100) NOT NULL,
    category_id BIGINT       NOT NULL,
    producer_id BIGINT       NOT NULL,
    price       BIGINT,
    image       VARCHAR(255),
    CONSTRAINT FK_product_category FOREIGN KEY (category_id) REFERENCES category (id),
    CONSTRAINT FK_product_producer FOREIGN KEY (producer_id) REFERENCES producer (id)
);

CREATE TABLE IF NOT EXISTS product_parameter_type (
    id          BIGINT       PRIMARY KEY,
    category_id BIGINT          NOT NULL,
    name        VARCHAR(100)    NOT NULL,
    UNIQUE (category_id, name),
    CONSTRAINT FK_product_parameter_category FOREIGN KEY (category_id) REFERENCES category (id)
);

CREATE TABLE IF NOT EXISTS product_parameter_type_translation (
    id                  BIGINT       NOT NULL,
    locale              VARCHAR(10)  NOT NULL,
    localized_name      VARCHAR(100) DEFAULT '',
    CONSTRAINT PK_parameter_type_translation PRIMARY KEY (id, locale),
    CONSTRAINT FK_translation_parameter_type FOREIGN KEY (id) REFERENCES product_parameter_type (id)
);

CREATE TABLE IF NOT EXISTS product_parameter (
    id                  BIGINT       PRIMARY KEY,
    parameter_type_id   BIGINT          NOT NULL,
    value_               VARCHAR(255)    NOT NULL,
    UNIQUE (parameter_type_id, value_),
    CONSTRAINT FK_parameter_parameter_type  FOREIGN KEY (parameter_type_id) REFERENCES product_parameter_type (id)
);

CREATE TABLE IF NOT EXISTS product_product_parameter (
    id                  BIGINT       PRIMARY KEY,
    product_id          BIGINT          NOT NULL,
    parameter_id        BIGINT          NOT NULL,
    UNIQUE (product_id, parameter_id),
    CONSTRAINT FK_parameter_product         FOREIGN KEY (product_id)   REFERENCES product (id),
    CONSTRAINT FK_parameter_product_parameter_type  FOREIGN KEY (parameter_id) REFERENCES product_parameter (id)
);

CREATE TABLE IF NOT EXISTS product_translation (
    id             BIGINT       NOT NULL,
    locale         VARCHAR(10)  NOT NULL,
    localized_name VARCHAR(100) DEFAULT '',
    localized_desc VARCHAR(100) DEFAULT '',
    CONSTRAINT PK_product_translation PRIMARY KEY (id, locale),
    CONSTRAINT FK_translation_product FOREIGN KEY (id) REFERENCES product (id)
);

CREATE TABLE IF NOT EXISTS roles (
    id   BIGINT PRIMARY KEY,
    name VARCHAR(100) UNIQUE
);

CREATE TABLE IF NOT EXISTS user_status (
    id   BIGINT PRIMARY KEY,
    name VARCHAR(100) UNIQUE
);

CREATE TABLE IF NOT EXISTS users (
    id         BIGINT PRIMARY KEY,
    loginForm      VARCHAR(255) NOT NULL,
    first_name VARCHAR(255) NOT NULL,
    last_name  VARCHAR(255),
    password   VARCHAR(255) NOT NULL,
    locale     VARCHAR(10)  NOT NULL,
    status_id  BIGINT       NOT NULL,
    role_id    BIGINT       NOT NULL,
    CONSTRAINT FK_users_status FOREIGN KEY (status_id) REFERENCES user_status (id),
    CONSTRAINT FK_users_roles FOREIGN KEY (role_id) REFERENCES roles (id)
);

CREATE TABLE IF NOT EXISTS shopping_cart (
    id                  BIGINT PRIMARY KEY,
    user_id             BIGINT      NOT NULL,
    product_id          BIGINT      NOT NULL,
    quantity            INT         NOT NULL    DEFAULT 1,
    checkout_date_time  BIGINT,
--     CONSTRAINT PK_shopping_cart PRIMARY KEY (user_id, product_id),
    CONSTRAINT FK_shopping_cart_user FOREIGN KEY (user_id) REFERENCES users (id),
    CONSTRAINT FK_shopping_cart_product FOREIGN KEY (product_id) REFERENCES product (id)
);

------------------------------------------------------------------------------

INSERT INTO category (id, name, parent_category_id, image)
VALUES (1, '', 0, ''),
       (2, 'Computers', 1, '/img/tech/desktop1.jpg'),
       (3, 'Computers', 1, '/img/tech/desktop1.jpg'),
       (4, 'Laptops', 3, '/img/tech/laptop1.jpg'),
       (5, 'Desktops', 3, '/img/tech/desktop1.jpg');

INSERT INTO producer (id, name)
VALUES (default, 'Samsung'),
       (default, 'Dell'),
       (default, 'Philips'),
       (default, 'Acer');

INSERT INTO roles (id, name)
VALUES (default, 'ROLE_ADMIN'),
       (default, 'ROLE_SELLER'),
       (default, 'ROLE_USER');

INSERT INTO user_status (id, name)
VALUES (default, 'ACTIVE'),
       (default, 'BLOCKED'),
       (default, 'DELETED');

INSERT INTO users (id, loginForm, first_name, last_name, password, locale, status_id, role_id)
VALUES (default, 'Admin', 'Admin', '', '$2a$10$C63f4zq5z49aFGITgnivJumGgj1hcG1NBZ73dPouoqWjuethhj9VO', 'en', 1, 1),
       (default, 'Seller', 'Seller', '', '$2a$10$uT8S8.j40JpkIfK7XRnq1.fwQ6FgzMIedE3ghwtgZvGQb5LT8Dkne', 'uk', 1, 2),
       (default, 'User', 'User', '', '$2a$10$qv8nsFFWSjgzzOH1m8nh6uu4ssolpwRlcANB3yNOctjb6iPtq8gha', 'uk', 1, 3),
       (default, 'BlockedUser', 'BlockedUser', '', '$2a$10$JxabC.bNvdAUb1BD8XOFp./985Zo25R.yS70WFM.SYlxJ2X1EtPam', 'uk', 2, 3),
       (default, 'DeletedUser', 'DeletedUser', '', '$2a$10$qXPfi1YOAyv9Um1MukML9uiy0YI5BCuLdJPQ3Idilnl0cp0ahKb8y', 'uk', 3, 3);


INSERT INTO category_translation (id, locale, localized_name) VALUES
                                                                  (1, 'en', ''),
                                                                  (1, 'uk', ''),
                                                                  (3, 'en', 'Computers'),
                                                                  (3, 'uk', 'Комп''ютери');

INSERT INTO product (name, category_id, producer_id, price, image)
--     (2302, 'en', 'Monitors'),
VALUES ('223V7QHSB/01', 3, 3, 1234, '/img/tech/monitor1.jpg'),
       ('203V5LSB26/10', 4, 3, 452, '/img/tech/monitor1.jpg'),
       ('E1920H (210-AURI)', 5, 2, 452, '/img/tech/monitor1.jpg')
;


INSERT INTO product_parameter_type (category_id, name) VALUES
                                                           (3, 'Monitor Size'),
                                                           (4, 'Panel')
;

INSERT INTO product_parameter_type_translation (id, locale, localized_name)
VALUES (1, 'en', 'Monitor Size'),
       (1, 'uk', 'Диагональ екрану'),
       (2, 'en', 'Panel'),
       (2, 'uk', 'Тип матриці')
;

INSERT INTO product_parameter (parameter_type_id, value_) VALUES
--     (2302, "Діагональ екрану")
(1, '24"'),
(1, '25"'),
(1, '23"')
;

INSERT INTO product_product_parameter (product_id, parameter_id) VALUES
                                                                     (1, 3),
                                                                     (2, 2),
                                                                     (3, 1)
;

INSERT INTO shopping_cart (user_id, product_id, quantity, checkout_date_time) VALUES
--   Products for User
(3, 1, 1, null),
(3, 2, 3, null),
(3, 3, 1, null),
(3, 2, 2, 1654516560),
(3, 1, 1, 1654516560)
;

