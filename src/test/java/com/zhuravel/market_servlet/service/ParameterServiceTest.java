package com.zhuravel.market_servlet.service;

import com.zhuravel.market_servlet.dao.ParameterDao;
import com.zhuravel.market_servlet.dao.exception.DaoException;
import com.zhuravel.market_servlet.model.entity.LocalizedParameterType;
import com.zhuravel.market_servlet.model.entity.ParameterType;
import com.zhuravel.market_servlet.model.entity.ParameterTypeTranslation;
import com.zhuravel.market_servlet.service.exception.ResourceNotFoundException;
import com.zhuravel.market_servlet.service.exception.ServiceException;
import com.zhuravel.market_servlet.service.impl.ParameterServiceImpl;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertInstanceOf;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

/**
 * @author Evgenii Zhuravel created on 09.06.2022
 * @version 1.0
 */
public class ParameterServiceTest {

    private static ParameterDao dao;
    private static ParameterService service;

    private static final Map<Long, ParameterType> types = new HashMap<>();
    private static final Map<Long, LocalizedParameterType> localizedTypes = new HashMap<>();

    @BeforeAll
    static void setUp() {
        dao = mock(ParameterDao.class);
        service = new ParameterServiceImpl(dao);

        initTypes();
        initLocalizedTypes();
    }

    private static void initTypes() {
        types.put(1L, new ParameterType(1L, 3L, "Monitor Size", List.of(
                new ParameterTypeTranslation(1L, "en", "Monitor Size"),
                new ParameterTypeTranslation(1L, "uk", "Диагональ екрану")
        )));
        types.put(2L, new ParameterType(2L, 3L, "Panel", List.of(
                new ParameterTypeTranslation(1L, "en", "Panel"),
                new ParameterTypeTranslation(1L, "uk", "Тип матриці")
        )));
        types.put(3L, new ParameterType(3L, 4L, "Color", List.of(
                new ParameterTypeTranslation(1L, "en", "Color"),
                new ParameterTypeTranslation(1L, "uk", "Колір")
        )));
    }

    private static void initLocalizedTypes() {
        localizedTypes.put(1L, new LocalizedParameterType(1L, 3L, "Monitor Size", "en", "Monitor Size"));
        localizedTypes.put(2L, new LocalizedParameterType(2L, 3L, "Panel", "en", "Panel"));
        localizedTypes.put(3L, new LocalizedParameterType(3L, 4L, "Color", "en", "Color"));
    }

    @AfterEach
    public void resetMockStructuredLogger() {
        try {
            verifyNoMoreInteractions(dao);
        } finally {
            reset(dao);
        }
    }

    @Test
    void testBaseConstructor() throws NoSuchFieldException, IllegalAccessException {
        ParameterService parameterService = new ParameterServiceImpl();

        Field f = parameterService.getClass().getDeclaredField("parameterDao");

        f.setAccessible(true);

        ParameterDao actualDao = (ParameterDao) f.get(parameterService);

        assertNotNull(actualDao);
        assertInstanceOf(ParameterDao.class, actualDao);
    }

    @Test
    void whenFindByIdConnectionErrorThenThrowException() throws DaoException {
        doThrow(new DaoException("")).when(dao).findById(1L);

        assertThrows(ServiceException.class, () -> service.getById(1L));

        verify(dao, times(1)).findById(1L);
    }

    @Test
    void whenFindByIdNothingFoundThenThrowException() throws DaoException {
        when(dao.findById(1L)).thenReturn(Optional.empty());

        assertThrows(ResourceNotFoundException.class, () -> service.getById(1L));

        verify(dao, times(1)).findById(1L);
    }

    @Test
    void whenFindAllByProductIdConnectionErrorThenThrowException() throws DaoException {
        doThrow(new DaoException("")).when(dao).findAllByProductId(1L);

        assertThrows(ServiceException.class, () -> service.getByProductId(1L));

        verify(dao, times(1)).findAllByProductId(1L);
    }

    @Test
    void testDeleteById() throws DaoException, ServiceException {
        service.delete(1L);

        verify(dao, times(1)).deleteById(1L);
    }

    @Test
    void whenDeleteByIdErrorThenThrowException() throws DaoException {
        doThrow(new DaoException("")).when(dao).deleteById(100L);

        assertThrows(ServiceException.class, () -> service.delete(100L));

        verify(dao, times(1)).deleteById(100L);
    }
}
