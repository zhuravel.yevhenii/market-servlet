package com.zhuravel.market_servlet.service;

import com.zhuravel.market_servlet.dao.ProducerDao;
import com.zhuravel.market_servlet.dao.ProductDao;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.verifyNoMoreInteractions;

/**
 * @author Evgenii Zhuravel created on 09.06.2022
 * @version 1.0
 */
public class ProductServiceTest {

    private static ProductDao dao;
    private static ProducerDao producerDao;

    @BeforeAll
    static void setUp() {
        dao = mock(ProductDao.class);
        producerDao = mock(ProducerDao.class);
    }

    @AfterEach
    public void resetMockStructuredLogger() {
        try {
            verifyNoMoreInteractions(dao);
            verifyNoMoreInteractions(producerDao);
        } finally {
            reset(dao);
            reset(producerDao);
        }
    }

    /*@Test
    void testBaseConstructor() throws NoSuchFieldException, IllegalAccessException {
        ProductService parameterService = new ProductServiceImpl();

        Field f = parameterService.getClass().getDeclaredField("productDao");

        f.setAccessible(true);

        ProductDao actualDao = (ProductDao) f.get(parameterService);

        assertNotNull(actualDao);
        assertInstanceOf(ProductDao.class, actualDao);
    }

    @Test
    void testGetById() throws DaoException, ServiceException {
        *//*Product product = new Product(1L, "223V7QHSB/01", "description", 3L, 3L, 1234L, "/img/tech/monitor1.jpg");

        ProductDto expected = ProductDto.newBuilder()
                .setId(1L)
                .setNames("223V7QHSB/01")
                .setCategoryId(3L)
                .build();
        //new ProductDto(1L, , , 3L, 1234L, "/img/tech/monitor1.jpg");
        when(dao.findById(1L)).thenReturn(Optional.of(product));*//*

        ProductDto actual = service.getById(1L);

        //assertEquals(expected, actual);
        verify(dao, times(1)).findById(1L);
    }

    @Test
    void whenGetByIdConnectionErrorThenThrowException() throws DaoException {
        doThrow(new DaoException("")).when(dao).findById(1L);

        assertThrows(ServiceException.class, () -> service.getById(1L));

        verify(dao, times(1)).findById(1L);
    }

    @Test
    void whenGetByIdNothingFoundThenThrowException() throws DaoException {
        when(dao.findById(1L)).thenReturn(Optional.empty());

        assertThrows(ResourceNotFoundException.class, () -> service.getById(1L));

        verify(dao, times(1)).findById(1L);
    }

    @Test
    void testGetAll() throws DaoException, ServiceException {
        List<Product> products = new ArrayList<>();
        *//*products.add(new Product(1L, "223V7QHSB/01", "description", 3L, 3L, 1234L, "/img/tech/monitor1.jpg"));
        products.add(new Product(2L, "203V5LSB26/10", "description", 4L, 3L, 452L, "/img/tech/monitor1.jpg"));
        products.add(new Product(3L, "E1920H (210-AURI)", "description", 5L, 2L, 452L, "/img/tech/monitor1.jpg"));
        products.add(new Product(4L, "V206HQLAb (UM.IV6EE.A02)", "description", 5L, 4L, 3452L, "/img/tech/monitor1.jpg"));
        products.add(new Product(5L, "S22F350F (LS22F350FHIXCI)", "description", 5L, 1L, 4522L, "/img/tech/monitor1.jpg"));
        products.add(new Product(6L, "193V5LSB2/10", "description", 5L, 3L, 2356L, "/img/tech/monitor1.jpg"));
        products.add(new Product(7L, "193V5LSB2/10", "description", 2L, 3L, 2356L, "/img/tech/monitor1.jpg"));
        products.add(new Product(8L, "Chromebook 4 (XE310XBA-KA1US)", "description", 1L, 1L, 356L, "/img/tech/laptop1.jpg"));
        products.add(new Product(9L, "Chromebook 4 193V5LSB2/10", "description", 2L, 1L, 356L, "/img/tech/laptop1.jpg"));
        products.add(new Product(10L, "Chromebook 4 (IV6EE-KA1US)", "description", 4L, 1L, 356L, "/img/tech/laptop1.jpg"));
*//*
        Pageable pageable = new Pageable();

        when(dao.findAll(pageable)).thenReturn(pageable.buildPage(products, 100));

        List<ProductDto> expected = new ArrayList<>();

        Page<ProductDto> actual = service.getAll(pageable);

        assertTrue(expected.size() == actual.getContent().size()
                && expected.containsAll(actual.getContent())
                && actual.getContent().containsAll(expected));
        verify(dao, times(1)).findAll();
    }

    @Test
    void whenGetAllConnectionErrorThenThrowException() throws DaoException {
        doThrow(new DaoException("")).when(dao).findAll();

        Pageable pageable = new Pageable();

        assertThrows(ServiceException.class, () -> service.getAll(pageable));
        verify(dao, times(1)).findAll(pageable);
    }

    @Test
    void testGetAllByProducerName() throws DaoException, ServiceException {
        List<Product> products = new ArrayList<>();
        *//*products.add(new Product(1L, "223V7QHSB/01", "description", 3L, 3L, 1234L, "/img/tech/monitor1.jpg"));
        products.add(new Product(2L, "203V5LSB26/10", "description", 4L, 3L, 452L, "/img/tech/monitor1.jpg"));
        products.add(new Product(6L, "193V5LSB2/10", "description", 5L, 3L, 2356L, "/img/tech/monitor1.jpg"));
        products.add(new Product(7L, "193V5LSB2/10", "description", 2L, 3L, 2356L, "/img/tech/monitor1.jpg"));
*//*
        Pageable pageable = new Pageable();

        when(dao.findAllByProducerName("Philips", pageable)).thenReturn(pageable.buildPage(products, 100));

        List<ProductDto> expected = new ArrayList<>();
        Page<ProductDto> actual = service.getAllByProducerName("Philips", pageable);

        assertTrue(expected.size() == actual.getContent().size()
                && expected.containsAll(actual.getContent())
                && actual.getContent().containsAll(expected));
        verify(dao, times(1)).findAllByProducerName("Philips", pageable);
    }

    @Test
    void whenGetAllByProducerNameConnectionErrorThenThrowException() throws DaoException {
        Pageable pageable = new Pageable();

        doThrow(new DaoException("")).when(dao).findAllByProducerName("Philips", pageable);

        assertThrows(ServiceException.class, () -> service.getAllByProducerName("Philips", pageable));
        verify(dao, times(1)).findAllByProducerName("Philips", pageable);
    }

    @Test
    void testGetAllWithCriteriaAndParameter() throws DaoException, ServiceException {
        List<Product> products = new ArrayList<>();
        //products.add(new Product(3L, "E1920H (210-AURI)", "description", 5L, 2L, 452L, "/img/tech/monitor1.jpg"));

        Producer producer1 = new Producer(2L, "Dell");
        Set<Producer> producers = new TreeSet<>();
        producers.add(producer1);

        List<ProductDto> productDtos = new ArrayList<>();
        *//*productDtos.add(ProductDto.newBuilder()
                .setId(3L)
                .setNames("E1920H (210-AURI)")
                .setProducer(producer1)
                .setPrice("$452.00")
                .setImagePath("/img/tech/monitor1.jpg")
                .build());*//*

        Pageable pageable = new Pageable(1, 5);

        ProductsCatalogDto expected = new ProductsCatalogDto();
        expected.setProducts(pageable.buildPage(productDtos, 100));
        expected.setProducers(producers);

        ParameterSearchCriteria criteria = ProductDaoTest.generateTestCriteria();

        when(dao.findAll(criteria, "", pageable)).thenReturn(pageable.buildPage(products, 100));
        when(producerDao.findById(2L)).thenReturn(Optional.of(producer1));

        ProductsCatalogDto actual = service.getAll(criteria, "", pageable);

        assertEquals(expected, actual);
        verify(dao, times(1)).findAll(criteria, "", pageable);
        verify(producerDao, times(products.size())).findById(anyLong());
    }

    @Test
    void whenGetAllWithCriteriaErrorThenThrowException() throws DaoException {
        ParameterSearchCriteria criteria = ProductDaoTest.generateTestCriteria();
        Pageable pageable = new Pageable(2, 2);

        doThrow(new DaoException("")).when(dao).findAll(criteria, "", pageable);

        assertThrows(ServiceException.class, () -> service.getAll(criteria, "", pageable));
        verify(dao, times(1)).findAll(criteria, "", pageable);
    }

    @Test
    void whenGetProducerByIdErrorThenThrowException() throws DaoException {
        List<Product> products = new ArrayList<>();
        //products.add(new Product(3L, "E1920H (210-AURI)", "description", 5L, 2L, 452L, "/img/tech/monitor1.jpg"));

        ParameterSearchCriteria criteria = ProductDaoTest.generateTestCriteria();
        Pageable pageable = new Pageable(2, 2);

        when(dao.findAll(criteria, "", pageable)).thenReturn(pageable.buildPage(products, 100));
        doThrow(new DaoException("")).when(producerDao).findById(anyLong());

        assertThrows(ServiceException.class, () -> service.getAll(criteria, "", pageable));
        verify(dao, times(1)).findAll(criteria, "", pageable);
        verify(producerDao, times(1)).findById(anyLong());
    }

    @Test
    void whenGetProducerByIdNothingFoundThenThrowException() throws DaoException {
        List<Product> products = new ArrayList<>();
        //products.add(new Product(3L, "E1920H (210-AURI)", "description", 5L, 2L, 452L, "/img/tech/monitor1.jpg"));

        ParameterSearchCriteria criteria = ProductDaoTest.generateTestCriteria();
        Pageable pageable = new Pageable(2, 2);

        when(dao.findAll(criteria, "", pageable)).thenReturn(pageable.buildPage(products, 100));
        when(producerDao.findById(anyLong())).thenReturn(Optional.empty());

        assertThrows(ResourceNotFoundException.class, () -> service.getAll(criteria, "", pageable));

        verify(dao, times(1)).findAll(criteria, "", pageable);
        verify(producerDao, times(products.size())).findById(anyLong());
    }

    @Test
    void testSave() throws DaoException, ServiceException {
        ProductDto expected = getTestProduct();

        service.save(expected);

        verify(dao, times(1)).save(any());
    }

    @Test
    void whenSaveWrongDataThenThrowException() throws DaoException {
        ProductDto product = getWrongEntity();

        doThrow(new DaoException("")).when(dao).save(any());

        assertThrows(ServiceException.class, () -> service.save(product));

        verify(dao, times(1)).save(any());
    }

    @Test
    void testDeleteById() throws DaoException, ServiceException {
        service.delete(1L);

        verify(dao, times(1)).deleteById(1L);
    }

    @Test
    void whenDeleteByIdErrorThenThrowException() throws DaoException {
        doThrow(new DaoException("")).when(dao).deleteById(100L);

        assertThrows(ServiceException.class, () -> service.delete(100L));

        verify(dao, times(1)).deleteById(100L);
    }

    private ProductDto getTestProduct() {
        ProductDto.Builder expected = ProductDto.newBuilder()
        .setName("test")
        .setCategoryId(1L)
        //.setProducer(1L)
        .setPrice(1L)
        .setImagePath("test");

        return expected.build();
    }

    private ProductDto getWrongEntity() {
        //return new Product(1L, null, 3L, 3L, 1234L, "/img/tech/monitor1.jpg");
        ProductDto.Builder expected = ProductDto.newBuilder()
                .setId(1L)
                .setNames(null)
                .setCategoryId(3L)
                //.setProducer(1L)
                .setPrice(1234L)
                .setImagePath("/img/tech/monitor1.jpg");

        return expected.build();
    }*/
}
