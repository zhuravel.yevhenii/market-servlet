package com.zhuravel.market_servlet.dao;

import com.zhuravel.market_servlet.config.DataSource;
import com.zhuravel.market_servlet.dao.exception.DaoException;
import com.zhuravel.market_servlet.dao.impl.ParameterDaoImpl;
import com.zhuravel.market_servlet.model.entity.Parameter;
import com.zhuravel.market_servlet.model.pageable.Page;
import com.zhuravel.market_servlet.model.pageable.Pageable;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockedStatic;
import org.mockito.Mockito;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertIterableEquals;
import static org.junit.jupiter.api.Assertions.assertNotSame;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * @author Evgenii Zhuravel created on 09.06.2022
 * @version 1.0
 */
public class ParameterDaoTest {

    private static ParameterDao dao;

    private static final Map<Long, Parameter> parameters = new HashMap<>();

    private static List<String> lines;

    @BeforeAll
    static void setUp() throws IOException {
        dao = new ParameterDaoImpl();

        initTestData();

        lines = Files.readAllLines(Path.of("db/initiateDb.sql"));
    }

    @BeforeEach
    void beforeEach() throws SQLException {
        String query = String.join("", lines);
        DataSource.reload();
        try(Connection connection = DataSource.getConnection()){
            connection.createStatement().executeUpdate(query);
        }
    }

    @Test
    void testFindById() throws DaoException {
        Parameter expected = new Parameter(4L, 2L, "IPS");

        Optional<Parameter> parameter = dao.findById(4L);

        assertTrue(parameter.isPresent());
        assertEquals(expected, parameter.get());
    }

    @Test
    void whenFindByIdConnectionErrorThenThrowException() {
        try (MockedStatic<DataSource> dataSource = Mockito.mockStatic(DataSource.class)){

            dataSource.when(DataSource::getConnection).thenThrow(new SQLException());

            assertThrows(DaoException.class, () -> dao.findById(2L));
        }
    }

    @Test
    void testFindAllByProductId() throws DaoException {
        List<Parameter> expected = new ArrayList<>();

        expected.add(new Parameter(3L, 1L, "23\""));
        expected.add(new Parameter(4L, 2L, "IPS"));

        List<Parameter> actual = dao.findAllByProductId(1L);

        assertIterableEquals(expected, actual);
        assertNotSame(expected, actual);
    }

    @Test
    void whenFindAllByProductIdConnectionErrorThenThrowException() {
        try (MockedStatic<DataSource> dataSource = Mockito.mockStatic(DataSource.class)){

            dataSource.when(DataSource::getConnection).thenThrow(new SQLException());

            assertThrows(DaoException.class, () -> dao.findAllByProductId(1L));
        }
    }

    @Test
    void testFindAll() throws DaoException {
        List<Parameter> expected = new ArrayList<>(parameters.values());

        Pageable pageable = new Pageable();

        Page<Parameter> actual = dao.findAll(pageable);

        assertTrue(expected.size() == actual.getContent().size()
                && expected.containsAll(actual.getContent())
                && actual.getContent().containsAll(expected));
    }

    private static void initTestData() {
        parameters.put(1L, new Parameter(1L, 1L, "24\""));
        parameters.put(2L, new Parameter(2L, 1L, "25\""));
        parameters.put(3L, new Parameter(3L, 1L, "23\""));
        parameters.put(4L, new Parameter(4L, 2L, "IPS"));
        parameters.put(5L, new Parameter(5L, 2L, "TN"));
        parameters.put(6L, new Parameter(6L, 1L, "20\""));
        parameters.put(7L, new Parameter(7L, 1L, "18\""));
        parameters.put(8L, new Parameter(8L, 1L, "19\""));
        parameters.put(9L, new Parameter(9L, 1L, "29\""));
        parameters.put(10L, new Parameter(10L, 1L, "28\""));
        parameters.put(11L, new Parameter(11L, 2L, "VA"));
        parameters.put(12L, new Parameter(12L, 2L, "PLS"));
    }

    @Test
    void whenFindAllConnectionErrorThenThrowException() {
        try (MockedStatic<DataSource> dataSource = Mockito.mockStatic(DataSource.class)){

            dataSource.when(DataSource::getConnection).thenThrow(new SQLException());

            assertThrows(DaoException.class, () -> dao.findAll());
        }
    }

    @Test
    void testSave() throws DaoException {
        Parameter expected = getTestParameter();

        dao.save(expected);

        Optional<Parameter> actual = dao.findById(expected.getId());

        assertTrue(actual.isPresent());
        assertEquals(expected, actual.get());
    }

    @Test
    void testDeleteById() throws DaoException {
        Parameter expected = getTestParameter();

        dao.save(expected);

        Optional<Parameter> parameter = dao.findById(expected.getId());

        assertTrue(parameter.isPresent());

        dao.deleteById(parameter.get().getId());

        Optional<Parameter> deleted = dao.findById(expected.getId());
        assertFalse(deleted.isPresent());
    }

    @Test
    void whenDeleteByIdConnectionErrorThenThrowException() {
        try (MockedStatic<DataSource> dataSource = Mockito.mockStatic(DataSource.class)){

            dataSource.when(DataSource::getConnection).thenThrow(new SQLException());

            assertThrows(DaoException.class, () -> dao.deleteById(100L));
        }
    }

    @Test
    void testAddToProduct() throws DaoException {
        Parameter expected = dao.findById(1L).get();

        List<Parameter> actual = dao.findAllByProductId(10L);
        assertFalse(actual.contains(expected));

        dao.addToProduct(10L, expected);

        actual = dao.findAllByProductId(10L);
        assertTrue(actual.contains(expected));
    }

    @Test
    void testRemoveAllFromProduct() throws DaoException {
        List<Parameter> before = dao.findAllByProductId(1L);

        dao.removeAllFromProduct(1L);

        List<Parameter> after = dao.findAllByProductId(10L);
        assertEquals(0, after.size());
        assertFalse(after.containsAll(before));
    }

    @Test
    void testFindByTypeId() throws DaoException {
        List<Parameter> expected = new ArrayList<>();
        expected.add(parameters.get(4L));
        expected.add(parameters.get(5L));
        expected.add(parameters.get(11L));
        expected.add(parameters.get(12L));

        List<Parameter> actual = dao.findByTypeId(2L);

        assertNotSame(expected, actual);
        assertTrue(expected.size() == actual.size()
                && expected.containsAll(actual)
                && actual.containsAll(expected));
    }

    private Parameter getTestParameter() {
        Parameter expected = new Parameter();
        expected.setTypeId(1L);
        expected.setValue("test");
        return expected;
    }
}
