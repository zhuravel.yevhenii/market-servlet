package com.zhuravel.market_servlet.dao;

import com.zhuravel.market_servlet.config.DataSource;
import com.zhuravel.market_servlet.dao.exception.DaoException;
import com.zhuravel.market_servlet.dao.impl.LocalizedParameterTypeDaoImpl;
import com.zhuravel.market_servlet.model.entity.LocalizedParameterType;
import com.zhuravel.market_servlet.model.pageable.Page;
import com.zhuravel.market_servlet.model.pageable.Pageable;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockedStatic;
import org.mockito.Mockito;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * @author Evgenii Zhuravel created on 09.06.2022
 * @version 1.0
 */
public class LocalizedParameterTypeDaoTest {

    private static LocalizedParameterTypeDao dao;

    private static List<String> lines;

    @BeforeAll
    static void setUp() throws IOException {
        dao = new LocalizedParameterTypeDaoImpl();
        dao.setLocale("en");

        lines = Files.readAllLines(Path.of("db/initiateDb.sql"));
    }

    @BeforeEach
    void beforeEach() throws SQLException {
        String query = String.join("", lines);
        try(Connection connection = DataSource.getConnection()){
            connection.createStatement().executeUpdate(query);
        }
    }

    @Test
    void testFindById() throws DaoException {
        LocalizedParameterType expected = new LocalizedParameterType(1L, 3L, "Monitor Size", "en", "Monitor Size");

        Optional<LocalizedParameterType> parameterType = dao.findById(1L);

        assertTrue(parameterType.isPresent());
        assertEquals(expected, parameterType.get());
    }

    @Test
    void whenFindByIdConnectionErrorThenThrowException() {
        try (MockedStatic<DataSource> dataSource = Mockito.mockStatic(DataSource.class)){

            dataSource.when(DataSource::getConnection).thenThrow(new SQLException());

            assertThrows(DaoException.class, () -> dao.findById(2L));
        }
    }

    @Test
    void testFindAllByCategoryId() throws DaoException {
        List<LocalizedParameterType> expected = new ArrayList<>();

        expected.add(new LocalizedParameterType(1L, 3L, "Monitor Size", "en", "Monitor Size"));
        expected.add(new LocalizedParameterType(2L, 3L, "Panel", "en", "Panel"));

        Pageable pageable = new Pageable();

        Page<LocalizedParameterType> actual = dao.findAllByCategoryId(3L, pageable);

        assertTrue(expected.size() == actual.getContent().size()
                && expected.containsAll(actual.getContent())
                && actual.getContent().containsAll(expected));
    }

    @Test
    void whenFindAllByCategoryIdConnectionErrorThenThrowException() {
        try (MockedStatic<DataSource> dataSource = Mockito.mockStatic(DataSource.class)){

            dataSource.when(DataSource::getConnection).thenThrow(new SQLException());

            assertThrows(DaoException.class, () -> dao.findAllByCategoryId(3L, new Pageable()));
        }
    }
}
