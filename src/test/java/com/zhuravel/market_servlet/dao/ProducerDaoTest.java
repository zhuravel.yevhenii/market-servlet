package com.zhuravel.market_servlet.dao;

import com.zhuravel.market_servlet.config.DataSource;
import com.zhuravel.market_servlet.dao.exception.DaoException;
import com.zhuravel.market_servlet.dao.impl.ProducerDaoImpl;
import com.zhuravel.market_servlet.model.entity.Producer;
import com.zhuravel.market_servlet.model.pageable.Page;
import com.zhuravel.market_servlet.model.pageable.Pageable;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockedStatic;
import org.mockito.Mockito;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * @author Evgenii Zhuravel created on 09.06.2022
 * @version 1.0
 */
public class ProducerDaoTest {

    private static ProducerDao dao;

    private static List<String> lines;

    @BeforeAll
    static void setUp() throws IOException {
        dao = new ProducerDaoImpl();

        lines = Files.readAllLines(Path.of("db/initiateDb.sql"));
    }

    @BeforeEach
    void beforeEach() throws SQLException {
        String query = String.join("", lines);
        DataSource.reload();
        try(Connection connection = DataSource.getConnection()){
            connection.createStatement().executeUpdate(query);
        }
    }

    @Test
    void testFindById() throws DaoException {
        Producer expected = new Producer(1L, "Samsung");

        Optional<Producer> producer = dao.findById(1L);

        assertTrue(producer.isPresent());
        assertEquals(expected, producer.get());
    }

    @Test
    void whenFindByIdConnectionErrorThenThrowException() {
        try (MockedStatic<DataSource> dataSource = Mockito.mockStatic(DataSource.class)){

            dataSource.when(DataSource::getConnection).thenThrow(new SQLException());

            assertThrows(DaoException.class, () -> dao.findById(2L));
        }
    }

    @Test
    void testFindAll() throws DaoException {
        List<Producer> expected = new ArrayList<>();

        expected.add(new Producer(1L, "Samsung"));
        expected.add(new Producer(2L, "Dell"));
        expected.add(new Producer(3L, "Philips"));
        expected.add(new Producer(4L, "Acer"));

        Pageable pageable = new Pageable();

        Page<Producer> actual = dao.findAll(pageable);

        assertTrue(expected.size() == actual.getContent().size()
                && expected.containsAll(actual.getContent())
                && actual.getContent().containsAll(expected));
    }

    @Test
    void whenFindAllConnectionErrorThenThrowException() {
        try (MockedStatic<DataSource> dataSource = Mockito.mockStatic(DataSource.class)){

            dataSource.when(DataSource::getConnection).thenThrow(new SQLException());

            assertThrows(DaoException.class, () -> dao.findAll());
        }
    }

    @Test
    void testSave() throws DaoException {
        Producer expected = getTestProducer();

        dao.save(expected);

        Optional<Producer> actual = dao.findById(expected.getId());

        assertTrue(actual.isPresent());
        assertEquals(expected, actual.get());
    }

    @Test
    void whenSaveWrongDataThenThrowException() {
        assertThrows(DaoException.class, () -> dao.save(getWrongEntity()));
    }

    @Test
    void testDeleteById() throws DaoException {
        Producer expected = getTestProducer();

        dao.save(expected);

        Optional<Producer> producer = dao.findById(expected.getId());

        assertTrue(producer.isPresent());

        dao.deleteById(producer.get().getId());

        Optional<Producer> deleted = dao.findById(expected.getId());
        assertFalse(deleted.isPresent());
    }

    @Test
    void whenDeleteByIdConnectionErrorThenThrowException() {
        try (MockedStatic<DataSource> dataSource = Mockito.mockStatic(DataSource.class)){

            dataSource.when(DataSource::getConnection).thenThrow(new SQLException());

            assertThrows(DaoException.class, () -> dao.deleteById(100L));
        }
    }

    private Producer getTestProducer() {
        Producer expected = new Producer();
        expected.setName("test");
        return expected;
    }

    private Producer getWrongEntity() {
        return new Producer(0L, null);
    }
}
