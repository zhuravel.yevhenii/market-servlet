package com.zhuravel.market_servlet.dao;

import com.zhuravel.market_servlet.config.DataSource;
import com.zhuravel.market_servlet.dao.exception.DaoException;
import com.zhuravel.market_servlet.dao.impl.LocalizedCategoryDaoImpl;
import com.zhuravel.market_servlet.model.entity.LocalizedCategory;
import com.zhuravel.market_servlet.model.pageable.Page;
import com.zhuravel.market_servlet.model.pageable.Pageable;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockedStatic;
import org.mockito.Mockito;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertIterableEquals;
import static org.junit.jupiter.api.Assertions.assertNotSame;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * @author Evgenii Zhuravel created on 09.06.2022
 * @version 1.0
 */
public class LocalizedCategoryDaoTest {

    private static LocalizedCategoryDao dao;

    private static List<String> lines;

    @BeforeAll
    static void setUp() throws IOException {
        dao = new LocalizedCategoryDaoImpl();
        dao.setLocale("en");

        lines = Files.readAllLines(Path.of("db/initiateDb.sql"));
    }

    @BeforeEach
    void beforeEach() throws SQLException {
        String query = String.join("", lines);
        DataSource.reload();
        try(Connection connection = DataSource.getConnection()){
            connection.createStatement().executeUpdate(query);
        }
    }

    @Test
    void testFindById() throws DaoException {
        LocalizedCategory expected = new LocalizedCategory(2L, "Computers", 1L, "/img/tech/desktop1.jpg", "en", "Computers");

        Optional<LocalizedCategory> category = dao.findById(2L);

        assertTrue(category.isPresent());
        assertEquals(expected, category.get());
    }

    @Test
    void whenFindByIdConnectionErrorThenThrowException() {
        try (MockedStatic<DataSource> dataSource = Mockito.mockStatic(DataSource.class)){

            dataSource.when(DataSource::getConnection).thenThrow(new SQLException());

            assertThrows(DaoException.class, () -> dao.findById(2L));
        }
    }

    @Test
    void testFindAllByParentCategoryId() throws DaoException {
        List<LocalizedCategory> expected = new ArrayList<>();

        expected.add(new LocalizedCategory(2L, "Computers", 1L, "/img/tech/desktop1.jpg", "en", "Computers"));

        Pageable pageable = new Pageable();

        dao.setLocale("en");
        Page<LocalizedCategory> actual = dao.findAllByParentCategoryId(1L, pageable);

        System.out.println(expected);
        System.out.println(actual);

        assertIterableEquals(expected, actual.getContent());
        assertNotSame(expected, actual.getContent());
    }

    @Test
    void whenFindAllByParentCategoryIdConnectionErrorThenThrowException() {
        try (MockedStatic<DataSource> dataSource = Mockito.mockStatic(DataSource.class)){

            dataSource.when(DataSource::getConnection).thenThrow(new SQLException());

            assertThrows(DaoException.class, () -> dao.findAllByParentCategoryId(1L, new Pageable()));
        }
    }
}
