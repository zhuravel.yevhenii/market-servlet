package com.zhuravel.market_servlet.dao;

import com.zhuravel.market_servlet.config.DataSource;
import com.zhuravel.market_servlet.dao.exception.DaoException;
import com.zhuravel.market_servlet.dao.impl.ProductDaoImpl;
import com.zhuravel.market_servlet.model.entity.Product;
import com.zhuravel.market_servlet.model.entity.ProductTranslation;
import com.zhuravel.market_servlet.model.pageable.Page;
import com.zhuravel.market_servlet.model.pageable.Pageable;
import com.zhuravel.market_servlet.service.filter_criteria.ParameterSearchCriteria;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockedStatic;
import org.mockito.Mockito;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * @author Evgenii Zhuravel created on 09.06.2022
 * @version 1.0
 */
public class ProductDaoTest {

    private static ProductDao dao;

    private static final Map<Long, Product> productMap = new HashMap<>();

    private static List<String> lines;


    @BeforeAll
    static void setUp() throws IOException {
        dao = new ProductDaoImpl();
        dao.setLocale("en");

        lines = Files.readAllLines(Path.of("db/initiateDb.sql"));

        initProducts();
    }

    @BeforeEach
    void beforeEach() throws SQLException {
        String query = String.join("", lines);
        DataSource.reload();
        try(Connection connection = DataSource.getConnection()){
            connection.createStatement().executeUpdate(query);
        }
    }

    @Test
    void testFindById() throws DaoException {
        Product expected = productMap.get(1L);

        Optional<Product> product = dao.findById(1L);

        assertTrue(product.isPresent());
        assertEquals(expected, product.get());
    }

    @Test
    void whenFindByIdConnectionErrorThenThrowException() {
        try (MockedStatic<DataSource> dataSource = Mockito.mockStatic(DataSource.class)){

            dataSource.when(DataSource::getConnection).thenThrow(new SQLException());

            assertThrows(DaoException.class, () -> dao.findById(2L));
        }
    }

    @Test
    void testFindAll() throws DaoException {
        List<Product> expected = new ArrayList<>(productMap.values().stream().toList());
        Pageable pageable = new Pageable();

        Page<Product> actual = dao.findAll(pageable);

        assertEquals(expected, actual.getContent());
        assertTrue(expected.size() == actual.getContent().size()
                && expected.containsAll(actual.getContent())
                && actual.getContent().containsAll(expected));
    }

    @Test
    void whenFindAllConnectionErrorThenThrowException() {
        try (MockedStatic<DataSource> dataSource = Mockito.mockStatic(DataSource.class)){

            dataSource.when(DataSource::getConnection).thenThrow(new SQLException());

            assertThrows(DaoException.class, () -> dao.findAll());
        }
    }

    @Test
    void testFindAllByProducerName() throws DaoException {
        List<Product> expected = new ArrayList<>();
        expected.add(productMap.get(1L));
        expected.add(productMap.get(2L));
        expected.add(productMap.get(6L));
        expected.add(productMap.get(7L));

        Pageable pageable = new Pageable();

        Page<Product> actual = dao.findAllByProducerName("Philips", pageable);

        assertTrue(expected.size() == actual.getContent().size()
                && expected.containsAll(actual.getContent())
                && actual.getContent().containsAll(expected));
    }

    @Test
    void whenFindAllByProducerNameConnectionErrorThenThrowException() {
        try (MockedStatic<DataSource> dataSource = Mockito.mockStatic(DataSource.class)){

            dataSource.when(DataSource::getConnection).thenThrow(new SQLException());

            assertThrows(DaoException.class, () -> dao.findAllByProducerName("Philips", new Pageable()));
        }
    }

    @Test
    void testFindAllWithCriteriaAndParameter() throws DaoException {
        List<Product> expected = new ArrayList<>();
        expected.add(productMap.get(3L));
        expected.add(productMap.get(6L));

        ParameterSearchCriteria criteria = generateTestCriteria();

        Page<Product> actual = dao.findAll(criteria, "", new Pageable());

        assertEquals(expected, actual.getContent());
        assertTrue(expected.size() == actual.getContent().size()
                && expected.containsAll(actual.getContent())
                && actual.getContent().containsAll(expected));
    }

    @Test
    void whenFindAllWithCriteriaErrorThenThrowException() {
        try (MockedStatic<DataSource> dataSource = Mockito.mockStatic(DataSource.class)){

            dataSource.when(DataSource::getConnection).thenThrow(new SQLException());

            ParameterSearchCriteria criteria = generateTestCriteria();
            assertThrows(DaoException.class, () -> dao.findAll(criteria, "", new Pageable(2, 2)));
        }
    }

    @Test
    void testSave() throws DaoException {
        Product expected = getTestProduct();

        dao.save(expected);

        Optional<Product> actual = dao.findById(expected.getId());

        assertTrue(actual.isPresent());
        assertEquals(expected, actual.get());
    }

    @Test
    void whenSaveWrongDataThenThrowException() {
        assertThrows(DaoException.class, () -> dao.save(getWrongEntity()));
    }

    @Test
    void testDeleteById() throws DaoException {
        Product expected = getTestProduct();

        dao.save(expected);

        Optional<Product> product = dao.findById(expected.getId());

        assertTrue(product.isPresent());

        dao.deleteById(product.get().getId());

        Optional<Product> deleted = dao.findById(expected.getId());
        assertFalse(deleted.isPresent());
    }

    @Test
    void whenDeleteByIdConnectionErrorThenThrowException() {
        try (MockedStatic<DataSource> dataSource = Mockito.mockStatic(DataSource.class)){

            dataSource.when(DataSource::getConnection).thenThrow(new SQLException());

            assertThrows(DaoException.class, () -> dao.deleteById(100L));
        }
    }

    private Product getTestProduct() {
        ProductTranslation translation1 = new ProductTranslation(1L, "en", "test", "test desc");
        ProductTranslation translation2 = new ProductTranslation(1L, "uk", "тест", "тест desc");

        Product expected = new Product();
        expected.setName("test");
        expected.setCategoryId(1L);
        expected.setProducerId(1L);
        expected.setPrice(1L);
        expected.setImagePath("test");
        expected.setCreateTime(123456789L);
        expected.setTranslations(List.of(translation1, translation2));
        return expected;
    }

    private Product getWrongEntity() {
        return new Product(1L, null, null, 3L, 3L, 1234L, "/img/tech/monitor1.jpg", 1111111L);
    }

    public static ParameterSearchCriteria generateTestCriteria() {
        ParameterSearchCriteria criteria = new ParameterSearchCriteria();

        ParameterSearchCriteria.ParametersType producers = new ParameterSearchCriteria.ParametersType();

        List<ParameterSearchCriteria.SelectedParameter> parameters = new ArrayList<>();

        parameters.add(new ParameterSearchCriteria.SelectedParameter(2L, "Dell", true));
        parameters.add(new ParameterSearchCriteria.SelectedParameter(3L, "Philips", true));

        producers.setParameters(parameters);

        List<ParameterSearchCriteria.ParametersType> parametersTypes = new ArrayList<>();

        List<ParameterSearchCriteria.SelectedParameter> selectedParametersMonitorSize = new ArrayList<>();
        selectedParametersMonitorSize.add(new ParameterSearchCriteria.SelectedParameter(1L, "24", true));
        selectedParametersMonitorSize.add(new ParameterSearchCriteria.SelectedParameter(3L, "23", true));
        selectedParametersMonitorSize.add(new ParameterSearchCriteria.SelectedParameter(6L, "20", true));

        List<ParameterSearchCriteria.SelectedParameter> selectedParametersPanel = new ArrayList<>();
        selectedParametersPanel.add(new ParameterSearchCriteria.SelectedParameter(12L, "PLS", true));
        selectedParametersPanel.add(new ParameterSearchCriteria.SelectedParameter(11L, "VA", true));

        parametersTypes.add(new ParameterSearchCriteria.ParametersType("Monitor Size", selectedParametersMonitorSize));
        parametersTypes.add(new ParameterSearchCriteria.ParametersType("Panel", selectedParametersPanel));

        criteria.setCategoryId(5L);
        criteria.setProducers(producers);
        criteria.setParameterTypes(parametersTypes);

        return criteria;
    }

    private static void initProducts() {
        productMap.put(1L, new Product(1L, "223V7QHSB/01", 3L, 3L, 1234L, "/img/tech/monitor1.jpg", 1650346711L));  //Philips, 23", IPS
        productMap.put(2L, new Product(2L, "203V5LSB26/10", 4L, 3L, 452L, "/img/tech/monitor1.jpg", 1659346711L));  //Philips, 28"
        productMap.put(3L, new Product(3L, "E1920H (210-AURI)", 5L, 2L, 452L, "/img/tech/monitor1.jpg", 1658346711L));  //Dell, 24", VA
        productMap.put(6L, new Product(6L, "193V5LSB2/10", 5L, 3L, 2356L, "/img/tech/monitor1.jpg", 1657356711L));  //Philips, 23", VA
        productMap.put(7L, new Product(7L, "193V5LSB2/10", 2L, 3L, 2356L, "/img/tech/monitor1.jpg", 1652345711L));  //Philips, 25", VA

        productMap.put(4L, new Product(4L, "V206HQLAb (UM.IV6EE.A02)", 5L, 4L, 3452L, "/img/tech/monitor1.jpg", 1657342711L));  //Acer, 28", PLS
        productMap.put(5L, new Product(5L, "S22F350F (LS22F350FHIXCI)", 5L, 1L, 4522L, "/img/tech/monitor1.jpg", 1657345711L));  //Samsung, 29", IPS

        productMap.put(8L, new Product(8L, "Chromebook 4 (XE310XBA-KA1US)", 1L, 1L, 356L, "/img/tech/laptop1.jpg", 1657146711L));  //Samsung, 24", IPS
        productMap.put(9L, new Product(9L, "Chromebook 4 193V5LSB2/10", 2L, 1L, 356L, "/img/tech/laptop1.jpg", 1657246711L));  //Samsung, 19", PLS
        productMap.put(10L, new Product(10L, "Chromebook 4 (IV6EE-KA1US)", 4L, 1L, 356L, "/img/tech/laptop1.jpg", 1657316711L));  //Samsung
        
        productMap.get(1L).setTranslations(List.of(
                new ProductTranslation(1L, "en", "223V7QHSB/01", "en description for 223V7QHSB/01"),
                new ProductTranslation(1L, "uk", "223V7QHSB/01", "uk опис для 223V7QHSB/01")));
        productMap.get(2L).setTranslations(List.of(
                new ProductTranslation(2L, "en", "203V5LSB26/10", "en description for 203V5LSB26/10"),
                new ProductTranslation(2L, "uk", "203V5LSB26/10", "uk опис для 203V5LSB26/10")));
        productMap.get(3L).setTranslations(List.of(
                new ProductTranslation(3L, "en", "E1920H (210-AURI)", "en description for E1920H (210-AURI)"),
                new ProductTranslation(3L, "uk", "E1920H (210-AURI)", "uk опис для E1920H (210-AURI)")));
        productMap.get(4L).setTranslations(List.of(
                new ProductTranslation(4L, "en", "V206HQLAb (UM.IV6EE.A02)", "en description for V206HQLAb (UM.IV6EE.A02)"),
                new ProductTranslation(4L, "uk", "V206HQLAb (UM.IV6EE.A02)", "uk опис для V206HQLAb (UM.IV6EE.A02)")));
        productMap.get(5L).setTranslations(List.of(
                new ProductTranslation(5L, "en", "S22F350F (LS22F350FHIXCI)", "en description for S22F350F (LS22F350FHIXCI)"),
                new ProductTranslation(5L, "uk", "S22F350F (LS22F350FHIXCI)", "uk опис для S22F350F (LS22F350FHIXCI)")));
        productMap.get(6L).setTranslations(List.of(
                new ProductTranslation(6L, "en", "193V5LSB2/10", "en description for 193V5LSB2/10"),
                new ProductTranslation(6L, "uk", "193V5LSB2/10", "uk опис для 193V5LSB2/10")));
        productMap.get(7L).setTranslations(List.of(
                new ProductTranslation(7L, "en", "193V5LSB2/10", "en description for 193V5LSB2/10"),
                new ProductTranslation(7L, "uk", "193V5LSB2/10", "uk опис для 193V5LSB2/10")));
        productMap.get(8L).setTranslations(List.of(
                new ProductTranslation(8L, "en", "Chromebook 4 (XE310XBA-KA1US)", "en description for Chromebook 4 (XE310XBA-KA1US)"),
                new ProductTranslation(8L, "uk", "Chromebook 4 (XE310XBA-KA1US)", "uk опис для Chromebook 4 (XE310XBA-KA1US)")));
        productMap.get(9L).setTranslations(List.of(
                new ProductTranslation(9L, "en", "Chromebook 4 193V5LSB2/10", "en description for Chromebook 4 193V5LSB2/10"),
                new ProductTranslation(9L, "uk", "Chromebook 4 193V5LSB2/10", "uk опис для Chromebook 4 193V5LSB2/10")));
        productMap.get(10L).setTranslations(List.of(
                new ProductTranslation(10L, "en", "Chromebook 4 (IV6EE-KA1US)", "en description for Chromebook 4 (IV6EE-KA1US)"),
                new ProductTranslation(10L, "uk", "Chromebook 4 (IV6EE-KA1US)", "uk опис для Chromebook 4 (IV6EE-KA1US)")));
    }
}
