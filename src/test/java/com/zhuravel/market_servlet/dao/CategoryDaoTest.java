package com.zhuravel.market_servlet.dao;

import com.zhuravel.market_servlet.config.DataSource;
import com.zhuravel.market_servlet.dao.exception.DaoException;
import com.zhuravel.market_servlet.dao.impl.CategoryDaoImpl;
import com.zhuravel.market_servlet.model.entity.Category;
import com.zhuravel.market_servlet.model.entity.CategoryTranslation;
import com.zhuravel.market_servlet.model.pageable.Page;
import com.zhuravel.market_servlet.model.pageable.Pageable;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockedStatic;
import org.mockito.Mockito;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertIterableEquals;
import static org.junit.jupiter.api.Assertions.assertNotSame;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * @author Evgenii Zhuravel created on 09.06.2022
 * @version 1.0
 */
public class CategoryDaoTest {

    private static CategoryDao dao;

    private static final Map<Long, Category> categories = new HashMap<>();
    
    private static List<String> lines;

    @BeforeAll
    static void setUp() throws IOException {
        dao = new CategoryDaoImpl();

        lines = Files.readAllLines(Path.of("db/initiateDb.sql"));
        initCategories();
    }

    private static void initCategories() {
        categories.put(1L, new Category(1L, "", 0L, "", 
                List.of(new CategoryTranslation(1L, "en", ""),
                        new CategoryTranslation(1L, "uk", ""))));
        categories.put(2L, new Category(2L, "Computers", 1L, "/img/tech/desktop1.jpg",
                List.of(new CategoryTranslation(2L, "en", "Computers"),
                        new CategoryTranslation(2L, "uk", "Комп'ютери"))));
        categories.put(3L, new Category(3L, "Phones", 2L, "/img/tech/phones1.jpg",
                List.of(new CategoryTranslation(3L, "en", "Phones"),
                        new CategoryTranslation(3L, "uk", "Телефони"))));
        categories.put(4L, new Category(4L, "Laptops", 2L, "/img/tech/laptop1.jpg",
                List.of(new CategoryTranslation(4L, "en", "Laptops"),
                        new CategoryTranslation(4L, "uk", "Ноутбуки"))));
        categories.put(5L, new Category(5L, "Desktops", 2L, "/img/tech/desktop1.jpg",
                List.of(new CategoryTranslation(5L, "en", "Desktops"),
                        new CategoryTranslation(5L, "uk", "Настільні комп'ютери"))));
    }

    @BeforeEach
    void beforeEach() throws SQLException {
        String query = String.join("", lines);
        DataSource.reload();
        try(Connection connection = DataSource.getConnection()){
            connection.createStatement().executeUpdate(query);
        }
    }

    @Test
    void testFindById() throws DaoException {
        Category expected = categories.get(2L);

        Optional<Category> category = dao.findById(2L);

        assertTrue(category.isPresent());
        assertEquals(expected, category.get());
    }

    @Test
    void whenFindByIdConnectionErrorThenThrowException() {
        try (MockedStatic<DataSource> dataSource = Mockito.mockStatic(DataSource.class)){

            dataSource.when(DataSource::getConnection).thenThrow(new SQLException());

            assertThrows(DaoException.class, () -> dao.findById(2L));
        }
    }

    @Test
    void testFindAll() throws DaoException {
        List<Category> expected = new ArrayList<>(categories.values());

        List<Category> actual = dao.findAll();

        assertIterableEquals(expected, actual);
        assertNotSame(expected, actual);
    }

    @Test
    void testFindAllByParentCategoryId() throws DaoException {
        List<Category> expected = new ArrayList<>();
        expected.add(categories.get(2L));

        Pageable pageable = new Pageable();

        Page<Category> actual = dao.findAllByParentCategoryId(1L, pageable);

        assertIterableEquals(expected, actual.getContent());
        assertNotSame(expected, actual.getContent());
    }

    @Test
    void testFindAllByParentCategoryIdWithFilter() throws DaoException {
        List<Category> expected = new ArrayList<>();
        expected.add(categories.get(3L));
        expected.add(categories.get(4L));
        expected.add(categories.get(5L));

        Pageable pageable = new Pageable();

        Page<Category> actual = dao.findAllByParentCategoryId(2L, "ops", "фон", pageable);

        assertIterableEquals(expected, actual.getContent());
        assertNotSame(expected, actual.getContent());
    }

    @Test
    void whenFindAllByParentCategoryIdConnectionErrorThenThrowException() {
        try (MockedStatic<DataSource> dataSource = Mockito.mockStatic(DataSource.class)){

            dataSource.when(DataSource::getConnection).thenThrow(new SQLException());

            assertThrows(DaoException.class, () -> dao.findAllByParentCategoryId(1L, new Pageable()));
        }
    }

    @Test
    void testSave() throws DaoException {
        Category expected = new Category(6L, "testSave", 0L, "testSave",
                List.of(new CategoryTranslation(6L, "en", "testSave"),
                        new CategoryTranslation(6L, "uk", "ыпкук")));

        dao.save(expected);

        Optional<Category> actual = dao.findById(expected.getId());

        assertTrue(actual.isPresent());
        assertEquals(expected, actual.get());
    }

    @Test
    void testUpdate() throws DaoException {
        Optional<Category> expected = dao.findById(2L);

        expected.get().setName("testUpdate");
        for (CategoryTranslation translation : expected.get().getTranslations()) {
            if (translation.getLocale().equals("en")) {
                translation.setLocalizedName("testUpdate");
            } else {
                translation.setLocalizedName("ыпкук");
            }
        }

        dao.update(expected.get());

        Optional<Category> actual = dao.findById(2L);

        assertTrue(actual.isPresent());
        assertEquals(expected.get(), actual.get());
    }
}
