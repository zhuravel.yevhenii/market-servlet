package com.zhuravel.market_servlet.config;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Properties;

/**
 * @author Evgenii Zhuravel created on 08.06.2022
 * @version 1.0
 */
public class DataSource {

    private static final Logger logger = LoggerFactory.getLogger(DataSource.class);

    private static final HikariConfig config = new HikariConfig();

    private static HikariDataSource ds;

    static {
        init();
    }

    private DataSource() {}

    public static Connection getConnection() throws SQLException {
        logger.debug("Get Connection");
        return ds.getConnection();
    }

    public static void reload() {
        ds.close();
        ds = null;
        init();
    }

    private static void init() {
        try {
            Properties props = PropertyReceiver.getProperties();

            String url = props.getProperty("jdbc.url");

            config.setJdbcUrl(url);
            config.addDataSourceProperty( "cachePrepStmts" , "true" );
            config.addDataSourceProperty( "prepStmtCacheSize" , "3" );

            ds = new HikariDataSource(config);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
