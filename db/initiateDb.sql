DROP TABLE IF EXISTS shopping_cart;
DROP TABLE IF EXISTS users;
DROP TABLE IF EXISTS roles;
DROP TABLE IF EXISTS user_status;
DROP TABLE IF EXISTS order_status;
DROP TABLE IF EXISTS category_translation;
DROP TABLE IF EXISTS product_translation;
DROP TABLE IF EXISTS product_product_parameter;
DROP TABLE IF EXISTS product_parameter;
DROP TABLE IF EXISTS product;
DROP TABLE IF EXISTS producer;
DROP TABLE IF EXISTS product_parameter_type_translation;
DROP TABLE IF EXISTS product_parameter_type;
DROP TABLE IF EXISTS category;

CREATE TABLE category (
    id                 BIGSERIAL PRIMARY KEY,
    name               VARCHAR(100) NOT NULL,
    parent_category_id BIGINT,
    image              VARCHAR(255)
);

CREATE TABLE category_translation (
    id             BIGINT       NOT NULL,
    locale         VARCHAR(10)  NOT NULL,
    localized_name VARCHAR(100) DEFAULT '',
    CONSTRAINT PK_category_translation PRIMARY KEY (id, locale),
    CONSTRAINT FK_translation_category FOREIGN KEY (id) REFERENCES category (id) ON DELETE CASCADE
);

CREATE TABLE producer (
    id   BIGSERIAL PRIMARY KEY,
    name VARCHAR(100) NOT NULL
);

CREATE TABLE product (
    id          BIGSERIAL       PRIMARY KEY,
    name        VARCHAR(100)    NOT NULL,
    category_id BIGINT          NOT NULL,
    producer_id BIGINT          NOT NULL,
    price       BIGINT,
    image       VARCHAR(255),
    create_time BIGINT          NOT NULL,
    CONSTRAINT FK_product_category FOREIGN KEY (category_id) REFERENCES category (id),
    CONSTRAINT FK_product_producer FOREIGN KEY (producer_id) REFERENCES producer (id)
);

CREATE TABLE product_parameter_type (
    id          BIGSERIAL       PRIMARY KEY,
    category_id BIGINT          NOT NULL,
    name        VARCHAR(100)    NOT NULL,
    UNIQUE (category_id, name),
    CONSTRAINT FK_product_parameter_category FOREIGN KEY (category_id) REFERENCES category (id) ON DELETE CASCADE
);

CREATE TABLE product_parameter_type_translation (
    id                  BIGINT       NOT NULL,
    locale              VARCHAR(10)  NOT NULL,
    localized_name      VARCHAR(100) DEFAULT '',
    CONSTRAINT PK_parameter_type_translation PRIMARY KEY (id, locale),
    CONSTRAINT FK_translation_parameter_type FOREIGN KEY (id) REFERENCES product_parameter_type (id) ON DELETE CASCADE
);

CREATE TABLE product_parameter (
    id                  BIGSERIAL       PRIMARY KEY,
    parameter_type_id   BIGINT          NOT NULL,
    sense               VARCHAR(255)    NOT NULL,
    UNIQUE (parameter_type_id, sense),
    CONSTRAINT FK_parameter_parameter_type  FOREIGN KEY (parameter_type_id) REFERENCES product_parameter_type (id) ON DELETE CASCADE
);

CREATE TABLE product_product_parameter (
    id                  BIGSERIAL       PRIMARY KEY,
    product_id          BIGINT          NOT NULL,
    parameter_id        BIGINT          NOT NULL,
    UNIQUE (product_id, parameter_id),
    CONSTRAINT FK_parameter_product         FOREIGN KEY (product_id)   REFERENCES product (id) ON DELETE CASCADE,
    CONSTRAINT FK_parameter_product_parameter_type  FOREIGN KEY (parameter_id) REFERENCES product_parameter (id) ON DELETE CASCADE
);

CREATE TABLE product_translation (
    id             BIGINT       NOT NULL,
    locale         VARCHAR(10)  NOT NULL,
    localized_name VARCHAR(100) DEFAULT '',
    localized_desc VARCHAR(100) DEFAULT '',
    CONSTRAINT PK_product_translation PRIMARY KEY (id, locale),
    CONSTRAINT FK_translation_product FOREIGN KEY (id) REFERENCES product (id) ON DELETE CASCADE
);

CREATE TABLE roles (
    id   BIGSERIAL PRIMARY KEY,
    name VARCHAR(100) UNIQUE
);

CREATE TABLE user_status (
    id   BIGSERIAL PRIMARY KEY,
    name VARCHAR(100) UNIQUE
);

CREATE TABLE users (
    id         BIGSERIAL PRIMARY KEY,
    login      VARCHAR(255) NOT NULL,
    first_name VARCHAR(255) NOT NULL,
    last_name  VARCHAR(255),
    email      VARCHAR(255),
    password   VARCHAR(255) NOT NULL,
    locale     VARCHAR(10)  NOT NULL,
    status_id  BIGINT       NOT NULL,
    role_id    BIGINT       NOT NULL,
    UNIQUE (login),
    CONSTRAINT FK_users_status FOREIGN KEY (status_id) REFERENCES user_status (id) ON DELETE CASCADE,
    CONSTRAINT FK_users_roles FOREIGN KEY (role_id) REFERENCES roles (id) ON DELETE CASCADE
);

CREATE TABLE order_status (
    id   BIGSERIAL PRIMARY KEY,
    name VARCHAR(100) UNIQUE
);

CREATE TABLE shopping_cart (
    id                      BIGSERIAL PRIMARY KEY,
    user_id                 BIGINT      NOT NULL,
    product_id              BIGINT      NOT NULL,
    quantity                INT         NOT NULL    DEFAULT 1,
    status_id               BIGINT,
    registration_date_time  BIGINT,
    CONSTRAINT FK_shopping_cart_user FOREIGN KEY (user_id) REFERENCES users (id) ON DELETE CASCADE,
    CONSTRAINT FK_shopping_cart_product FOREIGN KEY (product_id) REFERENCES product (id) ON DELETE CASCADE,
    CONSTRAINT FK_shopping_cart_status FOREIGN KEY (status_id) REFERENCES order_status (id) ON DELETE CASCADE
);


INSERT INTO category (id, name, parent_category_id, image)
VALUES (default, '', 0, ''),
       (default, 'Computers', 1, '/img/tech/desktop1.jpg'),
       (default, 'Phones', 2, '/img/tech/phones1.jpg'),
       (default, 'Laptops', 2, '/img/tech/laptop1.jpg'),
       (default, 'Desktops', 2, '/img/tech/desktop1.jpg');

INSERT INTO producer (id, name)
VALUES (default, 'Samsung'),
       (default, 'Dell'),
       (default, 'Philips'),
       (default, 'Acer');

INSERT INTO roles (id, name)
VALUES (default, 'ROLE_ADMIN'),
       (default, 'ROLE_SELLER'),
       (default, 'ROLE_USER');

INSERT INTO user_status (id, name)
VALUES (default, 'ACTIVE'),
       (default, 'BLOCKED'),
       (default, 'DELETED');

INSERT INTO order_status (id, name)
VALUES (default, 'REGISTERED'),
       (default, 'PAID'),
       (default, 'CANCELED');

INSERT INTO users (id, login, first_name, last_name, email, password, locale, status_id, role_id)
VALUES (default, 'Admin', 'Admin', '', 'admin@mail.com', '$2a$10$C63f4zq5z49aFGITgnivJumGgj1hcG1NBZ73dPouoqWjuethhj9VO', 'en', 1, 1),
       (default, 'Seller', 'Seller', '', 'seller@mail.com', '$2a$10$uT8S8.j40JpkIfK7XRnq1.fwQ6FgzMIedE3ghwtgZvGQb5LT8Dkne', 'uk', 1, 2),
       (default, 'User', 'User', '', 'user@mail.com', '$2a$10$qv8nsFFWSjgzzOH1m8nh6uu4ssolpwRlcANB3yNOctjb6iPtq8gha', 'uk', 1, 3),
       (default, 'BlockedUser', 'BlockedUser', '', 'blockedUser@mail.com', '$2a$10$JxabC.bNvdAUb1BD8XOFp./985Zo25R.yS70WFM.SYlxJ2X1EtPam', 'uk', 2, 3),
       (default, 'DeletedUser', 'DeletedUser', '', 'deletedUser@mail.com', '$2a$10$qXPfi1YOAyv9Um1MukML9uiy0YI5BCuLdJPQ3Idilnl0cp0ahKb8y', 'uk', 3, 3);


INSERT INTO category_translation (id, locale, localized_name) VALUES
        (1, 'en', ''),
        (1, 'uk', ''),
        (2, 'en', 'Computers'),
        (2, 'uk', 'Комп''ютери'),
        (3, 'en', 'Phones'),
        (3, 'uk', 'Телефони'),
        (4, 'en', 'Laptops'),
        (4, 'uk', 'Ноутбуки'),
        (5, 'en', 'Desktops'),
        (5, 'uk', 'Настільні комп''ютери');

INSERT INTO product (name, category_id, producer_id, price, image, create_time) VALUES
        ('223V7QHSB/01', 3, 3, 1234, '/img/tech/monitor1.jpg', 1650346711),
        ('203V5LSB26/10', 4, 3, 452, '/img/tech/monitor1.jpg', 1659346711),
        ('E1920H (210-AURI)', 5, 2, 452, '/img/tech/monitor1.jpg', 1658346711),

        ('V206HQLAb (UM.IV6EE.A02)', 5, 4, 3452, '/img/tech/monitor1.jpg', 1657342711),
        ('S22F350F (LS22F350FHIXCI)', 5, 1, 4522, '/img/tech/monitor1.jpg', 1657345711),
        ('193V5LSB2/10', 5, 3, 2356, '/img/tech/monitor1.jpg', 1657356711),
        ('193V5LSB2/10', 2, 3, 2356, '/img/tech/monitor1.jpg', 1652345711),
        ('Chromebook 4 (XE310XBA-KA1US)', 1, 1, 356, '/img/tech/laptop1.jpg', 1657146711),
        ('Chromebook 4 193V5LSB2/10', 2, 1, 356, '/img/tech/laptop1.jpg', 1657246711),
        ('Chromebook 4 (IV6EE-KA1US)', 4, 1, 356, '/img/tech/laptop1.jpg', 1657316711)
;

INSERT INTO product_translation (id, locale, localized_name, localized_desc) VALUES
         (1, 'en', '223V7QHSB/01', 'en description for 223V7QHSB/01'),
         (1, 'uk', '223V7QHSB/01', 'uk опис для 223V7QHSB/01'),
         (2, 'en', '203V5LSB26/10', 'en description for 203V5LSB26/10'),
         (2, 'uk', '203V5LSB26/10', 'uk опис для 203V5LSB26/10'),
         (3, 'en', 'E1920H (210-AURI)', 'en description for E1920H (210-AURI)'),
         (3, 'uk', 'E1920H (210-AURI)', 'uk опис для E1920H (210-AURI)'),
         (4, 'en', 'V206HQLAb (UM.IV6EE.A02)', 'en description for V206HQLAb (UM.IV6EE.A02)'),
         (4, 'uk', 'V206HQLAb (UM.IV6EE.A02)', 'uk опис для V206HQLAb (UM.IV6EE.A02)'),
         (5, 'en', 'S22F350F (LS22F350FHIXCI)', 'en description for S22F350F (LS22F350FHIXCI)'),
         (5, 'uk', 'S22F350F (LS22F350FHIXCI)', 'uk опис для S22F350F (LS22F350FHIXCI)'),
         (6, 'en', '193V5LSB2/10', 'en description for 193V5LSB2/10'),
         (6, 'uk', '193V5LSB2/10', 'uk опис для 193V5LSB2/10'),
         (7, 'en', '193V5LSB2/10', 'en description for 193V5LSB2/10'),
         (7, 'uk', '193V5LSB2/10', 'uk опис для 193V5LSB2/10'),
         (8, 'en', 'Chromebook 4 (XE310XBA-KA1US)', 'en description for Chromebook 4 (XE310XBA-KA1US)'),
         (8, 'uk', 'Chromebook 4 (XE310XBA-KA1US)', 'uk опис для Chromebook 4 (XE310XBA-KA1US)'),
         (9, 'en', 'Chromebook 4 193V5LSB2/10', 'en description for Chromebook 4 193V5LSB2/10'),
         (9, 'uk', 'Chromebook 4 193V5LSB2/10', 'uk опис для Chromebook 4 193V5LSB2/10'),
         (10, 'en', 'Chromebook 4 (IV6EE-KA1US)', 'en description for Chromebook 4 (IV6EE-KA1US)'),
         (10, 'uk', 'Chromebook 4 (IV6EE-KA1US)', 'uk опис для Chromebook 4 (IV6EE-KA1US)')
         ;

INSERT INTO product_parameter_type (category_id, name) VALUES
        (3, 'Monitor Size'),
        (3, 'Panel'),
        (4, 'Color')
;

INSERT INTO product_parameter_type_translation (id, locale, localized_name) VALUES
        (1, 'en', 'Monitor Size'),
        (1, 'uk', 'Диагональ екрану'),
        (2, 'en', 'Panel'),
        (2, 'uk', 'Тип матриці'),
        (3, 'en', 'Color'),
        (3, 'uk', 'Колір')
;

INSERT INTO product_parameter (parameter_type_id, sense) VALUES
        (1, '24"'),
        (1, '25"'),
        (1, '23"'),
        (2, 'IPS'),
        (2, 'TN'),
        (1, '20"'),
        (1, '18"'),
        (1, '19"'),
        (1, '29"'),
        (1, '28"'),
        (2, 'VA'),
        (2, 'PLS')
;

INSERT INTO shopping_cart (id, user_id, product_id, quantity, status_id, registration_date_time) VALUES
        (default, 3, 1, 1, 1, null),
        (default, 3, 2, 3, 1, null),
        (default, 3, 3, 1, 2, null),
        (default, 3, 2, 2, 3, 1654516560),
        (default, 3, 1, 1, 3, 1654516560)
;

INSERT INTO product_product_parameter (product_id, parameter_id) VALUES
     (1, 3),
     (1, 4),
     (3, 1),
     (4, 10),
     (5, 4),
     (6, 3),
     (7, 2),
     (8, 1),
     (9, 8),

     (2, 10),
     (3, 11),
     (4, 12),
     (5, 9),
     (6, 11),
     (7, 11),
     (8, 4),
     (9, 12)
;